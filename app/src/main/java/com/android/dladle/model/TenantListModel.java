package com.android.dladle.model;

import java.io.Serializable;

/**
 * Created by Farman on 6/10/2017.
 */

public class TenantListModel implements Serializable{

    /**
     * emailId : bhajarocks@gmail.com
     * firstName : Bhajaman
     * lastName : behera
     * idNumber : fggff
     * cellNumber : null
     */

    private String emailId;
    private String firstName;
    private String lastName;
    private String idNumber;
    private Object cellNumber;

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public Object getCellNumber() {
        return cellNumber;
    }

    public void setCellNumber(Object cellNumber) {
        this.cellNumber = cellNumber;
    }
}
