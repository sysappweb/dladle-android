package com.android.dladle.model;

/**
 * Created by Farman on 5/30/2017.
 */

public class ContactModel {
String type="",address="",phoneNo="",name="";
int id;
    public ContactModel(String type, String address, String phoneNo, String name) {
        this.type = type;
        this.address = address;
        this.phoneNo = phoneNo;
        this.name = name;
    }

    public ContactModel(String type, String address, String phoneNo, String name, int id) {
        this.type = type;
        this.address = address;
        this.phoneNo = phoneNo;
        this.name = name;
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
