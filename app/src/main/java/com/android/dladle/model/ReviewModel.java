package com.android.dladle.model;

/**
 * Created by admin on 30/08/17.
 */

public class ReviewModel {
    long rate;
    String name,comment;

    public long getRate() {
        return rate;
    }

    public void setRate(long rate) {
        this.rate = rate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
