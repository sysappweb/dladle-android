package com.android.dladle.model;

import java.io.Serializable;

/**
 * Created by Farman on 6/9/2017.
 */

public class NotificationListModel implements Serializable {


    /**
     * id : 1
     * from : bhajarocks@gmail.com
     * to : farman.bluehorse@gmail.com
     * title : New Property Request
     * body : Please accept this property invitation
     * data : landlordEmailId:pradyumnaswain76@gmail.com,houseId:13
     * imageUrl : image
     * time : 2017-06-07 17:48:05.638
     * read : false
     * actioned : false
     * houseId : 13
     */

    private int id;
    private String from;
    private String to;
    private String title;
    private String body;
    private String data;
    private String imageUrl;
    private String time;
    private boolean read;
    private boolean actioned;
    private int houseId;
    private String name;
    private String profilePicture;
    private String notificationTypeId;
    private double rating;
    private String leaseEnddate;
    private int serviceId;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    public boolean isActioned() {
        return actioned;
    }

    public void setActioned(boolean actioned) {
        this.actioned = actioned;
    }

    public int getHouseId() {
        return houseId;
    }

    public void setHouseId(int houseId) {
        this.houseId = houseId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getNotificationTypeId() {
        return notificationTypeId;
    }

    public void setNotificationTypeId(String notificationTypeId) {
        this.notificationTypeId = notificationTypeId;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public String getLeaseEnddate() {
        return leaseEnddate;
    }

    public void setLeaseEnddate(String leaseEnddate) {
        this.leaseEnddate = leaseEnddate;
    }

    public int getServiceId() {
        return serviceId;
    }

    public void setServiceId(int serviceId) {
        this.serviceId = serviceId;
    }
}
