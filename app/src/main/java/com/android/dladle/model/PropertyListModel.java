package com.android.dladle.model;

import java.util.List;

/**
 * Created by Farman on 6/3/2017.
 */

public class PropertyListModel {


    /**
     * propertyId : 7
     * houseId : 3
     * address : new town kolkata
     * placeType : Flat
     * complexName : Alpana apartment
     * unitNumber : 13 C
     * placeImage : image_url
     * estateName :
     * propertyContactList : [{"contactType":"Electrician","name":"farman","address":"new town","contactNumber":"7318794212"},{"contactType":"Electrician","name":"farman","address":"new town","contactNumber":"9734582306"}]
     * tenantList : []
     * estate : false
     */

    private int propertyId;
    private int houseId;
    private String address;
    private String placeType;
    private String complexName;
    private String unitNumber;
    private String placeImage;
    private String estateName;
    private boolean estate;
    private List<PropertyContactListBean> propertyContactList;
    private List<TenantListBean> tenantList;
    private int tenantCount;
    private int notificationCount;
    private boolean isHome;
    private boolean isActiveJob;
    private int contactCount;
    private boolean isSwitchOn=false;
    private String landlordName="";
    private String landlordImagePath="";
    private String landlordEmail="";
    private boolean leaseStatus;
    public String getLandlordName() {
        return landlordName;
    }

    public void setLandlordName(String landlordName) {
        this.landlordName = landlordName;
    }

    public String getLandlordImagePath() {
        return landlordImagePath;
    }

    public void setLandlordImagePath(String landlordImagePath) {
        this.landlordImagePath = landlordImagePath;
    }

    public String getLandlordEmail() {
        return landlordEmail;
    }

    public void setLandlordEmail(String landlordEmail) {
        this.landlordEmail = landlordEmail;
    }

    public int getTenantCount() {
        return tenantCount;
    }

    public void setTenantCount(int tenantCount) {
        this.tenantCount = tenantCount;
    }

    public int getNotificationCount() {
        return notificationCount;
    }

    public void setNotificationCount(int notificationCount) {
        this.notificationCount = notificationCount;
    }

    public boolean isSwitchOn() {
        return isSwitchOn;
    }

    public void setSwitchOn(boolean switchOn) {
        isSwitchOn = switchOn;
    }

    public boolean isHome() {
        return isHome;
    }

    public void setHome(boolean home) {
        isHome = home;
    }

    public boolean isActiveJob() {
        return isActiveJob;
    }

    public void setActiveJob(boolean activeJob) {
        isActiveJob = activeJob;
    }

    public int getContactCount() {
        return contactCount;
    }

    public void setContactCount(int contactCount) {
        this.contactCount = contactCount;
    }

    public int getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(int propertyId) {
        this.propertyId = propertyId;
    }

    public int getHouseId() {
        return houseId;
    }

    public void setHouseId(int houseId) {
        this.houseId = houseId;
    }

    public String getAddress() {
        return address;
    }


    public boolean isLeaseStatus() {
        return leaseStatus;
    }

    public void setLeaseStatus(boolean leaseStatus) {
        this.leaseStatus = leaseStatus;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPlaceType() {
        return placeType;
    }

    public void setPlaceType(String placeType) {
        this.placeType = placeType;
    }

    public String getComplexName() {
        return complexName;
    }

    public void setComplexName(String complexName) {
        this.complexName = complexName;
    }

    public String getUnitNumber() {
        return unitNumber;
    }

    public void setUnitNumber(String unitNumber) {
        this.unitNumber = unitNumber;
    }

    public String getPlaceImage() {
        return placeImage;
    }

    public void setPlaceImage(String placeImage) {
        this.placeImage = placeImage;
    }

    public String getEstateName() {
        return estateName;
    }

    public void setEstateName(String estateName) {
        this.estateName = estateName;
    }

    public boolean isEstate() {
        return estate;
    }

    public void setEstate(boolean estate) {
        this.estate = estate;
    }

    public List<PropertyContactListBean> getPropertyContactList() {
        return propertyContactList;
    }

    public void setPropertyContactList(List<PropertyContactListBean> propertyContactList) {
        this.propertyContactList = propertyContactList;
    }

    public List<TenantListBean> getTenantList() {
        return tenantList;
    }

    public void setTenantList(List<TenantListBean> tenantList) {
        this.tenantList = tenantList;
    }

    public static class PropertyContactListBean {
        /**
         * contactType : Electrician
         * name : farman
         * address : new town
         * contactNumber : 7318794212
         */

        private String contactType;
        private String name;
        private String address;
        private String contactNumber;

        public String getContactType() {
            return contactType;
        }

        public void setContactType(String contactType) {
            this.contactType = contactType;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getContactNumber() {
            return contactNumber;
        }

        public void setContactNumber(String contactNumber) {
            this.contactNumber = contactNumber;
        }

    }
    public static class TenantListBean {

        private String emailId;
        private String firstName;
        private String lastName;
        private String idNumber;
        private String cellNumber;
        private String profilePicture;

        public String getEmailId() {
            return emailId;
        }

        public void setEmailId(String emailId) {
            this.emailId = emailId;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getIdNumber() {
            return idNumber;
        }

        public void setIdNumber(String idNumber) {
            this.idNumber = idNumber;
        }

        public String getCellNumber() {
            return cellNumber;
        }

        public void setCellNumber(String cellNumber) {
            this.cellNumber = cellNumber;
        }

        public String getProfilePicture() {
            return profilePicture;
        }

        public void setProfilePicture(String profilePicture) {
            this.profilePicture = profilePicture;
        }
    }


}
