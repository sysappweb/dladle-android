package com.android.dladle.customui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.android.dladle.utils.Logger;

/**
 * Created by sourav on 02-03-2017.
 */

@SuppressLint("AppCompatCustomView")
public class RoundedImageView extends ImageView {

    private int mDimen;
    public RoundedImageView(Context ctx, AttributeSet attrs) {
        super(ctx, attrs);
    }

    public RoundedImageView (Context ctx) {
        super(ctx);
    }

    public RoundedImageView (Context ctx, AttributeSet attrs, int defStyleAttr) {
        super(ctx, attrs, defStyleAttr);
    }

    public RoundedImageView (Context ctx, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(ctx, attrs, defStyleAttr, defStyleRes);
    }

    public void setDimen(int aDimen) {
        mDimen = aDimen;
    }
    @Override
    protected void onDraw(Canvas canvas) {
        Drawable drawable = getDrawable();
        if (drawable == null) {
            return;
        }

        if (getWidth() == 0 || getHeight() == 0) {
            return;
        }
        Bitmap b = null;
        try {
            int targetW = mDimen;
            int targetH = targetW;
            b = Bitmap.createBitmap(targetW, targetH, Bitmap.Config.ARGB_8888);
            Canvas c = new Canvas(b);
            drawable.setBounds(0, 0, c.getWidth(), c.getHeight());
            drawable.draw(c);
        } catch (OutOfMemoryError e) {
            Logger.printError(e.getMessage());
        }
        if (null != b) {
            Bitmap bitmap = b.copy(Bitmap.Config.ARGB_8888, true);

            int w = getWidth(), h = getHeight();

            Bitmap roundBitmap = getCircleBitmap(bitmap, w , h);
//            Bitmap roundBitmap = getRoundedCroppedBitmap(bitmap, w);
            canvas.drawBitmap(roundBitmap, 0, 0, null);
        }

    }

    private Bitmap getCircleBitmap(Bitmap bitmap, int width, int height) {
        final Bitmap output = Bitmap.createBitmap(width,
                height, Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(output);

        final int color = Color.TRANSPARENT;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, width, height);
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(Color.parseColor("#BAB399"));
        //paint.setColor(Color.TRANSPARENT);
        canvas.drawOval(rectF, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        bitmap.recycle();
        return output;
    }
}
