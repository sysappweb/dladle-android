package com.android.dladle.customui;

import android.graphics.Bitmap;
import android.view.Gravity;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.dladle.R;
import com.android.dladle.utils.Logger;

/**
 * Created by sourav on 15-01-2017.
 */

public class AppWebViewClients extends WebViewClient {
    private ProgressBar progressBar;
    private boolean mShowToast;

    public AppWebViewClients(ProgressBar progressBar, boolean showToast) {
        this.progressBar = progressBar;
        this.mShowToast = showToast;
    }
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        // TODO Auto-generated method stub
        view.loadUrl(url);
        return true;
    }
    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        progressBar.setVisibility(View.VISIBLE);
    }
    @Override
    public void onPageFinished(WebView view, String url) {
        // TODO Auto-generated method stub
        super.onPageFinished(view, url);
        Logger.printDebug("onPageFinished : "+url);
        progressBar.setVisibility(View.GONE);
        if (mShowToast) {
            Toast toast = Toast.makeText(progressBar.getContext(), R.string.scroll_till_end, Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
    }
}