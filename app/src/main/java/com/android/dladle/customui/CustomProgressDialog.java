package com.android.dladle.customui;

import android.animation.AnimatorInflater;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.view.Window;
import android.widget.ImageView;

import com.android.dladle.R;

/**
 * Created by Farman on 6/7/2017.
 */

public class CustomProgressDialog extends Dialog {
    private ObjectAnimator mProgressAnimator;
    ImageView mProgressIcon;
    Context c;
    public CustomProgressDialog(@NonNull Context context) {
        super(context);
    }
    public CustomProgressDialog(Activity a) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_progress_dialog);
        mProgressIcon=(ImageView)findViewById(R.id.progress_icon);
        mProgressAnimator = (ObjectAnimator) AnimatorInflater.loadAnimator(c, R.animator.flipping);
        mProgressAnimator.setTarget(mProgressIcon);
        mProgressAnimator.start();

    }
}
