package com.android.dladle.customui;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.dladle.R;
import com.android.dladle.utils.Logger;
import com.android.dladle.utils.NetworkConstants;

/**
 * Created by sourav on 15-01-2017.
 */

public class CustomDialog extends AlertDialog implements View.OnClickListener, CustomWebView.OnScrollChangedCallback {

    public static final int TYPE_ACCEPT_DECLINE_TNC = 100;
    public static final int TYPE_TNC = 101;
    public static final int TYPE_FAQ = 102;
    public static final int TYPE_CONTACT_US = 103;

    private int mType;

    @Override
    public void onScrollEndReached() {
        if (null != mOk) {
            mOk.setEnabled(true);
            mOk.setTextColor(mOk.getResources().getColor(R.color.btn_color));
        }
    }

    public interface DialogInteraction {
        public void onPositiveButtonClicked(int type);

        public void onNegativeButtonClicked(int type);
    }

    public void setCallback(DialogInteraction listener) {
        mCallback = listener;
    }

    private Button mOk, mCancel, mNegative;
    private DialogInteraction mCallback;
    private ProgressBar mProgressBar;
    private CustomWebView mWebView;
    private View mDualButton, mRootView, mHeaderView, mButtonContainer;
    private TextView mTitle;

    public CustomDialog(Context context, int aType) {
        super(context);
        mType = aType;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_dialog);
        mRootView = findViewById(R.id.root);
        mHeaderView = findViewById(R.id.header);
        mButtonContainer = findViewById(R.id.btn_layout);
        mOk = (Button) findViewById(R.id.btn_ok);
        mCancel = (Button) findViewById(R.id.btn_cancel);
        mNegative = (Button) findViewById(R.id.btn_negative);
        mDualButton = findViewById(R.id.dual_button);
        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);
        mWebView = (CustomWebView) findViewById(R.id.web_view);
        mWebView.loadUrl(getURL());
        mTitle = (TextView) findViewById(R.id.title);
        mTitle.setText(getTitle());
        mOk.setOnClickListener(this);
        mNegative.setOnClickListener(this);
        mCancel.setOnClickListener(this);
        mWebView.setWebViewClient(new AppWebViewClients(mProgressBar, TYPE_ACCEPT_DECLINE_TNC == mType) {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.startsWith("mailto:")) {
                    Intent emailIntent = new Intent(Intent.ACTION_SEND);
                    emailIntent.setData(Uri.parse("mailto:"));
                    emailIntent.setType("text/plain");
                    String[] arr = url.split(":");
                    if (null != arr && arr.length == 2) {
                        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] {arr[1]});
                    }
                    try {
                        view.getContext().startActivity(emailIntent);
                        view.reload();
                    } catch (NullPointerException | ActivityNotFoundException ex) {
                        Logger.printError(ex.getMessage());
                    }
                    return true;
                }
                if (url.startsWith("tel:")) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
                    try {
                        view.getContext().startActivity(intent);
                        view.reload();
                    } catch (NullPointerException | ActivityNotFoundException ex) {
                        Logger.printError(ex.getMessage());
                    }
                    return true;
                }
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                int webHeight = mWebView.getMeasuredHeight();
                int contentHeight = (int) Math.floor(view.getContentHeight() * view.getScale());
                contentHeight = webHeight > contentHeight ? webHeight : contentHeight;
                String msg = "C Height : "+contentHeight+"\n";
                FrameLayout.LayoutParams lParam = (FrameLayout.LayoutParams) mRootView.getLayoutParams();
                int diff = 0;
                if (contentHeight > 0 && view == null) {
                    msg = msg + "Btn Height : "+mButtonContainer.getMeasuredHeight()+"\n";
                    msg = msg + "Hdr Height : "+mHeaderView.getMeasuredHeight()+"\n";
                    msg = msg + "root Height : "+mRootView.getMeasuredHeight()+"\n";
                    diff = mRootView.getMeasuredHeight() - (mButtonContainer.getMeasuredHeight() + mHeaderView.getMeasuredHeight() + contentHeight);
                    msg = msg + "diff : "+diff+"\n";
                    if (diff > 0 && mRootView.getMeasuredHeight() > diff) {
                        msg = msg + "lParam.height b4: "+lParam.height+"\n";
                        lParam.height = mRootView.getMeasuredHeight() - diff;
                        msg = msg + "lParam.height : "+lParam.height+"";
                        mRootView.setLayoutParams(lParam);
                        mRootView.requestLayout();
                    }
                }
                Logger.printInfo(msg);
                super.onPageFinished(view, url);
            }
        });
        if (TYPE_ACCEPT_DECLINE_TNC == mType) {
            mWebView.setOnScrollChangedCallback(this);
            setOnDismissListener(new OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    mCallback.onNegativeButtonClicked(mType);
                }
            });
        } else {
            mDualButton.setVisibility(View.GONE);
            mCancel.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_ok:
                mCallback.onPositiveButtonClicked(mType);
                break;
            case R.id.btn_negative:
                dismiss();
                mCallback.onNegativeButtonClicked(mType);
                break;
            case R.id.btn_cancel:
                dismiss();
                break;
            default:
                break;
        }
        dismiss();
    }

    private String getURL() {
        if (TYPE_ACCEPT_DECLINE_TNC == mType || TYPE_TNC == mType) {
            return NetworkConstants.TNC_URL;
        } else if (TYPE_CONTACT_US == mType) {
            return NetworkConstants.CONTACT_US_URL;
        } else if (TYPE_FAQ == mType) {
            return NetworkConstants.FAQ_URL;
        } else {
            return NetworkConstants.HOME_URL;
        }
    }

    private int getTitle() {
        if (TYPE_ACCEPT_DECLINE_TNC == mType || TYPE_TNC == mType) {
            return R.string.terms_and_conditions;
        } else if (TYPE_CONTACT_US == mType) {
            return R.string.nav_contact_us;
        } else if (TYPE_FAQ == mType) {
            return R.string.nav_help;
        } else {
            return R.string.app_name;
        }
    }
}