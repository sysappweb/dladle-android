package com.android.dladle.customui;

import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.android.dladle.utils.Logger;

/**
 * Created by sourav on 15-01-2017.
 */

public class CustomWebView extends WebView {

    private OnScrollChangedCallback mOnScrollChangedCallback;

    public CustomWebView(final Context context) {
        super(context);
    }

    public CustomWebView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomWebView(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void loadUrl(String url) {
        WebSettings settings = getSettings();
        settings.setAppCacheMaxSize(1024 * 1024);
        settings.setAppCachePath(this.getContext().getApplicationContext().getCacheDir().getAbsolutePath());
        settings.setAllowFileAccess(true);
        settings.setAppCacheEnabled(true);
        settings.setJavaScriptEnabled(true);
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        super.loadUrl(url);
    }

    @Override
    protected void onScrollChanged(final int l, final int t, final int oldl, final int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        WebView view = this;
        int contentHeight = (int) Math.floor(view.getContentHeight() * view.getScale());
        int diff = (contentHeight - (view.getHeight() + view.getScrollY()));
        Logger.printDebug("diff : "+diff);
        if (mOnScrollChangedCallback != null && diff < 50) {
            mOnScrollChangedCallback.onScrollEndReached();
        }
    }

    public void setOnScrollChangedCallback(final OnScrollChangedCallback onScrollChangedCallback) {
        mOnScrollChangedCallback = onScrollChangedCallback;
    }

    /**
     * Impliment in the activity/fragment/view that you want to listen to the webview
     */
    public static interface OnScrollChangedCallback {
        public void onScrollEndReached();
    }
}
