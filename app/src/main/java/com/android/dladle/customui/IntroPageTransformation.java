package com.android.dladle.customui;

import android.support.v4.view.ViewPager;
import android.view.View;

import com.android.dladle.R;

/**
 * Created by sourav on 08-01-2017.
 */

public class IntroPageTransformation implements ViewPager.PageTransformer {

    @Override
    public void transformPage(View page, float position) {
        int pagePosition = (int) page.getTag();
        int pageWidth = page.getWidth();
        float pageWidthTimesPosition = pageWidth * position;
        float absPosition = Math.abs(position);

        if (position <= -1.0f || position >= 1.0f || position == 0.0f) {
            //Do Nothing as this is the end.
        } else {
            View description = page.findViewById(R.id.content);
            description.setAlpha(1.0f - absPosition);
            description.setTranslationY(-pageWidthTimesPosition / 2f);

            View container = page.findViewById(R.id.view_container);
            container.setAlpha(1.0f - absPosition);
            container.setTranslationX(-pageWidthTimesPosition * 1.5f);
        }
    }
}
