package com.android.dladle.customui;

import android.content.Context;
import android.util.AttributeSet;

import com.android.dladle.R;

/**
 * Created by soura on 20-03-2017.
 */

public class BigImageView extends RoundedImageView {
    public BigImageView(Context ctx, AttributeSet attrs) {
        super(ctx, attrs);
        super.setDimen((int) ctx.getResources().getDimension(R.dimen.property_pic_size));
    }

    public BigImageView(Context ctx) {
        super(ctx);
        super.setDimen((int) ctx.getResources().getDimension(R.dimen.property_pic_size));
    }

    public BigImageView(Context ctx, AttributeSet attrs, int defStyleAttr) {
        super(ctx, attrs, defStyleAttr);
        super.setDimen((int) ctx.getResources().getDimension(R.dimen.property_pic_size));
    }

    public BigImageView(Context ctx, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(ctx, attrs, defStyleAttr, defStyleRes);
        super.setDimen((int) ctx.getResources().getDimension(R.dimen.property_pic_size));
    }
}
