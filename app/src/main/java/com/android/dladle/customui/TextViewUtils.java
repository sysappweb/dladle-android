package com.android.dladle.customui;

/**
 * Created by sourav on 14-01-2017.
 */

import android.text.TextUtils;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.android.dladle.R;

public class TextViewUtils {
    public static String removeEndSpaces(String str) {
        if (!TextUtils.isEmpty(str)) {
            return str.replaceAll("^\\s+", "").replaceAll("\\s+$", "");
        }
        return str;
    }

    public static void setPasswordViewMargin(final EditText lView) {
        if (lView.getLayoutParams() instanceof FrameLayout.LayoutParams) {
            FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) lView.getLayoutParams();
            lp.rightMargin = /*hasErrorMsg ? 0 :*/ (int)lView.getResources().getDimension(R.dimen.error_icon_width);
            lView.setLayoutParams(lp);
            //lView.setError(error);
        } else if (lView.getLayoutParams() instanceof RelativeLayout.LayoutParams) {
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) lView.getLayoutParams();
            lp.rightMargin = /*hasErrorMsg ? 0 :*/ (int)lView.getResources().getDimension(R.dimen.error_icon_width);
            lView.setLayoutParams(lp);
            //lView.setError(error);
        }
    }
}
