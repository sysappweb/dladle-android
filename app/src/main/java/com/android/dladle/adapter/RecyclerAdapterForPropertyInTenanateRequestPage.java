package com.android.dladle.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.dladle.R;
import com.android.dladle.activities.AboutTenantActivity;
import com.android.dladle.activities.NewTenantActivity;
import com.android.dladle.entities.Property;
import com.android.dladle.model.NotificationListModel;
import com.android.dladle.model.PropertyListModel;

import java.util.ArrayList;

import vn.luongvo.widget.iosswitchview.SwitchView;

/**
 * Created by Farman on 5/26/2017.
 */

public class RecyclerAdapterForPropertyInTenanateRequestPage extends RecyclerView.Adapter<RecyclerAdapterForPropertyInTenanateRequestPage.ItemViewHolder> {
    Activity activity;
    ArrayList<PropertyListModel> mPropertyListModel;

    public RecyclerAdapterForPropertyInTenanateRequestPage(Activity activity, ArrayList<PropertyListModel> mPropertyListModel) {
        this.activity = activity;
        this.mPropertyListModel = mPropertyListModel;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.property_tenant_row, parent, false);
        return new RecyclerAdapterForPropertyInTenanateRequestPage.ItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder holder, final int position) {
        final PropertyListModel propertyListModel = mPropertyListModel.get(position);
        int tenantListSize = propertyListModel.getTenantList().size();
       if(propertyListModel.getComplexName().equals(""))
       {
           holder.homeName.setText(propertyListModel.getEstateName());
       }
       else
       {
           holder.homeName.setText(propertyListModel.getComplexName());
       }
       holder.homeAddress.setText(propertyListModel.getAddress());
        boolean isHome = propertyListModel.isHome();
        if (isHome) {
            holder.switchView.setVisibility(View.GONE);
            holder.homeName.setText("Home");
        } else {
            holder.switchView.setVisibility(View.VISIBLE);
        }
        if (tenantListSize > 0) {

            holder.tenantContainer.setVisibility(View.VISIBLE);
            holder.noTenantAdded.setVisibility(View.GONE);
            if (tenantListSize == 1) {
                holder.tenantTwoContainer.setVisibility(View.VISIBLE);
            } else if (tenantListSize == 2) {
                holder.tenantThreeContainer.setVisibility(View.VISIBLE);
                holder.tenantTwoContainer.setVisibility(View.VISIBLE);
            } else if (tenantListSize == 3) {
                holder.tenantMaxLimit.setVisibility(View.VISIBLE);
                holder.switchView.setVisibility(View.GONE);
                holder.tenantOneContainer.setVisibility(View.VISIBLE);
                holder.tenantTwoContainer.setVisibility(View.VISIBLE);
                holder.tenantThreeContainer.setVisibility(View.VISIBLE);
            }

        } else {
            holder.tenantContainer.setVisibility(View.GONE);
            holder.tenantMaxLimit.setVisibility(View.GONE);
            holder.noTenantAdded.setVisibility(View.VISIBLE);
        }

        if (propertyListModel.isSwitchOn()) {
            holder.switchView.setChecked(true);
        } else {
            holder.switchView.setChecked(false);
        }
        holder.switchView.setOnCheckedChangeListener(new SwitchView.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SwitchView switchView, boolean b) {
                int i=holder.getAdapterPosition();
                if(b)
                {
                    if(activity instanceof NewTenantActivity){
                        ((NewTenantActivity)activity).selectHome(i);
                    }
                }





            }
        });
        holder.userOneImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToTenantActivity();
            }
        });
        holder.userTwoImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToTenantActivity();
            }
        });
        holder.userThreeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToTenantActivity();
            }
        });

    }

    private void goToTenantActivity() {
        Intent intent = new Intent(activity, AboutTenantActivity.class);
        activity.startActivity(intent);
    }

    @Override
    public int getItemCount() {
        return mPropertyListModel.size();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {
        protected vn.luongvo.widget.iosswitchview.SwitchView switchView;
        protected TextView homeName, homeAddress, userOneName, userTwoName, userThreeName, tenantMaxLimit, noTenantAdded;
        protected ImageView userOneImage, userTwoImage, userThreeImage;
        protected LinearLayout tenantContainer, tenantOneContainer, tenantTwoContainer, tenantThreeContainer;

        public ItemViewHolder(View itemView) {
            super(itemView);
            switchView = (SwitchView) itemView.findViewById(R.id.switchview);
            homeName = (TextView) itemView.findViewById(R.id.tv_home_name);
            homeAddress = (TextView) itemView.findViewById(R.id.tv_home_address);
            userOneName = (TextView) itemView.findViewById(R.id.tv_user_name1);
            userTwoName = (TextView) itemView.findViewById(R.id.tv_user_name2);
            userThreeName = (TextView) itemView.findViewById(R.id.tv_user_name3);
            tenantMaxLimit = (TextView) itemView.findViewById(R.id.tv_tenant_limit);
            userOneImage = (ImageView) itemView.findViewById(R.id.profile_pic1);
            userTwoImage = (ImageView) itemView.findViewById(R.id.profile_pic2);
            userThreeImage = (ImageView) itemView.findViewById(R.id.profile_pic3);
            tenantContainer = (LinearLayout) itemView.findViewById(R.id.tenant_conatiner);
            noTenantAdded = (TextView) itemView.findViewById(R.id.tv_no_tennat);
            tenantOneContainer = (LinearLayout) itemView.findViewById(R.id.tenant_one_container);
            tenantTwoContainer = (LinearLayout) itemView.findViewById(R.id.tenant_two_container);
            tenantThreeContainer = (LinearLayout) itemView.findViewById(R.id.tenant_three_container);
        }
    }
}
