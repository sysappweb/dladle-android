package com.android.dladle.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.dladle.R;
import com.android.dladle.activities.PropertyOptionActivity;
import com.android.dladle.activities.SelectTenantActivity;
import com.android.dladle.model.PropertyListModel;
import com.android.dladle.utils.LinearLayoutTarget;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

/**
 * Created by Farman on 5/24/2017.
 */

public class RecyclerAdapterForHome extends RecyclerView.Adapter<RecyclerAdapterForHome.ItemViewHolder> {
    Activity activity;
    ArrayList<PropertyListModel> propertyListModels;

    public RecyclerAdapterForHome(Activity activity, ArrayList<PropertyListModel> propertyListModels) {
        this.activity = activity;
        this.propertyListModels = propertyListModels;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.home_list_row, parent, false);
        return new ItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, final int position) {
        final PropertyListModel propertyListModel = propertyListModels.get(position);
        if (propertyListModel.getEstateName().equals("")) {
            holder.name.setText(propertyListModel.getComplexName());
        } else {
            holder.name.setText(propertyListModel.getEstateName());
        }
        if(propertyListModel.isHome())
        {
            holder.name.setText("Home");
        }
        if(propertyListModel.isActiveJob())
        {
            holder.tool.setVisibility(View.VISIBLE);
        }
        else
        {
            holder.tool.setVisibility(View.GONE);
        }
        if(propertyListModel.getNotificationCount()>0)
        {
            holder.notificationCount.setText(String.valueOf(propertyListModel.getNotificationCount()));
            holder.notificationCount.setVisibility(View.VISIBLE);
        }
        else
        {
            holder.notificationCount.setVisibility(View.GONE);
        }
        Glide.with(activity)
                .load(propertyListModel.getPlaceImage())
                .asBitmap()
                .error(R.drawable.blankimage)
                .into(new LinearLayoutTarget(activity, (LinearLayout) holder.container));
        holder.address.setText(propertyListModel.getAddress());
        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, PropertyOptionActivity.class);
                intent.putExtra("houseId",propertyListModels.get(position).getHouseId());
                Log.d("HouseId",String.valueOf(propertyListModels.get(position).getHouseId()));
                intent.putExtra("propertyId",propertyListModels.get(position).getPropertyId());
                intent.putExtra("tenantCount",propertyListModels.get(position).getTenantCount());
                intent.putExtra("contactCount",propertyListModels.get(position).getContactCount());
                intent.putExtra("isHome",propertyListModels.get(position).isHome());
                intent.putExtra("notificationCount",propertyListModels.get(position).getNotificationCount());
                intent.putExtra("propertyImagePath",propertyListModels.get(position).getPlaceImage());
                intent.putExtra("landlordImagePath",propertyListModels.get(position).getLandlordImagePath());
                intent.putExtra("landlordName",propertyListModels.get(position).getLandlordName());
                intent.putExtra("landlordEmail",propertyListModels.get(position).getLandlordEmail());
                intent.putExtra("leaseStatus",propertyListModels.get(position).isLeaseStatus());
                if(propertyListModels.get(position).getEstateName().equals(""))
                {
                    intent.putExtra("propertyName",propertyListModels.get(position).getComplexName());
                }
                else
                {
                    intent.putExtra("propertyName",propertyListModels.get(position).getEstateName());
                }

                activity.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return propertyListModels.size();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {
        protected LinearLayout container;
        protected TextView name, address,notificationCount;
        protected ImageView tool;

        public ItemViewHolder(View itemView) {
            super(itemView);
            container = (LinearLayout) itemView.findViewById(R.id.main_container);
            name = (TextView) itemView.findViewById(R.id.tv_home_name);
            address = (TextView) itemView.findViewById(R.id.tv_home_address);
            tool=(ImageView)itemView.findViewById(R.id.iv_tool);
            notificationCount=(TextView)itemView.findViewById(R.id.notification_count);
        }
    }
}
