package com.android.dladle.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.dladle.R;
import com.android.dladle.model.CardModel;
import com.android.dladle.utils.CardUtils;
import com.android.dladle.utils.ChangeTransformationMethod;

import java.util.ArrayList;

/**
 * Created by admin on 05/09/17.
 */

public class RecyclerAdapterOfCard extends RecyclerView.Adapter<RecyclerAdapterOfCard.ItemViewHolder>{

    ArrayList<CardModel> cardModelArrayList;
    Context context;

    public RecyclerAdapterOfCard(ArrayList<CardModel> cardModelArrayList, Context context) {
        this.cardModelArrayList = cardModelArrayList;
        this.context = context;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.card_row, parent, false);
        return new RecyclerAdapterOfCard.ItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {

        CardModel cardModel=cardModelArrayList.get(position);
        holder.cardNumber.setText(cardModel.getCardNumber());
        holder.cardNumber.setTransformationMethod(new ChangeTransformationMethod());
        holder.cardName.setText(cardModel.getNameOnCard());
        holder.expiryDate.setText(cardModel.getExpiryDate());
        showCardType(CardUtils.getCardID(cardModel.getCardNumber()),holder.cardIcon);





    }

    @Override
    public int getItemCount() {
        return cardModelArrayList.size();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {


        protected TextView cardName,cardNumber,expiryDate;
        ImageView cardIcon;

        public ItemViewHolder(View itemView) {
            super(itemView);
            cardNumber=(TextView)itemView.findViewById(R.id.tv_card_no);
            cardName=(TextView)itemView.findViewById(R.id.tv_card_name);
            expiryDate=(TextView)itemView.findViewById(R.id.tv_expiry_date);
            cardIcon=(ImageView) itemView.findViewById(R.id.card_icon);
        }
    }

    private void showCardType(String creitCardType,ImageView mIvCardIcon) {
        if (creitCardType.equalsIgnoreCase("visa")) {
            mIvCardIcon.setImageResource(R.drawable.visa);
        } else if (creitCardType.equalsIgnoreCase("MasterCard")) {
            mIvCardIcon.setImageResource(R.drawable.mastercard);
        } else if (creitCardType.equalsIgnoreCase("Maestro")) {
            mIvCardIcon.setImageResource(R.drawable.maestro);
        } else if (creitCardType.equalsIgnoreCase("Dankort")) {

        } else if (creitCardType.equalsIgnoreCase("Discover")) {
            mIvCardIcon.setImageResource(R.drawable.discover);
        } else if (creitCardType.equalsIgnoreCase("China Union pay")) {
            mIvCardIcon.setImageResource(R.drawable.unionpay);
        } else if (creitCardType.equalsIgnoreCase("American Express")) {
            mIvCardIcon.setImageResource(R.drawable.americanexpress);
        }
    }
}
