package com.android.dladle.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.dladle.R;
import com.android.dladle.activities.InviteFromLandlordActivity;
import com.android.dladle.activities.LeaseTerminationReqFromLandlord;
import com.android.dladle.activities.NewTenantActivity;
import com.android.dladle.activities.PropertyInvitationStatusOfTenant;
import com.android.dladle.activities.PropertyOptionActivity;
import com.android.dladle.activities.ReviewActivity;
import com.android.dladle.activities.TenantRemovedActivity;
import com.android.dladle.activities.TerminationRequestStatusActivity;
import com.android.dladle.activities.VendorFinalPriceActivity;
import com.android.dladle.activities.ViewJobActivity;
import com.android.dladle.model.NotificationListModel;
import com.android.dladle.utils.CommonConstants;
import com.android.dladle.utils.PreferenceManager;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Farman on 6/10/2017.
 */

public class RecyclerAdapterForNotificationList extends RecyclerView.Adapter<RecyclerAdapterForNotificationList.ItemViewHolder> {
    ArrayList<NotificationListModel> notificationListModelArrayList;
    Context context;
    SimpleDateFormat simpleDateFormat;
    String mUserType = "";


    public RecyclerAdapterForNotificationList(ArrayList<NotificationListModel> notificationListModelArrayList, Context context) {
        this.notificationListModelArrayList = notificationListModelArrayList;
        this.context = context;
        mUserType = PreferenceManager.getInstance(context).getUserType();
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.notification_list_row, parent, false);
        return new RecyclerAdapterForNotificationList.ItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, final int position) {
        final NotificationListModel notificationListModel = notificationListModelArrayList.get(position);
        if (position % 2 == 0) {
            holder.notificationContainer.setBackgroundColor(context.getResources().getColor(R.color.green_color));
        }
        holder.notificationTitle.setText(notificationListModel.getTitle());
        holder.notificationBody.setText(notificationListModel.getBody());
        Glide.with(context).load(notificationListModel.getImageUrl())
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.drawable.ic_avatar)
                .into(holder.profileImage);
        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = simpleDateFormat.parse(notificationListModel.getTime());
            simpleDateFormat = new SimpleDateFormat("dd MMM yyyy");
            String newDate = simpleDateFormat.format(date);
            holder.notificationDate.setText(newDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.notificationContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickOnNotification(position);
            }
        });

    }

    private void clickOnNotification(int position) {
        Gson gson = new Gson();
        NotificationListModel notificationListModel1 = notificationListModelArrayList.get(position);
        int notificationTypeId = Integer.parseInt(notificationListModel1.getNotificationTypeId());
        switch (notificationTypeId) {
            case CommonConstants.LANDLORD_PROPERTY_REQ_TO_TENANT:
                Intent intent1 = new Intent(context, InviteFromLandlordActivity.class);
                intent1.putExtra("notification", gson.toJson(notificationListModel1));
                context.startActivity(intent1);
                break;
            case CommonConstants.TENANT_PROPERTY_REQ_TO_LANDLORD:
                Intent intent = new Intent(context, NewTenantActivity.class);
                intent.putExtra("notification", gson.toJson(notificationListModel1));
                context.startActivity(intent);
                break;
            case CommonConstants.TENANT_ACCEPT_LANDLORDS_PROPERTY_INVITATION:
                Intent intent5 = new Intent(context, PropertyInvitationStatusOfTenant.class);
                intent5.putExtra("notification", gson.toJson(notificationListModel1));
                context.startActivity(intent5);
                break;
            case CommonConstants.TENANT_REJECT_LANDLORDS_PROPERTY_INVITATION:
                Intent intent7 = new Intent(context, PropertyInvitationStatusOfTenant.class);
                intent7.putExtra("notification", gson.toJson(notificationListModel1));
                context.startActivity(intent7);
                break;
            case CommonConstants.LANDLORD_ACCEPT_TENANTS_PROPRTY_INVITATION:
                Intent intent8 = new Intent(context, PropertyInvitationStatusOfTenant.class);
                intent8.putExtra("notification", gson.toJson(notificationListModel1));
                context.startActivity(intent8);
                break;
            case CommonConstants.LANDLORD_REJECT_TENANTS_PROPRTY_INVITATION:
                Intent intent2 = new Intent(context, InviteFromLandlordActivity.class);
                intent2.putExtra("notification", gson.toJson(notificationListModel1));
                context.startActivity(intent2);
                break;
            case CommonConstants.LEASE_TERMINATE_REQ_FROM_LANDLORD:
                Intent intent6 = new Intent(context, LeaseTerminationReqFromLandlord.class);
                intent6.putExtra("notification", gson.toJson(notificationListModel1));
                context.startActivity(intent6);
                break;
            case CommonConstants.LEASE_TERMINATE_REQ_FROM_TENANT:
                break;
            case CommonConstants.TENANT_ACCEPTED_LESAE_TERMINATION:
                Intent intent3 = new Intent(context, TerminationRequestStatusActivity.class);
                intent3.putExtra("notification", gson.toJson(notificationListModel1));
                context.startActivity(intent3);
                break;
            case CommonConstants.TENANT_REJECTED_LESAE_TERMINATION:
                Intent intent4 = new Intent(context, TerminationRequestStatusActivity.class);
                intent4.putExtra("notification", gson.toJson(notificationListModel1));
                context.startActivity(intent4);
                break;
            case CommonConstants.LANDLORD_ACCEPTED_LESAE_TERMINATION:
                break;
            case CommonConstants.LANDLORD_REJECTED_LESAE_TERMINATION:
                break;
            case CommonConstants.TENANT_LEFT_HOME:
                Intent intent9 = new Intent(context, TenantRemovedActivity.class);
                intent9.putExtra("notification", gson.toJson(notificationListModel1));
                context.startActivity(intent9);
                break;
            case CommonConstants.TENANT_REMOVED_BY_LANDLORD:
                Intent intent10 = new Intent(context, TenantRemovedActivity.class);
                intent10.putExtra("notification", gson.toJson(notificationListModel1));
                context.startActivity(intent10);
                break;
            case CommonConstants.RATE_TENANT:
                Intent intent12 = new Intent(context, ReviewActivity.class);
                intent12.putExtra("notification", gson.toJson(notificationListModel1));
                context.startActivity(intent12);
                break;
            case CommonConstants.RATE_LANDLORD:
                Intent intent13 = new Intent(context, ReviewActivity.class);
                intent13.putExtra("notification", gson.toJson(notificationListModel1));
                context.startActivity(intent13);
                break;
            case CommonConstants.RATE_VENDOR:
                break;
            case CommonConstants.SERVICE_REQUEST_TO_VENDOR:
                Intent intent14 = new Intent(context, ViewJobActivity.class);
                intent14.putExtra("serviceId", notificationListModel1.getServiceId());
                intent14.putExtra("notificationId",notificationListModel1.getId());
                intent14.putExtra("clientEmail",notificationListModel1.getFrom());
                intent14.putExtra("employerName",notificationListModel1.getName());
                intent14.putExtra("rating",String.valueOf(notificationListModel1.getRating()));
                intent14.putExtra("image",notificationListModel1.getProfilePicture());
                intent14.putExtra("actioned",notificationListModel1.isActioned());
                context.startActivity(intent14);
                break;

            case CommonConstants.VENDOR_REQ_ACCEPTED:
                Intent intent15 = new Intent(context, ViewJobActivity.class);
                intent15.putExtra("data", notificationListModel1.getData());
                intent15.putExtra("notificationId",notificationListModel1.getId());
                intent15.putExtra("employerName",notificationListModel1.getName());
                intent15.putExtra("rating",String.valueOf(notificationListModel1.getRating()));
                intent15.putExtra("image",notificationListModel1.getProfilePicture());
                intent15.putExtra("actioned",notificationListModel1.isActioned());
                context.startActivity(intent15);
            case CommonConstants.VENDOR_REQ_REJECTED:
                Toast.makeText(context, "Vendor has rejected job requested", Toast.LENGTH_SHORT).show();
                break;
            case CommonConstants.VENDOR_FINAL_PRICE:
                Intent intent17 = new Intent(context, VendorFinalPriceActivity.class);
                intent17.putExtra("data", notificationListModel1.getData());
                intent17.putExtra("notificationId",notificationListModel1.getId());
                intent17.putExtra("employerName",notificationListModel1.getName());
                intent17.putExtra("rating",String.valueOf(notificationListModel1.getRating()));
                intent17.putExtra("image",notificationListModel1.getProfilePicture());
                intent17.putExtra("name",notificationListModel1.getName());
                context.startActivity(intent17);
                break;
            default:
                break;
        }
    }

    @Override
    public int getItemCount() {
        return notificationListModelArrayList.size();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {
        protected TextView notificationTitle, notificationBody, notificationDate;
        protected CircleImageView profileImage;
        LinearLayout notificationContainer;

        public ItemViewHolder(View itemView) {
            super(itemView);
            notificationTitle = (TextView) itemView.findViewById(R.id.notification_title);
            notificationBody = (TextView) itemView.findViewById(R.id.notification_body);
            notificationDate = (TextView) itemView.findViewById(R.id.notification_date);
            profileImage = (CircleImageView) itemView.findViewById(R.id.profile_image);
            notificationContainer = (LinearLayout) itemView.findViewById(R.id.notification_container);
        }
    }
}
