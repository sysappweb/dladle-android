package com.android.dladle.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.dladle.R;
import com.android.dladle.model.ReviewModel;

import java.util.ArrayList;

public class RecyclerAdapterForReview extends RecyclerView.Adapter<RecyclerAdapterForReview.ItemViewHolder> {


    Context context;
    ArrayList<ReviewModel> reviewModelArrayList;

    public RecyclerAdapterForReview(Context context, ArrayList<ReviewModel> reviewModelArrayList) {
        this.context = context;
        this.reviewModelArrayList = reviewModelArrayList;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.review_row, parent, false);
        return new RecyclerAdapterForReview.ItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        ReviewModel reviewModel = reviewModelArrayList.get(position);
        holder.name.setText(reviewModel.getName());
        holder.review.setText(reviewModel.getComment());
        holder.ratingBar.setRating(Float.valueOf(reviewModel.getRate()));

    }

    @Override
    public int getItemCount() {
        return reviewModelArrayList.size();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {
        protected TextView name, ratingNo, review;
        protected RatingBar ratingBar;

        public ItemViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.tv_name);
          //  ratingNo = (TextView) itemView.findViewById(R.id.tv_rating_no);
            review = (TextView) itemView.findViewById(R.id.tv_review);
            ratingBar = (RatingBar) itemView.findViewById(R.id.rating_bar);

        }
    }
}
