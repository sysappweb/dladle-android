package com.android.dladle.adapter;

import android.app.Activity;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.dladle.R;
import com.android.dladle.activities.AddImportantContactsActivity;
import com.android.dladle.model.ContactModel;
import com.android.dladle.utils.Logger;
import com.android.dladle.utils.PreferenceManager;

import java.util.ArrayList;

/**
 * Created by Farman on 5/30/2017.
 */

public class RecyclerAdpterForContact extends RecyclerView.Adapter<RecyclerAdpterForContact.ItemViewHolder> {
    ArrayList<ContactModel> contactModelArrayList;
    Activity activity;
    ArrayList<String> mContactType;
    ArrayList<String> mContactKey;
    ArrayAdapter<String> contactTypeAdapter;
    int count = 0;
    String userType = "";

    public RecyclerAdpterForContact(ArrayList<ContactModel> contactModelArrayList, Activity activity, ArrayList<String> mContactType, ArrayList<String> mContactKey) {
        this.contactModelArrayList = contactModelArrayList;
        this.activity = activity;
        this.mContactType = mContactType;
        contactTypeAdapter = new ArrayAdapter<String>(activity,
                R.layout.spiner_row, mContactType);
        this.mContactKey = mContactKey;
        userType = PreferenceManager.getInstance(activity.getApplicationContext()).getUserType();
    }


    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.contact_row, parent, false);
        return new RecyclerAdpterForContact.ItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder holder, final int position) {
        holder.contactType.setAdapter(contactTypeAdapter);
        ContactModel contactModel = contactModelArrayList.get(holder.getAdapterPosition());
            holder.address.setText(contactModel.getAddress());

            holder.name.setText(contactModel.getName());

            holder.cellNumber.setText(contactModel.getPhoneNo());

            holder.cellNumber.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {

                    ContactModel contactModel1 = contactModelArrayList.get(holder.getAdapterPosition());
                    contactModel1.setPhoneNo(s.toString());

                }
            });
            holder.name.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    ContactModel contactModel1 = contactModelArrayList.get(holder.getAdapterPosition());
                    contactModel1.setName(s.toString());
                }
            });
            holder.address.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    ContactModel contactModel1 = contactModelArrayList.get(holder.getAdapterPosition());
                    contactModel1.setAddress(s.toString());
                }
            });
            holder.contactType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    boolean isContactTypePresent = false;
                    String contactType = "";
                    if (holder.contactType.getSelectedItemPosition() != 0) {
                        contactType = mContactKey.get(holder.contactType.getSelectedItemPosition() - 1);
                        for (int i = 0; i < contactModelArrayList.size(); i++) {
                            if (!(contactModelArrayList.get(i).getType().equals(""))) {
                                if (contactModelArrayList.get(i).getType().equals(contactType)) {
                                    isContactTypePresent = true;
                                    holder.contactType.setSelection(0);
                                }
                            }
                        }
                    }

                    if (isContactTypePresent) {
                        if (count != 0) {
                            Toast.makeText(activity, contactType + " is already present,please another type of contact", Toast.LENGTH_SHORT).show();
                            ContactModel contactModel1 = contactModelArrayList.get(holder.getAdapterPosition());
                            contactModel1.setType("");
                        }

                    } else {
                        if (contactType.equals("HOSPITAL")) {
                            holder.contactTypeIcon.setImageResource(R.drawable.hospital);
                        } else if (contactType.equals("FIRE_STATION")) {
                            holder.contactTypeIcon.setImageResource(R.drawable.firestation);
                        } else if (contactType.equals("POLICE_STATION")) {
                            holder.contactTypeIcon.setImageResource(R.drawable.police);
                        } else if (contactType.equals("LANDLORD")) {
                            holder.contactTypeIcon.setImageResource(R.drawable.ic_avatar);
                        }
                        if (holder.contactType.getSelectedItemPosition() != 0) {
                            ContactModel contactModel1 = contactModelArrayList.get(holder.getAdapterPosition());
                            contactModel1.setType(mContactKey.get(holder.contactType.getSelectedItemPosition() - 1));
                        }
                    }
                    count = count + 1;
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    ContactModel contactModel1 = contactModelArrayList.get(holder.getAdapterPosition());
                    contactModel1.setType("");
                }
            });
            holder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((AddImportantContactsActivity) activity).deleteContact(position);
                }
            });



    }

    @Override
    public int getItemCount() {
        return contactModelArrayList.size();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {
        EditText name, cellNumber, address;
        protected View nameDivider;
        protected Spinner contactType;
        protected ImageView delete, contactTypeIcon;


        public ItemViewHolder(View itemView) {
            super(itemView);
            nameDivider = (View) itemView.findViewById(R.id.divider_name);
            contactType = (Spinner) itemView.findViewById(R.id.contact_type);
            address = (EditText) itemView.findViewById(R.id.address);
            name = (EditText) itemView.findViewById(R.id.et_name);
            cellNumber = (EditText) itemView.findViewById(R.id.cell_number);
            delete = (ImageView) itemView.findViewById(R.id.iv_delete);
            contactTypeIcon = (ImageView) itemView.findViewById(R.id.iv_contact_type);

        }
    }
}
