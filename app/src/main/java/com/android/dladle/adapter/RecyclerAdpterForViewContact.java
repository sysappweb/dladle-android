package com.android.dladle.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.dladle.R;
import com.android.dladle.activities.PropertyOptionActivity;
import com.android.dladle.activities.ViewContactActivity;
import com.android.dladle.customui.MyJsonObjectRequest;
import com.android.dladle.model.ContactModel;
import com.android.dladle.model.TenantListModel;
import com.android.dladle.utils.Logger;
import com.android.dladle.utils.NetworkConstants;
import com.android.dladle.utils.NetworkUtils;
import com.android.dladle.utils.PreferenceManager;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by admin on 21/06/17.
 */

public class RecyclerAdpterForViewContact extends RecyclerView.Adapter<RecyclerAdpterForViewContact.ItemViewHolder> {
    Context context;
    ArrayList<ContactModel> contactModelArrayList;
    String userType="";
    public RecyclerAdpterForViewContact(Context context, ArrayList<ContactModel> contactModelArrayList) {
        this.context = context;
        this.contactModelArrayList = contactModelArrayList;
        userType = PreferenceManager.getInstance(context).getUserType();
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.view_contact, parent, false);
        return new RecyclerAdpterForViewContact.ItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, final int position) {
        ContactModel contactModel=contactModelArrayList.get(position);
        String contactType = contactModel.getType();
        SpannableString name=new SpannableString(contactModel.getType() + ": " +contactModel.getName());
        int length=contactModel.getType().length();
        name.setSpan(new StyleSpan(Typeface.BOLD), 0, length+1, 0);
        holder.name.setText(name);
        SpannableString number=new SpannableString("Tel number: " + contactModel.getPhoneNo());
        number.setSpan(new StyleSpan(Typeface.BOLD),0,11,0);
        holder.number.setText(number);
        SpannableString address=new SpannableString("Address: " + contactModel.getAddress());
        address.setSpan(new StyleSpan(Typeface.BOLD),0,7,0);
        holder.address.setText(address);
        if (contactType.equalsIgnoreCase("HOSPITAL")) {
            holder.contactTypeIcon.setImageResource(R.drawable.hospital);
        } else if (contactType.equalsIgnoreCase("FIRE STATION")) {
            holder.contactTypeIcon.setImageResource(R.drawable.firestation);
        } else if (contactType.equalsIgnoreCase("POLICE STATION")) {
            holder.contactTypeIcon.setImageResource(R.drawable.police);
        } else if (contactType.equalsIgnoreCase("LANDLORD")) {
          Glide.with(context)
                   .load(((ViewContactActivity)context).mLandlordImagePath)
                   .asBitmap()
                   .error(R.drawable.ic_avatar)
                   .into(holder.contactTypeIcon);
        }
        if(position==contactModelArrayList.size()-1)
        {
            holder.divider.setVisibility(View.GONE);
        }
        if(userType.equalsIgnoreCase("landlord") && contactModelArrayList.size()>2)
        {
            holder.deleteContactIcon.setVisibility(View.VISIBLE);
        }
        else
        {
            holder.deleteContactIcon.setVisibility(View.INVISIBLE);
        }
        holder.deleteContactIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(NetworkUtils.isNetworkAvailable(context)) {
                    ((ViewContactActivity) context).deleteContact(contactModelArrayList.get(position).getId(), position);
                }
                {
                    Toast.makeText(context,context.getResources().getString(R.string.no_internet),Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return contactModelArrayList.size();
    }



    public static class ItemViewHolder extends RecyclerView.ViewHolder {
        TextView name, number, address;
        ImageView contactTypeIcon,deleteContactIcon;
        View divider;

        public ItemViewHolder(View itemView) {
            super(itemView);
            address = (TextView) itemView.findViewById(R.id.address);
            name = (TextView) itemView.findViewById(R.id.et_name);
            number = (TextView) itemView.findViewById(R.id.cell_number);
            contactTypeIcon = (ImageView) itemView.findViewById(R.id.iv_contact_type);
            divider = (View)itemView.findViewById(R.id.divider);
            deleteContactIcon=(ImageView)itemView.findViewById(R.id.iv_delete);
        }
    }
}
