package com.android.dladle.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

import com.android.dladle.utils.CommonConstants;
import com.android.dladle.utils.Logger;
import com.android.dladle.utils.NetworkConstants;
import com.android.dladle.utils.PreferenceManager;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by sourav on 21-01-2017.
 */

public class WelcomeService extends IntentService {

    private Context mAppContext;

    public WelcomeService() {
        super(WelcomeService.class.getSimpleName());
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mAppContext = getApplicationContext();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (CommonConstants.WELCOME_SERVICE_INTENT.equals(intent.getAction())) {
            Logger.printInfo("WelcomeService Started.");
            final String version = PreferenceManager.getInstance(mAppContext).getCurrentVersion();
            JSONObject request = new JSONObject();
            JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.GET, NetworkConstants.Welcome.URL, request,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Logger.printDebug(""+response);
                            try {
                                String status = response.getString(NetworkConstants.JsonResponse.STATUS);
                                String message = response.getString(NetworkConstants.JsonResponse.MESSAGE);
                                Logger.printDebug("WelcomeService Status : "+status+" Message : "+message);
                                if (NetworkConstants.JsonResponse.STATUS_SUCCESS.equals(status)) {
                                    JSONObject data = response.getJSONObject(NetworkConstants.JsonResponse.DATA);
                                    if (data != null) {
                                        String upgradeToVersion = data.getString(NetworkConstants.Welcome.NEED_UPGRADE_TO_VERSION);
                                        if (CommonConstants.isLowerVersion(version, upgradeToVersion)) {
                                            PreferenceManager.getInstance(mAppContext).setExpectedVersion(upgradeToVersion);
                                            sendUpgradeBroadcast();
                                        } else if (version.equals(upgradeToVersion)) {
                                            PreferenceManager.getInstance(mAppContext).setExpectedVersion("");
                                        }
                                    }
                                    Logger.printInfo("WelcomeService Successful.");
                                } else if (NetworkConstants.JsonResponse.STATUS_FAILED.equals(status)) {
                                    Logger.printInfo("WelcomeService Failed.");
                                }
                            } catch (JSONException e) {
                                Logger.printError("WelcomeService Unable to parse JSON : "+e.getMessage());
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Logger.printError("WelcomeService error : "+error.toString());
                        }
                    });
            RequestQueue requestQueue = Volley.newRequestQueue(mAppContext);
            jsonRequest.setTag(NetworkConstants.Welcome.TAG);
            requestQueue.add(jsonRequest);
        }
    }

    private void sendUpgradeBroadcast() {
        Intent i = new Intent(CommonConstants.FORCE_UPGRADE_INTENT);
        i.setPackage(CommonConstants.PACKAGE_NAME);
        Logger.printDebug("UpgradeBroadcast send");
        mAppContext.sendBroadcast(i);
    }
}
