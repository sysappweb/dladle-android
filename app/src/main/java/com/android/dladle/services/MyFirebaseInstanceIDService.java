package com.android.dladle.services;

import android.content.Intent;
import android.util.Log;

import com.android.dladle.R;
import com.android.dladle.activities.AddImportantContactsActivity;
import com.android.dladle.activities.PlaceAddedActivity;
import com.android.dladle.customui.MyJsonObjectRequest;
import com.android.dladle.utils.Logger;
import com.android.dladle.utils.NetworkConstants;
import com.android.dladle.utils.PreferenceManager;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Farman on 6/3/2017.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";

    @Override
    public void onTokenRefresh() {

        //Getting registration token
        final String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, NetworkConstants.User.ADD_DEVICE_ID,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                     Logger.printDebug(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Logger.printDebug("error");
                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("deviceId",refreshedToken);
                params.put("emailId",  PreferenceManager.getInstance(getApplicationContext()).getUserName());
                Logger.printDebug(params.toString());
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    private void sendRegistrationToServer(String token) {
        //You can implement this method to store the token on your server
        //Not required for current project
    }
}
