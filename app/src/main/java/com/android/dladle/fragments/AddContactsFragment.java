package com.android.dladle.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.android.dladle.R;
import com.android.dladle.utils.Logger;

/**
 * Created by sourav on 23-02-2017.
 */

public class AddContactsFragment extends BaseFragment {

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (null != activity) {
            Intent i = activity.getIntent();
            if (null != i) {
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.add_important_contact, container, false);
        updateHeaderMargin(view);
        return view;
    }

    private void updateHeaderMargin(View view) {
        View v = view.findViewById(R.id.parent_view);
        Logger.printInfo(v.getLayoutParams().toString());
        if (v.getLayoutParams() instanceof FrameLayout.LayoutParams) {
            FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) v.getLayoutParams();
            Logger.printInfo("Height b4 : "+lp.topMargin);
            lp.topMargin = lp.topMargin + (int) getResources().getDimension(R.dimen.actionBar_Size);
            Logger.printInfo("Height : "+lp.topMargin);
            v.setLayoutParams(lp);
        }
    }
}
