package com.android.dladle.fragments;

import android.animation.AnimatorInflater;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.test.mock.MockPackageManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.dladle.Manifest;
import com.android.dladle.R;
import com.android.dladle.activities.AddImportantContactsActivity;
import com.android.dladle.activities.AddPropertyActivity;
import com.android.dladle.activities.PlaceAddedActivity;
import com.android.dladle.activities.PropertyOptionActivity;
import com.android.dladle.activities.SendInviteToLandlordByTenantActivity;
import com.android.dladle.activities.UserProfileActivity;
import com.android.dladle.activities.VendorAccountSetUpActivty;
import com.android.dladle.adapter.RecyclerAdapterForHome;
import com.android.dladle.adapter.RecyclerAdapterForTenantNotificationInHome;
import com.android.dladle.customui.CustomProgressDialog;
import com.android.dladle.customui.MyJsonObjectRequest;
import com.android.dladle.customui.RoundedImageView;
import com.android.dladle.entities.Property;
import com.android.dladle.model.ContactModel;
import com.android.dladle.model.NotificationListModel;
import com.android.dladle.model.PropertyListModel;
import com.android.dladle.utils.CommonConstants;
import com.android.dladle.utils.ImageUtils;
import com.android.dladle.utils.Logger;
import com.android.dladle.utils.NetworkConstants;
import com.android.dladle.utils.NetworkUtils;
import com.android.dladle.utils.PhotoManager;
import com.android.dladle.utils.PreferenceManager;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by sourav on 18-02-2017.
 */

public class ProfileDetailsFragment extends BaseFragment implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {
    RadioButton btn2, btn1;
    private String mFragmentKey;
    private TextView mNameView, mUserType, mBanner, mEmptyView, mProgressText;
    private View mButtonLine, mDualButton;
    private String mEmail, mName, mLast, mRating, mCellNo;
    private ImageView mProfilePicOverlay;
    private RoundedImageView mProfilePic;
    private RatingBar mRatingBar;
    private Button mSingleButton;
    RecyclerView mHomeList;
    private RecyclerAdapterForHome mRecyclerAdapterForHome;
    private RecyclerAdapterForTenantNotificationInHome mRecyclerAdapterForTenantNotificationInHome;
    LinearLayout mProgressbarContainer;
    private View mProgressView;
    private ObjectAnimator mProgressAnimator;
    ArrayList<PropertyListModel> mPropertyListModel = new ArrayList<>();
    ArrayList<NotificationListModel> notificationListModelArrayList = new ArrayList<>();
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private double currentLatitude;
    private double currentLongitude;
    int MY_PERMISSIONS_REQUEST_CODE = 2;
    CustomProgressDialog mCustomProgressDialog;

    public ProfileDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (null != activity) {
            Intent i = activity.getIntent();
            if (null != i) {
                mFragmentKey = i.getStringExtra(NetworkConstants.JsonResponse.DATA);
                mName = i.getStringExtra(NetworkConstants.Registration.FIRST_NAME_KEY);
                mLast = i.getStringExtra(NetworkConstants.Registration.LAST_NAME_KEY);

            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        boolean isCollapsible = getResources().getBoolean(R.bool.collapsible_profile_view)
                && !CommonConstants.FRAGMENT_KEY_TENANT.equalsIgnoreCase(mFragmentKey);
        View view = inflater.inflate(isCollapsible ? R.layout.user_profile_details_collapsible_fragment : R.layout.user_profile_details_fixed_fragment, container, false);
        updateHeaderMargin(view);
        mButtonLine = view.findViewById(R.id.button_divider);
        mUserType = (TextView) view.findViewById(R.id.user_type);
        mProfilePicOverlay = (ImageView) view.findViewById(R.id.profile_pic_overlay);
        mEmptyView = (TextView) view.findViewById(R.id.empty_view);
        mBanner = (TextView) view.findViewById(R.id.banner);
        mProgressText = (TextView) view.findViewById(R.id.progress_bar_text);
        mSingleButton = (Button) view.findViewById(R.id.single_button);
        mDualButton = view.findViewById(R.id.dual_btn_layout);
        mHomeList = (RecyclerView) view.findViewById(R.id.rv_home);
        mProgressView = (View) view.findViewById(R.id.progress_bar);
        mProgressbarContainer = (LinearLayout) view.findViewById(R.id.progressbar_container);
        setUserType();
        mNameView = (TextView) view.findViewById(R.id.name_view);
        mNameView.setText(mName + " " + mLast);
        mProfilePic = (RoundedImageView) view.findViewById(R.id.profile_pic);
        mRatingBar = (RatingBar) view.findViewById(R.id.rating_bar);
        PhotoManager.setProfilePic(getContext(), mProfilePic);
        mProfilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((UserProfileActivity) getActivity()).replaceFragment();
            }
        });
        if (!(PreferenceManager.getInstance(getActivity()).getProfileImageUrl().equals("") && PreferenceManager.getInstance(getActivity()).getProfileImageUrl()==null)) {
            Glide.with(this)
                    .load(PreferenceManager.getInstance(getActivity()).getProfileImageUrl())
                    .error(R.drawable.ic_avatar)
                    .into(mProfilePic);
        }
        getRating();
        return view;
    }

    @Override
    public void onResume() {
        mName = PreferenceManager.getInstance(getContext()).getFirstName();
        mLast = PreferenceManager.getInstance(getContext()).getLastName();
        mNameView.setText(mName + " " + mLast);

        super.onResume();
    }

    private void updateHeaderMargin(View view) {
        View v = view.findViewById(R.id.parent_view);
        Logger.printInfo(v.getLayoutParams().toString());
        if (v.getLayoutParams() instanceof FrameLayout.LayoutParams) {
            FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) v.getLayoutParams();
            lp.topMargin = lp.topMargin + (int) getResources().getDimension(R.dimen.actionBar_Size);
            v.setLayoutParams(lp);
        }
    }

    private void setUserType() {
        if (CommonConstants.FRAGMENT_KEY_LANDLORD.equalsIgnoreCase(mFragmentKey)) {
            mUserType.setText(R.string.user_type_landlord);
            mButtonLine.setBackground(getResources().getDrawable(R.mipmap.line_green));
            mProfilePicOverlay.setImageDrawable(getResources().getDrawable(R.mipmap.ic_green_overlay));
            mEmptyView.setText(R.string.no_place);
            mBanner.setText(R.string.my_place);
            mSingleButton.setText(getCustomString(getString(R.string.add_place), R.mipmap.ic_home_small));
            mSingleButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (null != getActivity()) {
                        Logger.printInfo(mFragmentKey);

                        if (mFragmentKey.equalsIgnoreCase(CommonConstants.FRAGMENT_KEY_TENANT)) {
                            Intent i = new Intent(getActivity(), SendInviteToLandlordByTenantActivity.class);
                            transitionTo(getActivity(), i);
                        } else {
                            Intent i = new Intent(getActivity(), AddPropertyActivity.class);
                            transitionTo(getActivity(), i);
                        }
                    }
                }
            });
        } else if (CommonConstants.FRAGMENT_KEY_TENANT.equalsIgnoreCase(mFragmentKey)) {
            mUserType.setText(R.string.user_type_tenant);
            mButtonLine.setBackground(getResources().getDrawable(R.mipmap.line_blue));
            mProfilePicOverlay.setImageDrawable(getResources().getDrawable(R.mipmap.ic_blue_overlay));
            mEmptyView.setText(R.string.no_home);
            mBanner.setText(R.string.my_home);
            mSingleButton.setText(getCustomString(getString(R.string.add_home), R.mipmap.ic_home_small));
            mSingleButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(getActivity(), SendInviteToLandlordByTenantActivity.class);
                    transitionTo(getActivity(), i);
                }
            });


        } else if (CommonConstants.FRAGMENT_KEY_VENDOR.equalsIgnoreCase(mFragmentKey)) {
            checkMyCurrentWorkStatus();
            String vendorType = PreferenceManager.getInstance(getContext()).getVendorType();
            if (PreferenceManager.getInstance(getContext()).getVendorType().equals("null"))
            {
                mUserType.setText("Vendor");

            }
            else
            {
                mUserType.setText(("Vendor\r\n"+""+PreferenceManager.getInstance(getContext()).getVendorType()));
            }
            boolean isUpdated = PreferenceManager.getInstance(getContext()).isProfileUpdated();
            mButtonLine.setBackground(getResources().getDrawable(R.mipmap.line_orange));
            mProfilePicOverlay.setImageDrawable(getResources().getDrawable(R.mipmap.ic_orange_overlay));
            mEmptyView.setText(isUpdated ?
                    R.string.notice_board_details_vendor_empty : R.string.notice_board_details_vendor);
            mBanner.setText(R.string.notice_board);
            mSingleButton.setText(getCustomString(getString(R.string.setup_account), R.drawable.ic_user_small));
            //  mSingleButton.setVisibility(isUpdated ? View.GONE : View.VISIBLE);
            mSingleButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), VendorAccountSetUpActivty.class);
                    startActivity(intent);
                }
            });
            //  mDualButton.setVisibility(isUpdated ? View.VISIBLE : View.GONE);
            mDualButton.setBackground(new ColorDrawable(Color.TRANSPARENT));

            boolean vendorAccountStatus = PreferenceManager.getInstance(getActivity()).getVendorAccountStatus();
            if (vendorAccountStatus) {
                mSingleButton.setVisibility(View.GONE);
                mButtonLine.setVisibility(View.GONE);
                mDualButton.setVisibility(View.VISIBLE);
                getNotificationList();

            } else {
                mSingleButton.setVisibility(View.VISIBLE);
                mButtonLine.setVisibility(View.VISIBLE);
                mDualButton.setVisibility(View.GONE);
            }

            btn1 = (RadioButton) mDualButton.findViewById(R.id.yes);
            btn2 = (RadioButton) mDualButton.findViewById(R.id.no);
            btn1.setText("At Work");
            btn1.setBackground(new ColorDrawable(Color.parseColor("#00DD00")));
            btn1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    btn2.setSelected(false);
                    btn2.setBackground(new ColorDrawable(Color.parseColor("#33DD0000")));
                    btn1.setBackground(new ColorDrawable(Color.parseColor("#00DD00")));
                    locationRequest();
                }
            });
            btn2.setText("Off Work");
            btn2.setBackground(new ColorDrawable(Color.parseColor("#33DD0000")));
            btn2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    btn1.setSelected(false);
                    btn1.setBackground(new ColorDrawable(Color.parseColor("#3300DD00")));
                    btn2.setBackground(new ColorDrawable(Color.parseColor("#DD0000")));
                    updateWorkAvailability(false);
                }
            });

        } else {
            Logger.printInfo("Invalid user type");
        }





    }

    private SpannableString getCustomString(String str, int res) {
        SpannableString ss = new SpannableString(str);
        Drawable d = getResources().getDrawable(res);
        int size = (int) getResources().getDimension(R.dimen.profile_btn_height);
        d.setBounds(0, 0, size, size);
        ImageSpan span = new ImageSpan(d, ImageSpan.ALIGN_BASELINE);
        ss.setSpan(span, 0, 1, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        return ss;
    }

    private void updateWorkAvailability(boolean flag) {
        showProgress(true);
        JSONObject input = new JSONObject();
        String url = null;
        try {
            url = "";
            if (flag) {
                url = NetworkConstants.VendorController.AT_WORK;
                input.put("currentLocationLatitude", String.valueOf(currentLatitude));
                input.put("currentLocationLongitude", String.valueOf(currentLongitude));
            } else {
                url = NetworkConstants.VendorController.OFF_WORK;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        showProgress(true);
        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.POST, url, input,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        showProgress(false);
                        int length = 0;
                        Logger.printDebug("" + response);
                        showMessage(response.optString("Message"));
                        if (response.optString("Status").equalsIgnoreCase(NetworkConstants.SUCCESS)) {
                            showMessage(response.optString("Message"));
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showProgress(false);
                        if (error instanceof TimeoutError) {
                            showMessage(getString(R.string.server_error));
                        } else {
                            showMessage(error.getMessage());
                        }
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        jsonRequest.setTag(NetworkConstants.Login.TAG);
        requestQueue.add(jsonRequest);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    private void getPlaceList() {
        showProgress(true);
        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.GET, NetworkConstants.AddPropertyLandLord.PROPERTY_LIST, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideProgress();
                        Logger.printDebug("" + response);
                        if (response.optString("Status").equalsIgnoreCase(NetworkConstants.SUCCESS)) {
                            JSONArray data = response.optJSONArray("Data");
                            if (data != null && data.length() > 0) {
                                for (int i = 0; i < data.length(); i++) {
                                    JSONObject proerty = data.optJSONObject(i);
                                    PropertyListModel propertyListModel = new PropertyListModel();
                                    propertyListModel.setHouseId(proerty.optInt("houseId"));
                                    propertyListModel.setPropertyId(proerty.optInt("propertyId"));
                                    propertyListModel.setAddress(proerty.optString("address"));
                                    propertyListModel.setPlaceType(proerty.optString("placeType"));
                                    propertyListModel.setComplexName(proerty.optString("complexName"));
                                    propertyListModel.setEstateName(proerty.optString("estateName"));
                                    propertyListModel.setPlaceImage(proerty.optString("placeImage"));
                                    propertyListModel.setUnitNumber(proerty.optString("unitNumber"));
                                    propertyListModel.setNotificationCount(proerty.optInt("notificationsCount"));
                                    propertyListModel.setContactCount(proerty.optInt("contactsCount"));
                                    propertyListModel.setHome(proerty.optBoolean("home"));
                                    propertyListModel.setTenantCount(proerty.optInt("tenantsCount"));
                                    propertyListModel.setActiveJob(proerty.optBoolean("activeJob"));
                                    propertyListModel.setLeaseStatus(proerty.optBoolean("leaseStatus"));
                                    JSONObject landlord = proerty.optJSONObject("landlord");
                                    if (landlord != null) {
                                        propertyListModel.setLandlordEmail(landlord.optString("emailId"));
                                        propertyListModel.setLandlordName(landlord.optString("firstName"));
                                        propertyListModel.setLandlordImagePath(landlord.optString("profilePicture"));
                                    }
                                    mPropertyListModel.add(propertyListModel);
                                }
                                showPlaceList();
                                if (mFragmentKey.equalsIgnoreCase("tenant")) {
                                    mSingleButton.setVisibility(View.GONE);
                                    mButtonLine.setVisibility(View.GONE);
                                    ((UserProfileActivity) getActivity()).showHideAddStatusOption(true);

                                }

                            } else {
                                // ((UserProfileActivity)getActivity()).showHideAddStatusOption(false);
                                if (mFragmentKey.equalsIgnoreCase("tenant")) {
                                    //call notification list
                                    getNotificationList();

                                } else {
                                    mHomeList.setVisibility(View.GONE);
                                    mEmptyView.setVisibility(View.VISIBLE);
                                }
                            }


                        } else {

                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof TimeoutError) {
                            showMessage(getString(R.string.server_error));
                        } else {
                            showMessage(error.getMessage());
                        }
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(NetworkConstants.TIMOUT_TIME,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        jsonRequest.setTag(NetworkConstants.Login.TAG);
        requestQueue.add(jsonRequest);
    }

    private void showProgress(final boolean show) {
        if (show) {
            mProgressView.setVisibility(View.VISIBLE);
            mProgressText.setVisibility(View.VISIBLE);
            mProgressAnimator = (ObjectAnimator) AnimatorInflater.loadAnimator(getActivity(), R.animator.flipping);
            mProgressAnimator.setTarget(mProgressView);
            mProgressAnimator.start();
        }
    }

    private void hideProgress() {
        mProgressAnimator.cancel();
        mProgressbarContainer.setVisibility(View.GONE);
    }

    private void showPlaceList() {
        mRecyclerAdapterForHome = new RecyclerAdapterForHome(getActivity(), mPropertyListModel);
        mHomeList.setLayoutManager(new LinearLayoutManager(getActivity()));
        mHomeList.setAdapter(mRecyclerAdapterForHome);
        mHomeList.setNestedScrollingEnabled(false);
        mHomeList.setVisibility(View.VISIBLE);
    }

    private void getNotificationList() {
        showProgress(true);
        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.GET, NetworkConstants.Notification.NOTIFICATION_LIST, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        showProgress(false);
                        int length = 0;
                        Logger.printDebug("" + response);
                        if (response.optString("Status").equalsIgnoreCase(NetworkConstants.SUCCESS)) {
                            JSONArray jsonArray = response.optJSONArray("Data");
                            if (jsonArray != null) {
                                length = jsonArray.length();
                                if (length > 0) {
                                    Gson gson = new Gson();
                                    for (int i = 0; i < length; i++) {
                                        JSONObject jsonObject = jsonArray.optJSONObject(i);
                                        NotificationListModel notificationListModel = gson.fromJson(jsonObject.toString(), NotificationListModel.class);

                                        if (!notificationListModel.isRead()) {
                                            String data = notificationListModel.getData();
                                            List<String> items = Arrays.asList(data.split("\\s*,\\s*"));
                                            for (int j = 0; j < items.size(); j++) {
                                                if (items.get(j).contains("houseId")) {
                                                    int index = items.get(j).lastIndexOf(":");
                                                    notificationListModel.setHouseId(Integer.parseInt(items.get(j).substring(index + 1)));
                                                }
                                                if (items.get(j).contains("leaseEndDate")) {
                                                    int index = items.get(j).lastIndexOf(":");
                                                    notificationListModel.setLeaseEnddate(items.get(j).substring(index + 1));
                                                }
                                                if (items.get(j).contains("serviceId")) {
                                                    int index = items.get(j).lastIndexOf(":");
                                                    notificationListModel.setServiceId(Integer.parseInt(items.get(j).substring(index + 1)));
                                                    Logger.printDebug(items.get(j).substring(index + 1));
                                                }
                                            }
                                            notificationListModelArrayList.add(notificationListModel);
                                        }
                                    }
                                    mHomeList.setLayoutManager(new LinearLayoutManager(getActivity()));
                                    mRecyclerAdapterForTenantNotificationInHome = new RecyclerAdapterForTenantNotificationInHome(notificationListModelArrayList, getActivity());
                                    mHomeList.setAdapter(mRecyclerAdapterForTenantNotificationInHome);
                                    mHomeList.setNestedScrollingEnabled(false);
                                    mHomeList.setVisibility(View.VISIBLE);
                                } else {
                                    mHomeList.setVisibility(View.GONE);
                                    mEmptyView.setVisibility(View.VISIBLE);
                                }
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showProgress(false);
                        if (error instanceof TimeoutError) {
                            showMessage(getString(R.string.server_error));
                        } else {
                            showMessage(error.getMessage());
                        }
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        jsonRequest.setTag(NetworkConstants.Login.TAG);
        requestQueue.add(jsonRequest);
    }

    private void getLastLocation() {
        if (NetworkUtils.isGPSEnabled(getActivity())) {
            if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            //  Toast.makeText(getActivity(), "connected", Toast.LENGTH_LONG).show();
            if (location == null) {
                // LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
                //Toast.makeText(getActivity(), "loc null", Toast.LENGTH_LONG).show();
                showMessage("Unable to get location,Please try again");
            } else {
                currentLatitude = location.getLatitude();
                currentLongitude = location.getLongitude();
                updateWorkAvailability(true);
                //  Toast.makeText(getActivity(), currentLatitude + " WORKS " + currentLongitude + "", Toast.LENGTH_LONG).show();
            }
        } else {
            showMessage("Please enable GPS");
            final Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(intent);
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        showProgress(false);
        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, MY_PERMISSIONS_REQUEST_CODE);
            } else {
                getLastLocation();
            }
        } else {
            getLastLocation();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(getActivity(), CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
            Log.e("Error", "Location services connection failed with code " + connectionResult.getErrorCode());
            showMessage("Unable to get location,Please try again");
        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    private void locationRequest() {
        showProgress(true);
        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)
                .setFastestInterval(1 * 1000);
        mGoogleApiClient.connect();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.e("Req Code", "" + requestCode);
        if (requestCode == MY_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length == 1 &&
                    grantResults[0] == MockPackageManager.PERMISSION_GRANTED) {
                getLastLocation();

            } else {
                showMessage("You have denied permission for location");
            }
        }

    }

    private void getRating() {
        showProgress(true);
        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.GET, NetworkConstants.ReviewController.GET_REVIEW_DETAILS, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideProgress();
                        Logger.printDebug("" + response);
                        if (response.optString("Status").equalsIgnoreCase(NetworkConstants.SUCCESS)) {
                            JSONObject data = response.optJSONObject("Data");
                            if (data != null && data.length() > 0) {

                                double rating = data.optDouble("rate");
                                mRatingBar.setRating((float) rating);
                                int count = data.optInt("count");
                                if (count == 0) {
                                    mRatingBar.setVisibility(View.GONE);
                                }
                                PreferenceManager.getInstance(getActivity()).setRating((float) rating);
                                PreferenceManager.getInstance(getActivity()).setRatingCount(count);
                                getPlaceList();

                            }


                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof TimeoutError) {
                            showMessage(getString(R.string.server_error));
                        } else {
                            showMessage(error.getMessage());
                        }
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        jsonRequest.setTag(NetworkConstants.Login.TAG);
        requestQueue.add(jsonRequest);
    }

    private void checkMyCurrentWorkStatus() {
        showProgress(true);
        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.GET, NetworkConstants.VendorController.WORK_STATUS, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideProgress();
                        Logger.printDebug("" + response);
                        if (response.optString("Status").equalsIgnoreCase(NetworkConstants.SUCCESS)) {

                            JSONObject jsonObject = response.optJSONObject("Data");
                            boolean status = jsonObject.optBoolean("currentWorkStatus");
                            if (status) {
                                btn2.setSelected(false);
                                btn2.setBackground(new ColorDrawable(Color.parseColor("#33DD0000")));
                                btn1.setBackground(new ColorDrawable(Color.parseColor("#00DD00")));
                            } else {
                                btn1.setSelected(false);
                                btn1.setBackground(new ColorDrawable(Color.parseColor("#3300DD00")));
                                btn2.setBackground(new ColorDrawable(Color.parseColor("#DD0000")));
                            }

                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof TimeoutError) {
                            showMessage(getString(R.string.server_error));
                        } else {
                            showMessage(error.getMessage());
                        }
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        jsonRequest.setTag(NetworkConstants.Login.TAG);
        requestQueue.add(jsonRequest);
    }


}
