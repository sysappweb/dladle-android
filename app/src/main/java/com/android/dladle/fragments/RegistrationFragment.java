package com.android.dladle.fragments;

/**
 * Created by sourav on 14-01-2017.
 */

import android.animation.AnimatorInflater;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.dladle.R;
import com.android.dladle.activities.BaseActivity;
import com.android.dladle.activities.RegistrationCompletedActivity;
import com.android.dladle.customui.CustomDialog;
import com.android.dladle.customui.TextViewUtils;
import com.android.dladle.utils.CommonConstants;
import com.android.dladle.utils.GetCertificate;
import com.android.dladle.utils.Logger;
import com.android.dladle.utils.NetworkConstants;
import com.android.dladle.utils.NetworkUtils;
import com.android.dladle.utils.PreferenceManager;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class RegistrationFragment extends BaseFragment implements BaseActivity.OnSoftKeyboardListener, CustomDialog.DialogInteraction {


    private View mButtonContainer, mScrollView, mHelpButton, mHeaderView, mButtonLine, mLogoView;
    private Button mSubmitButton;
    private ImageButton mBackButton;
    private View mProgressView, mUserPickerContainer, mTNCContainer;
    private Spinner mUserTypeSpinner;
    private TextView mUserTypeTextView, mUseRegisterTextView, mTNCTextView;
    private EditText mFirstNameView, mLastNameView, mEmailView, mPasswordView, mConfirmPasswordView, mIdNumberView;
    private String mFragmentKey;
    private Switch mTNCSwitch;

    private String mFirstName;
    private String mLastName;
    private String mEmail;
    private String mPassword;
    private String mIdNumber;

    private CustomDialog mTNCDialog;
    private ScrollView scrollView;

    private boolean mRegistrationInProgress, mShowUserTypePicker, mSupportReadFullTNC;

    public RegistrationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (null != activity) {
            Intent i = activity.getIntent();
            if (null != i) {
                mFragmentKey = i.getStringExtra(CommonConstants.FRAGMENT_KEY);
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.register_user_layout, container, false);
        mRootView = view.findViewById(R.id.rootView);
        if (null != mRootView) {
            mHeaderView = view.findViewById(R.id.include);
            mUseRegisterTextView = (TextView) view.findViewById(R.id.register_header);
            mUserPickerContainer = view.findViewById(R.id.user_picker_container);
            mShowUserTypePicker = getResources().getBoolean(R.bool.show_distinct_user_type);
            mUserPickerContainer.setVisibility(mShowUserTypePicker ? View.GONE : View.VISIBLE);
            mUseRegisterTextView.setVisibility(mShowUserTypePicker ? View.GONE : View.VISIBLE);
            mUserTypeSpinner = (Spinner) view.findViewById(R.id.user_picker);
            final String[] arraySpinner = {"Select User Type", "Landlord", "Tenant", "Vendor"};
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                    R.layout.spinner_item, arraySpinner);
            mUserTypeSpinner.setAdapter(adapter);
            mUserTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position > 0 && position < 4) {
                        mFragmentKey = arraySpinner[position].toLowerCase();
                    } else {
                        mFragmentKey = "";
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    mFragmentKey = "";
                }
            });

            mButtonContainer = view.findViewById(R.id.button_container);
            mSubmitButton = (Button) view.findViewById(R.id.btn_submit);
            mSubmitButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    hideKeyboardAndDoRegistration();
                }
            });
            mBackButton = (ImageButton) view.findViewById(R.id.back_btn);
            mBackButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            notifyListener();
                        }
                    }, isShowingKeyboard ? CommonConstants.APP_BACK_PRESS_DELAY : 0);
                    if (isShowingKeyboard) {
                        hideKeyboard();
                    }
                }
            });
            mScrollView = view.findViewById(R.id.reg_form);
            mProgressView = view.findViewById(R.id.progress_bar);
            mLogoView = view.findViewById(R.id.headerView);
            mFirstNameView = (EditText) view.findViewById(R.id.first_name);
            mLastNameView = (EditText) view.findViewById(R.id.last_name);
            mEmailView = (EditText) view.findViewById(R.id.email);
            mPasswordView = (EditText) view.findViewById(R.id.password);
            mConfirmPasswordView = (EditText) view.findViewById(R.id.confirm_password);
            mIdNumberView = (EditText) view.findViewById(R.id.id_num);
            mUserTypeTextView = (TextView) view.findViewById(R.id.vendor_type);
            mUserTypeTextView.setVisibility(mShowUserTypePicker ? View.VISIBLE : View.GONE);
            mButtonLine = view.findViewById(R.id.btn_line);
            mHelpButton = view.findViewById(R.id.help);
            mHelpButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showMessage(v.getContext().getString(R.string.id_num_help));
                }
            });
            mSupportReadFullTNC = getResources().getBoolean(R.bool.support_read_full_tnc);
            mTNCContainer = view.findViewById(R.id.tnc_container);
            mTNCSwitch = (Switch) view.findViewById(R.id.tnc_switch);
            mTNCTextView = (TextView) view.findViewById(R.id.tnc_textView);

            String text = getString(R.string.accept);
            String content = text + getString(R.string.terms_and_conditions);
            ClickableSpan clickableSpan = new ClickableSpan() {
                @Override
                public void onClick(View textView) {
                    showTNC();
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    super.updateDrawState(ds);
                    ds.setUnderlineText(true);
                }
            };
            SpannableString sp = new SpannableString(content);
            sp.setSpan(new ForegroundColorSpan(Color.WHITE), 0, text.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            sp.setSpan(clickableSpan, text.length(), content.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            mTNCTextView.setText(sp);
            mTNCTextView.setMovementMethod(LinkMovementMethod.getInstance());
            mTNCContainer.setVisibility(mSupportReadFullTNC ? View.GONE : View.VISIBLE);
            if (mShowUserTypePicker) {
                setUserType();
            }
        }
        return view;
    }

    private void setUserType() {
        if (CommonConstants.FRAGMENT_KEY_LANDLORD.equals(mFragmentKey)) {
            mUserTypeTextView.setBackground(getResources().getDrawable(R.mipmap.stripe_green));
            mUserTypeTextView.setText(R.string.user_type_landlord);
            mButtonLine.setBackground(getResources().getDrawable(R.mipmap.line_green));
        } else if (CommonConstants.FRAGMENT_KEY_TENANT.equals(mFragmentKey)) {
            mUserTypeTextView.setBackground(getResources().getDrawable(R.mipmap.stripe_blue));
            mUserTypeTextView.setText(R.string.user_type_tenant);
            mButtonLine.setBackground(getResources().getDrawable(R.mipmap.line_blue));
        } else if (CommonConstants.FRAGMENT_KEY_VENDOR.equals(mFragmentKey)) {
            mUserTypeTextView.setBackground(getResources().getDrawable(R.mipmap.stripe_orange));
            mUserTypeTextView.setText(R.string.user_type_vendor);
            mButtonLine.setBackground(getResources().getDrawable(R.mipmap.line_orange));
        } else {
            Logger.printInfo("Invalid user type");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mSubmitButton.setEnabled(true);
        mBackButton.setEnabled(true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseActivity) {
            ((BaseActivity) context).setOnSoftKeyboardListener(this);
        }
    }

    @Override
    public void onDetach() {
        if (getActivity() instanceof BaseActivity) {
            ((BaseActivity) getActivity()).resetOnKeyBoardListener();
        }
        super.onDetach();
    }

    private void hideKeyboardAndDoRegistration() {
        if (getActivity() != null) {
            mSubmitButton.setEnabled(false);
            mBackButton.setEnabled(false);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doRegistration();
                }
            }, isShowingKeyboard ? CommonConstants.HIDE_KEYBOARD_DELAY : 0);
            Logger.printDebug("isKeyboardShown : " + isShowingKeyboard);
            if (isShowingKeyboard) {
                hideKeyboard();
            }
        }
    }

    /**
     * Attempts to register the account specified by the registration form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual registration attempt is made.
     */
    private void doRegistration() {
        if (mRegistrationInProgress || getContext() == null) {
            return;
        }

        if (!NetworkUtils.isNetworkAvailable(getContext())) {
            Logger.printInfo("Not connected to internet");
            showMessage(getString(R.string.no_internet));
            mSubmitButton.setEnabled(true);
            mBackButton.setEnabled(true);
            return;
        }

//        hideKeyboard();

        // Reset errors.
        mFirstNameView.setError(null);
        mLastNameView.setError(null);
        mEmailView.setError(null);
        mPasswordView.setError(null);
        mConfirmPasswordView.setError(null);
        mIdNumberView.setError(null);

        // Store values at the time of the registration attempt.
        mFirstName = TextViewUtils.removeEndSpaces(mFirstNameView.getText().toString());
        mLastName = TextViewUtils.removeEndSpaces(mLastNameView.getText().toString());
        mEmail = TextViewUtils.removeEndSpaces(mEmailView.getText().toString());
        mPassword = mPasswordView.getText().toString();
        final String confirmPassword = mConfirmPasswordView.getText().toString();
        mIdNumber = TextViewUtils.removeEndSpaces(mIdNumberView.getText().toString());

        boolean cancel = false;
        View focusView = null;

        // Check if id number is valid or not.
        if (!CommonConstants.isValidIdNumber(mIdNumber)) {
            mIdNumberView.setError(getString(R.string.invalid_id_number));
            focusView = mIdNumberView;
            cancel = true;
        }

        // check if both the passwords are same or not.
        if (!TextUtils.isEmpty(mPassword) && !mPassword.equals(confirmPassword)) {
            TextViewUtils.setPasswordViewMargin(mConfirmPasswordView);
            mConfirmPasswordView.setError(getString(R.string.password_not_matched));
            focusView = mConfirmPasswordView;
            cancel = true;
        }

        // Check for a valid password, if the user entered one.
        if (!CommonConstants.isPasswordValid(mPassword)) {
            TextViewUtils.setPasswordViewMargin(mPasswordView);
            mPasswordView.setError(getString(R.string.error_invalid_password));
            Toast.makeText(getContext(), getString(R.string.invalid_password_message), Toast.LENGTH_LONG).show();
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(mEmail)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!CommonConstants.isEmailValid(mEmail)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }


        // Check for last name.
        if (TextUtils.isEmpty(mLastName)) {
            mLastNameView.setError(getString(R.string.error_field_required));
            focusView = mLastNameView;
            cancel = true;
        }


        // Check for first name
        if (TextUtils.isEmpty(mFirstName)) {
            mFirstNameView.setError(getString(R.string.error_field_required));
            focusView = mFirstNameView;
            cancel = true;
        }
        String msg = "";

        if (!mSupportReadFullTNC && !mTNCSwitch.isChecked()) {
            cancel = true;
            msg = "Accept the Terms and Conditions";

        }

        if (TextUtils.isEmpty(mFragmentKey)) {
            cancel = true;
            msg = "Select User Type";
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            if (null != focusView) {
                focusView.requestFocus();
            } else if (!TextUtils.isEmpty(msg)) {
                showMessage(msg);
            }
            mSubmitButton.setEnabled(true);
            mBackButton.setEnabled(true);
        } else {
            if (mSupportReadFullTNC) {
                showTNC();
            } else {
                doServerInteraction();
            }
        }
    }

    private void showTNC() {
        if (null != mTNCDialog && mTNCDialog.isShowing()) {
            return;
        }
        mTNCDialog = new CustomDialog(getContext(), mSupportReadFullTNC ? CustomDialog.TYPE_ACCEPT_DECLINE_TNC : CustomDialog.TYPE_TNC);
        mTNCDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mTNCDialog.setCallback(this);
        mTNCDialog.show();
    }

    private void doServerInteraction() {
        Logger.printDebug("Server interaction started");
        // Show a progress spinner
        showProgress(true);
        JSONObject request = new JSONObject();
        try {
            request.put(NetworkConstants.Registration.EMAIL_KEY, mEmail);
            request.put(NetworkConstants.Registration.FIRST_NAME_KEY, mFirstName);
            request.put(NetworkConstants.Registration.ID_KEY, mIdNumber);
            request.put(NetworkConstants.Registration.LAST_NAME_KEY, mLastName);
            request.put(NetworkConstants.Registration.PASSWORD_KEY, mPassword);
            request.put(NetworkConstants.Registration.USER_TYPE_KEY, mFragmentKey.toUpperCase());
            Logger.printDebug("JSON : " + request);
        } catch (Exception e) {
            showProgress(false);
            e.printStackTrace();
        }
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, NetworkConstants.Registration.URL, request,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Logger.printDebug("" + response);
                        try {
                            String status = response.getString(NetworkConstants.JsonResponse.STATUS);
                            String message = response.getString(NetworkConstants.JsonResponse.MESSAGE);
                            if (NetworkConstants.JsonResponse.STATUS_SUCCESS.equalsIgnoreCase(status)) {
                                PreferenceManager.getInstance(getContext()).setFirstTimeLaunch();
                                mProgressView.setVisibility(View.GONE);
                                Intent intent = new Intent();
                                intent.setClass(getActivity(), RegistrationCompletedActivity.class);
                                intent.putExtra(CommonConstants.USER_NAME, mFirstName);
                                intent.putExtra(CommonConstants.FRAGMENT_KEY, mFragmentKey);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                getActivity().finishAffinity();
                            } else {
                                showProgress(false);
                                showMessage(status + " : " + message);
                            }
                        } catch (JSONException e) {
                            Logger.printError("Unable to parse JSON : " + e.getMessage());
                            showProgress(false);
                            showMessage(getString(R.string.unexpected_error));
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showProgress(false);
                        Logger.printError("Some error : " + error.toString());
                        showMessage(error.getMessage());
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(getContext(), new HurlStack(null, new GetCertificate().getSocketFactory(getContext())));
        jsonRequest.setTag(NetworkConstants.Login.TAG);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(NetworkConstants.TIMOUT_TIME,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonRequest);
    }

    @Override
    public void onStop() {
        super.onStop();
        Volley.newRequestQueue(getActivity()).cancelAll(NetworkConstants.Registration.TAG);
        if (null != mTNCDialog && mTNCDialog.isShowing()) {
            mTNCDialog.dismiss();
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    private void showProgress(final boolean show) {
        if (getActivity() instanceof BaseActivity) {
            ((BaseActivity) getActivity()).setActionInProgress(show);
        }
        mRegistrationInProgress = show;

        mScrollView.setVisibility(show ? View.GONE : View.VISIBLE);
        mSubmitButton.setEnabled(true);
        mButtonContainer.setVisibility(show ? View.GONE : View.VISIBLE);
        mLogoView.setVisibility(show ? View.GONE : View.VISIBLE);
        mBackButton.setEnabled(true);
        mBackButton.setVisibility(show ? View.GONE : View.VISIBLE);
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);

        ObjectAnimator progressAnimator = (ObjectAnimator) AnimatorInflater.loadAnimator(getContext(), R.animator.flipping);
        progressAnimator.setTarget(mProgressView);
        progressAnimator.start();
    }


    @Override
    public void onKeyboardShown(int currentKeyboardHeight) {
        Logger.printDebug("onKeyboardShown height : " + currentKeyboardHeight);
        if (null != mRootView) {
            isShowingKeyboard = true;
            if (mRootView.getLayoutParams() instanceof FrameLayout.LayoutParams) {
                FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) mRootView.getLayoutParams();
                lp.bottomMargin = currentKeyboardHeight;
                mRootView.setLayoutParams(lp);
            } else if (mRootView.getLayoutParams() instanceof RelativeLayout.LayoutParams) {
                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mRootView.getLayoutParams();
                lp.bottomMargin = currentKeyboardHeight;
                mRootView.setLayoutParams(lp);
            }
//            if (null != mButtonContainer && null != mHeaderView && null != mScrollView) {
//                int formMargin = (int) getResources().getDimension(R.dimen.reg_form_margin_top);
//                int appHeaderMargin = (int) getResources().getDimension(R.dimen.app_header_parent_margin_top);
//                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mScrollView.getLayoutParams();
//                lp.bottomMargin += (formMargin + mButtonContainer.getHeight());
//                lp.topMargin += (formMargin + appHeaderMargin + mHeaderView.getHeight());
//                Logger.printDebug("Top margin : "+lp.topMargin+" Buttom margin : " + lp.bottomMargin);
//                mScrollView.setLayoutParams(lp);
//            }
        }
    }

    @Override
    public void onKeyboardHidden() {
        Logger.printDebug("onKeyboardHidden");
        if (null != mRootView) {
            isShowingKeyboard = false;
            if (mRootView.getLayoutParams() instanceof FrameLayout.LayoutParams) {
                FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) mRootView.getLayoutParams();
                lp.bottomMargin = 0;
                mRootView.setLayoutParams(lp);
            } else if (mRootView.getLayoutParams() instanceof RelativeLayout.LayoutParams) {
                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mRootView.getLayoutParams();
                lp.bottomMargin = 0;
                mRootView.setLayoutParams(lp);
            }
//            if (null != mButtonContainer && null != mHeaderView && null != mScrollView) {
//                int formMargin = (int) getResources().getDimension(R.dimen.reg_form_margin_top);
//                int appHeaderMargin = (int) getResources().getDimension(R.dimen.app_header_parent_margin_top);
//                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mScrollView.getLayoutParams();
//                lp.bottomMargin -= (formMargin + mButtonContainer.getHeight());
//                lp.topMargin -= (formMargin + appHeaderMargin + mHeaderView.getHeight());
//                Logger.printDebug("Top margin : "+lp.topMargin+" Button margin : " + lp.bottomMargin);
//                mScrollView.setLayoutParams(lp);
//            }
        }
    }

    private void hideKeyboard() {
        if (null != mRootView && null != getContext()) {
            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(mRootView.getWindowToken(), 0);
        }
    }

    @Override
    public void onPositiveButtonClicked(int type) {
        doServerInteraction();
    }

    @Override
    public void onNegativeButtonClicked(int type) {
        mSubmitButton.setEnabled(true);
        mBackButton.setEnabled(true);
    }
}