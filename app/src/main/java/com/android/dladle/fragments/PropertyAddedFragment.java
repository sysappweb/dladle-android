package com.android.dladle.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.dladle.R;

/**
 * Created by sourav on 19-03-2017.
 */

public class PropertyAddedFragment extends BaseFragment {

    public static final String KEY_PROP_NAME = "prop_name";
    public static final String KEY_PROP_IMAGE_PATH = "prop_image_path";
    private ImageView mBackButton, mPropertyImage, mPropertyImageSmall;
    private Button mOk;
    private String mPropertyName, mBitMapFilePath;
    private TextView mConfirmationView;
    public PropertyAddedFragment() {}

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Bundle b = getArguments();
        if (null != b) {
            mPropertyName = b.getString(KEY_PROP_NAME, "");
            mBitMapFilePath = b.getString(KEY_PROP_IMAGE_PATH, "");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.confirmation_screen_final, container, false);
        mBackButton = (ImageView) view.findViewById(R.id.back_btn);
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != getActivity()) {
                    getActivity().finish();
                }
            }
        });
        mOk = (Button) view.findViewById(R.id.ok_button);
        mOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != getActivity()) {
                    getActivity().finish();
                }
            }
        });
        mConfirmationView = (TextView) view.findViewById(R.id.confirmation_text_summary);
        mConfirmationView.setText(getContext().getString(R.string.add_tenant , mPropertyName));
        mPropertyImageSmall = (ImageView) view.findViewById(R.id.property_pic_small);
        mPropertyImage = (ImageView) view.findViewById(R.id.property_pic);

        return view;
    }
}
