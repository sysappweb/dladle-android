package com.android.dladle.fragments;

import android.animation.AnimatorInflater;
import android.animation.ObjectAnimator;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;

import com.android.dladle.R;
import com.android.dladle.activities.AddImportantContactsActivity;
import com.android.dladle.activities.AddPropertyActivity;
import com.android.dladle.activities.NewTenantActivity;
import com.android.dladle.activities.PlaceAddedActivity;
import com.android.dladle.activities.PropertyOptionActivity;
import com.android.dladle.adapter.RecyclerAdpterForContact;
import com.android.dladle.customui.CustomProgressDialog;
import com.android.dladle.customui.MyJsonObjectRequest;
import com.android.dladle.entities.Property;
import com.android.dladle.utils.ImageUtils;
import com.android.dladle.utils.Logger;
import com.android.dladle.utils.NetworkConstants;
import com.android.dladle.utils.NetworkUtils;
import com.android.dladle.utils.PhotoManager;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.app.Activity.RESULT_OK;

/**
 * Created by sourav on 18-03-2017.
 */

public class AddPropertyFragment extends BaseFragment implements AddPropertyActivity.FragmentInteraction {

    public static final String KEY = "frag_key";
    public static final int KEY_FLAT = 100;
    public static final int KEY_TOWN_HOUSE = 200;
    public static final int KEY_HOUSE = 300;
    CustomProgressDialog mCustomProgressDialog;
    private int mKey;
    private EditText mEstateName, mAddress, mUnitNumber, mComplexName;
    private Button mBtnSubmit;
    private ImageView mMap;
    private View mViewEstatename;
    private String mAddressValue;
    private static final int MAX_ALLOWED_PHOTOS = 1;//Configurable
    private String[] mImageUrls = new String[MAX_ALLOWED_PHOTOS];
    private String mTempCameraPath;
    private RadioGroup mToggle;
    private boolean isCropSupported, isAddressWarningShown;
    private AlertDialog mAlertDialog;
    private LinearLayout mImageContainer;

    private View.OnClickListener mImageClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getTag() instanceof Integer) {
                showMessage("View index : " + ((Integer) v.getTag()).intValue());
                selectImage((ImageView) v, ((Integer) v.getTag()).intValue());
            }
        }
    };

    public AddPropertyFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        isCropSupported = context.getResources().getBoolean(R.bool.support_crop);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mKey = getArguments().getInt(KEY, KEY_FLAT);
        View view = inflater.inflate(getLayout(mKey), container, false);
        mRootView = view.findViewById(R.id.rootView);
        mAddress = (EditText) view.findViewById(R.id.street_number);
        mMap = (ImageView) view.findViewById(R.id.map);
        mViewEstatename = (View) view.findViewById(R.id.view_estate_name);
        mMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initMap();
            }
        });
        mImageContainer = (LinearLayout) view.findViewById(R.id.image_container);
        checkAndAddImageView();
        mUnitNumber = (EditText) view.findViewById(R.id.unit_number);
        mComplexName = (EditText) view.findViewById(R.id.complex_name);
        mEstateName = (EditText) view.findViewById(R.id.estate_name);
        mToggle = (RadioGroup) view.findViewById(R.id.toggle);
        mBtnSubmit = (Button) view.findViewById(R.id.submit_button);
        mCustomProgressDialog = new CustomProgressDialog(getActivity());
        performAction();
        return view;
    }

    private void performAction() {
        if (mToggle != null) {
            mToggle.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                    if (checkedId == R.id.yes) {
                        mEstateName.setVisibility(View.VISIBLE);
                        mViewEstatename.setVisibility(View.VISIBLE);
                    } else {
                        mEstateName.setVisibility(View.GONE);
                        mViewEstatename.setVisibility(View.GONE);
                    }
                }
            });
        }
    }

    private void checkAndAddImageView() {
        int noOfViews = mImageContainer.getChildCount();
        int count = 0;
        for (String s : mImageUrls) {
            if (!TextUtils.isEmpty(s)) {
                count++;
            }
        }
        //Logger.printInfo("SRVRT count : "+count+" noOfViews : "+noOfViews);
        if (noOfViews < MAX_ALLOWED_PHOTOS && (noOfViews - count <= 0)) {
            int index = -1;
            //Logger.printInfo("SRVRT ADD {"+mImageUrls[0]+","+mImageUrls[1]+","+mImageUrls[2]+","+mImageUrls[3]+","+mImageUrls[4]+"}");
            for (int i = 0; i < MAX_ALLOWED_PHOTOS; i++) {
                if (TextUtils.isEmpty(mImageUrls[i]) && (null == getImageViewFromTag(i))) {
                    index = i;
                    break;
                }
            }
            if (index > -1) {
                ImageView iv = new ImageView(getContext());
                iv.setBackgroundResource(R.drawable.default_property_pic);
                iv.setTag(new Integer(index));
                iv.setOnClickListener(mImageClickListener);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                lp.setMarginStart(6);//2dp
                lp.setMarginEnd(6);//2dp
                iv.setLayoutParams(lp);
                mImageContainer.addView(iv);
                mImageUrls[index] = "";
            }
        }
        //Logger.printInfo("SRVRT ADDED {"+mImageUrls[0]+","+mImageUrls[1]+","+mImageUrls[2]+","+mImageUrls[3]+","+mImageUrls[4]+"}");
    }

    private void removeImageView(ImageView iv, int index) {
        int prevCount = mImageContainer.getChildCount();
        iv.setVisibility(View.GONE);
        mImageContainer.removeView(iv);
        int count = mImageContainer.getChildCount();
        mImageContainer.forceLayout();
        //showMessage("Prev : "+prevCount+" count : "+count);
        if (count == 0 || prevCount == 5) {
            checkAndAddImageView();
        }
        //Logger.printInfo("SRVRT REMOVED {"+mImageUrls[0]+","+mImageUrls[1]+","+mImageUrls[2]+","+mImageUrls[3]+","+mImageUrls[4]+"}");
    }

    private ImageView getImageViewFromTag(int index) {
        ImageView iv = null;
        int count = mImageContainer.getChildCount();
        for (int i = 0; i < count; i++) {
            View v = mImageContainer.getChildAt(i);
            if (index == ((Integer) v.getTag()).intValue()) {
                iv = (ImageView) v;
                break;
            }
        }
        return iv;
    }

    private int getLayout(int key) {
        if (key == KEY_HOUSE) {
            return R.layout.add_house;
        } else {
            return R.layout.add_flat_or_town_house;
        }
    }

    private void initMap() {
        if (checkPermission(300)) {
            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
            try {
                startActivityForResult(builder.build(getActivity()), 400);
            } catch (GooglePlayServicesRepairableException e) {
                e.printStackTrace();
            } catch (GooglePlayServicesNotAvailableException e) {
                e.printStackTrace();
            }
        }
    }

    private void selectImage(final ImageView imageView, final int which) {
        dismissDialog();
        final CharSequence[] items = {"Take Photo", "Choose from Gallery", "Remove Picture", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    if (checkPermission(100 + which)) {
                        try {
                            Uri photoUri = PhotoManager.getPropertyPhotoURI(getContext());
                            if (null != photoUri) {
                                mTempCameraPath = PhotoManager.getPropertyImagePathFromUri(photoUri, getContext());
                                Intent intent = PhotoManager.getCameraIntentForThumbnail(photoUri);
                                startActivityForResult(intent, 100 + which);
                            }
                        } catch (ActivityNotFoundException ex) {
                            Logger.printError(ex.getMessage());
                        }
                    } else {
                        showMessage("Please enable permission for Camera");
                    }
                } else if (items[item].equals("Choose from Gallery")) {
                    if (checkPermission(200 + which)) {
                        try {
                            startActivityForResult(PhotoManager.galleryIntent(getContext()), 200 + which);
                        } catch (ActivityNotFoundException ex) {
                            Logger.printError(ex.getMessage());
                        }
                    } else {
                        showMessage("Please enable permission for Gallery");
                    }
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                } else if (items[item].equals("Remove Picture")) {
                    int index = ((Integer) imageView.getTag()).intValue();
                    String url = mImageUrls[index];
                    //showMessage("Remove : "+url);
                    if (!TextUtils.isEmpty(url)) {
                        mImageUrls[index] = "";
                        removeImageView(imageView, index);
                    }
                }
            }
        });
        mAlertDialog = builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode >= 100 && requestCode < 200) { // CAMERA
                try {
                    Intent i = PhotoManager.getCroppedProfileThumbMailIntent(getContext());
                    int index = requestCode - 100;
                    if (mTempCameraPath != null) {
                        if (i != null && isCropSupported && index >= 0) {
                            startActivityForResult(i, 300 + index);
                        } else {
                            if (index >= 0) {
                                ImageView imageView = getImageViewFromTag(index);
                                String prev = mImageUrls[index];
                                mImageUrls[index] = mTempCameraPath;
                                //Logger.printInfo("Gallery size :"+mImageUrls.length);
                                PhotoManager.setPicture(getContext(), imageView, mTempCameraPath);
                                mTempCameraPath = null;
                                //Logger.printInfo("SRVRT CAMERA {"+mImageUrls[0]+","+mImageUrls[1]+","+mImageUrls[2]+","+mImageUrls[3]+","+mImageUrls[4]+"}");
                                if (TextUtils.isEmpty(prev)) {
                                    checkAndAddImageView();
                                }
                            }
                        }
                    }
                } catch (ActivityNotFoundException ex) {
                    Logger.printError(ex.getMessage());
                }
            } else if (requestCode >= 200 && requestCode < 300) { //GALLERY
                try {
                    Intent i = PhotoManager.getCroppedProfileThumbMailIntent(getContext());
                    int index = requestCode - 200;
                    if (i != null && isCropSupported && index >= 0) {
                        startActivityForResult(i, 300 + index);
                    } else {
                        if (index >= 0) {
                            ImageView imageView = getImageViewFromTag(index);
                            String path = PhotoManager.updateAndCachePropertyPic(getContext(), imageView, data.getData());
                            String prev = mImageUrls[index];
                            mImageUrls[index] = path;
                            //Logger.printInfo("SRVRT GALLERY {"+mImageUrls[0]+","+mImageUrls[1]+","+mImageUrls[2]+","+mImageUrls[3]+","+mImageUrls[4]+"}");
                            if (TextUtils.isEmpty(prev)) {
                                checkAndAddImageView();
                            }
                        }
                    }
                } catch (ActivityNotFoundException ex) {
                    Logger.printError(ex.getMessage());
                }
            } else if (requestCode >= 300 && requestCode < 400) { //Crop
                int index = requestCode - 300;
                if (index >= 0) {
                    ImageView imageView = getImageViewFromTag(index);
                    PhotoManager.setPicture(getContext(), imageView, mImageUrls[index]);//To Do
                }
            } else if (requestCode == 400) { // MAP
                if (resultCode == RESULT_OK) {

                    Place place = PlacePicker.getPlace(getContext(), data);
                    mAddress.setText(place.getAddress());
                    mAddress.setError(null);
                    mAddressValue = (new StringBuilder().append(place.getLatLng().latitude).append(";").append(place.getLatLng().longitude)).toString();
                }
            }
        }
    }

    private void dismissDialog() {
        if (null != mAlertDialog && mAlertDialog.isShowing()) {
            mAlertDialog.dismiss();
        }
    }

    private boolean checkPermission(final int type) {
        if (getActivity() != null && !getActivity().isFinishing()) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                return true;
            }
            ArrayList<String> arr = new ArrayList<String>();
            ArrayList<String> arrFinal = new ArrayList<String>();
            if (200 <= type && 300 > type) { // Gallery
                arr.add(READ_EXTERNAL_STORAGE);
            } else if (100 <= type || 200 > type) { // Camera
                arr.add(CAMERA);
            } else if (300 == type) { // Location
                arr.add(ACCESS_FINE_LOCATION);
            } else { // ALL
                arr.add(CAMERA);
                arr.add(READ_EXTERNAL_STORAGE);
                arr.add(ACCESS_FINE_LOCATION);
            }
            arrFinal.addAll(arr);
            for (String s : arr) {
                if (getActivity().checkSelfPermission(s) == PackageManager.PERMISSION_GRANTED) {
                    arrFinal.remove(s);
                }
            }
            if (arrFinal.size() == 0) {
                return true;
            }
            String[] a = arrFinal.toArray(new String[arrFinal.size()]);
            requestPermissions(a, type);
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode >= 100 || requestCode < 200) {
            if (grantResults.length >= 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                try {
                    int index = requestCode - 100;
                    if (index >= 0) {
                        Uri photoUri = PhotoManager.getPropertyPhotoURI(getContext());
                        mTempCameraPath = PhotoManager.getPropertyImagePathFromUri(photoUri, getContext());
                        if (null != photoUri) {
                            Intent intent = PhotoManager.getCameraIntentForThumbnail(photoUri);
                            startActivityForResult(intent, requestCode);
                        }
                    }
                } catch (ActivityNotFoundException ex) {
                    Logger.printError(ex.getMessage());
                }
            }
        } else if (requestCode >= 200 || requestCode < 300) {
            if (grantResults.length >= 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                try {
                    startActivityForResult(PhotoManager.galleryIntent(getContext()), requestCode);
                } catch (ActivityNotFoundException ex) {
                    Logger.printError(ex.getMessage());
                }
            }
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public boolean doFragmentInteraction(Property p) {
        return addPropertyDetails(p);
    }

    private void showAddressWarning(final Property p) {
        if (isAddressWarningShown) {
            return;
        }
        dismissDialog();
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        alertDialogBuilder.setMessage("This address may not be accurate as this is not picked from Maps. Do you want to continue?");
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                isAddressWarningShown = true;
                addPropertyDetails(p);
            }
        });

        alertDialogBuilder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                isAddressWarningShown = false;
                mAddress.setError("Please select address from the maps");
                mAddress.requestFocus();
            }
        });

        mAlertDialog = alertDialogBuilder.create();
        mAlertDialog.show();
    }

    private boolean addPropertyDetails(Property p) {
        p.mPropertyType = (mKey == 100 ? "FLAT" : (mKey == 200 ? "TOWN_HOUSE" : "HOUSE"));
        if (mKey == KEY_HOUSE) {
            p.mIsEstate = true;
            p.mIsEstate = (mToggle.getCheckedRadioButtonId() == R.id.yes);
            if (p.mIsEstate) {
                String estateName = mEstateName.getText().toString();
                if (TextUtils.isEmpty(estateName)) {
                    mEstateName.setError("Complex name can not be empty");
                    mEstateName.requestFocus();
                    return false;
                } else {
                    p.mEstateName = estateName;
                }
            }

        } else {
            p.mIsEstate = false;
            String unitNumber = mUnitNumber.getText().toString();
            if (TextUtils.isEmpty(unitNumber)) {
                mUnitNumber.setError("Unit number can not be empty");
                mUnitNumber.requestFocus();
                return false;
            } else {
                p.mUnitNumber = unitNumber;
            }

            String complexName = mComplexName.getText().toString();
            if (TextUtils.isEmpty(complexName)) {
                mComplexName.setError("Complex name can not be empty");
                mComplexName.requestFocus();
                return false;
            } else {
                p.mComplexName = complexName;
            }
        }
        if (TextUtils.isEmpty(mAddressValue)) {
            String address = mAddress.getText().toString();
            if (TextUtils.isEmpty(address)) {
                mAddress.setError("Address can not be empty");
                mAddress.requestFocus();
                return false;
            } else {
                showAddressWarning(p);
                if (!isAddressWarningShown) {
                    return isAddressWarningShown;
                }
            }
        } else {
            p.mAddress = mAddressValue;
        }
        p.mAddress = mAddress.getText().toString();
        if (mKey == KEY_HOUSE) {
            p.mEstateName = mEstateName.getText().toString();
            p.mComplexName = "";
        } else {
            p.mEstateName = "";
        }

        if (mImageUrls.length > 0) {
            int size = mImageUrls.length;
            p.mPropertyImage = mImageUrls[0];
            for (int i = 0; i < size; i++) {
                String s = mImageUrls[i];
                if (!TextUtils.isEmpty(s)) {
                    p.mImagePath.add(s);
                }
            }
        } else {
            showMessage("Please upload picture of the property");
            return false;
        }
        addPlace(p);
        return true;
    }

    private void addPlace(final Property p) {
        final JSONObject input = new JSONObject();
        try {

            if (p.mIsEstate) {
                String line = p.mAddress;
                Pattern pattern = Pattern.compile("\\p{L}");
                Matcher m = pattern.matcher(line);
                if (m.find()) {
                    int position = m.start();
                    String estateNumber = line.substring(0, position);
                    if (estateNumber.trim().equals("")) {
                        mAddress.setError("Please add unit number before address");
                        mAddress.requestFocus();
                        return;
                    }
                    input.put("estateName", estateNumber);
                    input.put("address", line.substring(position));
                }
            } else {
                input.put("estateName", p.mEstateName);
                input.put("address", p.mAddress);
            }
            input.put("complexName", p.mComplexName);
            input.put("inEstate", p.mIsEstate);
            input.put("placeType", p.mPropertyType);
            input.put("unitNo", p.mUnitNumber);
            Location location= NetworkUtils.getLocationFromAddress(getActivity(),p.mAddress);
            input.put("addressLatitude",String.valueOf(location.getLatitude()));
            input.put("addressLongitude",String.valueOf(location.getLongitude()));






            input.put("placeImage", "");
            Logger.printDebug(input.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        showProgress(true);
        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.POST, NetworkConstants.AddPropertyLandLord.ADD_PLACE, input,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Logger.printDebug("" + response);
                        if (response.optString("Status").equalsIgnoreCase(NetworkConstants.SUCCESS)) {
                            showProgress(false);
                            if (p.mPropertyImage != null && p.mPropertyImage.length() > 0) {
                                JSONObject jsonObject = response.optJSONObject("Data");
                                int propertyId = jsonObject.optInt("propertyId");
                                uploadImage(ImageUtils.readFileAsBase64String(p.mPropertyImage), propertyId);
                            }

                            Intent intent = new Intent(getActivity(), PlaceAddedActivity.class);
                            intent.putExtra("placeName", p.mComplexName);
                            intent.putExtra("placeImage", p.mPropertyImage);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        } else {
                            showProgress(false);
                            showMessage(response.optString("Message"));

                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof TimeoutError) {
                            showMessage(getString(R.string.server_error));
                        } else {
                            showMessage(error.getMessage());
                        }
                        showProgress(false);
                        Logger.printError("Some error : " + error.toString());
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        jsonRequest.setTag(NetworkConstants.Login.TAG);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(NetworkConstants.TIMOUT_TIME,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonRequest);
    }

    private void uploadImage(String image, int propertyId) {
        JSONObject input = new JSONObject();
        try {
            input.put("base64Image", image);
            input.put("propertyId", propertyId);
            Logger.printDebug("" + input);
            MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.POST, NetworkConstants.AddPropertyLandLord.UPLOAD_IAMGE, input,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Logger.printDebug("" + response);


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error instanceof TimeoutError) {
                                showMessage(getString(R.string.server_error));
                            } else {
                                showMessage(error.getMessage());
                            }
                            showProgress(false);
                            Logger.printError("Some error : " + error.toString());
                        }
                    });
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            jsonRequest.setTag(NetworkConstants.Login.TAG);
            requestQueue.add(jsonRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void showProgress(final boolean show) {
        if (show) {
            mCustomProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            mCustomProgressDialog.setCancelable(false);
            mCustomProgressDialog.show();
        } else {
            mCustomProgressDialog.dismiss();
        }
    }
}
