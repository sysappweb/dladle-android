package com.android.dladle.fragments;

import android.animation.AnimatorInflater;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.android.dladle.R;
import com.android.dladle.activities.BaseActivity;
import com.android.dladle.activities.UserProfileActivity;
import com.android.dladle.activities.VendorAccountSetUpActivty;
import com.android.dladle.activities.VendorProfileUpdatedActivity;
import com.android.dladle.customui.MyJsonObjectRequest;
import com.android.dladle.customui.RoundedImageView;
import com.android.dladle.customui.TextViewUtils;
import com.android.dladle.utils.CommonConstants;
import com.android.dladle.utils.Logger;
import com.android.dladle.utils.NetworkConstants;
import com.android.dladle.utils.NetworkUtils;
import com.android.dladle.utils.PhotoManager;
import com.android.dladle.utils.PreferenceManager;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.app.Activity.RESULT_OK;

/**
 * Created by sourav on 28-02-2017.
 */

public class ProfileEditFragmentVendor extends BaseFragment implements UserProfileActivity.FragmentInteraction {

    private String mFragmentKey, mCellNumber, mExistingAddress;
    private Spinner mVendorType, mExperience;
    private EditText mCellNo, mAddress;
    private ImageView mEditButton, mProfilePicOverlay, mCellNoHelp, mMapView;
    private RoundedImageView mProfilePic;
    private RadioGroup mTools, mTransport;
    private View mButtonLine, mProfilePhotoContainer, mScrollView;
    private Button mSave, mDelete;
    private AlertDialog mAlertDialog;
    private boolean isCropSupported = true, mHasTools, mHasTransport, mUpdateInProgress;
    private float mRating;
    private int mInitVendorTypeValue, mInitYearsOfExp;
    private ProgressBar mProgressView;
    private String mAddressValue;
    ArrayList<String> vendorExpKey = new ArrayList<>();
    ArrayList<String> vendorExpValue = new ArrayList<>();
    ArrayList<String> vendorTypeKey = new ArrayList<>();
    ArrayList<String> vendorTypeValue = new ArrayList<>();
    public ProfileEditFragmentVendor() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (null != activity) {
            Intent i = activity.getIntent();
            if (null != i) {
                mFragmentKey = i.getStringExtra(NetworkConstants.JsonResponse.DATA);
                mInitVendorTypeValue = i.getIntExtra(NetworkConstants.UpdateVendor.SERVICE_TYPE, 0);
                mInitYearsOfExp = i.getIntExtra(NetworkConstants.UpdateVendor.EXPERIENCE, 0);
                mCellNumber = i.getStringExtra(NetworkConstants.Registration.CELL_NUMBER);
                if (null == mCellNumber) {
                    mCellNumber = "";
                }
                mExistingAddress = i.getStringExtra(NetworkConstants.UpdateVendor.BUSINESS_ADDR);
                if (null == mExistingAddress) {
                    mExistingAddress = "";
                }
                mHasTools = i.getBooleanExtra(NetworkConstants.UpdateVendor.HAS_TOOL, false);
                mHasTransport = i.getBooleanExtra(NetworkConstants.UpdateVendor.HAS_TRANSPORT, false);
                isCropSupported = activity.getResources().getBoolean(R.bool.support_crop);
            }
            isCropSupported = activity.getResources().getBoolean(R.bool.support_crop);
        }
        vendorExpKey.add("Select Experience");
        vendorTypeKey.add("Select Type");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.vendor_profile_edit_fragment, container, false);
        updateHeaderMargin(view);
        mButtonLine = view.findViewById(R.id.button_divider);
        mProfilePic = (RoundedImageView) view.findViewById(R.id.profile_pic);
        mEditButton = (ImageView) view.findViewById(R.id.small_tick);
        mEditButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });
        mProfilePicOverlay = (ImageView) view.findViewById(R.id.profile_pic_overlay);
        mVendorType = (Spinner) view.findViewById(R.id.vendor_type);
        mExperience = (Spinner) view.findViewById(R.id.experience);
        mSave = (Button) view.findViewById(R.id.button_save);
        mDelete = (Button) view.findViewById(R.id.button_delete);
        mSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initSave();
            }
        });
        mDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete();
            }
        });
        mCellNo = (EditText) view.findViewById(R.id.cell_number);
        mCellNoHelp = (ImageView) view.findViewById(R.id.help);
        mCellNoHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMessage("Enter your cell phone number without country code.");
            }
        });
        mMapView = (ImageView) view.findViewById(R.id.map);
        mMapView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initMap();
            }
        });
        mAddress = (EditText) view.findViewById(R.id.address);
        mTools = (RadioGroup) (view.findViewById(R.id.tools_container).findViewById(R.id.toggle));
        mTools.check(mHasTools ? R.id.yes : R.id.no);
        mTransport = (RadioGroup) (view.findViewById(R.id.transport_container).findViewById(R.id.toggle));
        mTransport.check(mHasTransport ? R.id.yes : R.id.no);
        mProgressView = (ProgressBar) view.findViewById(R.id.progress_bar);
        mProfilePhotoContainer = view.findViewById(R.id.profile_photo);
        mScrollView = view.findViewById(R.id.edit_form);
        setUserType();
        new GetDetailsTask(getContext(), PreferenceManager.getInstance(getContext()).getUserName(), mProgressView).execute();
        checkPermission(1);
        return view;
    }

    private void initMap() {
        if (checkPermission(300)) {
            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
            try {
                startActivityForResult(builder.build(getActivity()), 400);
            } catch (GooglePlayServicesRepairableException e) {
                e.printStackTrace();
            } catch (GooglePlayServicesNotAvailableException e) {
                e.printStackTrace();
            }
        }
    }

    private void selectImage() {
        dismissDialog();
        final CharSequence[] items = {"Take Photo", "Choose from Gallery", "Remove Picture", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    if (checkPermission(100)) {
                        try {
                            Uri photoUri = PhotoManager.getProfilePhotoURI(getContext());
                            if (null != photoUri) {
                                Intent intent = PhotoManager.getCameraIntentForThumbnail(photoUri);
                                startActivityForResult(intent, 100);
                            }
                        } catch (ActivityNotFoundException ex) {
                            Logger.printError(ex.getMessage());
                        }
                    } else {
                        showMessage("Please enable permission for Camera");
                    }
                } else if (items[item].equals("Choose from Gallery")) {
                    if (checkPermission(200)) {
                        try {
                            startActivityForResult(PhotoManager.galleryIntent(getContext()), 200);
                        } catch (ActivityNotFoundException ex) {
                            Logger.printError(ex.getMessage());
                        }
                    } else {
                        showMessage("Please enable permission for Gallery");
                    }
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                } else if (items[item].equals("Remove Picture")) {
                    PhotoManager.removeProfilePic(getContext());
                    PhotoManager.setProfilePic(getContext(), mProfilePic);
                }
            }
        });
        mAlertDialog = builder.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == 100) {
            if (grantResults.length >= 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                try {
                    Uri photoUri = PhotoManager.getProfilePhotoURI(getContext());
                    if (null != photoUri) {
                        Intent intent = PhotoManager.getCameraIntentForThumbnail(photoUri);
                        startActivityForResult(intent, 100);
                    }
                } catch (ActivityNotFoundException ex) {
                    Logger.printError(ex.getMessage());
                }
            }
        } else if (requestCode == 200) {
            if (grantResults.length >= 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                try {
                    startActivityForResult(PhotoManager.galleryIntent(getContext()), 200);
                } catch (ActivityNotFoundException ex) {
                    Logger.printError(ex.getMessage());
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 100) { // Gallery
                try {
                    Intent i = PhotoManager.getCroppedProfileThumbMailIntent(getContext());
                    if (i != null && isCropSupported) {
                        startActivityForResult(i, 300);
                    } else {
                        //showMessage("Could not load picture from Gallery");
                        PhotoManager.setProfilePic(getContext(), mProfilePic);
                    }
                } catch (ActivityNotFoundException ex) {
                    Logger.printError(ex.getMessage());
                } finally {
                    PhotoManager.setProfilePic(getContext(), mProfilePic);
                }
            } else if (requestCode == 200) { //Camera
                try {
                    Intent i = PhotoManager.getCroppedProfileThumbMailIntent(getContext());
                    if (i != null && isCropSupported) {
                        startActivityForResult(i, 300);
                    } else {
                        //showMessage("Could not load picture from Gallery");
                        PhotoManager.updateAndCacheProfilePic(getContext(), mProfilePic, data.getData());
                    }
                } catch (ActivityNotFoundException ex) {
                    Logger.printError(ex.getMessage());
                } finally {
                    PhotoManager.updateAndCacheProfilePic(getContext(), mProfilePic, data.getData());
                }
            } else if (requestCode == 300) { //Crop
                PhotoManager.setProfilePic(getContext(), mProfilePic);
            } else if (requestCode == 400) { // MAP
                if (resultCode == RESULT_OK) {
                    Place place = PlacePicker.getPlace(getContext(), data);
                    //showMessage("Place is : "+place.getAddress());
                    mAddress.setText(place.getAddress());
                    mAddressValue = (new StringBuilder().append(place.getLatLng().latitude).append(";").append(place.getLatLng().longitude)).toString();
                }
            }
        }
    }

    private void initSave() {
        hindKeyBoardAndDoUpdate();
    }

    private void delete() {
        dismissDialog();
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        alertDialogBuilder.setMessage(R.string.delete_account_msg);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                showMessage("To be implemented");
            }
        });

        alertDialogBuilder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Do Nothing
            }
        });

        mAlertDialog = alertDialogBuilder.create();
        mAlertDialog.show();
    }

    private void dismissDialog() {
        if (null != mAlertDialog && mAlertDialog.isShowing()) {
            mAlertDialog.dismiss();
        }
    }

    private void updateHeaderMargin(View view) {
        View v = view.findViewById(R.id.parent_view);
        Logger.printInfo(v.getLayoutParams().toString());
        if (v.getLayoutParams() instanceof FrameLayout.LayoutParams) {
            FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) v.getLayoutParams();
            Logger.printInfo("Height b4 : " + lp.topMargin);
            lp.topMargin = lp.topMargin + (int) getResources().getDimension(R.dimen.actionBar_Size);
            Logger.printInfo("Height : " + lp.topMargin);
            v.setLayoutParams(lp);
        }
    }

    private void setUserType() {
        if (CommonConstants.FRAGMENT_KEY_VENDOR.equalsIgnoreCase(mFragmentKey)) {
            mButtonLine.setBackground(getResources().getDrawable(R.mipmap.line_orange));
            mProfilePicOverlay.setImageDrawable(getResources().getDrawable(R.mipmap.ic_orange_overlay));
        } else {
            Logger.printInfo("Invalid user type in this flow");
        }
    }

    private void setValues(Vendor v) {
        if (null != v) {
            PhotoManager.setProfilePic(getContext(), mProfilePic);
            mEditButton.setVisibility(View.VISIBLE);
            mEditButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_edit_button));
           getVendorType();
            mExistingAddress = v.mAddress;
            mAddress.setText(v.mAddress);
            mCellNumber = v.mCellNo;
            mCellNo.setText(v.mCellNo);
            mHasTools = v.mHasTool;
            mTools.check(v.mHasTool ? R.id.yes : R.id.no);
            mHasTransport = v.mHasTransport;
            mTransport.check(v.mHasTransport ? R.id.yes : R.id.no);
            setWatchers();
        }
    }

    private void setWatchers() {
        mCellNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                notifyActivityOnEdit();
            }
        });
        mAddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                notifyActivityOnEdit();
            }
        });

        mExperience.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                notifyActivityOnEdit();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        mVendorType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                notifyActivityOnEdit();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        mTools.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                notifyActivityOnEdit();
            }
        });

        mTransport.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                notifyActivityOnEdit();
            }
        });
    }

    private void notifyActivityOnEdit() {
        UserProfileActivity activity = (UserProfileActivity) getActivity();
        if (null != activity && !activity.isFinishing()) {
            boolean hasChanged = false;
            hasChanged = !TextUtils.equals(mAddress.getText(), mExistingAddress)
                    || !TextUtils.equals(mCellNo.getText(), mCellNumber)
                    || (mExperience.getSelectedItemPosition() != mInitYearsOfExp)
                    || (mVendorType.getSelectedItemPosition() != mInitVendorTypeValue)
                    || (mTools.getCheckedRadioButtonId() != (mHasTools ? R.id.yes : R.id.no))
                    || (mTransport.getCheckedRadioButtonId() != (mHasTransport ? R.id.yes : R.id.no));
            activity.notifyOnEdit(hasChanged);
        }
    }

    private boolean checkPermission(final int type) {
        if (getActivity() != null && !getActivity().isFinishing()) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                return true;
            }
            ArrayList<String> arr = new ArrayList<String>();
            ArrayList<String> arrFinal = new ArrayList<String>();
            if (200 == type) {
                arr.add(READ_EXTERNAL_STORAGE);
            } else if (100 == type) {
                arr.add(CAMERA);
            } else if (300 == type) {
                arr.add(ACCESS_FINE_LOCATION);
            } else {
                arr.add(CAMERA);
                arr.add(READ_EXTERNAL_STORAGE);
                arr.add(ACCESS_FINE_LOCATION);
            }
            arrFinal.addAll(arr);
            for (String s : arr) {
                if (getActivity().checkSelfPermission(s) == PackageManager.PERMISSION_GRANTED) {
                    arrFinal.remove(s);
                }
            }
            if (arrFinal.size() == 0) {
                return true;
            }
            String[] a = arrFinal.toArray(new String[arrFinal.size()]);
            requestPermissions(a, type);
        }
        return false;
    }

    private void hindKeyBoardAndDoUpdate() {
        final UserProfileActivity activity = (UserProfileActivity) getActivity();
        if (null != activity && !activity.isFinishing()) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mDelete.setEnabled(false);
                    mSave.setEnabled(false);
                    doUpdate();
                }
            }, activity.isShowingKeyBoard ? CommonConstants.HIDE_KEYBOARD_DELAY : 0);
            if (activity.isShowingKeyBoard) {
                activity.hideKeyboard();
            }
        }
    }

    private void doUpdate() {
        if (mUpdateInProgress || getContext() == null) {
            return;
        }

        if (!NetworkUtils.isNetworkAvailable(getContext())) {
            Logger.printInfo("Not connected to internet");
            showMessage(getString(R.string.no_internet));
            mSave.setEnabled(true);
            mDelete.setEnabled(true);
            return;
        }

        // Reset errors.
        mCellNo.setError(null);
        mAddress.setError(null);

        // Store values at the time of the registration attempt.
        String cellNumber = TextViewUtils.removeEndSpaces(mCellNo.getText().toString());
        String address = mAddress.getText().toString();
        String vendorType = getVendorType(mVendorType.getSelectedItemPosition());
        String experience = getExperienceType(mExperience.getSelectedItemPosition());
        boolean hasTools = mTools.getCheckedRadioButtonId() == R.id.yes;
        boolean hasTransport = mTransport.getCheckedRadioButtonId() == R.id.yes;

        boolean cancel = false;
        View focusView = null;

        // Check if id number is valid or not.
        if (TextUtils.isEmpty(cellNumber) || !TextUtils.isDigitsOnly(cellNumber)
                || cellNumber.length() < 10) {
            mCellNo.setError(getString(R.string.invalid_cell_number));
            focusView = mCellNo;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(mAddressValue)) {
            if (TextUtils.isEmpty(address)) {
                mAddress.setError(getString(R.string.error_field_required));
                focusView = mAddress;
                cancel = true;
            } else if (mAddress.length() > 50) {
                showMessage("This may not be the exact location, Please use address picker from Maps");
                focusView = mAddress;
                cancel = true;
            }
        } else {
            address = mAddressValue;
        }


        // Check for last name.
        if (TextUtils.isEmpty(experience)) {
            focusView = mExperience;
            showMessage("Select Years of Experience");
            cancel = true;
        }


        // Check for first name
        if (TextUtils.isEmpty(vendorType)) {
            focusView = mVendorType;
            showMessage("Select your work type.");
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            if (null != focusView) {
                focusView.requestFocus();
            }
            mSave.setEnabled(true);
            mDelete.setEnabled(true);
        } else {
            mUpdateInProgress = true;
            doServerInteraction(address, cellNumber, experience,
                    vendorType, hasTools, hasTransport);
        }
    }



    private void doServerInteraction(String address, String cellNum, String experience,
                                     String serviceType, boolean hasTools, boolean hasTransport) {
        Logger.printDebug("Server interaction started");
        // Show a progress spinner
        showProgress(true);
        JSONObject request = new JSONObject();
        try {
            request.put(NetworkConstants.UpdateVendor.BUSINESS_ADDR, address);
            request.put(NetworkConstants.UpdateVendor.CELL_NUM, cellNum);
            request.put(NetworkConstants.UpdateVendor.EXPERIENCE, experience);
            request.put(NetworkConstants.UpdateVendor.HAS_TOOL, hasTools);
            request.put(NetworkConstants.UpdateVendor.HAS_TRANSPORT, hasTransport);
            request.put(NetworkConstants.UpdateVendor.SERVICE_TYPE, serviceType);
            Logger.printDebug("JSON : " + request);
        } catch (Exception e) {
            showProgress(false);
            e.printStackTrace();
        }
        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.POST, NetworkConstants.UpdateVendor.URL, request,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Logger.printDebug("" + response);
                        boolean isUpdated = PreferenceManager.getInstance(getContext()).isProfileUpdated();
                        try {
                            String status = response.getString(NetworkConstants.JsonResponse.STATUS);
                            String message = response.getString(NetworkConstants.JsonResponse.MESSAGE);
                            if (NetworkConstants.JsonResponse.STATUS_SUCCESS.equalsIgnoreCase(status)) {
                                showMessage("Success");
                                PreferenceManager.getInstance(getContext()).setFirstTimeLaunch();
                                mProgressView.setVisibility(View.GONE);
                                PreferenceManager.getInstance(getContext()).setProfileUpdateStatus(true);
                                UserProfileActivity activity = (UserProfileActivity) getActivity();
                                if (null != activity) {
                                    activity.notifyOnEdit(false);
                                    activity.handleLeftButtonClick();
                                    if (!isUpdated) {
                                        Intent i = new Intent(getActivity(), VendorProfileUpdatedActivity.class);
                                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(i);
                                    }
                                }
                            } else {
                                showProgress(false);
                                showMessage(status + " : " + message);
                            }
                        } catch (JSONException e) {
                            Logger.printError("Unable to parse JSON : " + e.getMessage());
                            showProgress(false);
                            showMessage(getString(R.string.unexpected_error));
                        } catch (Exception ex) {
                            // Do Nothing -- Temp fix for server error
                            mProgressView.setVisibility(View.GONE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showProgress(false);
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(NetworkConstants.TIMOUT_TIME,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        jsonRequest.setTag(NetworkConstants.UpdateVendor.TAG);
        requestQueue.add(jsonRequest);
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    private void showProgress(final boolean show) {
        if (getActivity() instanceof BaseActivity) {
            ((BaseActivity) getActivity()).setActionInProgress(show);
        }
        mUpdateInProgress = show;

        mScrollView.setVisibility(show ? View.GONE : View.VISIBLE);
        mDelete.setEnabled(true);
        mSave.setEnabled(true);

        ObjectAnimator progressAnimator = (ObjectAnimator) AnimatorInflater.loadAnimator(getContext(), R.animator.flipping);
        progressAnimator.setTarget(mProgressView);
        progressAnimator.start();
    }

    @Override
    public void doFragmentInteraction() {
        // This is not needed currently. Can be used in future
        initSave();
    }

    private class GetDetailsTask extends AsyncTask<Void, Void, Vendor> {

        private String mEmail;
        private Context mContext;
        private ProgressBar mProgressBar;

        public GetDetailsTask(Context ctx, String aEmail, ProgressBar pb) {
            mEmail = aEmail;
            mContext = ctx;
            mProgressBar = pb;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (null != mProgressBar) {
                mProgressBar.setVisibility(View.VISIBLE);
            }
        }

        @Override
        protected Vendor doInBackground(Void... params) {
            if (null != mContext) {
                String address = "";
                try {
                    Geocoder geocoder;
                    Address a;
                    geocoder = new Geocoder(mContext, Locale.getDefault());
                    a = geocoder.getFromLocation(12.962052500000008, 77.70214453124996, 1).get(0);
                    address = a.getAddressLine(0) + a.getLocality() + a.getAdminArea() + a.getCountryName() + a.getPostalCode();
                } catch (IOException ex) {
                    Logger.printInfo("Unable to get address details");
                }
                return new Vendor(address, false, false, "", "", mCellNumber, "");
            }
            return null;
        }

        @Override
        protected void onPostExecute(Vendor result) {
            if (null != mProgressBar) {
                mProgressBar.setVisibility(View.GONE);
            }
            setValues(result);
            super.onPostExecute(result);
        }
    }

    public class Vendor {
        String mAddress;
        boolean mHasTool;
        boolean mHasTransport;
        String mUserType;
        String mExperience;
        String mCellNo;
        String mRating;

        public Vendor (String address, boolean hasTools, boolean hasTransport,
                       String userType, String experience, String cellNo, String rating) {
            this.mAddress = address;
            this.mExperience = experience;
            this.mHasTool = hasTools;
            this.mHasTransport = hasTransport;
            this.mUserType = userType;
            this.mCellNo = cellNo;
            this.mRating = rating;
        }
    }
    private void getVendorExp() {
        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.GET, NetworkConstants.Select.GET_VENDOR_EXP, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Logger.printDebug("" + response);
                        showProgress(false);
                        try {
                            if (response.optString("Status").equalsIgnoreCase("success")) {
                                JSONObject data = response.optJSONObject("Data");
                                Iterator<String> iterator = data.keys();
                                while (iterator.hasNext()) {
                                    String key = iterator.next();
                                    vendorExpKey.add(key);
                                    vendorExpValue.add(data.optString(key));
                                    // Log.i("TAG","key:"+key +"--Value::"+categoryJSONObj.optString(key);
                                }
                                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                                        R.layout.spinner_item, vendorExpKey);
                                mExperience.setAdapter(adapter);
                            } else {
                                // Logger.printError("Unable to parse JSON : " + e.getMessage());
                                showProgress(false);
                                showMessage(response.optString("Message"));
                            }
                        } catch (Exception e) {
                            Logger.printError("Unable to parse JSON : " + e.getMessage());
                            showProgress(false);
                            showMessage(getString(R.string.unexpected_error));
                            mProgressView.setVisibility(View.GONE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showProgress(false);
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        jsonRequest.setTag(NetworkConstants.UpdateVendor.TAG);
        requestQueue.add(jsonRequest);
    }

    private void getVendorType() {
        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.GET, NetworkConstants.Select.GET_VENDOR_TYPE, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Logger.printDebug("" + response);
                        showProgress(false);
                        try {
                            getVendorExp();
                            if (response.optString("Status").equalsIgnoreCase("success")) {
                                JSONObject data = response.optJSONObject("Data");
                                Iterator<String> iterator = data.keys();
                                while (iterator.hasNext()) {
                                    String key = iterator.next();
                                    vendorTypeKey.add(key);
                                    vendorTypeValue.add(data.optString(key));
                                    // Log.i("TAG","key:"+key +"--Value::"+categoryJSONObj.optString(key);
                                }
                                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                                        R.layout.spinner_item, vendorTypeKey);
                                mVendorType.setAdapter(adapter);
                            } else {
                                // Logger.printError("Unable to parse JSON : " + e.getMessage());
                                showProgress(false);
                                showMessage(response.optString("Message"));
                            }
                        } catch (Exception e) {
                            Logger.printError("Unable to parse JSON : " + e.getMessage());
                            showProgress(false);
                            showMessage(getString(R.string.unexpected_error));
                            mProgressView.setVisibility(View.GONE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showProgress(false);
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        jsonRequest.setTag(NetworkConstants.UpdateVendor.TAG);
        requestQueue.add(jsonRequest);
    }
    private String getVendorType(int position) {
        if (position == 0) {
            return vendorTypeValue.get(position);
        }
        else
        {
            return "";
        }
    }

    private String getExperienceType(int position) {
        if (position == 0) {
        return vendorExpValue.get(position);
    }
        else
    {
        return "";
    }
    }
}
