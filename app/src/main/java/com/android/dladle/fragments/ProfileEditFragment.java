package com.android.dladle.fragments;

import android.animation.AnimatorInflater;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.dladle.R;
import com.android.dladle.activities.BaseActivity;
import com.android.dladle.activities.RequestVendorActivity;
import com.android.dladle.activities.UserLoginActivity;
import com.android.dladle.activities.UserProfileActivity;
import com.android.dladle.customui.CustomProgressDialog;
import com.android.dladle.customui.MyJsonObjectRequest;
import com.android.dladle.customui.RoundedImageView;
import com.android.dladle.customui.TextViewUtils;
import com.android.dladle.utils.CommonConstants;
import com.android.dladle.utils.ImageUtils;
import com.android.dladle.utils.LinearLayoutTarget;
import com.android.dladle.utils.Logger;
import com.android.dladle.utils.NetworkConstants;
import com.android.dladle.utils.NetworkUtils;
import com.android.dladle.utils.PhotoManager;
import com.android.dladle.utils.PreferenceManager;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.app.Activity.RESULT_OK;

/**
 * Created by sourav on 18-02-2017.
 */

public class ProfileEditFragment extends BaseFragment {

    private String mFragmentKey;
    private TextView mEmailView, mUserType, mRatingView;
    private EditText mFirstName, mLastName, mCellNoView;
    //private Spinner mCurrentHomeView;
    private ImageView mEditButton, mProfilePicOverlay;
    private RoundedImageView mProfilePic,mNewProfilePic;
    private View mButtonLine, mProgressView, mScrollView;
    private String mEmail, mName, mLast, mRating, mCellNo;
    //private int mInitPosition;
    private Button mSave, mDelete;
    private AlertDialog mAlertDialog;
    private boolean isCropSupported = true,mUpdateInProgress;
   private String mProfileImagePath="";
    CustomProgressDialog mCustomProgressDialog;
    public ProfileEditFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (null != activity) {
            Intent i = activity.getIntent();
            if (null != i) {
                mFragmentKey = i.getStringExtra(NetworkConstants.JsonResponse.DATA);
               // mEmail = i.getStringExtra(NetworkConstants.Login.EMAIL_KEY);
                mEmail=PreferenceManager.getInstance(getActivity()).getUserName();
                mName = PreferenceManager.getInstance(getContext()).getFirstName();
                if (null == mName) {
                    mName = "";
                }
                mLast = PreferenceManager.getInstance(getContext()).getLastName();
                if (null == mLast) {
                    mLast = "";
                }
                mCellNo = PreferenceManager.getInstance(getContext()).getCellNumber();
                if (null == mCellNo) {
                    mCellNo = "";
                }
                float rating = PreferenceManager.getInstance(getContext()).getRating();
                if (rating == 0) {
                    mRating = "No Rating available";
                }
                //mInitPosition = i.getIntExtra(NetworkConstants.UpdateVendor.EXPERIENCE,0);
                isCropSupported = activity.getResources().getBoolean(R.bool.support_crop);
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.user_profile_edit_fragment, container, false);
        updateHeaderMargin(view);
        mFirstName = (EditText) view.findViewById(R.id.first_name);
        mLastName = (EditText) view.findViewById(R.id.last_name);
        mEmailView = (TextView) view.findViewById(R.id.email);
        mUserType = (TextView) view.findViewById(R.id.user_type);
        mCellNoView = (EditText) view.findViewById(R.id.cell_number);
        mRatingView = (TextView) view.findViewById(R.id.ratings);
        mButtonLine = view.findViewById(R.id.button_divider);
        mProfilePic = (RoundedImageView) view.findViewById(R.id.profile_pic);
        mNewProfilePic = (RoundedImageView) view.findViewById(R.id.profile_pic_new);
        mEditButton = (ImageView) view.findViewById(R.id.small_tick);
        mEditButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });
        mProfilePicOverlay = (ImageView) view.findViewById(R.id.profile_pic_overlay);
        mSave = (Button) view.findViewById(R.id.button_save);
        mDelete = (Button) view.findViewById(R.id.button_delete);
        mSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hindKeyBoardAndDoUpdate();
            }
        });
        mDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete();
            }
        });
        mProgressView = view.findViewById(R.id.progress_bar);
        mScrollView = view.findViewById(R.id.edit_form);
        setValues();
        setUserType();
        checkPermission(1);

        if (!(PreferenceManager.getInstance(getActivity()).getProfileImageUrl().equals("") && PreferenceManager.getInstance(getActivity()).getProfileImageUrl()==null)) {
            Glide.with(getActivity())
                    .load(PreferenceManager.getInstance(getActivity()).getProfileImageUrl())
                    .error(R.drawable.ic_avatar)
                    .into(mProfilePic);
        }


        return view;
    }

    private void selectImage() {

        final CharSequence[] items = {"Take Photo", "Choose from Gallery", "Remove Picture", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    if (checkPermission(100)) {
                        try {
                            Uri photoUri = PhotoManager.getProfilePhotoURI(getActivity());
                            if (null != photoUri) {
                                Intent intent = PhotoManager.getCameraIntent();
                                startActivityForResult(intent, 100);
                            }
                        } catch (ActivityNotFoundException ex) {
                            Logger.printError(ex.getMessage());
                        }
                    } else {
                        showMessage("Please enable permission for Camera");
                    }
                } else if (items[item].equals("Choose from Gallery")) {
                    if (checkPermission(200)) {
                        try {
                            startActivityForResult(PhotoManager.galleryIntent(getActivity()), 200);
                        } catch (ActivityNotFoundException ex) {
                            Logger.printError(ex.getMessage());
                        }
                    } else {
                        showMessage("Please enable permission for Gallery");
                    }
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                } else if (items[item].equals("Remove Picture")) {
                    PhotoManager.removeProfilePic(getContext());
                    PhotoManager.setProfilePic(getContext(), mProfilePic);
                }
            }
        });
        mAlertDialog = builder.create();
        mAlertDialog.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == 100) {
            if (grantResults.length >= 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                try {
                    Uri photoUri = PhotoManager.getProfilePhotoURI(getContext());
                    if (null != photoUri) {
                        Intent intent = PhotoManager.getCameraIntentForThumbnail(photoUri);
                        startActivityForResult(intent, 100);
                    }
                } catch (ActivityNotFoundException ex) {
                    Logger.printError(ex.getMessage());
                }
            }
        } else if (requestCode == 200) {
            if (grantResults.length >= 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                try {
                    startActivityForResult(PhotoManager.galleryIntent(getContext()), 200);
                } catch (ActivityNotFoundException ex) {
                    Logger.printError(ex.getMessage());
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
           // mProfilePic.setImageDrawable(null);
            mProfilePic.setVisibility(View.GONE);
            mNewProfilePic.setVisibility(View.VISIBLE);
            if (requestCode == 100) {
                try {
                    Intent i = PhotoManager.getCroppedProfileThumbMailIntent(getContext());
                    if (i != null && isCropSupported) {
                       // startActivityForResult(i, 300);
                    } else {
                        mProfileImagePath = PhotoManager.updateAndCachePropertyPic(getActivity(), mNewProfilePic, data.getData());
                    }
                } catch (ActivityNotFoundException ex) {
                    Logger.printError(ex.getMessage());
                }

            } else if (requestCode == 200) {
                try {
                    Intent i = PhotoManager.getCroppedProfileThumbMailIntent(getContext());
                    if (i != null && isCropSupported) {
                      //  startActivityForResult(i, 300);
                    } else {
                        mProfileImagePath = PhotoManager.updateAndCachePropertyPic(getActivity(), mNewProfilePic, data.getData());
                    }
                } catch (ActivityNotFoundException ex) {
                    Logger.printError(ex.getMessage());
                }

            }
            else if (requestCode == 300) {
                mProfileImagePath = PhotoManager.updateAndCachePropertyPic(getActivity(), mNewProfilePic, data.getData());
            }

            Logger.printDebug("path image");
            Logger.printDebug(mProfileImagePath);
        }
    }

    private void hindKeyBoardAndDoUpdate() {
        final UserProfileActivity activity = (UserProfileActivity) getActivity();
        if (null != activity && !activity.isFinishing()) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mDelete.setEnabled(false);
                    mSave.setEnabled(false);
                    doUpdate();
                }
            }, activity.isShowingKeyBoard ? CommonConstants.HIDE_KEYBOARD_DELAY : 0);
            if (activity.isShowingKeyBoard) {
                activity.hideKeyboard();
            }
        }
    }

    private void doUpdate() {
        if (mUpdateInProgress || getContext() == null) {
            return;
        }

        if (!NetworkUtils.isNetworkAvailable(getContext())) {
            Logger.printInfo("Not connected to internet");
            showMessage(getString(R.string.no_internet));
            mSave.setEnabled(true);
            mDelete.setEnabled(true);
            return;
        }

        // Reset errors.
        mCellNoView.setError(null);
        mFirstName.setError(null);
        mLastName.setError(null);

        // Store values at the time of the registration attempt.
        String cellNumber = TextViewUtils.removeEndSpaces(mCellNoView.getText().toString());
        String firstName = mFirstName.getText().toString();
        String lastName = mLastName.getText().toString();
//        String homeType = (String) mCurrentHomeView.getSelectedItem();

        boolean cancel = false;
        View focusView = null;

        // Check if id number is valid or not.
        if (TextUtils.isEmpty(cellNumber) || !TextUtils.isDigitsOnly(cellNumber)
                || cellNumber.length() < 10) {
            mCellNoView.setError(getString(R.string.invalid_cell_number));
            focusView = mCellNoView;
            cancel = true;
        }

        if (TextUtils.isEmpty(firstName)) {
            mFirstName.setError(getString(R.string.error_field_required));
            focusView = mFirstName;
            cancel = true;
        }

        if (TextUtils.isEmpty(lastName)) {
            mLastName.setError(getString(R.string.error_field_required));
            focusView = mLastName;
            cancel = true;
        }


        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            if (null != focusView) {
                focusView.requestFocus();
            }
            mSave.setEnabled(true);
            mDelete.setEnabled(true);
        } else {
            mUpdateInProgress = true;
            doServerInteraction(firstName, lastName, Long.parseLong(cellNumber));
        }
    }

    private void doServerInteraction(final String firstName, final String lastName, final long cellNum) {
        Logger.printDebug("Server interaction started");
        // Show a progress spinner
        showProgress(true);
        JSONObject request = new JSONObject();
        boolean isLandLord = CommonConstants.FRAGMENT_KEY_LANDLORD.equalsIgnoreCase(mFragmentKey);
        try {
            request.put(NetworkConstants.UpdateUser.CELL_NUM, cellNum);
            request.put(NetworkConstants.UpdateUser.FIRST_NAME, firstName);
            request.put(NetworkConstants.UpdateUser.LAST_NAME, lastName);
            Logger.printDebug("JSON : " + request);
        } catch (Exception e) {
            showProgress(false);
            e.printStackTrace();
        }
        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.POST,
                isLandLord ? NetworkConstants.UpdateUser.URL_LANDLORD : NetworkConstants.UpdateUser.URL_TENANT, request,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Logger.printDebug("" + response);
                        try {
                            String status = response.getString(NetworkConstants.JsonResponse.STATUS);
                            String message = response.getString(NetworkConstants.JsonResponse.MESSAGE);
                            if (NetworkConstants.JsonResponse.STATUS_SUCCESS.equalsIgnoreCase(status)) {

                                if(!mProfileImagePath.equals(""))
                                {
                                    updateProfilePic();
                                }


                                PreferenceManager.getInstance(getContext()).setFirstTimeLaunch();
                                mProgressView.setVisibility(View.GONE);
                                PreferenceManager.getInstance(getContext()).setProfileUpdateStatus(true);
                                PreferenceManager.getInstance(getContext()).setName(firstName, lastName);
                                PreferenceManager.getInstance(getContext()).setCellNumber(Long.toString(cellNum));
                                UserProfileActivity activity = (UserProfileActivity) getActivity();
                                if (null != activity) {
                                    activity.notifyOnEdit(false);
                                    activity.handleLeftButtonClick();
                                }
                            } else {
                                showProgress(false);
                                showMessage(status + " : " + message);
                            }
                        } catch (JSONException e) {
                            Logger.printError("Unable to parse JSON : " + e.getMessage());
                            showProgress(false);
                            showMessage(getString(R.string.unexpected_error));
                        } catch (Exception ex) {
                            // Do Nothing -- Temp fix for server error
                            mProgressView.setVisibility(View.GONE);
                            PreferenceManager.getInstance(getContext()).setProfileUpdateStatus(true);
                            UserProfileActivity activity = (UserProfileActivity) getActivity();
                            if (null != activity) {
                                activity.notifyOnEdit(false);
                                activity.handleLeftButtonClick();
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showProgress(false);
                        Logger.printError("Some error : " + error.toString());
                        showMessage(error.getMessage());
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        jsonRequest.setTag(NetworkConstants.UpdateUser.TAG);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(NetworkConstants.TIMOUT_TIME,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonRequest);
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    private void showProgress(final boolean show) {
        if (getActivity() instanceof BaseActivity) {
            ((BaseActivity) getActivity()).setActionInProgress(show);
        }
        mUpdateInProgress = show;

        mScrollView.setVisibility(show ? View.GONE : View.VISIBLE);
        mDelete.setEnabled(true);
        mSave.setEnabled(true);

        ObjectAnimator progressAnimator = (ObjectAnimator) AnimatorInflater.loadAnimator(getContext(), R.animator.flipping);
        progressAnimator.setTarget(mProgressView);
        progressAnimator.start();
    }
    public String getFileName(String path) {
        int pos = path.lastIndexOf("/") + 1;

        return path.substring(pos);
    }
    private void delete() {
        dismissDialog();
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Do you want to delete your account?");
        final EditText input = new EditText(getContext());
        input.setHint("Enter your password");
        input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        builder.setView(input);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                String password = input.getText().toString().trim();
                if(TextUtils.isEmpty(password))
                {
                    showMessage("Please enter password");
                }
                else
                {
                    deleteAcoount(password);
                }


            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private void deleteAcoount(String password) {
        Logger.printDebug("Server interaction started");
        // Show a progress spinner
        showProgress(true);
        JSONObject request = new JSONObject();
        try {
            request.put("password",password);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.POST,NetworkConstants.User.DELETE_ACCOUNT, request,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Logger.printDebug("" + response);
                        try {
                            String status = response.getString(NetworkConstants.JsonResponse.STATUS);
                            String message = response.getString(NetworkConstants.JsonResponse.MESSAGE);
                            if (NetworkConstants.JsonResponse.STATUS_SUCCESS.equalsIgnoreCase(status)) {



                                Intent intent = new Intent(getActivity(), UserLoginActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);

                            } else {
                                showProgress(false);
                                showMessage(status + " : " + message);
                            }
                        } catch (JSONException e) {
                            Logger.printError("Unable to parse JSON : " + e.getMessage());
                            showProgress(false);
                            showMessage(getString(R.string.unexpected_error));
                        } catch (Exception ex) {
                            // Do Nothing -- Temp fix for server error
                            mProgressView.setVisibility(View.GONE);
                            PreferenceManager.getInstance(getContext()).setProfileUpdateStatus(true);
                            UserProfileActivity activity = (UserProfileActivity) getActivity();
                            if (null != activity) {
                                activity.notifyOnEdit(false);
                                activity.handleLeftButtonClick();
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showProgress(false);
                        Logger.printError("Some error : " + error.toString());
                        showMessage(error.getMessage());
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        jsonRequest.setTag(NetworkConstants.UpdateUser.TAG);
        requestQueue.add(jsonRequest);
    }

    private void updateProfilePic() {
        Logger.printDebug("Server interaction started");
        // Show a progress spinner
    showProgressDialog(true);
        JSONObject request = new JSONObject();
        try {
            request.put("base64Image", ImageUtils.readFileAsBase64String(mProfileImagePath));
            request.put("fileName",getFileName(mProfileImagePath));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        writeToFile(request.toString());
      Logger.printDebug(request.toString());

        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.POST,NetworkConstants.User.UPDATE_PROFILE_IMAGE, request,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Logger.printDebug("" + response);
                        showProgressDialog(false);
                        try {
                            String status = response.getString(NetworkConstants.JsonResponse.STATUS);
                            String message = response.getString(NetworkConstants.JsonResponse.MESSAGE);
                            showMessage(message);
                            if (NetworkConstants.JsonResponse.STATUS_SUCCESS.equals(status)) {
                                PreferenceManager.getInstance(getContext()).setProfileImageUrl(response.getString(NetworkConstants.JsonResponse.DATA));

                            } else {
                                showProgress(false);
                                showMessage(status + " : " + message);
                            }
                        } catch (JSONException e) {
                            Logger.printError("Unable to parse JSON : " + e.getMessage());
                            showProgress(false);
                            showMessage(getString(R.string.unexpected_error));
                        } catch (Exception ex) {
                            // Do Nothing -- Temp fix for server error
                            mProgressView.setVisibility(View.GONE);
                            PreferenceManager.getInstance(getContext()).setProfileUpdateStatus(true);
                            UserProfileActivity activity = (UserProfileActivity) getActivity();
                            if (null != activity) {
                                activity.notifyOnEdit(false);
                                activity.handleLeftButtonClick();
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showProgressDialog(false);
                        Logger.printError("Some error : " + error.toString());
                        showMessage(error.getMessage());
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        jsonRequest.setTag(NetworkConstants.UpdateUser.TAG);
        requestQueue.add(jsonRequest);
    }





    private void dismissDialog() {
        if (null != mAlertDialog && mAlertDialog.isShowing()) {
            mAlertDialog.dismiss();
        }
    }

    private void updateHeaderMargin(View view) {
        View v = view.findViewById(R.id.parent_view);
        Logger.printInfo(v.getLayoutParams().toString());
        if (v.getLayoutParams() instanceof FrameLayout.LayoutParams) {
            FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) v.getLayoutParams();
            Logger.printInfo("Height b4 : "+lp.topMargin);
            lp.topMargin = lp.topMargin + (int) getResources().getDimension(R.dimen.actionBar_Size);
            Logger.printInfo("Height : "+lp.topMargin);
            v.setLayoutParams(lp);
        }
    }

    private void setUserType() {
        if (CommonConstants.FRAGMENT_KEY_LANDLORD.equalsIgnoreCase(mFragmentKey)) {
            mUserType.setText("User type : "+getString(R.string.user_type_landlord));
            mButtonLine.setBackground(getResources().getDrawable(R.mipmap.line_green));
            mProfilePicOverlay.setImageDrawable(getResources().getDrawable(R.mipmap.ic_green_overlay));
        } else if (CommonConstants.FRAGMENT_KEY_TENANT.equalsIgnoreCase(mFragmentKey)) {
            mUserType.setText("User type : "+getString(R.string.user_type_tenant));
            mButtonLine.setBackground(getResources().getDrawable(R.mipmap.line_blue));
            mProfilePicOverlay.setImageDrawable(getResources().getDrawable(R.mipmap.ic_blue_overlay));
        } else {
            mUserType.setText("User type : "+getString(R.string.user_type_vendor));
            Logger.printInfo("Invalid user type in this flow");
        }
    }

    private void setValues() {
        mFirstName.setText(mName);
        mLastName.setText(mLast);
        mEmailView.setText("Email : "+mEmail);
        mCellNoView.setText(mCellNo);
        if(PreferenceManager.getInstance(getActivity()).getRatingCount().equals("0")) {
            mRatingView.setText("Rating : " + (TextUtils.isEmpty(mRating) ? "No Rating available" : mRating));
        }
        else {
            Logger.printDebug(String.valueOf(PreferenceManager.getInstance(getActivity()).getRatingCount()));
            mRatingView.setText("Rating : " +PreferenceManager.getInstance(getActivity()).getRating() +" stars from "+PreferenceManager.getInstance(getActivity()).getRatingCount() +" people");
        }
   /*     if(!(PreferenceManager.getInstance(getActivity()).getProfileImageUrl().equals("")  && PreferenceManager.getInstance(getActivity()).getProfileImageUrl()==null)) {
            Glide.with(this)
                    .load(PreferenceManager.getInstance(getActivity()).getProfileImageUrl())
                    .error(R.drawable.blankimage)
                    .into(mProfilePic);
        }*/

        mEditButton.setVisibility(View.VISIBLE);
        mEditButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_edit_button));
        setWatchers();
    }

    private void setWatchers() {
        mFirstName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                notifyActivityOnEdit();
            }
        });
        mLastName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                notifyActivityOnEdit();
            }
        });
        mCellNoView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                notifyActivityOnEdit();
            }
        });

//        mCurrentHomeView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                notifyActivityOnEdit();
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//            }
//        });
    }

    private void notifyActivityOnEdit() {
        UserProfileActivity activity = (UserProfileActivity) getActivity();
        if (null != activity && !activity.isFinishing()) {
            boolean hasChanged = false;
            hasChanged = !TextUtils.equals(mFirstName.getText(), mName)
                    || !TextUtils.equals(mLastName.getText(), mLast)
                    || !TextUtils.equals(mCellNoView.getText(), mCellNo);
                    //|| (mCurrentHomeView.getSelectedItemPosition() != mInitPosition);
            activity.notifyOnEdit(hasChanged);
        }
    }

    private boolean checkPermission(final int type) {
        if (getActivity() != null && !getActivity().isFinishing()) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                return true;
            }
            ArrayList<String> arr = new ArrayList<String>();
            ArrayList<String> arrFinal = new ArrayList<String>();
            if (200 == type) {
                arr.add(READ_EXTERNAL_STORAGE);
            } else if (100 == type) {
                arr.add(CAMERA);
            } else {
                arr.add(CAMERA);
                arr.add(READ_EXTERNAL_STORAGE);
            }
            arrFinal.addAll(arr);
            for (String s : arr) {
                if (getActivity().checkSelfPermission(s) == PackageManager.PERMISSION_GRANTED) {
                    arrFinal.remove(s);
                }
            }
            if (arrFinal.size() == 0) {
                return true;
            }
            String[] a = arrFinal.toArray(new String[arrFinal.size()]);
            requestPermissions(a, type);
        }
        return false;
    }
    private void showProgressDialog(final boolean show) {
        if (show) {
            mCustomProgressDialog = new CustomProgressDialog(getActivity());
            mCustomProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            mCustomProgressDialog.setCancelable(false);
            mCustomProgressDialog.show();
        } else {
            mCustomProgressDialog.dismiss();
        }
    }
    public void writeToFile(String data) {
        Logger.printDebug("file write started");
        // Get the directory for the user's public pictures directory.
        File path = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator);
        final File file = new File(path, "update_profile.txt");

        // Save your stream, don't forget to flush() it before closing it.

        try {
            file.createNewFile();
            FileOutputStream fOut = new FileOutputStream(file);
            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
            myOutWriter.append(data);

            myOutWriter.close();

            fOut.flush();
            fOut.close();
            Logger.printDebug("file write end");
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }
}
