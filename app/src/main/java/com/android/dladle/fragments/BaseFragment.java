package com.android.dladle.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

/**
 * Created by sourav on 20-11-2016.
 */

public class BaseFragment extends Fragment{

    protected OnFragmentInteractionListener mListener;
    protected View mRootView;
    protected boolean isShowingKeyboard;
    protected boolean isLOrLow = Build.VERSION.SDK_INT < Build.VERSION_CODES.M;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    protected void notifyListener() {
        if (null != mListener) {
            mListener.onBackKeyPressed();
        }
    }

    protected void transitionTo(Activity activity, Intent i) {
        ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(activity);
        startActivity(i, transitionActivityOptions.toBundle());
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onBackKeyPressed();
        void notifyActivity();
    }

    public interface ModificationWatcher {
        public void notifyOnEdit(boolean isModified);
    }

    protected int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    protected void showMessage(String message) {
        if (!TextUtils.isEmpty(message) && null != getContext()) {
            Toast toast = Toast.makeText(getContext(), message, Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
    }
}
