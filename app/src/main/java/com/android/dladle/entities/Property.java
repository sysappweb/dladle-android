package com.android.dladle.entities;

import java.util.ArrayList;

/**
 * Created by sourav on 09-04-2017.
 */

public class Property {

    public Property() {
        mImagePath = new ArrayList<String>();
        mContacts = new ArrayList<Contact>();
    }
    public String mPropertyImage;
    public String mUnitNumber;
    public String mComplexName;
    public String mEstateName;
    public String mAddress;
    public boolean mIsEstate=false;
    public String mPropertyType;
    public ArrayList<String> mImagePath;
    public ArrayList<Contact> mContacts;
}
