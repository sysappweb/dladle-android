package com.android.dladle.entities;

/**
 * Created by soura on 09-04-2017.
 */

public class Contact {
    public String mType;
    public String mName;
    public String mNumber;
    public String mEmail;
}
