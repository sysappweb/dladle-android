package com.android.dladle.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.dladle.R;
import com.android.dladle.customui.CustomProgressDialog;
import com.android.dladle.customui.MyJsonObjectRequest;
import com.android.dladle.customui.ProfileImageView;
import com.android.dladle.utils.Logger;
import com.android.dladle.utils.NetworkConstants;
import com.android.dladle.utils.PreferenceManager;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

public class QuoteFinalPrice extends BaseActivity {
    Float mMinPrice, mMaxPrice, mFinalPrice;
    String mServiceId, mNotificationId;
    Button mBtnFinalPriceSubmit;
    boolean withinInitial = false;
    CustomProgressDialog mCustomProgressDialog;
    ImageButton mBtnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quote_final_price);
        initView();
        performAction();
    }

    private void initView() {

        mMinPrice = Float.parseFloat(getIntent().getStringExtra("MinPrice"));
        mMaxPrice = Float.parseFloat(getIntent().getStringExtra("MaxPrice"));
        mFinalPrice = Float.parseFloat(getIntent().getStringExtra("FinalPrice"));
        mServiceId = getIntent().getStringExtra("ServiceId");
        mNotificationId = getIntent().getStringExtra("NotificationId");
        mBtnFinalPriceSubmit = (Button) findViewById(R.id.btn_submit);
        mCustomProgressDialog = new CustomProgressDialog(this);
        TextView finalPrice = (TextView) findViewById(R.id.final_price);
        mBtnBack = (ImageButton) findViewById(R.id.back_btn);
        TextView finalPriceMesaage = (TextView) findViewById(R.id.final_price_message);
        finalPrice.setText(String.valueOf(mFinalPrice));

        RatingBar ratingBar = (RatingBar) findViewById(R.id.rating_bar);
        ProfileImageView profileImageView = (ProfileImageView) findViewById(R.id.profile_pic);
        TextView textView = (TextView) findViewById(R.id.job_client_description);
        String empoyerName = getIntent().getStringExtra("employerName");
        ratingBar.setRating(Float.parseFloat(getIntent().getStringExtra("rating")));
        textView.setText(empoyerName + " needs a " + PreferenceManager.getInstance(QuoteFinalPrice.this).getVendorType() + " in ");
        Glide.with(QuoteFinalPrice.this)
                .load(getIntent().getStringExtra("image"))
                .asBitmap()
                .error(R.drawable.ic_avatar)
                .into(profileImageView);

        if (mFinalPrice > mMaxPrice) {
            withinInitial = false;
            finalPriceMesaage.setText("The above amount is more than your initial.\\n Please explain why to Mike");
            finalPriceMesaage.setTextColor(Color.parseColor("#F44336"));
        } else {
            withinInitial = true;
            finalPriceMesaage.setText("Great ! The above amount is within in your initial you gave");
            finalPriceMesaage.setTextColor(Color.parseColor("#64DD17"));
        }


    }

    private void performAction() {
        mBtnFinalPriceSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!withinInitial) {
                    showAlertForReason();
                } else {
                    submitFinalPrice();
                }
            }
        });
        mBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    private void showAlertForReason() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(QuoteFinalPrice.this);
        alertDialog.setTitle("Reason");
        alertDialog.setMessage("Please enter your reason for price hike");

        final EditText input = new EditText(QuoteFinalPrice.this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        alertDialog.setView(input);

        alertDialog.setPositiveButton("Submit",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        String price = input.getText().toString();
                        if (TextUtils.isEmpty(input.getText().toString().trim())) {
                            showMessage("Please enter reason");
                        } else {

                            submitFinalPrice();
                        }
                    }
                });

        alertDialog.setNegativeButton("Cancle",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        alertDialog.show();
    }

    private void submitFinalPrice() {

        showProgress(true);
        JSONObject input = new JSONObject();
        try {
            input.put("finalPrice", Double.valueOf(mFinalPrice));
            input.put("serviceId", Integer.parseInt(mServiceId));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("Input", input.toString());
        Log.d("URL", NetworkConstants.VendorController.VENDOR_FINAL_PRICE);
        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.POST, NetworkConstants.VendorController.VENDOR_FINAL_PRICE, input,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Logger.printDebug("FinalPrice" + response);
                        showProgress(false);
                        if (response.optString("Status").equalsIgnoreCase(NetworkConstants.SUCCESS)) {

                            Intent intent = new Intent(QuoteFinalPrice.this, UserProfileActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra(NetworkConstants.JsonResponse.DATA, PreferenceManager.getInstance(QuoteFinalPrice.this).getUserType());
                            startActivity(intent);


                        }
                        markNotificationAsRead();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showProgress(false);
                        if (error instanceof TimeoutError) {
                            showMessage(getString(R.string.server_error));
                        } else {
                            showMessage(error.getMessage());
                        }
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        jsonRequest.setTag(NetworkConstants.Login.TAG);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(NetworkConstants.TIMOUT_TIME,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonRequest);
    }

    private void showProgress(final boolean show) {
        super.setActionInProgress(show);
        if (show) {
            mCustomProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            mCustomProgressDialog.setCancelable(false);
            mCustomProgressDialog.show();
        } else {
            mCustomProgressDialog.dismiss();
        }
    }
    private void markNotificationAsRead() {
        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.POST, NetworkConstants.Notification.NOTIFICATION_READ + String.valueOf(mNotificationId) + "&read=true", null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Logger.printDebug("" + response);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof TimeoutError) {
                            showMessage(getString(R.string.server_error));
                        } else {
                            showMessage(error.getMessage());
                        }
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(QuoteFinalPrice.this);
        jsonRequest.setTag(NetworkConstants.Login.TAG);
        requestQueue.add(jsonRequest);
    }
}
