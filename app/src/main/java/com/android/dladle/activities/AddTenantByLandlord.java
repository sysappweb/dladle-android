package com.android.dladle.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.android.dladle.R;
import com.android.dladle.customui.CustomProgressDialog;
import com.android.dladle.customui.MyJsonObjectRequest;
import com.android.dladle.utils.CommonConstants;
import com.android.dladle.utils.Logger;
import com.android.dladle.utils.NetworkConstants;
import com.android.dladle.utils.NetworkUtils;
import com.android.dladle.utils.PreferenceManager;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class AddTenantByLandlord extends BaseActivity {
    ImageView mBack;
    Button mSubmit;
    EditText mTenantName,mTenantemail;
    int mHouseId=0;
    CustomProgressDialog mCustomProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_tenant_by_landlord);
        initView();
        performAction();
    }
    private void initView()
    {
        mBack=(ImageView)findViewById(R.id.back_btn);
        mSubmit=(Button)findViewById(R.id.btn_submit);
        mTenantName=(EditText)findViewById(R.id.name);
        mTenantemail=(EditText)findViewById(R.id.email);
        mCustomProgressDialog = new CustomProgressDialog(AddTenantByLandlord.this);
        mHouseId=getIntent().getIntExtra("houseId",0);
    }
    private void performAction()
    {
        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email=mTenantemail.getText().toString().trim();
                String name=mTenantName.getText().toString().trim();
                if (TextUtils.isEmpty(email)) {
                    mTenantemail.setError(getString(R.string.error_field_required));
                     mTenantemail.requestFocus();
                } else if (!CommonConstants.isEmailValid(email)) {
                    mTenantemail.setError(getString(R.string.error_invalid_email));
                    mTenantemail.requestFocus();
                }
                else if(TextUtils.isEmpty(name))
                {
                    mTenantName.setError("Invalid name");
                    mTenantName.requestFocus();
                }
                else
                {
                    if(NetworkUtils.isNetworkAvailable(AddTenantByLandlord.this)) {
                        sendInvite(email, name);
                    }
                    else {
                        showMessage(getResources().getString(R.string.no_internet));
                    }
                }
            }
        });
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
    private void sendInvite(final String email, String name)
    {
        showProgress(true);
      JSONObject input=new JSONObject();
        try {
            input.put("houseId",mHouseId);
            input.put("emailId",email);
            Logger.printDebug("" + input.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.POST, NetworkConstants.Property.INVITE, input,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        showProgress(false);
                        Logger.printDebug("" + response);
                        showMessage(response.optString("Message"));
                        if (response.optString("Status").equalsIgnoreCase(NetworkConstants.SUCCESS)) {
                            Intent intent = new Intent(AddTenantByLandlord.this, InviteSendSuccessfulActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra(NetworkConstants.JsonResponse.DATA,"landlord");
                            startActivity(intent);
                        }




                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Logger.printDebug("" + error.getMessage().toString());
                        showProgress(false);
                        if (error instanceof TimeoutError) {
                           showMessage(getString(R.string.server_error));
                        } else {
                         showMessage(error.getMessage());
                        }
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(AddTenantByLandlord.this);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(NetworkConstants.TIMOUT_TIME,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        jsonRequest.setTag(NetworkConstants.Login.TAG);
        requestQueue.add(jsonRequest);
    }
    private void showProgress(final boolean show) {
        super.setActionInProgress(show);
        if (show) {
            mCustomProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            mCustomProgressDialog.setCancelable(false);
            mCustomProgressDialog.show();
        } else {
            mCustomProgressDialog.dismiss();
        }
    }
}
