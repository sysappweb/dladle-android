package com.android.dladle.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.dladle.R;
import com.android.dladle.customui.CustomProgressDialog;
import com.android.dladle.customui.MyJsonObjectRequest;
import com.android.dladle.customui.TextViewUtils;
import com.android.dladle.utils.CommonConstants;
import com.android.dladle.utils.Logger;
import com.android.dladle.utils.NetworkConstants;
import com.android.dladle.utils.NetworkUtils;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.util.Util;

import org.json.JSONException;
import org.json.JSONObject;

public class SendInviteToLandlordByTenantActivity extends BaseActivity {
    EditText mEtName, mEtEmail;
    Button mBtnSubmit;
    CustomProgressDialog mCustomProgressDialog;
    TextView mTvAmount;
    ImageView mIvBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_invite_to_landlord_by_tenant);
        initView();
        performAction();
    }

    private void initView() {
        mEtName = (EditText) findViewById(R.id.name);
        mEtEmail = (EditText) findViewById(R.id.email);
        mBtnSubmit = (Button) findViewById(R.id.btn_submit);
        mIvBack = (ImageView) findViewById(R.id.back_btn);
    //    mTvAmount = (TextView) findViewById(R.id.amount);
        SpannableString text = new SpannableString("R10.00");
        text.setSpan(new StyleSpan(Typeface.BOLD), 1, 5, 0);
       // mTvAmount.setText(text);
        mCustomProgressDialog = new CustomProgressDialog(SendInviteToLandlordByTenantActivity.this);
    }

    private void performAction() {
        mBtnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = mEtEmail.getText().toString().trim();
                String name = mEtName.getText().toString().trim();
                if (TextUtils.isEmpty(name)) {
                    mEtName.setError("Please enter name");
                    mEtName.requestFocus();
                } else if (!CommonConstants.isEmailValid(email)) {
                    if (TextUtils.isEmpty(email)) {
                        mEtEmail.setError("Please enter email");
                        mEtEmail.requestFocus();

                    }
                } else if (!NetworkUtils.isNetworkAvailable(SendInviteToLandlordByTenantActivity.this)) {
                    showMessage(getResources().getString(R.string.no_internet));
                } else {
                    if(NetworkUtils.isNetworkAvailable(SendInviteToLandlordByTenantActivity.this)) {
                        requestToLandlordForPlace(email, name);
                    }
                    else {
                        showMessage(getResources().getString(R.string.no_internet));
                    }
                }
            }
        });
        mIvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void requestToLandlordForPlace(String email, final String name) {
        showProgress(true);
        JSONObject input = new JSONObject();
        try {
            input.put("emailId", email);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Logger.printDebug("input" + input);
        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.POST, NetworkConstants.Tenant.SEND_REQUEST_TO_LANDLORD, input,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        showProgress(false);
                        Logger.printDebug("" + response);
                        showMessage(response.optString("Message"));
                        if (response.optString("Status").equalsIgnoreCase(NetworkConstants.SUCCESS)) {
                            Intent intent = new Intent(SendInviteToLandlordByTenantActivity.this, InviteSendActivity.class);
                            intent.putExtra("landlordName", name);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);

                        } else {

                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showProgress(false);
                        if (error instanceof TimeoutError) {
                            showMessage(getString(R.string.server_error));
                        } else {
                            showMessage(error.getMessage());
                        }
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(SendInviteToLandlordByTenantActivity.this);
        jsonRequest.setTag(NetworkConstants.Login.TAG);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(NetworkConstants.TIMOUT_TIME,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonRequest);
    }

    private void showProgress(final boolean show) {
        super.setActionInProgress(show);
        if (show) {
            mCustomProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            mCustomProgressDialog.setCancelable(false);
            mCustomProgressDialog.show();
        } else {
            mCustomProgressDialog.dismiss();
        }
    }

}
