package com.android.dladle.activities;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.android.dladle.R;

public class LandlordAdviceActivity extends Activity {
    ImageView mBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landlord_advice);
        initView();
        performAction();
    }
    private void initView()
    {
        mBack=(ImageView)findViewById(R.id.back_btn);
    }
    private void performAction()
    {
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
