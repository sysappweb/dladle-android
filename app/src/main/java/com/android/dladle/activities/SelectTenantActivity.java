package com.android.dladle.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.dladle.R;
import com.android.dladle.customui.CustomProgressDialog;
import com.android.dladle.customui.MyJsonObjectRequest;
import com.android.dladle.model.NotificationListModel;
import com.android.dladle.model.TenantListModel;
import com.android.dladle.utils.LinearLayoutTarget;
import com.android.dladle.utils.Logger;
import com.android.dladle.utils.NetworkConstants;
import com.android.dladle.utils.NetworkUtils;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SelectTenantActivity extends BaseActivity {

    TextView mPropertyName,mUserNameOne,mUserNameTwo,mUserNameThree,mTvterminationText;
    ImageView mBack,mUserOneImage,mUserTwoImage,mUserThreeImage;
    ArrayList<TenantListModel> tenantListModelArrayList=new ArrayList<>();
    LinearLayout mTenantOneConatiner,mTenantTwoContainer,mTenantThreeConatainer,mPropertyImageContainer;
    String propertyname,propertyImagePath;
    int propertyId;
    CustomProgressDialog mCustomProgressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_tenant);
        initView();
        performAction();
    }
    private void initView()
    {
        mCustomProgressDialog = new CustomProgressDialog(this);
        mPropertyName=(TextView)findViewById(R.id.titleView);
        mUserNameOne=(TextView)findViewById(R.id.tv_user_name1);
        mUserNameTwo=(TextView)findViewById(R.id.tv_user_name2);
        mUserNameThree=(TextView)findViewById(R.id.tv_user_name3);
        mBack=(ImageView)findViewById(R.id.back_btn);
        mUserOneImage=(ImageView)findViewById(R.id.profile_pic1);
        mUserTwoImage=(ImageView)findViewById(R.id.profile_pic2);
        mUserThreeImage=(ImageView)findViewById(R.id.profile_pic3);
        mTenantOneConatiner=(LinearLayout)findViewById(R.id.tenant_one_conatainer);
        mTenantTwoContainer=(LinearLayout)findViewById(R.id.tenant_two_conatiner);
        mTenantThreeConatainer=(LinearLayout)findViewById(R.id.tenant_three_conatainer);
        mPropertyImageContainer=(LinearLayout)findViewById(R.id.property_image_container);
        mTvterminationText=(TextView)findViewById(R.id.tv_termination_text);
        tenantListModelArrayList=(ArrayList<TenantListModel>) getIntent().getSerializableExtra("tenantList");
        int tenantListSize=tenantListModelArrayList.size();
        propertyname=getIntent().getStringExtra("propertyName");
        propertyImagePath=getIntent().getStringExtra("propertyImage");
        propertyId=getIntent().getIntExtra("houseId",0);
        mPropertyName.setText(propertyname);
        mTvterminationText.setText(getResources().getString(R.string.select_tenant_desc,propertyname));
        Glide.with(this)
                .load(propertyImagePath)
                .asBitmap()
                .error(R.drawable.blankimage)
                .into(new LinearLayoutTarget(this, (LinearLayout)mPropertyImageContainer));
        if(tenantListSize==1)
        {
            mTenantOneConatiner.setVisibility(View.GONE);
            mTenantThreeConatainer.setVisibility(View.GONE);
            mUserNameTwo.setText(tenantListModelArrayList.get(0).getFirstName());
        }
        else if(tenantListSize==2)
        {
            mTenantOneConatiner.setVisibility(View.GONE);
            mUserNameThree.setText(tenantListModelArrayList.get(1).getFirstName());
            mUserNameTwo.setText(tenantListModelArrayList.get(0).getFirstName());
        }
        else if(tenantListSize==3)
        {

            mUserNameOne.setText(tenantListModelArrayList.get(2).getFirstName());
            mUserNameTwo.setText(tenantListModelArrayList.get(1).getFirstName());
            mUserNameThree.setText(tenantListModelArrayList.get(1).getFirstName());
        }
    }
    private void performAction()
    {
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mUserOneImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showWarning(tenantListModelArrayList.get(2).getEmailId(),tenantListModelArrayList.get(2).getFirstName());
            }
        });
        mUserTwoImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showWarning(tenantListModelArrayList.get(0).getEmailId(),tenantListModelArrayList.get(0).getFirstName());
            }
        });
        mUserThreeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showWarning(tenantListModelArrayList.get(1).getEmailId(),tenantListModelArrayList.get(1).getFirstName());
            }
        });

    }
    private void removeTenant(String tenantID, final String tenantName)
    {
        showProgress(true);
        final JSONObject input=new JSONObject();
        try {
            input.put("emailId",tenantID);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Logger.printDebug(input.toString());
        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.POST, NetworkConstants.LeaseController.REMOVE_TENNANT, input,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        showProgress(false);
                        Logger.printDebug("" + response);
                        showMessage(response.optString("Message"));
                        if (response.optString("Status").equalsIgnoreCase(NetworkConstants.SUCCESS)) {
                           Intent intent=new Intent(SelectTenantActivity.this,TenantRemovedActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("tenantName",tenantName);
                            intent.putExtra("houseName",propertyname);
                            startActivity(intent);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showProgress(false);
                        if (error instanceof TimeoutError) {
                            showMessage(getString(R.string.server_error));
                        } else {
                            showMessage(error.getMessage());
                        }
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        jsonRequest.setTag(NetworkConstants.Login.TAG);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(NetworkConstants.TIMOUT_TIME,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonRequest);
    }
    private void showWarning(final String email,final String name)
    {
        final AlertDialog.Builder builder;
            builder = new AlertDialog.Builder(SelectTenantActivity.this);
        builder.setTitle("Remove tenant")
                .setMessage("Are you sure you want to remove tenant from this property?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if(NetworkUtils.isNetworkAvailable(SelectTenantActivity.this)) {
                            removeTenant(email, name);
                        }
                        else {
                            showMessage(getResources().getString(R.string.no_internet));
                        }
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
    private void showProgress(final boolean show) {
        super.setActionInProgress(show);
        if (show) {
            mCustomProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            mCustomProgressDialog.setCancelable(false);
            mCustomProgressDialog.show();
        } else {
            mCustomProgressDialog.dismiss();
        }
    }
}
