package com.android.dladle.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.android.dladle.R;
import com.android.dladle.utils.NetworkConstants;
import com.android.dladle.utils.PreferenceManager;

public class WalletAddedActivity extends BaseActivity {

    ImageView mIvBack;
    Button mBtnCntd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_added);
        initView();
        performAction();
    }
    private void initView()
    {
        mIvBack=(ImageView)findViewById(R.id.back_btn);
        mBtnCntd=(Button)findViewById(R.id.button_ok);
    }
    private void performAction()
    {
       mIvBack.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               onBackPressed();
           }
       });
        mBtnCntd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(WalletAddedActivity.this,ViewCardDetailsActivity.class);
                startActivity(intent);
            }
        });
    }
}
