package com.android.dladle.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.android.dladle.R;

/**
 * Created by sourav on 23-02-2017.
 */

public class WalletActivity extends BaseActivity {
   Button mBtnSetUpPaymentMethod;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_wallet);
        initView();
        performAction();
    }
    private void initView()
    {
        mAppBackButton = (ImageButton) findViewById(R.id.back_btn);
        mBtnSetUpPaymentMethod=(Button)findViewById(R.id.btn_submit);
    }
    private void performAction()
    {
        mAppBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        mBtnSetUpPaymentMethod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(WalletActivity.this,AddPaymentCardActivity.class);
                startActivity(intent);
            }
        });

    }
}
