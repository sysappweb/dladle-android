package com.android.dladle.activities;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.dladle.R;

public class DeletePlaceActivity extends BaseActivity {
    ImageView mUserProfile, mBack;
    TextView mTitle;
    Spinner mNotification;
    Button mAdvForLandlords, mAddImportContacts, mInviteTenante, mMyHome, mCallVendor, mDeletePlace;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_place);
        initView();
        performAction();
    }

    private void initView() {
        mUserProfile = (ImageView) findViewById(R.id.profile_pic);
        mBack = (ImageView) findViewById(R.id.back_btn);
        mTitle = (TextView) findViewById(R.id.titleView);
        mNotification = (Spinner) findViewById(R.id.notification);
        mAdvForLandlords = (Button) findViewById(R.id.button_adv_landlords);
        mAddImportContacts = (Button) findViewById(R.id.button_import_contacts);
        mInviteTenante = (Button) findViewById(R.id.button_invite_tenant);
        mMyHome = (Button) findViewById(R.id.button_my_home);
        mCallVendor = (Button) findViewById(R.id.button_call_vendor);
        mDeletePlace = (Button) findViewById(R.id.button_delete_place);

    }

    private void performAction() {
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mDeletePlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }
}
