package com.android.dladle.activities;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.android.dladle.R;
import com.android.dladle.adapter.RecyclerAdapterForNotificationList;
import com.android.dladle.adapter.RecyclerAdapterForTenantNotificationInHome;
import com.android.dladle.customui.CustomProgressDialog;
import com.android.dladle.customui.MyJsonObjectRequest;
import com.android.dladle.model.NotificationListModel;
import com.android.dladle.utils.Logger;
import com.android.dladle.utils.NetworkConstants;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class NotificationListActivity extends BaseActivity {
    RecyclerAdapterForNotificationList mRecyclerAdapterForNotificationList;
    RecyclerView mRecyclerViewForNotificationList;
    ArrayList<NotificationListModel> notificationListModelArrayList = new ArrayList<>();
    ImageView mBack;
    CustomProgressDialog mCustomProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_list);
        initView();
        performAction();
    }

    private void initView() {
        mRecyclerViewForNotificationList = (RecyclerView) findViewById(R.id.rv_notification_list);
        mBack = (ImageView) findViewById(R.id.back_btn);
        mRecyclerViewForNotificationList.setLayoutManager(new LinearLayoutManager(this));
        mCustomProgressDialog = new CustomProgressDialog(NotificationListActivity.this);

        if (getIntent().hasExtra("migratedFrom")) {
            getNotificationList();
        } else {

            notificationListModelArrayList = (ArrayList<NotificationListModel>) getIntent().getSerializableExtra("notificationList");
            mRecyclerAdapterForNotificationList = new RecyclerAdapterForNotificationList(notificationListModelArrayList, this);
            mRecyclerViewForNotificationList.setAdapter(mRecyclerAdapterForNotificationList);
        }
    }

    private void performAction() {
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void getNotificationList() {
        showProgress(true);
        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.GET, NetworkConstants.Notification.NOTIFICATION_LIST, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        showProgress(false);
                        int length = 0;
                        Logger.printDebug("" + response);
                        if (response.optString("Status").equalsIgnoreCase(NetworkConstants.SUCCESS)) {
                            JSONArray jsonArray = response.optJSONArray("Data");
                            if (jsonArray != null) {
                                length = jsonArray.length();
                                if (length > 0) {
                                    Gson gson = new Gson();
                                    for (int i = 0; i < length; i++) {
                                        JSONObject jsonObject = jsonArray.optJSONObject(i);
                                        NotificationListModel notificationListModel = gson.fromJson(jsonObject.toString(), NotificationListModel.class);

                                        if (!notificationListModel.isRead()) {
                                            String data = notificationListModel.getData();
                                            List<String> items = Arrays.asList(data.split("\\s*,\\s*"));
                                            for (int j = 0; j < items.size(); j++) {
                                                if (items.get(j).contains("houseId")) {
                                                    int index = items.get(j).lastIndexOf(":");
                                                    notificationListModel.setHouseId(Integer.parseInt(items.get(j).substring(index + 1)));
                                                }
                                                if (items.get(j).contains("leaseEndDate")) {
                                                    int index = items.get(j).lastIndexOf(":");
                                                    notificationListModel.setLeaseEnddate(items.get(j).substring(index + 1));
                                                }
                                                if(items.get(j).contains("serviceId"))
                                                {
                                                    int index = items.get(j).lastIndexOf(":");
                                                    notificationListModel.setServiceId(Integer.parseInt(items.get(j).substring(index + 1)));
                                                    Logger.printDebug(items.get(j).substring(index + 1));
                                                }
                                            }
                                            notificationListModelArrayList.add(notificationListModel);
                                            mRecyclerAdapterForNotificationList = new RecyclerAdapterForNotificationList(notificationListModelArrayList, NotificationListActivity.this);
                                            mRecyclerViewForNotificationList.setAdapter(mRecyclerAdapterForNotificationList);
                                        }
                                    }

                                }
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showProgress(false);
                        if (error instanceof TimeoutError) {
                            showMessage(getString(R.string.server_error));
                        } else {
                            showMessage(error.getMessage());
                        }
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        jsonRequest.setTag(NetworkConstants.Login.TAG);
        requestQueue.add(jsonRequest);
    }

    private void showProgress(final boolean show) {
        super.setActionInProgress(show);
        if (show) {
            mCustomProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            mCustomProgressDialog.setCancelable(false);
            mCustomProgressDialog.show();
        } else {
            mCustomProgressDialog.dismiss();
        }
    }
}
