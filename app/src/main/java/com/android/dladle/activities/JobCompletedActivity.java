package com.android.dladle.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.android.dladle.R;

public class JobCompletedActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_completed);
    }
}
