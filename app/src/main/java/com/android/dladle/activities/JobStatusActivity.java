package com.android.dladle.activities;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.android.dladle.R;

public class JobStatusActivity extends BaseActivity {

    private ImageButton mIvButton;
    private ImageView mIvSearch;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_status);
        initView();
        performAction();
    }
    private void initView()
    {
        mIvButton = (ImageButton)findViewById(R.id.back_btn);
        mIvSearch = (ImageView)findViewById(R.id.search_image);
        mIvButton.setVisibility(View.GONE);

    }
    private void performAction()
    {

    }
}
