package com.android.dladle.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.dladle.R;
import com.android.dladle.customui.MyJsonObjectRequest;
import com.android.dladle.model.NotificationListModel;
import com.android.dladle.utils.Logger;
import com.android.dladle.utils.NetworkConstants;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONObject;

public class TenantRemovedActivity extends BaseActivity {
    ImageView mBack;
    Button mOK;
    String mTenantName,mPropertyName;
    TextView mTvTenantRemoveDescription,mTvHeaderText;
    NotificationListModel mNotificationListModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tenant_removed);
        initView();
        performAction();
    }
    private void initView()
    {
        mBack=(ImageView)findViewById(R.id.back_btn);
        mOK=(Button)findViewById(R.id.button_ok);
        mBack.setVisibility(View.GONE);
        mTvTenantRemoveDescription=(TextView)findViewById(R.id.tv_bottom_txt);
        mPropertyName = getIntent().getStringExtra("houseName");
        mTvHeaderText=(TextView)findViewById(R.id.header_text);
        if(getIntent().hasExtra("notification"))
        {
            Gson gson=new Gson();
            mNotificationListModel = gson.fromJson(getIntent().getStringExtra("notification"), NotificationListModel.class);
            if (!mNotificationListModel.isRead()) {
                markNotificationAsRead();
            }
            mTvHeaderText.setText(mNotificationListModel.getTitle());
            mTvTenantRemoveDescription.setText(mNotificationListModel.getBody());
        }
        else {
            if (getIntent().hasExtra("tenantName")) {
                mTenantName = getIntent().getStringExtra("tenantName");
                mTvTenantRemoveDescription.setText(getResources().getString(R.string.tenant_removed_txt, mTenantName, mPropertyName));
            } else {
                mTvHeaderText.setText("Successfully left");
                mTvTenantRemoveDescription.setText("You have successfully left from " + mPropertyName);
            }
        }

    }
    private void performAction()
    {
        mOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TenantRemovedActivity.this, UserProfileActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                if(getIntent().hasExtra("tenantName")) {
                    intent.putExtra(NetworkConstants.JsonResponse.DATA, "landlord");
                }
                else {
                    intent.putExtra(NetworkConstants.JsonResponse.DATA, "tenant");
                }
                startActivity(intent);
            }
        });
    }
    private void markNotificationAsRead() {

        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.POST, NetworkConstants.Notification.NOTIFICATION_READ + String.valueOf(mNotificationListModel.getId()) + "&read=true", null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Logger.printDebug("" + response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof TimeoutError) {
                            showMessage(getString(R.string.server_error));
                        } else {
                            showMessage(error.getMessage());
                        }
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        jsonRequest.setTag(NetworkConstants.Login.TAG);
        requestQueue.add(jsonRequest);
    }
}
