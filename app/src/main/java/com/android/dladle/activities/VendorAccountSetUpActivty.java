package com.android.dladle.activities;

import android.animation.AnimatorInflater;
import android.animation.ObjectAnimator;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.android.dladle.R;
import com.android.dladle.customui.CustomProgressDialog;
import com.android.dladle.customui.MyJsonObjectRequest;
import com.android.dladle.customui.RoundedImageView;
import com.android.dladle.customui.TextViewUtils;
import com.android.dladle.fragments.ProfileEditFragmentVendor;
import com.android.dladle.utils.CommonConstants;
import com.android.dladle.utils.Logger;
import com.android.dladle.utils.NetworkConstants;
import com.android.dladle.utils.NetworkUtils;
import com.android.dladle.utils.PhotoManager;
import com.android.dladle.utils.PreferenceManager;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;

public class VendorAccountSetUpActivty extends BaseActivity {
    private String mFragmentKey, mCellNumber, mExistingAddress;
    private Spinner mVendorType, mExperience;
    private EditText mCellNo, mAddress;
    private ImageView mEditButton, mProfilePicOverlay, mCellNoHelp, mMapView, mBack;
    private RoundedImageView mProfilePic;
    private RadioGroup mTools, mTransport;
    private View mButtonLine, mProfilePhotoContainer, mScrollView;
    private Button mSave, mDelete;
    CustomProgressDialog mCustomProgressDialog;
    private AlertDialog mAlertDialog;
    private boolean isCropSupported = true, mHasTools, mHasTransport, mUpdateInProgress;
    private float mRating;
    private int mInitVendorTypeValue, mInitYearsOfExp;
    private ProgressBar mProgressView;
    private String mAddressValue;
    ArrayList<String> vendorExpKey = new ArrayList<>();
    ArrayList<String> vendorExpValue = new ArrayList<>();
    ArrayList<String> vendorTypeKey = new ArrayList<>();
    ArrayList<String> vendorTypeValue = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor_account_set_up_activty);
        vendorExpKey.add("Select Experience");
        vendorTypeKey.add("Select Type");
        initView();
        setValues();
    }

    private void initView() {
        mProfilePic = (RoundedImageView) findViewById(R.id.profile_pic);
        mEditButton = (ImageView) findViewById(R.id.small_tick);
        mBack = (ImageView) findViewById(R.id.back_btn);
        mEditButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });
        mProfilePicOverlay = (ImageView) findViewById(R.id.profile_pic_overlay);
        mVendorType = (Spinner) findViewById(R.id.vendor_type);
        mExperience = (Spinner) findViewById(R.id.experience);
        mSave = (Button) findViewById(R.id.button_save);
        mDelete = (Button) findViewById(R.id.button_delete);
        mCustomProgressDialog = new CustomProgressDialog(this);
        mTools = (RadioGroup) findViewById(R.id.tools_container).findViewById(R.id.toggle);
        mTransport = (RadioGroup) findViewById(R.id.transport_container).findViewById(R.id.toggle);
        mSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetworkUtils.isNetworkAvailable(VendorAccountSetUpActivty.this)) {
                    doUpdate();
                } else {
                    showMessage(getResources().getString(R.string.no_internet));
                }
            }
        });
        mDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete();
            }
        });
        mCellNo = (EditText) findViewById(R.id.cell_number);
        mCellNoHelp = (ImageView) findViewById(R.id.help);
        mCellNoHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMessage("Enter your cell phone number without country code.");
            }
        });
        mMapView = (ImageView) findViewById(R.id.map);
        mMapView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initMap();
            }
        });
        mAddress = (EditText) findViewById(R.id.address);
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void setValues() {
        PhotoManager.setProfilePic(VendorAccountSetUpActivty.this, mProfilePic);
        mEditButton.setVisibility(View.VISIBLE);
        mEditButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_edit_button));
        getVendorType();

    }

    private void initMap() {
        if (checkPermission(300)) {
            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
            try {
                startActivityForResult(builder.build(this), 400);
            } catch (GooglePlayServicesRepairableException e) {
                e.printStackTrace();
            } catch (GooglePlayServicesNotAvailableException e) {
                e.printStackTrace();
            }
        }
    }

    private void selectImage() {
        dismissDialog();
        final CharSequence[] items = {"Take Photo", "Choose from Gallery", "Remove Picture", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    if (checkPermission(100)) {
                        try {
                            Uri photoUri = PhotoManager.getProfilePhotoURI(VendorAccountSetUpActivty.this);
                            if (null != photoUri) {
                                Intent intent = PhotoManager.getCameraIntentForThumbnail(photoUri);
                                startActivityForResult(intent, 100);
                            }
                        } catch (ActivityNotFoundException ex) {
                            Logger.printError(ex.getMessage());
                        }
                    } else {
                        showMessage("Please enable permission for Camera");
                    }
                } else if (items[item].equals("Choose from Gallery")) {
                    if (checkPermission(200)) {
                        try {
                            startActivityForResult(PhotoManager.galleryIntent(VendorAccountSetUpActivty.this), 200);
                        } catch (ActivityNotFoundException ex) {
                            Logger.printError(ex.getMessage());
                        }
                    } else {
                        showMessage("Please enable permission for Gallery");
                    }
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                } else if (items[item].equals("Remove Picture")) {
                    PhotoManager.removeProfilePic(VendorAccountSetUpActivty.this);
                    PhotoManager.setProfilePic(VendorAccountSetUpActivty.this, mProfilePic);
                }
            }
        });
        mAlertDialog = builder.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == 100) {
            if (grantResults.length >= 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                try {
                    Uri photoUri = PhotoManager.getProfilePhotoURI(VendorAccountSetUpActivty.this);
                    if (null != photoUri) {
                        Intent intent = PhotoManager.getCameraIntentForThumbnail(photoUri);
                        startActivityForResult(intent, 100);
                    }
                } catch (ActivityNotFoundException ex) {
                    Logger.printError(ex.getMessage());
                }
            }
        } else if (requestCode == 200) {
            if (grantResults.length >= 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                try {
                    startActivityForResult(PhotoManager.galleryIntent(VendorAccountSetUpActivty.this), 200);
                } catch (ActivityNotFoundException ex) {
                    Logger.printError(ex.getMessage());
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 100) { // Gallery
                try {
                    Intent i = PhotoManager.getCroppedProfileThumbMailIntent(VendorAccountSetUpActivty.this);
                    if (i != null && isCropSupported) {
                        startActivityForResult(i, 300);
                    } else {
                        //showMessage("Could not load picture from Gallery");
                        PhotoManager.setProfilePic(VendorAccountSetUpActivty.this, mProfilePic);
                    }
                } catch (ActivityNotFoundException ex) {
                    Logger.printError(ex.getMessage());
                } finally {
                    PhotoManager.setProfilePic(VendorAccountSetUpActivty.this, mProfilePic);
                }
            } else if (requestCode == 200) { //Camera
                try {
                    Intent i = PhotoManager.getCroppedProfileThumbMailIntent(VendorAccountSetUpActivty.this);
                    if (i != null && isCropSupported) {
                        startActivityForResult(i, 300);
                    } else {
                        //showMessage("Could not load picture from Gallery");
                        PhotoManager.updateAndCacheProfilePic(VendorAccountSetUpActivty.this, mProfilePic, data.getData());
                    }
                } catch (ActivityNotFoundException ex) {
                    Logger.printError(ex.getMessage());
                } finally {
                    PhotoManager.updateAndCacheProfilePic(VendorAccountSetUpActivty.this, mProfilePic, data.getData());
                }
            } else if (requestCode == 300) { //Crop
                PhotoManager.setProfilePic(VendorAccountSetUpActivty.this, mProfilePic);
            } else if (requestCode == 400) { // MAP
                if (resultCode == RESULT_OK) {
                    Place place = PlacePicker.getPlace(VendorAccountSetUpActivty.this, data);
                    //showMessage("Place is : "+place.getAddress());
                    mAddress.setText(place.getAddress());
                    mAddressValue = (new StringBuilder().append(place.getLatLng().latitude).append(";").append(place.getLatLng().longitude)).toString();
                }
            }
        }
    }


    private void delete() {
        dismissDialog();
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(VendorAccountSetUpActivty.this);
        alertDialogBuilder.setMessage(R.string.delete_account_msg);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                showMessage("To be implemented");
            }
        });

        alertDialogBuilder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Do Nothing
            }
        });

        mAlertDialog = alertDialogBuilder.create();
        mAlertDialog.show();
    }

    private void dismissDialog() {
        if (null != mAlertDialog && mAlertDialog.isShowing()) {
            mAlertDialog.dismiss();
        }
    }


    private boolean checkPermission(final int type) {
        if (VendorAccountSetUpActivty.this != null && !VendorAccountSetUpActivty.this.isFinishing()) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                return true;
            }
            ArrayList<String> arr = new ArrayList<String>();
            ArrayList<String> arrFinal = new ArrayList<String>();
            if (200 == type) {
                arr.add(READ_EXTERNAL_STORAGE);
            } else if (100 == type) {
                arr.add(CAMERA);
            } else if (300 == type) {
                arr.add(ACCESS_FINE_LOCATION);
            } else {
                arr.add(CAMERA);
                arr.add(READ_EXTERNAL_STORAGE);
                arr.add(ACCESS_FINE_LOCATION);
            }
            arrFinal.addAll(arr);
            for (String s : arr) {
                if (VendorAccountSetUpActivty.this.checkSelfPermission(s) == PackageManager.PERMISSION_GRANTED) {
                    arrFinal.remove(s);
                }
            }
            if (arrFinal.size() == 0) {
                return true;
            }
            String[] a = arrFinal.toArray(new String[arrFinal.size()]);
            requestPermissions(a, type);
        }
        return false;
    }


    private void doUpdate() {


        if (!NetworkUtils.isNetworkAvailable(VendorAccountSetUpActivty.this)) {
            Logger.printInfo("Not connected to internet");
            showMessage(getString(R.string.no_internet));
            mSave.setEnabled(true);
            mDelete.setEnabled(true);
            return;
        }

        // Reset errors.
        mCellNo.setError(null);
        mAddress.setError(null);

        // Store values at the time of the registration attempt.
        String cellNumber = TextViewUtils.removeEndSpaces(mCellNo.getText().toString());
        String address = mAddress.getText().toString();
        String vendorType = getVendorType(mVendorType.getSelectedItemPosition());
        String experience = getExperienceType(mExperience.getSelectedItemPosition());
        boolean hasTools = mTools.getCheckedRadioButtonId() == R.id.yes;
        boolean hasTransport = mTransport.getCheckedRadioButtonId() == R.id.yes;

        boolean cancel = false;
        View focusView = null;

        // Check if id number is valid or not.
        if (TextUtils.isEmpty(cellNumber) || !TextUtils.isDigitsOnly(cellNumber)
                || cellNumber.length() < 10) {
            mCellNo.setError(getString(R.string.invalid_cell_number));
            focusView = mCellNo;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(mAddressValue)) {
            if (TextUtils.isEmpty(address)) {
                mAddress.setError(getString(R.string.error_field_required));
                focusView = mAddress;
                cancel = true;
            } else if (mAddress.length() > 50) {
                showMessage("This may not be the exact location, Please use address picker from Maps");
                focusView = mAddress;
                cancel = true;
            }
        } else {
            address = mAddressValue;
        }


        // Check for last name.
        if (TextUtils.isEmpty(experience)) {
            focusView = mExperience;
            showMessage("Select Years of Experience");
            cancel = true;
        }


        // Check for first name
        if (TextUtils.isEmpty(vendorType)) {
            focusView = mVendorType;
            showMessage("Select your work type.");
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            if (null != focusView) {
                focusView.requestFocus();
            }
            mSave.setEnabled(true);
            mDelete.setEnabled(true);
        } else {
            mUpdateInProgress = true;
            doServerInteraction(address, cellNumber, experience,
                    vendorType, hasTools, hasTransport);
        }
    }

    private String getVendorType(int position) {
        if (position ==0)
        {
            return "";
        }
        else {
            return vendorTypeValue.get(position);
        }
    }

    private String getExperienceType(int position) {
        if (position ==0)
        {
            return "";
        }
        else {
            return vendorExpValue.get(position);
        }
    }

    private void doServerInteraction(String address, String cellNum, String experience,
                                     String serviceType, boolean hasTools, boolean hasTransport) {
        Logger.printDebug("Server interaction started");
        // Show a progress spinner
        showProgress(true);
        JSONObject request = new JSONObject();
        try {
            request.put(NetworkConstants.UpdateVendor.BUSINESS_ADDR, address);
            request.put(NetworkConstants.UpdateVendor.CELL_NUM, cellNum);
            request.put(NetworkConstants.UpdateVendor.EXPERIENCE, experience);
            request.put(NetworkConstants.UpdateVendor.HAS_TOOL, hasTools);
            request.put(NetworkConstants.UpdateVendor.HAS_TRANSPORT, hasTransport);
            request.put(NetworkConstants.UpdateVendor.SERVICE_TYPE, serviceType);
            Logger.printDebug("JSON : " + request);
        } catch (Exception e) {
            showProgress(false);
            e.printStackTrace();
        }
        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.POST, NetworkConstants.UpdateVendor.URL, request,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Logger.printDebug("" + response);
                        showProgress(false);
                        try {
                            String status = response.getString(NetworkConstants.JsonResponse.STATUS);
                            String message = response.getString(NetworkConstants.JsonResponse.MESSAGE);
                            showMessage(message);
                            if (NetworkConstants.JsonResponse.STATUS_SUCCESS.equalsIgnoreCase(status)) {
                                PreferenceManager.getInstance(getApplicationContext()).setVendorAccountStatus(true);
                                PreferenceManager.getInstance(getApplicationContext()).setVendorType(mVendorType.getSelectedItem().toString());
                                Intent intent = new Intent(VendorAccountSetUpActivty.this, VendorProfileUpdatedActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        } catch (JSONException e) {
                            Logger.printError("Unable to parse JSON : " + e.getMessage());
                            showProgress(false);
                            showMessage(getString(R.string.unexpected_error));
                        } catch (Exception ex) {
                            // Do Nothing -- Temp fix for server error
                            mProgressView.setVisibility(View.GONE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showProgress(false);
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(VendorAccountSetUpActivty.this);
        jsonRequest.setTag(NetworkConstants.UpdateVendor.TAG);
        requestQueue.add(jsonRequest);
    }

    private void showProgress(final boolean show) {
        super.setActionInProgress(show);
        if (show) {
            mCustomProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            mCustomProgressDialog.setCancelable(false);
            mCustomProgressDialog.show();
        } else {
            mCustomProgressDialog.dismiss();
        }
    }

    private void getVendorExp() {
        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.GET, NetworkConstants.Select.GET_VENDOR_EXP, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Logger.printDebug("" + response);
                        showProgress(false);
                        try {
                            if (response.optString("Status").equalsIgnoreCase("success")) {
                                JSONObject data = response.optJSONObject("Data");
                                Iterator<String> iterator = data.keys();
                                while (iterator.hasNext()) {
                                    String key = iterator.next();
                                    vendorExpKey.add(key);
                                    vendorExpValue.add(data.optString(key));
                                    // Log.i("TAG","key:"+key +"--Value::"+categoryJSONObj.optString(key);
                                }
                                ArrayAdapter<String> adapter = new ArrayAdapter<String>(VendorAccountSetUpActivty.this,
                                        R.layout.spinner_item, vendorExpKey);
                                mExperience.setAdapter(adapter);
                            } else {
                                // Logger.printError("Unable to parse JSON : " + e.getMessage());
                                showProgress(false);
                                showMessage(response.optString("Message"));
                            }
                        } catch (Exception e) {
                            Logger.printError("Unable to parse JSON : " + e.getMessage());
                            showProgress(false);
                            showMessage(getString(R.string.unexpected_error));
                            mProgressView.setVisibility(View.GONE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showProgress(false);
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(VendorAccountSetUpActivty.this);
        jsonRequest.setTag(NetworkConstants.UpdateVendor.TAG);
        requestQueue.add(jsonRequest);
    }

    private void getVendorType() {
        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.GET, NetworkConstants.Select.GET_VENDOR_TYPE, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Logger.printDebug("" + response);
                        showProgress(false);
                        try {
                            getVendorExp();
                            if (response.optString("Status").equalsIgnoreCase("success")) {
                                JSONObject data = response.optJSONObject("Data");
                                Iterator<String> iterator = data.keys();
                                while (iterator.hasNext()) {
                                    String key = iterator.next();
                                    vendorTypeKey.add(key);
                                    vendorTypeValue.add(data.optString(key));
                                    // Log.i("TAG","key:"+key +"--Value::"+categoryJSONObj.optString(key);
                                }
                                ArrayAdapter<String> adapter = new ArrayAdapter<String>(VendorAccountSetUpActivty.this,
                                        R.layout.spinner_item, vendorTypeKey);
                                mVendorType.setAdapter(adapter);
                            } else {
                                // Logger.printError("Unable to parse JSON : " + e.getMessage());
                                showProgress(false);
                                showMessage(response.optString("Message"));
                            }
                        } catch (Exception e) {
                            Logger.printError("Unable to parse JSON : " + e.getMessage());
                            showProgress(false);
                            showMessage(getString(R.string.unexpected_error));
                            mProgressView.setVisibility(View.GONE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showProgress(false);
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(VendorAccountSetUpActivty.this);
        jsonRequest.setTag(NetworkConstants.UpdateVendor.TAG);
        requestQueue.add(jsonRequest);
    }


}

