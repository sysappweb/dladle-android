package com.android.dladle.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Spinner;

import com.android.dladle.R;

public class ProfileSettingsActivity extends AppCompatActivity {
    ImageView mBack, mProfileImage;
    RatingBar mUserRating;
    EditText mInitialName, mSurName, mEmail, mCellNo, mUserType, mRating;
    Spinner mHome;
    Button mSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_settings);
        initView();
        performAction();
    }

    private void initView() {
        mBack = (ImageView) findViewById(R.id.back_btn);
        mProfileImage = (ImageView) findViewById(R.id.profile_pic);
        mUserRating = (RatingBar) findViewById(R.id.rating_bar);
        mInitialName = (EditText) findViewById(R.id.tv_name);
        mSurName = (EditText) findViewById(R.id.tv_surname);
        mEmail = (EditText) findViewById(R.id.tv_email);
        mCellNo = (EditText) findViewById(R.id.tv_cellno);
        mUserType = (EditText) findViewById(R.id.et_usertype);
        mRating = (EditText) findViewById(R.id.et_rating);
        mHome = (Spinner) findViewById(R.id.home_picker);
        mSave=(Button)findViewById(R.id.button_save);
    }

    private void performAction() {
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }
}
