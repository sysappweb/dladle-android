package com.android.dladle.activities;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.dladle.R;
import com.android.dladle.customui.CustomProgressDialog;
import com.android.dladle.customui.MyJsonObjectRequest;
import com.android.dladle.utils.AudioUtils;
import com.android.dladle.utils.ImageUtils;
import com.android.dladle.utils.Logger;
import com.android.dladle.utils.NetworkConstants;
import com.android.dladle.utils.NetworkUtils;
import com.android.dladle.utils.PhotoManager;
import com.android.dladle.utils.PreferenceManager;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class RequestVendorActivity extends BaseActivity {

    LinearLayout mMoreImageContaniner, mAddMoreImage;
    Button mOK;
    Calendar mCalenndar;
    RadioButton mRbCalendar, mRbClock, mRbYes, mRbNo, mRbLandlordPay, mRbTenantPay;
    String AudioSavePathInDevice = "";
    MediaRecorder mediaRecorder;
    ImageView mRecord, mIvBack;
    public static final int RequestPermissionCode = 1;
    String[] arraySpinner1 = {"Type of vendor needed", "Plumber", "Painter", "Electrician"};
    ArrayAdapter<String> adapter;
    Spinner mSpVendorType;
    ImageView mImageOne, mImageTwo, mImageThree, mImageFour;
    private AlertDialog mAlertDialog;
    private boolean isCropSupported = true;
    EditText mEtShortDescription;
    int imageNo = 0;
    CustomProgressDialog mCustomProgressDialog;
    LinearLayout mPayContainer;
    ArrayList<String> imagePath = new ArrayList<>();
    ArrayList<String> imagename = new ArrayList<>();
    AlertDialog alertDialog;
    int mHouseId = 0;
    RadioGroup mToggle;
    TextView mTvDateTime;
    ArrayList<String> vendorTypeKey = new ArrayList<>();
    ArrayList<String> vendorTypeValue = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_vendor);
        initView();
        performAction();
    }

    private void initView() {
        mAddMoreImage = (LinearLayout) findViewById(R.id.tv_add_more_image);
        mMoreImageContaniner = (LinearLayout) findViewById(R.id.more_image_conatiner);
        mCustomProgressDialog = new CustomProgressDialog(this);
        mOK = (Button) findViewById(R.id.ok_button);
        mCalenndar = Calendar.getInstance();
        mRbCalendar = (RadioButton) findViewById(R.id.calendar);
        mRbClock = (RadioButton) findViewById(R.id.time);
        mRbYes = (RadioButton) findViewById(R.id.yes);
        mRbNo = (RadioButton) findViewById(R.id.no);
        mRecord = (ImageView) findViewById(R.id.record);
        mSpVendorType = (Spinner) findViewById(R.id.vendor_type);
        mImageOne = (ImageView) findViewById(R.id.image_one);
        mImageTwo = (ImageView) findViewById(R.id.image_two);
        mImageThree = (ImageView) findViewById(R.id.image_three);
        mImageFour = (ImageView) findViewById(R.id.image_four);
        mEtShortDescription = (EditText) findViewById(R.id.et_short_descriptiob);
        mPayContainer = (LinearLayout) findViewById(R.id.pay);
        mRbLandlordPay = (RadioButton) findViewById(R.id.landlord_pay);
        mRbTenantPay = (RadioButton) findViewById(R.id.tenant_pay);
        mToggle = (RadioGroup) findViewById(R.id.toggle_date);
        mTvDateTime = (TextView) findViewById(R.id.tv_date_time);
        mRbCalendar.setText(getCurrentDate());
        mRbClock.setText(getCurrentTime());
        mRbNo.setChecked(true);
        mIvBack = (ImageView) findViewById(R.id.back_btn);
        mHouseId = getIntent().getIntExtra("houseId", 0);
        Logger.printDebug("HouseId " + mHouseId);
        imagePath.add("");
        imagePath.add("");
        imagePath.add("");
        imagePath.add("");
        imagename.add("");
        imagename.add("");
        imagename.add("");
        imagename.add("");
        getVendorType();

    }

    private void performAction() {
        mIvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mAddMoreImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMoreImageContaniner.setVisibility(View.VISIBLE);
                mAddMoreImage.setVisibility(View.GONE);
            }
        });
        mRbCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(RequestVendorActivity.this, date, mCalenndar
                        .get(Calendar.YEAR), mCalenndar.get(Calendar.MONTH),
                        mCalenndar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.show();
            }
        });
        mRbClock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int hour = mCalenndar.get(Calendar.HOUR_OF_DAY);
                int minute = mCalenndar.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(RequestVendorActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        String AM_PM = " AM";
                        String mm_precede = "";
                        if (selectedHour >= 12) {
                            AM_PM = " PM";
                            if (selectedHour >= 13 && selectedHour < 24) {
                                selectedHour -= 12;
                            } else {
                                selectedHour = 12;
                            }
                        } else if (selectedHour == 0) {
                            selectedHour = 12;
                        }
                        if (selectedMinute < 10) {
                            mm_precede = "0";
                        }
                        mRbClock.setText(selectedHour + ":" + mm_precede + selectedMinute + AM_PM);
                    }
                }, hour, minute, false);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });
        mRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recordAudio();
            }
        });

        mImageOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageNo = 1;
                selectImage();
            }
        });
        mImageTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageNo = 2;
                selectImage();
            }
        });
        mImageThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageNo = 3;
                selectImage();
            }
        });
        mImageFour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageNo = 4;
                selectImage();
            }
        });
        mOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetworkUtils.isNetworkAvailable(RequestVendorActivity.this)) {
                    if (validationChecking()) {
                        sendRequestToVendor();
                    }
                } else {
                    showMessage(getResources().getString(R.string.no_internet));
                }


            }
        });
        mRbYes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mToggle.setVisibility(View.GONE);
                    mTvDateTime.setVisibility(View.GONE);
                } else {
                    mToggle.setVisibility(View.VISIBLE);
                    mTvDateTime.setVisibility(View.VISIBLE);
                }

            }
        });


    }

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            mCalenndar.set(Calendar.YEAR, year);
            mCalenndar.set(Calendar.MONTH, monthOfYear);
            mCalenndar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            String date = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");
            Date testDate = null;
            try {
                testDate = sdf.parse(date);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            SimpleDateFormat formatter = new SimpleDateFormat("d MMM ''yy");
            String newFormat = formatter.format(testDate);
            mRbCalendar.setText(newFormat);

        }

    };

    private void recordAudio() {
        if (checkPermissionForAudio()) {

            File folder = new File(Environment.getExternalStorageDirectory() + "/dladle/vendor/audio");
            boolean success = true;
            if (!folder.exists()) {
                Logger.printDebug("not exist");
                success = folder.mkdirs();
            }
            if (success) {
                // Do something on success
                Logger.printDebug("success");
            } else {
                // Do something else on failure
                Logger.printDebug("Failed");
            }


            AudioSavePathInDevice =
                    Environment.getExternalStorageDirectory().getAbsolutePath() +
                            "/dladle/vendor/audio/" + getCurrentDateTime() + ".mp3";

            MediaRecorderReady();

            try {
                mediaRecorder.prepare();
                popUpForRecording();
                mediaRecorder.start();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        alertDialog.dismiss();
                        mediaRecorder.stop();
                        mediaRecorder.release();
                    }
                }, 60000);
            } catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            Toast.makeText(RequestVendorActivity.this, "Recording started",
                    Toast.LENGTH_LONG).show();
        } else {
            requestPermission();
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(RequestVendorActivity.this, new
                String[]{WRITE_EXTERNAL_STORAGE, RECORD_AUDIO}, RequestPermissionCode);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case RequestPermissionCode:
                if (grantResults.length > 0) {
                    boolean StoragePermission = grantResults[0] ==
                            PackageManager.PERMISSION_GRANTED;
                    boolean RecordPermission = grantResults[1] ==
                            PackageManager.PERMISSION_GRANTED;

                    if (StoragePermission && RecordPermission) {
                        Toast.makeText(RequestVendorActivity.this, "Permission Granted",
                                Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(RequestVendorActivity.this, "Permission Denied",
                                Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }


    public void MediaRecorderReady() {
        mediaRecorder = new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        mediaRecorder.setOutputFile(AudioSavePathInDevice);
    }

    //
    private void selectImage() {

        final CharSequence[] items = {"Take Photo", "Choose from Gallery", "Remove Picture", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    if (checkPermission(100)) {


                      /*  try {
                            Uri photoUri = PhotoManager.getProfilePhotoURI(RequestVendorActivity.this);
                            if (null != photoUri) {
                                Intent intent = PhotoManager.getCameraIntent();
                                startActivityForResult(intent, 100);
                            }
                        } catch (ActivityNotFoundException ex) {
                            Logger.printError(ex.getMessage());
                        }*/
                        Intent i = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        if (ImageUtils.hasImageCaptureBug()) {
                            i.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File("/sdcard/tmp")));
                        } else {
                            i.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        }
                        startActivityForResult(i, 100);
                    }



                    else {
                        showMessage("Please enable permission for Camera");
                    }
                } else if (items[item].equals("Choose from Gallery")) {
                    if (checkPermission(200)) {
                        try {
                            startActivityForResult(PhotoManager.galleryIntent(RequestVendorActivity.this), 200);
                        } catch (ActivityNotFoundException ex) {
                            Logger.printError(ex.getMessage());
                        }
                    } else {
                        showMessage("Please enable permission for Gallery");
                    }
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                } else if (items[item].equals("Remove Picture")) {
                    if (imageNo == 1) {
                        mImageOne.setImageResource(R.drawable.ic_camera_icon);
                        mImageOne.setScaleType(ImageView.ScaleType.CENTER);
                        imagePath.add(0, "");
                    } else if (imageNo == 2) {
                        mImageTwo.setImageResource(R.drawable.ic_camera_icon);
                        mImageTwo.setScaleType(ImageView.ScaleType.CENTER);
                        imagePath.add(1, "");
                    } else if (imageNo == 3) {
                        mImageThree.setImageResource(R.drawable.ic_camera_icon);
                        mImageThree.setScaleType(ImageView.ScaleType.CENTER);
                        imagePath.add(2, "");
                    } else {
                        mImageFour.setImageResource(R.drawable.ic_camera_icon);
                        mImageFour.setScaleType(ImageView.ScaleType.CENTER);
                        imagePath.add(3, "");
                    }
                }
            }
        });
        mAlertDialog = builder.create();
        mAlertDialog.show();
    }

    private boolean checkPermission(final int type) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        ArrayList<String> arr = new ArrayList<String>();
        ArrayList<String> arrFinal = new ArrayList<String>();
        if (200 == type) {
            arr.add(READ_EXTERNAL_STORAGE);
        } else if (100 == type) {
            arr.add(CAMERA);
        } else if (300 == type) {
            arr.add(ACCESS_FINE_LOCATION);
        } else {
            arr.add(CAMERA);
            arr.add(READ_EXTERNAL_STORAGE);
            arr.add(ACCESS_FINE_LOCATION);
        }
        arrFinal.addAll(arr);
        for (String s : arr) {
            if (RequestVendorActivity.this.checkSelfPermission(s) == PackageManager.PERMISSION_GRANTED) {
                arrFinal.remove(s);
            }
        }
        if (arrFinal.size() == 0) {
            return true;
        }
        String[] a = arrFinal.toArray(new String[arrFinal.size()]);
        requestPermissions(a, type);
        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 200) { // Gallery
                try {
                    Intent i = PhotoManager.getCroppedProfileThumbMailIntent(RequestVendorActivity.this);
                    if (i != null && isCropSupported) {
                        // startActivityForResult(i, 300);
                    } else {
                        if (imageNo == 1) {
                            String path = PhotoManager.updateAndCachePropertyPic(RequestVendorActivity.this, mImageOne, data.getData());
                            imagePath.add(0, path);
                            imagename.add(0, getFileName(path));
                        } else if (imageNo == 2) {
                            String path = PhotoManager.updateAndCachePropertyPic(RequestVendorActivity.this, mImageTwo, data.getData());
                            imagePath.add(1, path);
                            imagename.add(1, getFileName(path));
                        } else if (imageNo == 3) {
                            String path = PhotoManager.updateAndCachePropertyPic(RequestVendorActivity.this, mImageThree, data.getData());
                            imagePath.add(2, path);
                            imagename.add(2, getFileName(path));
                        } else {
                            String path = PhotoManager.updateAndCachePropertyPic(RequestVendorActivity.this, mImageFour, data.getData());
                            imagePath.add(3, path);
                            imagename.add(3, getFileName(path));
                        }

                    }
                } catch (ActivityNotFoundException ex) {
                    Logger.printError(ex.getMessage());
                } finally {
                    if (imageNo == 1) {
                        String path = PhotoManager.updateAndCachePropertyPic(RequestVendorActivity.this, mImageOne, data.getData());
                        imagePath.add(0, path);
                        imagename.add(0, getFileName(path));
                    } else if (imageNo == 2) {
                        String path = PhotoManager.updateAndCachePropertyPic(RequestVendorActivity.this, mImageTwo, data.getData());
                        imagePath.add(1, path);
                        imagename.add(1, getFileName(path));
                    } else if (imageNo == 3) {
                        String path = PhotoManager.updateAndCachePropertyPic(RequestVendorActivity.this, mImageThree, data.getData());
                        imagePath.add(2, path);
                        imagename.add(2, getFileName(path));
                    } else {
                        String path = PhotoManager.updateAndCachePropertyPic(RequestVendorActivity.this, mImageFour, data.getData());
                        imagePath.add(3, path);
                        imagename.add(3, getFileName(path));
                    }
                }
            } else if (requestCode == 100) { //Camera
                try {
                    Intent i = PhotoManager.getCroppedProfileThumbMailIntent(RequestVendorActivity.this);
                    if (i != null && isCropSupported) {
                        // startActivityForResult(i, 300);
                    } else {
                        if (imageNo == 1) {
                            String path = PhotoManager.updateAndCachePropertyPic(RequestVendorActivity.this, mImageOne, data.getData());
                            imagePath.add(0, path);
                            imagename.add(0, getFileName(path));
                        } else if (imageNo == 2) {
                            String path = PhotoManager.updateAndCachePropertyPic(RequestVendorActivity.this, mImageTwo, data.getData());
                            imagePath.add(1, path);
                            imagename.add(1, getFileName(path));
                        } else if (imageNo == 3) {
                            String path = PhotoManager.updateAndCachePropertyPic(RequestVendorActivity.this, mImageThree, data.getData());
                            imagePath.add(2, path);
                            imagename.add(2, getFileName(path));
                        } else {
                            String path = PhotoManager.updateAndCachePropertyPic(RequestVendorActivity.this, mImageFour, data.getData());
                            imagePath.add(3, path);
                            imagename.add(3, getFileName(path));
                        }
                    }
                } catch (ActivityNotFoundException ex) {
                    Logger.printError(ex.getMessage());
                } finally {


                    Uri u=null;
                    if (ImageUtils.hasImageCaptureBug()) {
                        File fi = new File("/sdcard/tmp");
                        try {
                            u = Uri.parse(android.provider.MediaStore.Images.Media.insertImage(getContentResolver(), fi.getAbsolutePath(), null, null));
                            if (!fi.delete()) {
                                Log.i("logMarker", "Failed to delete " + fi);
                            }
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                    } else {
                        u = data.getData();
                    }


                    if (imageNo == 1) {
                        String path = PhotoManager.updateAndCachePropertyPic(RequestVendorActivity.this, mImageOne, u);
                        imagePath.add(0, path);
                        imagename.add(0, getFileName(path));
                    } else if (imageNo == 2) {
                        String path = PhotoManager.updateAndCachePropertyPic(RequestVendorActivity.this, mImageTwo, u);
                        imagePath.add(1, path);
                      imagename.add(1, getFileName(path));
                    } else if (imageNo == 3) {
                        String path = PhotoManager.updateAndCachePropertyPic(RequestVendorActivity.this, mImageThree, u);
                        imagePath.add(2, path);
                        imagename.add(2, getFileName(path));
                    } else {
                        String path = PhotoManager.updateAndCachePropertyPic(RequestVendorActivity.this, mImageFour, u);
                        imagePath.add(3, path);
                        imagename.add(3, getFileName(path));
                    }
                }
            } else if (requestCode == 300) { //Crop
                if (imageNo == 1) {
                    String path = PhotoManager.updateAndCachePropertyPic(RequestVendorActivity.this, mImageOne, data.getData());
                    imagePath.add(0, path);
                    imagename.add(0, getFileName(path));
                } else if (imageNo == 2) {
                    String path = PhotoManager.updateAndCachePropertyPic(RequestVendorActivity.this, mImageTwo, data.getData());
                    imagePath.add(1, path);
                    imagename.add(1, getFileName(path));
                } else if (imageNo == 3) {
                    String path = PhotoManager.updateAndCachePropertyPic(RequestVendorActivity.this, mImageThree, data.getData());
                    imagePath.add(2, path);
                    imagename.add(2, getFileName(path));
                } else {
                    String path = PhotoManager.updateAndCachePropertyPic(RequestVendorActivity.this, mImageFour, data.getData());
                    imagePath.add(3, path);
                    imagename.add(3, getFileName(path));
                }
            }
        }
    }

    private boolean checkPermissionForAudio() {
        String permission = "android.permission.RECORD_AUDIO";
        int result = RequestVendorActivity.this.checkCallingOrSelfPermission(permission);
        return (result == PackageManager.PERMISSION_GRANTED);
    }

    public void writeToFile(String data) {
        // Get the directory for the user's public pictures directory.
        final File path =
                Environment.getExternalStoragePublicDirectory
                        (
                                //Environment.DIRECTORY_PICTURES
                                Environment.DIRECTORY_DCIM + "/"
                        );

        // Make sure the path directory exists.
        if (!path.exists()) {
            // Make it, if it doesn't exit
            path.mkdirs();
        }

        final File file = new File(path, "input.txt");

        // Save your stream, don't forget to flush() it before closing it.

        try {
            file.createNewFile();
            FileOutputStream fOut = new FileOutputStream(file);
            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
            myOutWriter.append(data);

            myOutWriter.close();

            fOut.flush();
            fOut.close();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    private void sendRequestToVendor() {
        showProgress(true);
        Log.d("ServiceReq","1");
        JSONObject input = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        try {
            if (mRbYes.isChecked()) {
                input.put("emergency", true);
                input.put("serviceNeedTime", "");
            } else {
                input.put("emergency", false);
                input.put("serviceNeedTime", getServiceDateTime());
            }
           /* if (mRbTenantPay.isChecked()) {
                input.put("ownPay", true);
            } else {
                input.put("ownPay", false);¬
            }*/
            if (!AudioSavePathInDevice.equals("")) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("documentType", "AUDIO");
                jsonObject.put("base64", new AudioUtils().convertAudioToBaseString(AudioSavePathInDevice));
                jsonObject.put("fileName", getFileName(AudioSavePathInDevice));
                jsonArray.put(jsonObject);
            }
            input.put("serviceType", vendorTypeValue.get(mSpVendorType.getSelectedItemPosition()-1));
            input.put("serviceNote", mEtShortDescription.getText().toString());

            input.put("serviceDocuments", jsonArray);
            input.put("houseId", mHouseId);

            for (int i = 0; i < imagePath.size(); i++) {
                if (imagePath.get(i) != null && !imagePath.get(i).equals("")) {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("documentType", "IMAGE");
                    jsonObject.put("base64", ImageUtils.readFileAsBase64String(imagePath.get(i)));
                    jsonObject.put("fileName", imagename.get(i));

                    jsonArray.put(jsonObject);
                }


            }
            input.put("serviceDocuments", jsonArray);
            Logger.printDebug(input.toString());


        } catch (JSONException e) {
            e.printStackTrace();
        }


        writeToFile(input.toString());

        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.POST, NetworkConstants.VendorController.REQUEST_VENDOR, input,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Logger.printDebug("" + response);

                        showProgress(false);
                        showMessage(response.optString("Message"));
                        if (response.optString("Status").equalsIgnoreCase(NetworkConstants.SUCCESS)) {

                            int serviceId = response.optInt("Data");

                            Intent intent = new Intent(RequestVendorActivity.this, VendorSearchActivity.class);
                            intent.putExtra("serviceId", serviceId);
                            intent.putExtra("vendorType",mSpVendorType.getSelectedItem().toString());
                            startActivity(intent);
                            finish();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showProgress(false);
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(RequestVendorActivity.this);
        jsonRequest.setTag(NetworkConstants.UpdateVendor.TAG);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(NetworkConstants.TIMOUT_TIME,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonRequest);
    }

    private void showProgress(final boolean show) {
        super.setActionInProgress(show);
        if (show) {
            mCustomProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            mCustomProgressDialog.setCancelable(false);
            mCustomProgressDialog.show();
        } else {
            mCustomProgressDialog.dismiss();
        }
    }
    private String getCurrentDate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("d MMM ''yy");
        return df.format(c.getTime());
    }
    private String getCurrentTime() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat formatDate = new SimpleDateFormat("hh:mm a");
        return formatDate.format(c.getTime());

    }
    private String getCurrentDateTime() {
        Calendar c = Calendar.getInstance();
        return String.valueOf(c.get(Calendar.MILLISECOND));
    }
    private String getServiceDateTime() {
        String date = mRbCalendar.getText().toString();
        SimpleDateFormat sdf = new SimpleDateFormat("d MMM ''yy");
        Date testDate = null;
        try {
            testDate = sdf.parse(date);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String newFormat = formatter.format(testDate);
        return newFormat + " " + convertTo24Hour(mRbClock.getText().toString());
    }
    private void popUpForRecording() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.recording_dialog, null);
        dialogBuilder.setView(dialogView);
        final TextView textView = (TextView) dialogView.findViewById(R.id.tv_timer);
        final Button stopRecording = (Button) dialogView.findViewById(R.id.button_stop_record);
        alertDialog = dialogBuilder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
        new CountDownTimer(60000, 1000) {

            public void onTick(long millisUntilFinished) {
                textView.setText("00 : " + String.valueOf(millisUntilFinished / 1000));
            }

            public void onFinish() {
                textView.setText("Done");
            }

        }.start();
        stopRecording.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                try {
                    mediaRecorder.stop();
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                } finally {
                    mediaRecorder.release();
                }
            }
        });

    }
    private boolean validationChecking() {
        String imageOnePath = imagePath.get(0);
        String imageTwoPath = imagePath.get(1);
        String imagePathThree = imagePath.get(2);
        String imagePathFour = imagePath.get(3);
        String desc = mEtShortDescription.getText().toString();
        if (mSpVendorType.getSelectedItemPosition() == 0) {
            showMessage("Please select vendor type");
            return false;
        } else if (imageOnePath.equals("") && imageTwoPath.equals("") && imagePathThree.equals("") && imagePathFour.equals("") && desc.equals("") && AudioSavePathInDevice.equals("")) {
            showMessage("Please provide image or audio or short decription");
            return false;
        }
        else {
            return true;
        }



    }
    public String convertTo24Hour(String Time) {
        DateFormat f1 = new SimpleDateFormat("hh:mm a"); //11:00 pm
        Date d = null;
        try {
            d = f1.parse(Time);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        DateFormat f2 = new SimpleDateFormat("HH:mm:ss");
        String x = f2.format(d); // "23:00"

        return x;
    }
    public String getFileName(String path) {
        int pos = path.lastIndexOf("/") + 1;

        return path.substring(pos);
    }
    private void getVendorType() {
        showProgress(true);
        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.GET, NetworkConstants.Select.GET_VENDOR_TYPE, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Logger.printDebug("" + response);
                        showProgress(false);
                        try {

                            if (response.optString("Status").equalsIgnoreCase("success")) {
                                JSONObject data = response.optJSONObject("Data");
                                Iterator<String> iterator = data.keys();
                                vendorTypeKey.add("Select Vendor Type");
                                while (iterator.hasNext()) {
                                    String key = iterator.next();
                                    vendorTypeKey.add(key);
                                    vendorTypeValue.add(data.optString(key));
                                    // Log.i("TAG","key:"+key +"--Value::"+categoryJSONObj.optString(key);
                                }
                                adapter = new ArrayAdapter<String>(RequestVendorActivity.this,
                                        R.layout.spinner_item, vendorTypeKey);
                                mSpVendorType.setAdapter(adapter);
                            } else {
                                // Logger.printError("Unable to parse JSON : " + e.getMessage());
                                showProgress(false);
                                showMessage(response.optString("Message"));
                            }
                        } catch (Exception e) {
                            Logger.printError("Unable to parse JSON : " + e.getMessage());
                            showProgress(false);
                            showMessage(getString(R.string.unexpected_error));

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showProgress(false);
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(RequestVendorActivity.this);
        jsonRequest.setTag(NetworkConstants.UpdateVendor.TAG);
        requestQueue.add(jsonRequest);
    }
}