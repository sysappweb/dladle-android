package com.android.dladle.activities;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.dladle.R;
import com.android.dladle.utils.NetworkConstants;

public class InviteSendActivity extends Activity {
TextView mTvDescriptionText;
    Button mBtnOk;
    ImageView mBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_send);
        initView();
        performACtion();
    }
    private void initView()
    {
        mTvDescriptionText=(TextView)findViewById(R.id.tv_bottom_txt);
        mBtnOk=(Button)findViewById(R.id.button_ok);
        mBack=(ImageView)findViewById(R.id.back_btn);
        mBack.setVisibility(View.GONE);
        String landlordName=getIntent().getStringExtra("landlordName");
        mTvDescriptionText.setText(getResources().getString(R.string.invite_send_to_landlord,landlordName,landlordName));
    }
    private void performACtion()
    {
      mBtnOk.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
              Intent intent = new Intent(InviteSendActivity.this, UserProfileActivity.class);
              intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
              intent.putExtra(NetworkConstants.JsonResponse.DATA,"tenant");
              startActivity(intent);
          }
      });
    }

}
