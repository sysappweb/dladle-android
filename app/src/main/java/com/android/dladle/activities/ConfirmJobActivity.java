package com.android.dladle.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.dladle.R;
import com.android.dladle.customui.MyJsonObjectRequest;
import com.android.dladle.customui.ProfileImageView;
import com.android.dladle.utils.Logger;
import com.android.dladle.utils.NetworkConstants;
import com.android.dladle.utils.NetworkUtils;
import com.android.dladle.utils.PreferenceManager;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

public class ConfirmJobActivity extends BaseActivity {

    RelativeLayout mRlBack;
    TextView mTvTotalJob, mTvExperience, mTvJobFees;
    Button mBtnAccept, mBtnDeclined;
    ProfileImageView profileImageView;
    RatingBar ratingBar;
    TextView mTvName, mTvVendorType;
    String mVendorEmailId = "";
    int mServiceId;
    ArrayList<String> mReasonForRejection = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_job);

        initView();
        performAction();

    }

    private void initView() {
        mRlBack = (RelativeLayout) findViewById(R.id.app_header);
        mTvTotalJob = (TextView) findViewById(R.id.tv_vendor_total_job);
        mTvExperience = (TextView) findViewById(R.id.tv_vendor_total_exp);
        mTvJobFees = (TextView) findViewById(R.id.tv_price);
        mBtnAccept = (Button) findViewById(R.id.button_accept);
        mBtnDeclined = (Button) findViewById(R.id.button_decline);
        profileImageView = (ProfileImageView) findViewById(R.id.profile_pic);
        ratingBar = (RatingBar) findViewById(R.id.rating_bar);

        mTvName = (TextView) findViewById(R.id.tv_vendor_name);
        mTvVendorType = (TextView) findViewById(R.id.tv_vendor_occupation);

        if (getIntent().getExtras() != null) {
            mTvExperience.setText("Years of experience: " + getIntent().getStringExtra("yearsOfExperience"));
            mTvTotalJob.setText("Nuber of DlaDle jobs :" + getIntent().getStringExtra("numberOfJobs"));
            mServiceId = getIntent().getIntExtra("serviceId", 0);
            mVendorEmailId = getIntent().getStringExtra("vendorEmailId");

            mTvName.setText(getIntent().getStringExtra("vendorName"));
            mTvVendorType.setText(getIntent().getStringExtra("jobType"));

            mTvJobFees.setText("R" + getIntent().getStringExtra("proposedFeeStartRange") + " - R" + getIntent().getStringExtra("proposedFeeEndRange"));

        }


    }

    private void performAction() {
        mBtnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (NetworkUtils.isNetworkAvailable(ConfirmJobActivity.this)) {
                    acceptSelectedVendor();
                }
                else {
                    showMessage(getResources().getString(R.string.no_internet));
                }

            }
        });
        mBtnDeclined.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetworkUtils.isNetworkAvailable(ConfirmJobActivity.this))
                {
                    getRejectionList();
                }
                else {
                    showMessage(getResources().getString(R.string.no_internet));
                }
            }
        });
        mRlBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


    }

    private void acceptSelectedVendor() {
        final ProgressDialog progressDialog = new ProgressDialog(ConfirmJobActivity.this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();

        JSONObject input = new JSONObject();
        try {
            input.put("serviceId", mServiceId);
            input.put("vendorEmailId", mVendorEmailId);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.POST, NetworkConstants.VendorController.ACCEPT_SELECTED_VENDOR, input,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Logger.printDebug("" + response);
                        progressDialog.hide();

                        showMessage(response.optString("Message"));
                        if (response.optString("Status").equalsIgnoreCase(NetworkConstants.SUCCESS)) {

                            Intent intent = new Intent(ConfirmJobActivity.this, UserProfileActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra(NetworkConstants.JsonResponse.DATA, PreferenceManager.getInstance(ConfirmJobActivity.this).getUserType());
                            startActivity(intent);


                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.hide();
                        if (error instanceof TimeoutError) {
                            //showMessage(getString(R.string.server_error));
                        } else {
                            //showMessage(error.getMessage());
                        }
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        jsonRequest.setTag(NetworkConstants.Login.TAG);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(NetworkConstants.TIMOUT_TIME,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonRequest);
    }

    private void rejectSelectedVendor(String reason) {
        final ProgressDialog progressDialog = new ProgressDialog(ConfirmJobActivity.this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();

        JSONObject input = new JSONObject();
        try {
            input.put("serviceId", mServiceId);
            input.put("vendorEmailId", mVendorEmailId);
            input.put("rejectionReason", reason);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("Input",input.toString());

        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.POST, NetworkConstants.VendorController.REJECT_SELECTED_VENDOR, input,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Logger.printDebug("" + response);
                        progressDialog.hide();

                        showMessage(response.optString("Message"));
                        if (response.optString("Status").equalsIgnoreCase(NetworkConstants.SUCCESS)) {

                            Intent intent = new Intent(ConfirmJobActivity.this, UserProfileActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra(NetworkConstants.JsonResponse.DATA, PreferenceManager.getInstance(ConfirmJobActivity.this).getUserType());
                            startActivity(intent);


                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.hide();
                        if (error instanceof TimeoutError) {
                            //showMessage(getString(R.string.server_error));
                        } else {
                            //showMessage(error.getMessage());
                        }
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        jsonRequest.setTag(NetworkConstants.Login.TAG);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(NetworkConstants.TIMOUT_TIME,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonRequest);
    }

    private void getRejectionList() {
        final ProgressDialog progressDialog = new ProgressDialog(ConfirmJobActivity.this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();


        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.GET, NetworkConstants.Select.GET_REJECTION_REASON, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Logger.printDebug("" + response);
                        progressDialog.hide();

                        showMessage(response.optString("Message"));
                        if (response.optString("Status").equalsIgnoreCase(NetworkConstants.SUCCESS)) {


                            JSONObject data = response.optJSONObject("Data");
                            Iterator<String> iterator = data.keys();

                            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(ConfirmJobActivity.this, android.R.layout.select_dialog_singlechoice);
                            while (iterator.hasNext()) {
                                String key = iterator.next();
                                arrayAdapter.add(key);
                                // vendorTypeValue.add(data.optString(key));
                                mReasonForRejection.add(data.optString(key));
                                // Log.i("TAG","key:"+key +"--Value::"+categoryJSONObj.optString(key);
                            }
                            showReason(arrayAdapter);
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.hide();
                        if (error instanceof TimeoutError) {
                            //showMessage(getString(R.string.server_error));
                        } else {
                            //showMessage(error.getMessage());
                        }
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        jsonRequest.setTag(NetworkConstants.Login.TAG);
        requestQueue.add(jsonRequest);
    }


    private void showReason(final ArrayAdapter<String> arrayAdapter) {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(ConfirmJobActivity.this);
        builderSingle.setTitle("Select reason");


        builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                rejectSelectedVendor(mReasonForRejection.get(which));
            }
        });
        builderSingle.show();
    }


}
