package com.android.dladle.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.dladle.R;
import com.android.dladle.utils.NetworkConstants;

public class LeaseTerminatedActivity extends BaseActivity {
    ImageView mBack;
    Button mOK;
    String mTenantName,mPropertyName;
    TextView mTvTenantRemoveDescription;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lease_terminated);
        initView();
        performAction();
    }
    private void initView()
    {
        mBack=(ImageView)findViewById(R.id.back_btn);
        mOK=(Button)findViewById(R.id.button_ok);
        mBack.setVisibility(View.GONE);
        mTvTenantRemoveDescription=(TextView)findViewById(R.id.tv_bottom_txt);
        mTenantName=getIntent().getStringExtra("tenantName");
        mPropertyName=getIntent().getStringExtra("houseName");
        mTvTenantRemoveDescription.setText(getResources().getString(R.string.tenant_removed_txt,mTenantName,mPropertyName));

    }
    private void performAction()
    {
        mOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LeaseTerminatedActivity.this, UserProfileActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(NetworkConstants.JsonResponse.DATA,"landlord");
                startActivity(intent);
            }
        });
    }
}
