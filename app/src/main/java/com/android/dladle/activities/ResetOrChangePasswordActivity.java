package com.android.dladle.activities;

/**
 * Created by sourav on 17-11-2016.
 */

import android.animation.AnimatorInflater;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.dladle.R;
import com.android.dladle.customui.MyJsonObjectRequest;
import com.android.dladle.customui.TextViewUtils;
import com.android.dladle.utils.CommonConstants;
import com.android.dladle.utils.Logger;
import com.android.dladle.utils.NetworkConstants;
import com.android.dladle.utils.NetworkUtils;
import com.android.dladle.utils.PreferenceManager;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class ResetOrChangePasswordActivity extends BaseActivity implements BaseActivity.OnSoftKeyboardListener{

    public static final String CHANGE_PASSWORD_TAG = "change_password";
    private View mHelpView, mHeaderView, mFormView, mProgressView,
            mLogoView, mOTPContainer, mOldPasswordContainer, mEmailContainer;
    private Button mSubmitButton;
    private boolean mInProgress, mIsChangePassword;
    private EditText mEmail, mNewPassword, mConfPassword, mOTP, mOldPassword;
    private ObjectAnimator mProgressAnimator;
    private TextView mText, mEmailTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        super.setOnSoftKeyboardListener(this);
        mIsChangePassword = getIntent().getBooleanExtra(CHANGE_PASSWORD_TAG, false);
        mRootView = findViewById(R.id.rootView);
        if (mIsChangePassword) {
            mRootView.setBackground(getResources().getDrawable(R.drawable.noimagetemplate));
        }
        mLogoView = findViewById(R.id.headerView);
        mAppBackButton = (ImageButton) findViewById(R.id.back_btn);
        mAppBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        mOldPasswordContainer = findViewById(R.id.old_password_container);
        mOTPContainer = findViewById(R.id.otp_container);
        mText = (TextView) findViewById(R.id.textView3);
        mEmail = (EditText) findViewById(R.id.email);
        mEmailTextView = (TextView) findViewById(R.id.email_textview);
        mEmailContainer = findViewById(R.id.email_container);
        if (mIsChangePassword) {
            mText.setText(R.string.change_password);
            mEmailContainer.setVisibility(View.GONE);
            mOTPContainer.setVisibility(View.GONE);
            mEmailTextView.setVisibility(View.VISIBLE);
            mEmailTextView.setText("Email : "+PreferenceManager.getInstance(this).getUserName());
            mOldPasswordContainer.setVisibility(View.VISIBLE);
        } else {
            mText.setText(R.string.reset_password);
            mOTPContainer.setVisibility(View.VISIBLE);
            mOldPasswordContainer.setVisibility(View.GONE);
        }
        mHelpView = findViewById(R.id.help);
        mHelpView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMessage(getString(R.string.reset_password_help));
            }
        });
        mHeaderView = findViewById(R.id.include);
        mFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.progress_bar);
        //mEmail.setText(PreferenceManager.getInstance(this).getUserName());
        mNewPassword = (EditText) findViewById(R.id.password);
        mConfPassword = (EditText) findViewById(R.id.confirm_password);
        mOTP = (EditText) findViewById(R.id.otp);
        mOldPassword = (EditText) findViewById(R.id.old_password);
        mSubmitButton = (Button) findViewById(R.id.btn_submit);
        mSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSubmitButton.setEnabled(false);
                    mAppBackButton.setEnabled(false);
                    doReset();
                }
            }, isShowingKeyBoard ? CommonConstants.HIDE_KEYBOARD_DELAY : 0);
                if (isShowingKeyBoard) {
                    hideKeyboard();
                }
            }
        });
        Intent i = getIntent();
        if (i.getBooleanExtra(NetworkConstants.JsonResponse.STATUS_SUCCESS, false)) {
            showMessage(getString(R.string.reset_password_help));
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        Volley.newRequestQueue(this).cancelAll(NetworkConstants.ResetPassword.TAG);
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void doReset() {
        if (mInProgress) {
            return;
        }

        // Store values at the time of the login attempt.
        final String email = TextViewUtils.removeEndSpaces(mEmail.getText().toString());
        final String password = mNewPassword.getText().toString();
        final String confPassword = mConfPassword.getText().toString();
        final String otp = TextViewUtils.removeEndSpaces(mOTP.getText().toString());
        final String oldPassword = mOldPassword.getText().toString();

        if (!NetworkUtils.isNetworkAvailable(this)) {
            Logger.printInfo("Not connected to internet");
            showMessage(getString(R.string.no_internet));
            mSubmitButton.setEnabled(true);
            mAppBackButton.setEnabled(true);
            return;
        }

        // Reset errors.
        mEmail.setError(null);
        mOTP.setError(null);
        mNewPassword.setError(null);
        mConfPassword.setError(null);

        boolean cancel = false;
        View focusView = null;

        // Check if OTP is valid or not.
        if (!CommonConstants.isValidOTP(otp) && !mIsChangePassword) {
            mOTP.setError(getString(R.string.invalid_otp));
            focusView = mOTP;
            cancel = true;
        }

        if (mIsChangePassword && !CommonConstants.isPasswordValid(oldPassword)) {
            TextViewUtils.setPasswordViewMargin(mOldPassword);
            mOldPassword.setError(getString(R.string.error_invalid_password));
            Toast.makeText(this, getString(R.string.invalid_password_message), Toast.LENGTH_LONG).show();
            focusView = mOldPassword;
            cancel = true;
        }

        // check if both the passwords are same or not.
        if (!TextUtils.isEmpty(password) && ! password.equals(confPassword)) {
            TextViewUtils.setPasswordViewMargin(mConfPassword);
            mConfPassword.setError(getString(R.string.password_not_matched));
            focusView = mConfPassword;
            cancel = true;
        }

        // Check for a valid password, if the user entered one.
        if (!CommonConstants.isPasswordValid(password)) {
            TextViewUtils.setPasswordViewMargin(mNewPassword);
            mNewPassword.setError(getString(R.string.error_invalid_password));
            Toast.makeText(this, getString(R.string.invalid_password_message), Toast.LENGTH_LONG).show();
            focusView = mNewPassword;
            cancel = true;
        }

        // Check for a valid email address.
        if (!mIsChangePassword) {
            if (TextUtils.isEmpty(email)) {
                mEmail.setError(getString(R.string.error_field_required));
                focusView = mEmail;
                cancel = true;
            } else if (!CommonConstants.isEmailValid(email)) {
                mEmail.setError(getString(R.string.error_invalid_email));
                focusView = mEmail;
                cancel = true;
            }
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
            mSubmitButton.setEnabled(true);
            mAppBackButton.setEnabled(true);
        } else {
            // Show a progress spinner
            showProgress(true);
            JSONObject request = new JSONObject();
            try {
                request.put(NetworkConstants.ResetPassword.NEW_PASSWORD, password);
                if (mIsChangePassword) {
                    request.put(NetworkConstants.ChangePassword.OLD_PASSWORD, oldPassword);
                    request.put(NetworkConstants.ChangePassword.EMAIL, PreferenceManager.getInstance(this).getUserName());
                    request.put(NetworkConstants.ChangePassword.NEW_CONF_PWD, confPassword);
                    request.put(NetworkConstants.ChangePassword.NEW_PASSWORD, password);
                } else {
                    request.put(NetworkConstants.ResetPassword.EMAIL_KEY, email);
                    request.put(NetworkConstants.ResetPassword.OTP, otp);
                }
                Logger.printDebug("JSON : "+request);
            }
            catch(Exception e) {
                e.printStackTrace();
                showProgress(false);
            }
            MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.POST,
                    mIsChangePassword ? NetworkConstants.ChangePassword.URL : NetworkConstants.ResetPassword.URL, request,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Logger.printDebug(""+response);
                            try {
                                String status = response.getString(NetworkConstants.JsonResponse.STATUS);
                                String message = response.getString(NetworkConstants.JsonResponse.MESSAGE);
                                Logger.printDebug("Status : "+status+" Message : "+message);
                                if (NetworkConstants.JsonResponse.STATUS_SUCCESS.equalsIgnoreCase(status)) {
                                    showMessage("Password changed successfully");
                                    if (mIsChangePassword) {
                                        if(!TextUtils.isEmpty(PreferenceManager.getInstance(ResetOrChangePasswordActivity.this).getPassword())) {
                                            PreferenceManager.getInstance(ResetOrChangePasswordActivity.this).setPassword(password);
                                        }
                                        onBackPressed();
                                    } else {
                                        PreferenceManager.getInstance(ResetOrChangePasswordActivity.this).resetCredentials();
                                        Intent i = new Intent(ResetOrChangePasswordActivity.this, UserLoginActivity.class);
                                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        i.putExtra(NetworkConstants.JsonResponse.STATUS_SUCCESS, true);
                                        transitionTo(i);
                                        finishAffinity();
                                    }
                                } else if (NetworkConstants.JsonResponse.STATUS_FAILED.equals(status)) {
                                    showMessage(message);
                                }
                            } catch (JSONException e) {
                                Logger.printError("Unable to parse JSON : "+e.getMessage());
                                showMessage(getString(R.string.unexpected_error));
                            }
                            showProgress(false);
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if(error instanceof TimeoutError) {
                                showMessage(getString(R.string.server_error));
                            } else {
                                showMessage(error.getMessage());
                            }
                            showProgress(false);
                            Logger.printError("Some error : "+error.toString());
                        }
                    });
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            jsonRequest.setRetryPolicy(new DefaultRetryPolicy(NetworkConstants.TIMOUT_TIME,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            jsonRequest.setTag(NetworkConstants.ForgotPassword.TAG);
            requestQueue.add(jsonRequest);
        }
    }


    /**
     * Shows the progress UI and hides the login form.
     */
    private void showProgress(final boolean show) {
        super.setActionInProgress(show);
        mInProgress = show;

        mFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        mSubmitButton.setVisibility(show ? View.GONE : View.VISIBLE);
        mSubmitButton.setEnabled(true);
        mAppBackButton.setVisibility(show ? View.GONE : View.VISIBLE);
        mLogoView.setVisibility(show ? View.GONE : View.VISIBLE);
        mAppBackButton.setEnabled(true);
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressAnimator = (ObjectAnimator) AnimatorInflater.loadAnimator(this, R.animator.flipping);
        mProgressAnimator.setTarget(mProgressView);
        mProgressAnimator.start();
    }

    @Override
    public void onKeyboardShown(int currentKeyboardHeight) {
        Logger.printDebug("onKeyboardShown height : "+currentKeyboardHeight);
        if (null != mRootView) {
            if (mRootView.getLayoutParams() instanceof FrameLayout.LayoutParams) {
                FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) mRootView.getLayoutParams();
                lp.bottomMargin = currentKeyboardHeight;
                mRootView.setLayoutParams(lp);
            } else if (mRootView.getLayoutParams() instanceof RelativeLayout.LayoutParams) {
                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mRootView.getLayoutParams();
                lp.bottomMargin = currentKeyboardHeight;
                mRootView.setLayoutParams(lp);
            }
            if (null != mSubmitButton && null != mHeaderView && null != mFormView) {
                int formMargin = (int) getResources().getDimension(R.dimen.act_form_margin_top);
                int appHeaderMargin = (int) getResources().getDimension(R.dimen.app_header_parent_margin_top);
                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mFormView.getLayoutParams();
                lp.bottomMargin += (mSubmitButton.getHeight());
                lp.topMargin += (formMargin + appHeaderMargin + mHeaderView.getHeight());
                Logger.printDebug("Top margin : "+lp.topMargin+" Buttom margin : " + lp.bottomMargin);
                mFormView.setLayoutParams(lp);
            }
        }
    }

    @Override
    public void onKeyboardHidden() {
        Logger.printDebug("onKeyboardHidden");
        if (null != mRootView) {
            if (mRootView.getLayoutParams() instanceof FrameLayout.LayoutParams) {
                FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) mRootView.getLayoutParams();
                lp.bottomMargin = 0;
                mRootView.setLayoutParams(lp);
            } else if (mRootView.getLayoutParams() instanceof RelativeLayout.LayoutParams) {
                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mRootView.getLayoutParams();
                lp.bottomMargin = 0;
                mRootView.setLayoutParams(lp);
            }
            if (null != mSubmitButton && null != mHeaderView && null != mFormView) {
                int formMargin = (int) getResources().getDimension(R.dimen.act_form_margin_top);
                int appHeaderMargin = (int) getResources().getDimension(R.dimen.app_header_parent_margin_top);
                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mFormView.getLayoutParams();
                lp.bottomMargin -= (mSubmitButton.getHeight());
                lp.topMargin -= (formMargin + appHeaderMargin + mHeaderView.getHeight());
                Logger.printDebug("Top margin : "+lp.topMargin+" Buttom margin : " + lp.bottomMargin);
                mFormView.setLayoutParams(lp);
            }
        }
    }

}