package com.android.dladle.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.dladle.R;
import com.android.dladle.customui.CustomProgressDialog;
import com.android.dladle.customui.MyJsonObjectRequest;
import com.android.dladle.customui.ProfileImageView;
import com.android.dladle.model.NotificationListModel;
import com.android.dladle.utils.Logger;
import com.android.dladle.utils.NetworkConstants;
import com.android.dladle.utils.NetworkUtils;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

public class LeaseTerminationReqFromLandlord extends BaseActivity {
    private TextView mStartDateText, mDescriptionText;
    Button mAboutLandlord, mAccept, mDecline;
    ImageView mBack;
    NotificationListModel mNotificationListModel;
    CircleImageView mPropertyImage;
    CustomProgressDialog mCustomProgressDialog;
    ProfileImageView mProfileImageView;
    RatingBar mRatingBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lease_termination_req_from_landlord);
        initView();
        performAction();
    }
    private void initView() {
        mStartDateText = (TextView) findViewById(R.id.lease_start_text);
        mDescriptionText = (TextView) findViewById(R.id.description);
        mAboutLandlord = (Button) findViewById(R.id.button_about_landlord);
        mAccept = (Button) findViewById(R.id.button_accept);
        mDecline = (Button) findViewById(R.id.button_decline);
        mBack = (ImageView) findViewById(R.id.back_btn);
        mPropertyImage = (CircleImageView) findViewById(R.id.property_image);
        mProfileImageView = (ProfileImageView) findViewById(R.id.profile_pic);
        mCustomProgressDialog = new CustomProgressDialog(LeaseTerminationReqFromLandlord.this);
        mRatingBar = (RatingBar) findViewById(R.id.rating_bar);
        String notification = getIntent().getStringExtra("notification");
        Gson gson = new Gson();
        mNotificationListModel = gson.fromJson(notification, NotificationListModel.class);
        mDescriptionText.setText(mNotificationListModel.getName() + " has send request to terminate lease.Please accept or decline it.");




        Glide.with(this)
                .load(mNotificationListModel.getImageUrl())
                .asBitmap()
                .error(R.drawable.blankimage)
                .into(mPropertyImage);
        Glide.with(this)
                .load(mNotificationListModel.getProfilePicture())
                .asBitmap()
                .error(R.drawable.ic_avatar)
                .into(mProfileImageView);

        if(mNotificationListModel.isActioned())
        {
            mAccept.setEnabled(false);
            mDecline.setEnabled(false);
        }


    }

    private void performAction() {
        mAboutLandlord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LeaseTerminationReqFromLandlord.this, AboutLandlordActivity.class);
                intent.putExtra("landlordIdId", mNotificationListModel.getFrom());
                startActivity(intent);
            }
        });
        mAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetworkUtils.isNetworkAvailable(LeaseTerminationReqFromLandlord.this)) {
                    getDetailsOfLease(true);
                    markNotificationAsActioned();
                } else {
                    showMessage(getResources().getString(R.string.no_internet));
                }
            }
        });
        mDecline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetworkUtils.isNetworkAvailable(LeaseTerminationReqFromLandlord.this)) {
                    getDetailsOfLease(false);
                    markNotificationAsActioned();
                } else
                {
                    showMessage(getResources().getString(R.string.no_internet));
                }
            }
        });
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void showProgress(final boolean show) {
        super.setActionInProgress(show);
        if (show) {
            mCustomProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            mCustomProgressDialog.setCancelable(false);
            mCustomProgressDialog.show();
        } else {
            mCustomProgressDialog.dismiss();
        }
    }
    private void acceptRequest(int leaseId) {
        showProgress(true);
        JSONObject input = new JSONObject();
        try {
            input.put("leaseId", leaseId);
            input.put("houseId", mNotificationListModel.getHouseId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Logger.printDebug("input" + input);
        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.POST, NetworkConstants.LeaseController.TERMINATION_ACCEPT, input,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        showProgress(false);
                        Logger.printDebug("" + response);
                        if (response.optString("Status").equalsIgnoreCase(NetworkConstants.SUCCESS)) {
                           // markNotificationAsRead();
                            markNotificationAsActioned();
                            Intent intent = new Intent(LeaseTerminationReqFromLandlord.this, UserProfileActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra(NetworkConstants.JsonResponse.DATA, "tenant");
                            startActivity(intent);
                        } else {

                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof TimeoutError) {
                            showMessage(getString(R.string.server_error));
                        } else {
                            showMessage(error.getMessage());
                        }
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(LeaseTerminationReqFromLandlord.this);
        jsonRequest.setTag(NetworkConstants.Login.TAG);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(NetworkConstants.TIMOUT_TIME,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonRequest);
    }

    private void declineRequest(int leaseId) {
        showProgress(true);
        JSONObject input = new JSONObject();
        try {
            input.put("leaseId", leaseId);
            input.put("houseId", mNotificationListModel.getHouseId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Logger.printDebug("input" + input);
        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.POST, NetworkConstants.LeaseController.TERMINATION_REJECT, input,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        showProgress(false);
                        showMessage(response.optString("Message"));
                        Logger.printDebug("" + response);
                        if (response.optString("Status").equalsIgnoreCase(NetworkConstants.SUCCESS)) {
                           // markNotificationAsRead();
                            markNotificationAsActioned();
                            Intent intent = new Intent(LeaseTerminationReqFromLandlord.this, UserProfileActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra(NetworkConstants.JsonResponse.DATA, "tenant");
                            startActivity(intent);

                        } else {

                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof TimeoutError) {
                            showMessage(getString(R.string.server_error));
                        } else {
                            showMessage(error.getMessage());
                        }
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(LeaseTerminationReqFromLandlord.this);
        jsonRequest.setTag(NetworkConstants.Login.TAG);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(NetworkConstants.TIMOUT_TIME,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonRequest);
    }

    private void markNotificationAsRead() {

        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.POST, NetworkConstants.Notification.NOTIFICATION_READ + String.valueOf(mNotificationListModel.getId()) + "&read=true", null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        showProgress(false);
                        Logger.printDebug("" + response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof TimeoutError) {
                            showMessage(getString(R.string.server_error));
                        } else {
                            showMessage(error.getMessage());
                        }
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(LeaseTerminationReqFromLandlord.this);
        jsonRequest.setTag(NetworkConstants.Login.TAG);
        requestQueue.add(jsonRequest);
    }
    private void markNotificationAsActioned() {

        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.GET, NetworkConstants.Notification.NOTIFICATION_ACTIONED + String.valueOf(mNotificationListModel.getId()), null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        showProgress(false);
                        Logger.printDebug("" + response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof TimeoutError) {
                            showMessage(getString(R.string.server_error));
                        } else {
                            showMessage(error.getMessage());
                        }
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(LeaseTerminationReqFromLandlord.this);
        jsonRequest.setTag(NetworkConstants.Login.TAG);
        requestQueue.add(jsonRequest);
    }
    private void getDetailsOfLease(final boolean isRequestAccepted)
    {
        showProgress(true);
        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.GET, NetworkConstants.LeaseController.LEASE_VIEW, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        showProgress(false);
                        Logger.printDebug("" + response);
                        if (response.optString("Status").equalsIgnoreCase(NetworkConstants.SUCCESS)) {
                            JSONObject data=response.optJSONObject("Data");
                            int leaseId=data.optInt("leaseId");
                            if(isRequestAccepted) {
                                acceptRequest(leaseId);
                            }
                            else
                            {
                                declineRequest(leaseId);
                            }

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showProgress(false);
                        if (error instanceof TimeoutError) {
                            showMessage(getString(R.string.server_error));
                        } else {
                            showMessage(error.getMessage());
                        }
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        jsonRequest.setTag(NetworkConstants.Login.TAG);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(NetworkConstants.TIMOUT_TIME,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonRequest);
    }
}
