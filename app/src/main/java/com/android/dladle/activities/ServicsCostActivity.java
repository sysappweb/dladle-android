package com.android.dladle.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.dladle.R;
import com.android.dladle.customui.CustomProgressDialog;
import com.android.dladle.customui.MyJsonObjectRequest;
import com.android.dladle.customui.ProfileImageView;
import com.android.dladle.utils.Logger;
import com.android.dladle.utils.NetworkConstants;
import com.android.dladle.utils.NetworkUtils;
import com.android.dladle.utils.PreferenceManager;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

public class ServicsCostActivity extends BaseActivity {
    EditText mEtMin, mEtMax;
    Button mBtnSubmit;
    TextView mTvClientDescription;
    ImageButton mIbBack;
    CustomProgressDialog mCustomProgressDialog;
    int mServiceId = 0;
    int mNotificationId;
    ProfileImageView mProfileImageView;
    RatingBar ratingBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_servics_cost);
        initView();
        performAction();
    }

    private void initView() {
        mEtMin = (EditText) findViewById(R.id.et_min);
        mEtMax = (EditText) findViewById(R.id.et_max);
        mBtnSubmit = (Button) findViewById(R.id.btn_submit);
        mIbBack = (ImageButton) findViewById(R.id.back_btn);
        mCustomProgressDialog = new CustomProgressDialog(this);
        mTvClientDescription = (TextView)findViewById(R.id.job_client_description) ;
        mServiceId = getIntent().getIntExtra("serviceId", 0);
        ratingBar = (RatingBar)findViewById(R.id.rating_bar);
        mNotificationId = getIntent().getIntExtra("notificationId",0);
        String name  = getIntent().getStringExtra("name");
        String location = getIntent().getStringExtra("location");
        String image_path = getIntent().getStringExtra("image");
        String rating = getIntent().getStringExtra("rating");
        mProfileImageView = (ProfileImageView)findViewById(R.id.profile_pic);
        mTvClientDescription.setText(name + " needs a "+PreferenceManager.getInstance(getApplicationContext()).getVendorType() + "\n" +" in "+location);
        Glide.with(ServicsCostActivity.this)
                .load(image_path)
                .asBitmap()
                .error(R.drawable.ic_avatar)
                .into(mProfileImageView);
        if (!(rating==null || rating.equals("null")))
        {
            ratingBar.setRating(Float.parseFloat(rating));
        }
    }

    private void performAction() {
        mBtnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetworkUtils.isNetworkAvailable(ServicsCostActivity.this)) {
                    provideEstimation();
                    markNotificationAsRead();
                } else {
                    showMessage(getResources().getString(R.string.no_internet));
                }
            }
        });
        mIbBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void provideEstimation() {


        if (TextUtils.isEmpty(mEtMin.getText().toString())) {
            mEtMin.setError("Please enter minimum value");
            mEtMin.requestFocus();
        } else if (TextUtils.isEmpty(mEtMax.getText().toString())) {
            mEtMax.setError("Please enter minimum value");
            mEtMax.requestFocus();
        } else if (Float.parseFloat(mEtMin.getText().toString()) > Float.parseFloat(mEtMax.getText().toString())) {
            mEtMax.setError("Maximum amount must be greater or equal to minimum amount");
            mEtMax.requestFocus();
        } else {
            if (NetworkUtils.isNetworkAvailable(ServicsCostActivity.this)) {
                showProgress(true);
                JSONObject input = new JSONObject();
                try {
                    input.put("serviceId", mServiceId);
                    input.put("feeStartRange", mEtMin.getText().toString());
                    input.put("feeEndRange", mEtMax.getText().toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.POST, NetworkConstants.VendorController.PROVIDE_ESTIMATION, input,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                Logger.printDebug("" + response);

                                showProgress(false);
                                showMessage(response.optString("Message"));
                                if (response.optString("Status").equalsIgnoreCase(NetworkConstants.SUCCESS)) {
                                    PreferenceManager.getInstance(getApplicationContext()).setHomeStatus(true);
                                    Intent intent = new Intent(ServicsCostActivity.this, UserProfileActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    intent.putExtra(NetworkConstants.JsonResponse.DATA, PreferenceManager.getInstance(getApplicationContext()).getUserType());
                                    startActivity(intent);
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                showProgress(false);
                            }
                        });
                RequestQueue requestQueue = Volley.newRequestQueue(ServicsCostActivity.this);
                jsonRequest.setTag(NetworkConstants.UpdateVendor.TAG);
                jsonRequest.setRetryPolicy(new DefaultRetryPolicy(NetworkConstants.TIMOUT_TIME,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                requestQueue.add(jsonRequest);
            } else {
                showMessage(getResources().getString(R.string.no_internet));
            }
        }
    }

    private void showProgress(final boolean show) {
        super.setActionInProgress(show);
        if (show) {
            mCustomProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            mCustomProgressDialog.setCancelable(false);
            mCustomProgressDialog.show();
        } else {
            mCustomProgressDialog.dismiss();
        }
    }
    private void markNotificationAsRead() {
        mCustomProgressDialog.show();
        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.POST, NetworkConstants.Notification.NOTIFICATION_READ + String.valueOf(mNotificationId) + "&read=true", null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Logger.printDebug("" + response);
                        mCustomProgressDialog.hide();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mCustomProgressDialog.hide();
                        if (error instanceof TimeoutError) {
                            showMessage(getString(R.string.server_error));
                        } else {
                            showMessage(error.getMessage());
                        }
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        jsonRequest.setTag(NetworkConstants.Login.TAG);
        requestQueue.add(jsonRequest);
    }
}
