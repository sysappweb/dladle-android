package com.android.dladle.activities;

/**
 * Created by sourav on 17-11-2016.
 */

import android.animation.AnimatorInflater;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.dladle.R;
import com.android.dladle.customui.TextViewUtils;
import com.android.dladle.utils.CommonConstants;
import com.android.dladle.utils.Logger;
import com.android.dladle.utils.NetworkConstants;
import com.android.dladle.utils.NetworkUtils;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class ForgetPasswordActivity extends BaseActivity implements BaseActivity.OnSoftKeyboardListener{

    private View mHelpView, mProgressView, mFormView, mLogoView;
    private TextView mResetTextView;
    private EditText mEmail;
    private boolean mInProgress;
    private Button mSubmitButton;
    private ObjectAnimator mProgressAnimator;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        super.setOnSoftKeyboardListener(this);
        mRootView = findViewById(R.id.rootView);
        mProgressView = findViewById(R.id.progress_bar);
        mFormView = findViewById(R.id.login_form);
        mLogoView = findViewById(R.id.headerView);
        mAppBackButton = (ImageButton) findViewById(R.id.back_btn);
        mAppBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        mHelpView = findViewById(R.id.help);
        mHelpView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMessage(getString(R.string.forgot_password_help));
            }
        });
        mResetTextView = (TextView) findViewById(R.id.reset_password);
        String text = getString(R.string.reset_password_link_text);
        SpannableString content = new SpannableString(text);
        content.setSpan(new UnderlineSpan(), 0, text.length(), 0);
        mResetTextView.setText(content);
        mResetTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initRestorePassword("");
            }
        });
        mEmail = (EditText) findViewById(R.id.email);
        String email = "";//PreferenceManager.getInstance(this).getUserName();
        if (!TextUtils.isEmpty(email)) {
            mEmail.setText(email);
        }
        mSubmitButton = (Button) findViewById(R.id.btn_submit);
        mSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mSubmitButton.setEnabled(false);
                        mAppBackButton.setEnabled(false);
                        requestOTP();
                    }
                }, isShowingKeyBoard ? CommonConstants.HIDE_KEYBOARD_DELAY : 0);
                if (isShowingKeyBoard) {
                    hideKeyboard();
                }
            }
        });
    }

    @Override
    public void onKeyboardShown(int currentKeyboardHeight) {
        Logger.printDebug("onKeyboardShown height : "+currentKeyboardHeight);
        if(mRootView.getLayoutParams() instanceof FrameLayout.LayoutParams) {
            FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) mRootView.getLayoutParams();
            lp.bottomMargin = currentKeyboardHeight;
            mRootView.setLayoutParams(lp);
        } else if (mRootView.getLayoutParams() instanceof RelativeLayout.LayoutParams) {
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mRootView.getLayoutParams();
            lp.bottomMargin = currentKeyboardHeight;
            mRootView.setLayoutParams(lp);
        }
    }

    @Override
    public void onKeyboardHidden() {
        Logger.printDebug("onKeyboardHidden");
        if(mRootView.getLayoutParams() instanceof FrameLayout.LayoutParams) {
            FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) mRootView.getLayoutParams();
            lp.bottomMargin = 0;
            mRootView.setLayoutParams(lp);
        } else if (mRootView.getLayoutParams() instanceof RelativeLayout.LayoutParams) {
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mRootView.getLayoutParams();
            lp.bottomMargin = 0;
            mRootView.setLayoutParams(lp);
        }
    }

    private void initRestorePassword(String extra) {
        Intent i = new Intent(this, ResetOrChangePasswordActivity.class);
        if (!TextUtils.isEmpty(extra)) {
            i.putExtra(extra,true);
        }
        transitionTo(i);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Volley.newRequestQueue(this).cancelAll(NetworkConstants.ForgotPassword.TAG);
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void requestOTP() {
        if (mInProgress) {
            return;
        }

        // Store values at the time of the login attempt.
        final String email = TextViewUtils.removeEndSpaces(mEmail.getText().toString());

        if (!NetworkUtils.isNetworkAvailable(this)) {
            Logger.printInfo("Not connected to internet");
            showMessage(getString(R.string.no_internet));
            mSubmitButton.setEnabled(true);
            mAppBackButton.setEnabled(true);
            return;
        }

//        hideKeyboard();

        // Reset errors.
        mEmail.setError(null);

        boolean cancel = false;
        View focusView = null;


        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmail.setError(getString(R.string.error_field_required));
            focusView = mEmail;
            cancel = true;
        } else if (!CommonConstants.isEmailValid(email)) {
            mEmail.setError(getString(R.string.error_invalid_email));
            focusView = mEmail;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
            mSubmitButton.setEnabled(true);
            mAppBackButton.setEnabled(true);
        } else {
            // Show a progress spinner
            showProgress(true);
            JSONObject request = new JSONObject();
            try {
                request.put(NetworkConstants.ForgotPassword.EMAIL_KEY, email);
                Logger.printDebug("JSON : "+request);
            }
            catch(Exception e) {
                e.printStackTrace();
                showProgress(false);
            }
            JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, NetworkConstants.ForgotPassword.URL, request,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Logger.printDebug(""+response);
                            try {
                                String status = response.getString(NetworkConstants.JsonResponse.STATUS);
                                String message = response.getString(NetworkConstants.JsonResponse.MESSAGE);
                                Logger.printDebug("Status : "+status+" Message : "+message);
                                if (NetworkConstants.JsonResponse.STATUS_SUCCESS.equalsIgnoreCase(status)) {
                                    initRestorePassword(NetworkConstants.JsonResponse.STATUS_SUCCESS);
                                } else if (NetworkConstants.JsonResponse.STATUS_FAILED.equals(status)) {
                                    showMessage(message);
                                }
                            } catch (JSONException e) {
                                Logger.printError("Unable to parse JSON : "+e.getMessage());
                                showMessage(getString(R.string.unexpected_error));
                            }
                            showProgress(false);
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if(error instanceof TimeoutError) {
                                showMessage(getString(R.string.server_error));
                            } else {
                                showMessage(error.getMessage());
                            }
                            showProgress(false);
                            Logger.printError("Some error : "+error.toString());
                        }
                    });
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            jsonRequest.setTag(NetworkConstants.ForgotPassword.TAG);
            jsonRequest.setRetryPolicy(new DefaultRetryPolicy(NetworkConstants.TIMOUT_TIME,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(jsonRequest);
        }
    }


    /**
     * Shows the progress UI and hides the login form.
     */
    private void showProgress(final boolean show) {
        super.setActionInProgress(show);
        mInProgress = show;

        mFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        mSubmitButton.setVisibility(show ? View.GONE : View.VISIBLE);
        mSubmitButton.setEnabled(true);
        mAppBackButton.setVisibility(show ? View.GONE : View.VISIBLE);
        mLogoView.setVisibility(show ? View.GONE : View.VISIBLE);
        mAppBackButton.setEnabled(true);
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressAnimator = (ObjectAnimator) AnimatorInflater.loadAnimator(this, R.animator.flipping);
        mProgressAnimator.setTarget(mProgressView);
        mProgressAnimator.start();
    }
}
