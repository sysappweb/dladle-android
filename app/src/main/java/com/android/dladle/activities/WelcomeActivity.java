package com.android.dladle.activities;

/**
 * Created by sourav on 17-11-2016.
 */

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.dladle.R;
import com.android.dladle.customui.DepthPageTransformer;
import com.android.dladle.customui.IntroPageTransformation;
import com.android.dladle.customui.ZoomOutPageTransformer;
import com.android.dladle.utils.CommonConstants;
import com.android.dladle.utils.Logger;
import com.android.dladle.utils.PreferenceManager;

import java.util.Timer;
import java.util.TimerTask;

public class WelcomeActivity extends BaseActivity {

    private ViewPager mViewPager;
    private LinearLayout mDotsLayout;
    private int[] mLayouts;
    private Button mBtnRegister, mBtnLogin;
    private PreferenceManager mPref;
    private Timer mTimer;
    private int count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Logger.printInfo("Device SDK version : "+ Build.VERSION.SDK_INT);
     //   wakeUpCallToServer();
        setContentView(R.layout.activity_welcome);
        setPressBackTwiceToExitFlag(true);
        mPref = PreferenceManager.getInstance(this);

        // If not first launch then directly go to the login page.
        if(!mPref.isFirstTimeLaunch()) {
            startActivity(new Intent(WelcomeActivity.this, UserLoginActivity.class));
            finish();
        }

        mViewPager = (ViewPager) findViewById(R.id.view_pager);
        mDotsLayout = (LinearLayout) findViewById(R.id.layoutDots);
        mBtnRegister = (Button) findViewById(R.id.btn_register);
        mBtnLogin = (Button) findViewById(R.id.btn_login);

        mLayouts = new int[]{
                R.layout.welcome_slide1,
                R.layout.welcome_slide2,
                R.layout.welcome_slide2,
                R.layout.welcome_slide2};
        updateDotsAndButtonView(0);

        mViewPager.setAdapter(new MyViewPagerAdapter());
        mViewPager.setPageTransformer(true, getPageTransformation());
        mViewPager.setOffscreenPageLimit(2);
        mViewPager.addOnPageChangeListener(viewPagerPageChangeListener);
        checkAndSetAutoScroll();

        mBtnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Logger.printDebug("Login button clicked");
                launchLoginActivity();
            }
        });

        mBtnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 Logger.printDebug("Register button clicked");
                 launchRegisterActivity();
            }
        });
    }

    private void updateDotsAndButtonView(int currentPage) {
        if (currentPage < mLayouts.length) {
            TextView[] lDots = new TextView[mLayouts.length];
            mDotsLayout.setVisibility(View.VISIBLE);
            mDotsLayout.removeAllViews();
            for (int i = 0; i < lDots.length; i++) {
                lDots[i] = new TextView(this);
                lDots[i].setText(Html.fromHtml("&#8226;"));
                lDots[i].setTextSize(35);
                lDots[i].setTextColor(getResources().getColor((i == currentPage) ? R.color.dot_selected : R.color.dot_not_selected));
                mDotsLayout.addView(lDots[i]);
            }
        }
    }

    private ViewPager.PageTransformer getPageTransformation() {
        int flag = getResources().getInteger(R.integer.transformation_setting);
        if (0 == flag) {
            return new ZoomOutPageTransformer();
        } else if (1 == flag) {
            return new IntroPageTransformation();
        } else {
            return new DepthPageTransformer();
        }
    }

    private void checkAndSetAutoScroll() {
        if (getResources().getBoolean(R.bool.support_auto_intro_slide)) {
            mTimer = new Timer();
            mTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (count <= mLayouts.length) {
                                mViewPager.setCurrentItem(count);
                                count++;
                            } else {
                                count = 0;
                                mViewPager.setCurrentItem(count);
                            }
                        }
                    });
                }
            }, 500, 3000);
        }
    }

    private int getItem(int i) {
        return mViewPager.getCurrentItem() + i;
    }

    private void launchLoginActivity() {
        Intent i = new Intent(WelcomeActivity.this, UserLoginActivity.class);
        transitionTo(i);
    }

    private void launchRegisterActivity() {
        boolean showUserTypePicker = getResources().getBoolean(R.bool.show_distinct_user_type);
        Intent i = new Intent(this, showUserTypePicker ? UserRegisterPickerActivity.class : UserRegisterActivity.class);
        transitionTo(i);
    }

    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            updateDotsAndButtonView(position);
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    private void wakeUpCallToServer() {
        Intent i = new Intent(CommonConstants.WELCOME_SERVICE_INTENT);
        i.setPackage(CommonConstants.PACKAGE_NAME);
        startService(i);
    }

    /**
     * View pager adapter
     */
    class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        MyViewPagerAdapter() {}

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(mLayouts[position], container, false);
            TextView lDescTextView = (TextView) view.findViewById(R.id.textView4);
            if (null != lDescTextView && position > 0) {
                lDescTextView.setText(getTextBasedOnPosition(position));
            }
            ImageView lImageView = (ImageView) view.findViewById(R.id.imageView);
            if (null != lImageView) {
                lImageView.setImageBitmap(getImageBasedOnPosition(position));
            }
            view.setTag(position);
            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return mLayouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }

        private int getTextBasedOnPosition(int position) {
            switch (position) {
                case 1 :
                    return R.string.slide2_description;
                case 2 :
                    return R.string.slide3_description;
                case 3 :
                    return R.string.slide4_description;
                default:
                    return R.string.slide1_description;
            }
        }
        private Bitmap getImageBasedOnPosition (int position) {
            switch (position) {
                case 1 :
                    return BitmapFactory.decodeResource(getResources(),R.mipmap.splash_01);
                case 2 :
                    return BitmapFactory.decodeResource(getResources(),R.mipmap.splash_02);
                case 3 :
                    return BitmapFactory.decodeResource(getResources(),R.mipmap.splash_03);
                default:
                    return BitmapFactory.decodeResource(getResources(),R.mipmap.ic_logo);
            }
        }
    }
}
