package com.android.dladle.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.dladle.R;
import com.android.dladle.customui.MyJsonObjectRequest;
import com.android.dladle.model.NotificationListModel;
import com.android.dladle.utils.CommonConstants;
import com.android.dladle.utils.Logger;
import com.android.dladle.utils.NetworkConstants;
import com.android.dladle.utils.PreferenceManager;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.gson.Gson;

import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

public class TerminationRequestStatusActivity extends BaseActivity {
    TextView mDescription,mTvHeaderText;
    ImageView mBack,mHome;
    CircleImageView mPropertyImage;
    Button mOK;
    NotificationListModel mNotificationListModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_termination_request_status);
        initView();
        performAction();
    }
    private void initView() {
        mDescription = (TextView) findViewById(R.id.confirmation_text_summary);
        mBack = (ImageView) findViewById(R.id.back_btn);
        mPropertyImage = (CircleImageView) findViewById(R.id.property_image);
        mOK = (Button) findViewById(R.id.ok_button);
        mBack.setVisibility(View.GONE);
        mHome=(ImageView)findViewById(R.id.home_image);
        mTvHeaderText=(TextView)findViewById(R.id.header_text);
        String notification = getIntent().getStringExtra("notification");
        Gson gson = new Gson();
        mNotificationListModel = gson.fromJson(notification, NotificationListModel.class);
        if(Integer.parseInt(mNotificationListModel.getNotificationTypeId())== CommonConstants.TENANT_ACCEPTED_LESAE_TERMINATION)
        {
            mTvHeaderText.setText("Lease termination accepted");
            mDescription.setText("Your lease termination request has accepted by "+mNotificationListModel.getName());
        }
        else
        {
            mTvHeaderText.setText("Lease termination rejected");
            mDescription.setText("Your lease termination request has accepted by "+mNotificationListModel.getName());
        }




        if (!mNotificationListModel.isRead()) {
            markNotificationAsRead();
        }
    }

    private void performAction() {
        mOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TerminationRequestStatusActivity.this, UserProfileActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(NetworkConstants.JsonResponse.DATA, PreferenceManager.getInstance(getApplicationContext()).getUserType());
                startActivity(intent);
            }
        });
    }
    private void markNotificationAsRead() {

        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.POST, NetworkConstants.Notification.NOTIFICATION_READ + String.valueOf(mNotificationListModel.getId()) + "&read=true", null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Logger.printDebug("" + response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof TimeoutError) {
                            showMessage(getString(R.string.server_error));
                        } else {
                            showMessage(error.getMessage());
                        }
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(TerminationRequestStatusActivity.this);
        jsonRequest.setTag(NetworkConstants.Login.TAG);
        requestQueue.add(jsonRequest);
    }
}
