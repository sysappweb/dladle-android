package com.android.dladle.activities;

/**
 * Created by sourav on 17-11-2016.
 */

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.transition.Fade;
import android.transition.Slide;
import android.view.Gravity;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.dladle.R;
import com.android.dladle.utils.CommonConstants;
import com.android.dladle.utils.Logger;
import com.android.dladle.utils.PreferenceManager;

public class BaseActivity extends AppCompatActivity {

    private long mBackPressEventTimer = 0;
    private boolean mPressBackToExitFlag = false;
    protected ImageButton mAppBackButton;
    private boolean mActionInProgress = false;
    protected View mRootView;
    public boolean isShowingKeyBoard;
    protected static boolean mSupportTransition;
    AlertDialog mUpgradeDialog;
    UpgradeReceiver mReceiver;
    CustomHandler mHandler;

    // Threshold for minimal keyboard height.
    final int MIN_KEYBOARD_HEIGHT_PX = 150;
    private static final int MSG_BACK = 100;
    private static final int MSG_UPGRADE = 101;

    public interface OnSoftKeyboardListener {
        public void onKeyboardShown(int currentKeyboardHeight);
        public void onKeyboardHidden();
    }

    OnSoftKeyboardListener mOnSoftKeyboardListener;

    // Call this to disable back button press if some action is in progress
    public void setActionInProgress (boolean flag) {
        mActionInProgress = flag;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHandler = new CustomHandler();
        mSupportTransition = getResources().getBoolean(R.bool.support_transition);
        setupWindowAnimations();
        // Making notification bar transparent
        if (Build.VERSION.SDK_INT >= 21) {
            final View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        changeStatusBarColor();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    /**
     * Call this from child class to get notification for soft keyboard change state.
     * This is useful when "adjustResize" behavior has to be replicated.
     * @param listener
     */
    public final void setOnSoftKeyboardListener(final OnSoftKeyboardListener listener) {
        if (null != listener) {
            this.mOnSoftKeyboardListener = listener;
            //final View rootView = getWindow().getDecorView();
            final View rootView = findViewById(R.id.rootView);
            if (null != rootView) {
                rootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    private final Rect windowVisibleDisplayFrame = new Rect();
                    private int lastVisibleDecorViewHeight;

                    @Override
                    public void onGlobalLayout() {
                        // Retrieve visible rectangle inside window.
                        rootView.getWindowVisibleDisplayFrame(windowVisibleDisplayFrame);
                        final int visibleDecorViewHeight = windowVisibleDisplayFrame.height();
                        Logger.printDebug("visibleDecorViewHeight : " + visibleDecorViewHeight);

                        // Decide whether keyboard is visible from changing decor view height.
                        if (lastVisibleDecorViewHeight != 0) {
                            if (lastVisibleDecorViewHeight > visibleDecorViewHeight + MIN_KEYBOARD_HEIGHT_PX) {
                                int statusBarHeight = 0;
                                int contentViewTop = getWindow().findViewById(Window.ID_ANDROID_CONTENT).getTop();
                                if (Build.VERSION.SDK_INT < 21) {
                                    statusBarHeight = contentViewTop - windowVisibleDisplayFrame.top;
                                }
                                // Calculate current keyboard height (this includes also navigation bar height when in fullscreen mode).
                                int currentKeyboardHeight = rootView.getHeight() - statusBarHeight - windowVisibleDisplayFrame.bottom;
                                Logger.printDebug("decorView.getHeight() : " + rootView.getHeight());
                                Logger.printDebug("windowVisibleDisplayFrame.bottom : " + windowVisibleDisplayFrame.bottom);
                                // Notify listener about keyboard being shown.
                                if (null != mOnSoftKeyboardListener) {
                                    isShowingKeyBoard = true;
                                    mOnSoftKeyboardListener.onKeyboardShown(currentKeyboardHeight);
                                }
                            } else if (lastVisibleDecorViewHeight + MIN_KEYBOARD_HEIGHT_PX < visibleDecorViewHeight) {
                                // Notify listener about keyboard being hidden.
                                if (null != mOnSoftKeyboardListener) {
                                    isShowingKeyBoard = false;
                                    mOnSoftKeyboardListener.onKeyboardHidden();
                                }
                            }
                        }
                        // Save current decor view height for the next call.
                        lastVisibleDecorViewHeight = visibleDecorViewHeight;
                    }
                });
            }
        }
    }
    /**
     * Making notification bar transparent
     */
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkForUpgrade();
        mBackPressEventTimer = 0;
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (null != mUpgradeDialog && mUpgradeDialog.isShowing()) {
            mUpgradeDialog.dismiss();
        }
    }

    protected void setPressBackTwiceToExitFlag(boolean flag) {
        mPressBackToExitFlag = flag;
    }

    private void initFinishSequence(boolean isDelayed) {
        //If user operation is in progress then do not finish the activity
        if (!mActionInProgress) {
            long lCurrentTime = System.currentTimeMillis();
            long realTimer = isDelayed ? CommonConstants.BACK_PRESS_TIME_DELAY - CommonConstants.APP_BACK_PRESS_DELAY : CommonConstants.BACK_PRESS_TIME_DELAY;
            if (!mPressBackToExitFlag || lCurrentTime - mBackPressEventTimer < realTimer) {
                finish();
            } else {
                mBackPressEventTimer = System.currentTimeMillis();
                Toast.makeText(this, R.string.press_again_to_exit, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void resetOnKeyBoardListener() {
        Logger.printDebug("Reset keyboard listener");
        mOnSoftKeyboardListener = null;
    }

    @Override
    public void onBackPressed() {
        Logger.printDebug("Back button pressed : "+isShowingKeyBoard);
        final boolean shouldDelay;
        if (isShowingKeyBoard) {
            hideKeyboard();
            shouldDelay = true;
        } else {
            shouldDelay = false;
        }
        Message msg = mHandler.obtainMessage(MSG_BACK);
        msg.arg1 = shouldDelay ? 1 : 2;
        mHandler.sendMessageDelayed(msg, shouldDelay ? CommonConstants.APP_BACK_PRESS_DELAY : 0);
    }

    @Override
    protected void onDestroy() {
        if (null != mHandler) {
            mHandler.removeMessages(MSG_UPGRADE);
            mHandler.removeMessages(MSG_BACK);
        }
        if (null != mReceiver) {
            unregisterReceiver(mReceiver);
        }
        super.onDestroy();
    }


    public void hideKeyboard() {
        if (null !=  mRootView) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(mRootView.getWindowToken(), 0);
        }
    }


    protected void showMessage(String message) {
        if (!TextUtils.isEmpty(message)) {
            Toast toast = Toast.makeText(this, message, Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
    }


    private void setupWindowAnimations() {
        if (mSupportTransition && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Fade fade = new Fade();
            fade.setDuration(500);
            getWindow().setReenterTransition(fade);
            getWindow().setExitTransition(fade);
            Slide slide = new Slide();
            slide.setDuration(100);
            slide.setSlideEdge(Gravity.RIGHT);
            getWindow().setEnterTransition(slide);
        }
    }

    // Use this only if both screen Background is same.
    protected void transitionTo(Intent i) {
        ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(this);
        startActivity(i, transitionActivityOptions.toBundle());
    }

    private void checkForUpgrade() {
        PreferenceManager lPref = PreferenceManager.getInstance(this);
        String currentVersion = lPref.getCurrentVersion();
        String expectedVersion = lPref.getExpectedVersion();
        if (CommonConstants.isLowerVersion(currentVersion, expectedVersion)) {
            Message msg = mHandler.obtainMessage(MSG_UPGRADE);
            mHandler.sendMessageDelayed(msg, CommonConstants.UPGRADE_DIALOG_DELAY);
        }
        IntentFilter filter = new IntentFilter(CommonConstants.FORCE_UPGRADE_INTENT);
        mReceiver = new UpgradeReceiver();
        registerReceiver(mReceiver, filter);
    }

    private void showUpgradeDialog() {
        if (null != mUpgradeDialog && mUpgradeDialog.isShowing()) {
            return;
        }
        AlertDialog.Builder lBuilder = new AlertDialog.Builder(this)
                .setTitle(R.string.upgrade_to_continue)
                .setMessage(R.string.upgrade_to_continue_msg)
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        gotoPlayStore();
                        mUpgradeDialog.dismiss();
                    }
                });
        mUpgradeDialog = lBuilder.create();
        mUpgradeDialog.show();
    }

    private void gotoPlayStore() {
        final String appPackageName = getPackageName();
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException ex) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    private class UpgradeReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
	        Logger.printDebug("UpgradeBroadcast received");
            if (CommonConstants.FORCE_UPGRADE_INTENT.equals(intent.getAction())) {
                showUpgradeDialog();
            }
        }
    }

    private class CustomHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_BACK :
                    initFinishSequence(msg.arg1 == 1);
                    break;
                case MSG_UPGRADE :
                    showUpgradeDialog();
                    break;
                default :
                    break;
            }
            super.handleMessage(msg);
        }
    }

    protected int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }
}
