package com.android.dladle.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.dladle.R;
import com.android.dladle.customui.CustomProgressDialog;
import com.android.dladle.customui.MyJsonObjectRequest;
import com.android.dladle.model.NotificationListModel;
import com.android.dladle.utils.LinearLayoutTarget;
import com.android.dladle.utils.Logger;
import com.android.dladle.utils.NetworkConstants;
import com.android.dladle.utils.NetworkUtils;
import com.android.dladle.utils.PreferenceManager;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class ReviewActivity extends BaseActivity {
    TextView mPropertyName, mUserName, mTerminationText;
    RatingBar mUserRating, mCurrentRating;
    ImageView mBack, mUserProfileImage;
    Button mSave;
    String propertyName, propertyImagePath;
    int propertyId;
    LinearLayout mPropertyImage;
    CustomProgressDialog mCustomProgressDialog;
    EditText mEtReview;
    NotificationListModel mNotificationListModel;
    String mUserType = "", mDate = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review);
        initView();
        performAction();
    }

    private void initView() {
        mPropertyName = (TextView) findViewById(R.id.titleView);
        mUserName = (TextView) findViewById(R.id.tv_name);
        mTerminationText = (TextView) findViewById(R.id.tv_termination_text);
        mUserRating = (RatingBar) findViewById(R.id.rating_bar);
        mCurrentRating = (RatingBar) findViewById(R.id.current_rating_bar);
        mBack = (ImageView) findViewById(R.id.back_btn);
        mUserProfileImage = (ImageView) findViewById(R.id.profile_pic);
        mCustomProgressDialog = new CustomProgressDialog(this);
        mEtReview = (EditText) findViewById(R.id.et_review);
        mSave = (Button) findViewById(R.id.button_ok);
        mPropertyImage = (LinearLayout) findViewById(R.id.property_image);
        propertyName = getIntent().getStringExtra("propertyName");
        propertyImagePath = getIntent().getStringExtra("propertyImagePath");
        propertyId = getIntent().getIntExtra("houseId", 0);
        mPropertyName.setText(propertyName);
        mUserType = PreferenceManager.getInstance(getApplicationContext()).getUserType();
        if (getIntent().hasExtra("notification")) {
            Gson gson = new Gson();
            mNotificationListModel = gson.fromJson(getIntent().getStringExtra("notification"), NotificationListModel.class);
        }
        mDate = convertDate(mNotificationListModel.getLeaseEnddate());
        Glide.with(this)
                .load(propertyImagePath)
                .asBitmap()
                .error(R.drawable.blankimage)
                .into(new LinearLayoutTarget(this, (LinearLayout) mPropertyImage));

        Glide.with(this)
                .load(mNotificationListModel.getProfilePicture())
                .asBitmap()
                .error(R.drawable.ic_avatar)
                .into(mUserProfileImage);
        mUserName.setText(mNotificationListModel.getName());
        try {
            mUserRating.setRating((float) mNotificationListModel.getRating());
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (mUserType.equalsIgnoreCase("tenant")) {
            mTerminationText.setText("You are about to terminate your lease with your landlord. The lease will be terminate at  - " + mDate + ".\n Please review the landlord.");
        } else if (mUserType.equalsIgnoreCase("landlord")) {
            mTerminationText.setText("You are about to terminate your lease with your tenant. The lease will be terminate at  - " + mDate + ".\n Please review the tenant.");
        } else {

        }
        getRating();
    }

    private void performAction() {
        mSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!NetworkUtils.isNetworkAvailable(ReviewActivity.this)) {
                    showMessage(getString(R.string.no_internet));
                    return;
                } else {
                    if (NetworkUtils.isNetworkAvailable(ReviewActivity.this)) {
                        saveReview();
                    } else {
                        showMessage(getResources().getString(R.string.no_internet));
                    }
                }
            }
        });
        mCurrentRating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                mCurrentRating.setRating(rating);
            }
        });
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void saveReview() {
        showProgress(true);
        JSONObject input = new JSONObject();
        try {
            input.put("comment", mEtReview.getText().toString().trim());
            input.put("rate", mCurrentRating.getRating());
            input.put("ratedUserEmailId", mNotificationListModel.getFrom());
        } catch (JSONException e) {
            e.printStackTrace();
        }


        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.POST, NetworkConstants.ReviewController.POST_REVIEW, input,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Logger.printDebug("" + response);

                        showProgress(false);
                        showMessage(response.optString("Message"));
                        if (response.optString("Status").equalsIgnoreCase(NetworkConstants.SUCCESS)) {
                            markNotificationAsRead();
                            PreferenceManager.getInstance(getApplicationContext()).setHomeStatus(true);
                            Intent intent = new Intent(ReviewActivity.this, UserProfileActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra(NetworkConstants.JsonResponse.DATA, PreferenceManager.getInstance(getApplicationContext()).getUserType());
                            startActivity(intent);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showProgress(false);
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(ReviewActivity.this);
        jsonRequest.setTag(NetworkConstants.UpdateVendor.TAG);
        requestQueue.add(jsonRequest);
    }

    private void showProgress(final boolean show) {
        super.setActionInProgress(show);
        if (show) {
            mCustomProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            mCustomProgressDialog.setCancelable(false);
            mCustomProgressDialog.show();
        } else {
            mCustomProgressDialog.dismiss();
        }
    }

    private void markNotificationAsRead() {

        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.POST, NetworkConstants.Notification.NOTIFICATION_READ + String.valueOf(mNotificationListModel.getId()) + "&read=true", null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Logger.printDebug("" + response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof TimeoutError) {
                            showMessage(getString(R.string.server_error));
                        } else {
                            showMessage(error.getMessage());
                        }
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        jsonRequest.setTag(NetworkConstants.Login.TAG);
        requestQueue.add(jsonRequest);
    }

    private String convertDate(String leaseDate) {
        SimpleDateFormat form = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date date = null;
        try {
            date = form.parse(leaseDate);
        } catch (ParseException e) {

            e.printStackTrace();
        }
        SimpleDateFormat postFormater = new SimpleDateFormat("dd MMM yyyy");
        String newDateStr = postFormater.format(date);
        return newDateStr;
    }

    private void getRating() {
        JSONObject input = new JSONObject();
        try {
            input.put("userEmailId", mNotificationListModel.getFrom());
        } catch (JSONException e) {
            e.printStackTrace();
        }


        showProgress(true);
        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.POST, NetworkConstants.ReviewController.GET_REVIEW_DETAILS, input,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        showProgress(false);
                        Logger.printDebug("" + response);
                        if (response.optString("Status").equalsIgnoreCase(NetworkConstants.SUCCESS)) {
                            JSONObject data = response.optJSONObject("Data");
                            if (data != null && data.length() > 0) {


                                int count = data.optInt("count");
                                if (count == 0) {
                                    mUserRating.setVisibility(View.GONE);
                                } else {
                                    double rating = data.optDouble("rate");
                                    mUserRating.setRating((float) rating);
                                }


                            }


                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof TimeoutError) {
                            showMessage(getString(R.string.server_error));
                        } else {
                            showMessage(error.getMessage());
                        }
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(ReviewActivity.this);
        jsonRequest.setTag(NetworkConstants.Login.TAG);
        requestQueue.add(jsonRequest);
    }
}
