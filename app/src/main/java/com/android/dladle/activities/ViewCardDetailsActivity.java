package com.android.dladle.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.dladle.R;
import com.android.dladle.adapter.RecyclerAdapterOfCard;
import com.android.dladle.customui.CustomProgressDialog;
import com.android.dladle.customui.MyJsonObjectRequest;
import com.android.dladle.model.CardModel;
import com.android.dladle.utils.Logger;
import com.android.dladle.utils.NetworkConstants;
import com.android.dladle.utils.NetworkUtils;
import com.android.dladle.utils.PreferenceManager;
import com.android.dladle.utils.RecyclerItemClickListener;
import com.android.dladle.utils.SwipeableRecyclerViewTouchListener;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ViewCardDetailsActivity extends BaseActivity {

    ImageView mIvCardIcon;
    String creitCardType = "";
    ImageView mIvBack;
    CustomProgressDialog mCustomProgressDialog;
    TextView mTvNameOnCard, mTvCardNo;
    ArrayList<CardModel> cardModelArrayList = new ArrayList<>();
    RecyclerView recyclerView;
    RecyclerAdapterOfCard recyclerAdapterOfCard;
    Button mLlAddNewcard;
    String finalPrice;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_card_details);
        initView();
        performAction();
        getCardDetails();
    }

    private void initView() {

        mCustomProgressDialog = new CustomProgressDialog(this);
        mIvBack = (ImageView) findViewById(R.id.back_btn);
        recyclerView = (RecyclerView) findViewById(R.id.rv_card);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mLlAddNewcard = (Button) findViewById(R.id.btn_submit);


        if (getIntent().hasExtra("mode")) {

            if (getIntent().getStringExtra("mode").equals("pay")) {
                mLlAddNewcard.setVisibility(View.GONE);
                finalPrice = getIntent().getStringExtra("finalPrice");
            }
            if (getIntent().getStringExtra("mode").equals("edit")) {
                swipe();
            }
        }
        mLlAddNewcard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ViewCardDetailsActivity.this, AddPaymentCardActivity.class);
                startActivity(intent);
            }
        });
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(
                recyclerView.getContext(),
                DividerItemDecoration.VERTICAL
        );

        dividerItemDecoration.setDrawable(
                ContextCompat.getDrawable(this, R.drawable.divider)
        );

        recyclerView.addItemDecoration(dividerItemDecoration);

        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getApplicationContext(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                // Edit Card
                if(getIntent().hasExtra("mode")) {
                    if (getIntent().getStringExtra("mode").equals("edit")) {
                        Intent intent = new Intent(ViewCardDetailsActivity.this, AddPaymentCardActivity.class);
                        intent.putExtra("nameonCard", cardModelArrayList.get(position).getNameOnCard());
                        intent.putExtra("cardNumber", cardModelArrayList.get(position).getCardNumber());
                        intent.putExtra("expiryDate", cardModelArrayList.get(position).getExpiryDate());
                        intent.putExtra("cardType", cardModelArrayList.get(position).getCardType());
                        startActivity(intent);
                    }
                    else {
                        askCVVNumber(position);
                    }
                }
                // Pay

            }

            @Override
            public void onItemLongClick(View view, int position) {
                //   Toast.makeText(getActivity(), "It's a long click", Toast.LENGTH_LONG).show();
            }
        }));

    }

    private void performAction() {


        mIvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    private void showProgress(final boolean show) {
        super.setActionInProgress(show);
        if (show) {
            mCustomProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            mCustomProgressDialog.setCancelable(false);
            mCustomProgressDialog.show();
        } else {
            mCustomProgressDialog.dismiss();
        }
    }

    private void getCardDetails() {
        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.GET, NetworkConstants.WalletController.VIEW_CARD, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        showProgress(false);
                        Logger.printDebug("" + response);
                        showMessage(response.optString("Message"));
                        if (response.optString("Status").equalsIgnoreCase(NetworkConstants.SUCCESS)) {


                            JSONArray jsonArray = response.optJSONArray("Data");

                            for (int i = 0; i < jsonArray.length(); i++) {


                                JSONObject jsonObject = jsonArray.optJSONObject(i);
                                String creditCardNo = jsonObject.optString("cardNumber");
                                String expiryDate = jsonObject.optString("expiryDate");
                                String nameOnCard = jsonObject.optString("nameOnCard");
                                String cardType = jsonObject.optString("cardType");
                                int cardId = jsonObject.optInt("cardId");

                                CardModel cardModel = new CardModel();
                                cardModel.setCardNumber(creditCardNo);
                                cardModel.setNameOnCard(nameOnCard);
                                cardModel.setExpiryDate(expiryDate);
                                cardModel.setCardType(cardType);
                                cardModel.setCardId(cardId);
                                cardModelArrayList.add(cardModel);

                            }
                            recyclerAdapterOfCard = new RecyclerAdapterOfCard(cardModelArrayList, ViewCardDetailsActivity.this);
                            recyclerView.setAdapter(recyclerAdapterOfCard);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showProgress(false);
                        if (error instanceof TimeoutError) {
                            showMessage(getString(R.string.server_error));
                        } else {
                            showMessage(error.getMessage());
                        }
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        jsonRequest.setTag(NetworkConstants.Login.TAG);
        requestQueue.add(jsonRequest);
    }

    private void deletePaymentMethod(String cardNumber, final int[] reverseSortedPositions) {
        showProgress(true);

        JSONObject input = new JSONObject();
        try {
            input.put("cardNumber", cardNumber);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Logger.printDebug(input.toString());

        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.POST, NetworkConstants.WalletController.DELETE_CARD, input,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        showProgress(false);
                        Logger.printDebug("" + response);
                        showProgress(false);
                        showMessage(response.optString("Message"));
                        if (response.optString("Status").equalsIgnoreCase(NetworkConstants.SUCCESS)) {
                            for (int position : reverseSortedPositions) {
                                //deletePaymentMethod(cardModelArrayList.get(position).getCardNumber());
                                cardModelArrayList.remove(position);

                                recyclerAdapterOfCard.notifyItemRemoved(position);
                            }
                            recyclerAdapterOfCard.notifyDataSetChanged();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showProgress(false);
                        showProgress(false);
                        if (error instanceof TimeoutError) {
                            showMessage(getString(R.string.server_error));
                        } else {
                            showMessage(error.getMessage());
                        }
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        jsonRequest.setTag(NetworkConstants.Login.TAG);
        requestQueue.add(jsonRequest);

    }

    private void swipe() {
        SwipeableRecyclerViewTouchListener swipeTouchListener = new SwipeableRecyclerViewTouchListener(recyclerView, new SwipeableRecyclerViewTouchListener.SwipeListener() {
            @Override
            public boolean canSwipeLeft(int position) {
                return true;
            }

            @Override
            public boolean canSwipeRight(int position) {
                return true;
            }

            @Override
            public void onDismissedBySwipeLeft(RecyclerView recyclerView, int[] reverseSortedPositions) {
                alertDialogForConfirmDelete(reverseSortedPositions);
                // onCardViewDismiss(reverseSortedPositions,cardModelArrayList,recyclerAdapterOfCard);
            }

            @Override
            public void onDismissedBySwipeRight(RecyclerView recyclerView, int[] reverseSortedPositions) {
                //on cardview swipe right dismiss update adapter
                alertDialogForConfirmDelete(reverseSortedPositions);
                // onCardViewDismiss(reverseSortedPositions,cardModelArrayList,recyclerAdapterOfCard);
            }
        });
        recyclerView.addOnItemTouchListener(swipeTouchListener);
    }

    private void alertDialogForConfirmDelete(final int[] reverseSortedPositions) {
        final AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(ViewCardDetailsActivity.this);
        builder.setTitle("Delete card")
                .setMessage("Do you want to delete this card?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (NetworkUtils.isNetworkAvailable(ViewCardDetailsActivity.this)) {
                            onCardViewDismiss(reverseSortedPositions, cardModelArrayList, recyclerAdapterOfCard);
                        } else {
                            showMessage(getResources().getString(R.string.no_internet));
                        }
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();

    }

    private void onCardViewDismiss(int[] reverseSortedPositions, ArrayList<CardModel> stringArrayList, RecyclerAdapterOfCard recyclerViewAdapter) {
        for (int position : reverseSortedPositions) {
            deletePaymentMethod(cardModelArrayList.get(position).getCardNumber(), reverseSortedPositions);
            //stringArrayList.remove(position);

            //recyclerViewAdapter.notifyItemRemoved(position);
        }
        // recyclerViewAdapter.notifyDataSetChanged();
    }

    private void askCVVNumber(final int pos) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ViewCardDetailsActivity.this);
        alertDialog.setTitle("Please enter CVV number and Pay");

        final EditText input = new EditText(ViewCardDetailsActivity.this);
        input.setHint("Please enter CVV number");
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        input.setMaxLines(1);
        int maxLength = 3;
        input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLength)});
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        alertDialog.setView(input);

        alertDialog.setPositiveButton("Submit",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        String cvv = input.getText().toString();
                        if (TextUtils.isEmpty(input.getText().toString().trim())) {
                            showMessage("Please enter CVV number");
                        } else {

                            pay(cvv, pos);
                        }
                    }
                });

        alertDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        alertDialog.show();
    }

    private void pay(String cvv, int pos) {
        showProgress(true);

        final JSONObject input = new JSONObject();

        try {
            input.put("amount", finalPrice);
            input.put("cardId", cardModelArrayList.get(pos).getCardId());
            input.put("cvvNumber", cvv);
            input.put("operationType", "KEY_GENERATION");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Logger.printDebug(input.toString());
        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.POST, NetworkConstants.PaymentController.PAY, input,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        showProgress(false);
                        Logger.printDebug("" + response);
                        showProgress(false);
                        showMessage(response.optString("Message"));
                        if (response.optString("Status").equalsIgnoreCase(NetworkConstants.SUCCESS)) {
                            String paygateId = null, payId = null, checkSumId = null;

                            JSONObject jsonObject = response.optJSONObject("Data");
                            JSONArray urlParams = jsonObject.optJSONArray("urlParams");
                            for (int i = 0; i < urlParams.length(); i++) {
                                JSONObject jsonObject1 = urlParams.optJSONObject(i);
                                String key = jsonObject1.optString("key");
                                String value = jsonObject1.optString("value");
                                if (key.equals("PAY_REQUEST_ID")) {
                                    payId = value;
                                } else if (key.equals("PAYGATE_ID")) {
                                    paygateId = value;
                                } else if (key.equals("CHECKSUM")) {
                                    checkSumId = value;
                                }
                            }

                            // finalPay(paygateId, payId, checkSumId);
                            Log.d("payId", paygateId);
                            Log.d("payId", payId);
                            Log.d("payId", checkSumId);
                            Intent intent = new Intent(ViewCardDetailsActivity.this, WebViewActivity.class);
                            intent.putExtra("payId", payId);
                            intent.putExtra("paygateId", paygateId);
                            intent.putExtra("finalPrice", finalPrice);
                            intent.putExtra("checkSumId", checkSumId);
                            startActivity(intent);


                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showProgress(false);
                        if (error instanceof TimeoutError) {
                            showMessage(getString(R.string.server_error));
                        } else {
                            showMessage(error.getMessage());
                        }
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        jsonRequest.setTag(NetworkConstants.Login.TAG);
        requestQueue.add(jsonRequest);
    }


}
