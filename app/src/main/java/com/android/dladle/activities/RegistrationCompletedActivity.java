package com.android.dladle.activities;

/**
 * Created by sourav on 17-11-2016.
 */

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.dladle.R;
import com.android.dladle.utils.CommonConstants;

public class RegistrationCompletedActivity extends BaseActivity {

    private Button mContinueButton;
    private TextView mTextView, mContentView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_profile_created);
        super.setPressBackTwiceToExitFlag(true);
        setPressBackTwiceToExitFlag(true);
        mAppBackButton = (ImageButton) findViewById(R.id.back_btn);
        mAppBackButton.setVisibility(View.GONE);
        mContinueButton = (Button) findViewById(R.id.btn_continue);
        mTextView = (TextView) findViewById(R.id.name_text);
        mContentView = (TextView) findViewById(R.id.content);

        Intent i = getIntent();
        if (null != i && null != i.getStringExtra(CommonConstants.USER_NAME)) {
            mTextView.setText(getText(R.string.registration_confirmation) +" "+i.getStringExtra(CommonConstants.USER_NAME));
        }

        if (null != i) {
            if (CommonConstants.FRAGMENT_KEY_VENDOR.equals(i.getStringExtra(CommonConstants.FRAGMENT_KEY))) {
                mContentView.setText(R.string.vendor_confirmation_message);
            } else {
                mContentView.setText(R.string.registration_confirmation_message);
            }
        }

        mContinueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoLogin();
            }
        });
    }

    private void gotoLogin() {
        Intent i = new Intent(this, UserLoginActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        transitionTo(i);
        finishAffinity();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
