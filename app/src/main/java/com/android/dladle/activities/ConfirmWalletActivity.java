package com.android.dladle.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.dladle.R;
import com.android.dladle.customui.CustomProgressDialog;
import com.android.dladle.customui.MyJsonObjectRequest;
import com.android.dladle.utils.Logger;
import com.android.dladle.utils.NetworkConstants;
import com.android.dladle.utils.NetworkUtils;
import com.android.dladle.utils.PreferenceManager;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

public class ConfirmWalletActivity extends BaseActivity {
    ImageView mIvBack;
    Button mBtnCntd;
    CustomProgressDialog mCustomProgressDialog;
    ImageView mIvCardIcon;
    TextView mTvCardNo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_wallet);
        initView();
        performAction();
    }

    private void initView() {
        mCustomProgressDialog = new CustomProgressDialog(this);
        mIvBack = (ImageView) findViewById(R.id.back_btn);
        mBtnCntd = (Button) findViewById(R.id.button_ok);
        mTvCardNo=(TextView)findViewById(R.id.tv_card_no);
        mIvCardIcon=(ImageView)findViewById(R.id.card_icon);
        if(NetworkUtils.isNetworkAvailable(ConfirmWalletActivity.this)) {
            getCardDetails();
        }
        else {
            showMessage(getResources().getString(R.string.no_internet));
        }
    }

    private void performAction() {
        mBtnCntd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ConfirmWalletActivity.this, UserProfileActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(NetworkConstants.JsonResponse.DATA, PreferenceManager.getInstance(getApplicationContext()).getUserType());
                startActivity(intent);
            }
        });
        mIvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void getCardDetails() {
        showProgress(true);
        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.GET, NetworkConstants.WalletController.VIEW_CARD, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        showProgress(false);
                        Logger.printDebug("" + response);
                        showMessage(response.optString("Message"));
                        if (response.optString("Status").equalsIgnoreCase(NetworkConstants.SUCCESS)) {
                            JSONObject jsonObject = response.optJSONObject("Data");
                            String creditCardNo = jsonObject.optString("cardNumber");
                            String cardType = jsonObject.optString("cardType");
                            mTvCardNo.setText("Card number:"+creditCardNo);
                            showCardType(cardType);

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showProgress(false);
                        if (error instanceof TimeoutError) {
                            showMessage(getString(R.string.server_error));
                        } else {
                            showMessage(error.getMessage());
                        }
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        jsonRequest.setTag(NetworkConstants.Login.TAG);
        requestQueue.add(jsonRequest);
    }
    private void showProgress(final boolean show) {
        super.setActionInProgress(show);
        if (show) {
            mCustomProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            mCustomProgressDialog.setCancelable(false);
            mCustomProgressDialog.show();
        } else {
            mCustomProgressDialog.dismiss();
        }
    }
    private void showCardType(String creitCardType) {
        if (creitCardType.equalsIgnoreCase("visa")) {
            mIvCardIcon.setImageResource(R.drawable.visa);
        } else if (creitCardType.equalsIgnoreCase("MasterCard")) {
            mIvCardIcon.setImageResource(R.drawable.mastercard);
        } else if (creitCardType.equalsIgnoreCase("Maestro")) {
            mIvCardIcon.setImageResource(R.drawable.maestro);
        } else if (creitCardType.equalsIgnoreCase("Dankort")) {

        } else if (creitCardType.equalsIgnoreCase("Discover")) {
            mIvCardIcon.setImageResource(R.drawable.discover);
        } else if (creitCardType.equalsIgnoreCase("China Union pay")) {
            mIvCardIcon.setImageResource(R.drawable.unionpay);
        } else if (creitCardType.equalsIgnoreCase("American Express")) {
            mIvCardIcon.setImageResource(R.drawable.americanexpress);
        }
    }
}
