package com.android.dladle.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.android.dladle.R;

public class AddHomeActivity extends BaseActivity {
    ImageView mBack;
    EditText mName, mEmail;
    Button mSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_home);
        initView();
        performAction();
    }
    private void initView()
    {
        mBack=(ImageView)findViewById(R.id.back_btn);
        mName=(EditText)findViewById(R.id.tv_name);
        mEmail=(EditText)findViewById(R.id.tv_email);
        mSubmit=(Button)findViewById(R.id.button_save);
    }
    private void performAction()
    {
        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
