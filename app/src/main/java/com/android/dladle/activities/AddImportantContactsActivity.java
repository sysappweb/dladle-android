package com.android.dladle.activities;

import android.animation.AnimatorInflater;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.dladle.R;
import com.android.dladle.adapter.RecyclerAdpterForContact;
import com.android.dladle.customui.MyJsonObjectRequest;
import com.android.dladle.entities.Property;
import com.android.dladle.model.ContactModel;
import com.android.dladle.utils.CommonConstants;
import com.android.dladle.utils.ImageUtils;
import com.android.dladle.utils.Logger;
import com.android.dladle.utils.NetworkConstants;
import com.android.dladle.utils.NetworkUtils;
import com.android.dladle.utils.PreferenceManager;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

public class AddImportantContactsActivity extends BaseActivity {
    RecyclerView mRecyclerViewForContactList;
    TextView mTvAddAnotherContact, mProgressText;
    ImageView mIvVBack;
    Button mBtnSave;
    ArrayList<ContactModel> mContactModelArrayList = new ArrayList<>();
    RecyclerAdpterForContact mRecyclerAdpterForContact;
    LinearLayout mProgressbarContainer, mContentContainer, mContactContainer;
    private View mProgressView;
    private ObjectAnimator mProgressAnimator;
    LinearLayoutManager mLinearLayoutManager;
    private Property mProperty;
    String property;
    int mHouseId = 0;
    ArrayList<String> mContactType = new ArrayList<>();
    ArrayList<String> mContactKey = new ArrayList<>();
    String mUserType = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_important_contacts);
        initView();
        mUserType = PreferenceManager.getInstance(getApplicationContext()).getUserType();
        mHouseId = getIntent().getIntExtra("houseId", 0);
        if (getIntent().hasExtra("property")) {
            property = getIntent().getStringExtra("property");
            Logger.printDebug(property);
        }
        else
        {
            getContactTypeList();
        }
        performAction();

    }

    private void initView() {
        mRecyclerViewForContactList = (RecyclerView) findViewById(R.id.rv_contact);
        mTvAddAnotherContact = (TextView) findViewById(R.id.tv_add_another_contact);
        mIvVBack = (ImageView) findViewById(R.id.back_btn);
        mBtnSave = (Button) findViewById(R.id.button_save);
        mProgressView = (View) findViewById(R.id.progress_bar);
        mProgressText = (TextView) findViewById(R.id.progress_bar_text);
        mProgressbarContainer = (LinearLayout) findViewById(R.id.progressbar_container);
        mContentContainer = (LinearLayout) findViewById(R.id.content_container);
        mLinearLayoutManager = new LinearLayoutManager(this);
        mContactContainer = (LinearLayout) findViewById(R.id.add_contact_container);
        mRecyclerViewForContactList.setLayoutManager(mLinearLayoutManager);
        if(getIntent().hasExtra("migratedFrom"))
        {
            mContactModelArrayList.add(new ContactModel("", "", "", ""));
        }
        else
        {
            mContactModelArrayList.add(new ContactModel("", "", "", ""));
            mContactModelArrayList.add(new ContactModel("", "", "", ""));
        }


    }

    private void performAction() {
        mTvAddAnotherContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addAnotherContact();
            }
        });
        mBtnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getIntent().hasExtra("property")) {
                    if (mRecyclerAdpterForContact.getItemCount() >= 2) {
                        if (!doContactValidation())
                            addContact();
                    } else {
                        showMessage("You have to add atleast two contact");
                    }
                } else if (getIntent().hasExtra("houseId")) {
                    mHouseId = getIntent().getIntExtra("houseId", 0);
                    if (mRecyclerAdpterForContact.getItemCount() >= 1) {
                        if (!doContactValidation())
                            addContact();
                    } else {
                        showMessage("You have to add atleast one contact");
                    }
                }

            }
        });
        mIvVBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void addContact() {
        showProgress(true);
        JSONObject input = new JSONObject();
        try {
            input.put("propertyContactList", getContactList());
            input.put("houseId", mHouseId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Logger.printDebug("" + input);
        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.POST, NetworkConstants.Property.ADD_CONTACT, input,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Logger.printDebug("" + response);
                        if (response.optString("Status").equalsIgnoreCase(NetworkConstants.SUCCESS)) {
                            showProgress(false);
                            Intent intent = new Intent(AddImportantContactsActivity.this, UserProfileActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra(NetworkConstants.JsonResponse.DATA, "landlord");
                            startActivity(intent);

                        } else {
                            showProgress(false);
                            showMessage(response.optString("Message"));

                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof TimeoutError) {
                            showMessage(getString(R.string.server_error));
                        } else {
                            showMessage(error.getMessage());
                        }
                        showProgress(false);
                        Logger.printError("Some error : " + error.toString());
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        jsonRequest.setTag(NetworkConstants.Login.TAG);
        requestQueue.add(jsonRequest);
    }

    private void getContactTypeList() {
        if (!NetworkUtils.isNetworkAvailable(this)) {
            Logger.printInfo("Not connected to internet");
            showMessage(getString(R.string.no_internet));
        } else {
            showProgress(true);
            JSONObject request = new JSONObject();
            MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.GET, NetworkConstants.Select.GET_CONTACT_TYPE, request,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Logger.printDebug("" + response);
                            if (response.optString("Status").equalsIgnoreCase(NetworkConstants.SUCCESS)) {
                                JSONObject data = response.optJSONObject("Data");
                                mContactType.clear();
                                mContactType.add("Select contact type");
                                for (Iterator<String> iter = data.keys(); iter.hasNext(); ) {
                                    String key = iter.next();
                                    String value = data.optString(key);

                                    mContactType.add(key);
                                    mContactKey.add(value);
                                }
                                showProgress(false);
                            } else {
                                showProgress(false);
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error instanceof TimeoutError) {
                                showMessage(getString(R.string.server_error));
                            } else {
                                showMessage(error.getMessage());
                            }
                            showProgress(false);
                            Logger.printError("Some error : " + error.toString());
                        }
                    });
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            jsonRequest.setTag(NetworkConstants.Login.TAG);
            requestQueue.add(jsonRequest);
        }
    }

    private void showProgress(final boolean show) {
        super.setActionInProgress(show);
        if (show) {
            mContentContainer.setVisibility(View.GONE);
            mProgressbarContainer.setVisibility(View.VISIBLE);
            mProgressView.setVisibility(View.VISIBLE);
            mProgressText.setVisibility(View.VISIBLE);
            mProgressAnimator = (ObjectAnimator) AnimatorInflater.loadAnimator(this, R.animator.flipping);
            mProgressAnimator.setTarget(mProgressView);
            mProgressAnimator.start();
        } else {
            mRecyclerAdpterForContact = new RecyclerAdpterForContact(mContactModelArrayList, AddImportantContactsActivity.this, mContactType, mContactKey);
            mRecyclerViewForContactList.setAdapter(mRecyclerAdpterForContact);
            mProgressbarContainer.setVisibility(View.GONE);
            mContentContainer.setVisibility(View.VISIBLE);
        }
    }

    private void addAnotherContact() {

        if (!doContactValidation()) {
            mContactModelArrayList.add(new ContactModel("", "", "", ""));
            mRecyclerAdpterForContact.notifyDataSetChanged();
            mLinearLayoutManager.scrollToPosition(mContactModelArrayList.size() - 1);
            if (mContactModelArrayList.size() >= 4) {
                mContactContainer.setVisibility(View.GONE);
            }

        }
    }

    private boolean doContactValidation() {
        if (mContactModelArrayList.size() > 0) {
            boolean isError = false;
            Logger.printDebug("Size"+String.valueOf(mContactModelArrayList.size()));
            for (int i = 0; i < mContactModelArrayList.size(); i++) {
                isError = false;
                ContactModel contactModel = mContactModelArrayList.get(i);
                if (contactModel.getPhoneNo().equals("") || contactModel.getPhoneNo().length() < 9) {
                    mLinearLayoutManager.scrollToPositionWithOffset(i, 0);
                    Toast.makeText(AddImportantContactsActivity.this, "Invalid contact no", Toast.LENGTH_SHORT).show();
                   // Toast.makeText(AddImportantContactsActivity.this, "Invalid contact no", Toast.LENGTH_SHORT).show();
                    isError = true;
                    return isError;
                } else if (contactModel.getAddress().equals("")) {

                    mRecyclerViewForContactList.scrollToPosition(i);
                    Toast.makeText(AddImportantContactsActivity.this, "Invalid address", Toast.LENGTH_SHORT).show();
                    isError = true;
                    return isError;
                } else if (contactModel.getName().equals("")) {
                    mRecyclerViewForContactList.scrollToPosition(i);
                    Toast.makeText(AddImportantContactsActivity.this, "Invalid name", Toast.LENGTH_SHORT).show();
                    isError = true;
                    return isError;
                } else if (contactModel.getType().equals("")) {
                    mRecyclerViewForContactList.scrollToPosition(i);
                    Toast.makeText(AddImportantContactsActivity.this, "Please select a contact type", Toast.LENGTH_SHORT).show();
                    isError = true;
                    return isError;
                }

            }
            return false;
        } else {
            return false;
        }
    }

    public void deleteContact(int position) {
        mContactModelArrayList.remove(position);
        mRecyclerAdpterForContact.notifyDataSetChanged();
        if (mContactModelArrayList.size() < 4) {
            mContactContainer.setVisibility(View.VISIBLE);
        }
    }

    private JSONArray getContactList() {
        JSONArray jsonArray = new JSONArray();
        try {
            for (int i = 0; i < mContactModelArrayList.size(); i++) {
                ContactModel contactModel = mContactModelArrayList.get(i);
                JSONObject jsonObject = new JSONObject();

                jsonObject.put("address", contactModel.getAddress());
                jsonObject.put("contactNumber", contactModel.getPhoneNo());
                jsonObject.put("contactType", contactModel.getType());
                jsonObject.put("name", contactModel.getName());
                jsonArray.put(jsonObject);


            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonArray;
    }




}
