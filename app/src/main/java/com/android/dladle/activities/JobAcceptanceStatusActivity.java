package com.android.dladle.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.dladle.R;

public class JobAcceptanceStatusActivity extends BaseActivity {
ImageButton mBtnBack;
    TextView mTvOne,mTvTwo,mTvMessage;
    ImageView mIvStatus;
    Button mBtnOk;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_acceptance_status);
        initView();
        performAction();
    }


    private void initView()
    {
        mBtnBack = (ImageButton)findViewById(R.id.back_btn);
        mTvOne = (TextView)findViewById(R.id.tv_one);
        mTvTwo = (TextView)findViewById(R.id.tv_two);
        mTvMessage = (TextView)findViewById(R.id.tv_bottom_txt);
        mBtnBack.setVisibility(View.INVISIBLE);
        mIvStatus = (ImageView)findViewById(R.id.iv_status);
        mBtnOk = (Button)findViewById(R.id.button_ok);
    }

    private void performAction()
    {
      mBtnOk.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {

          }
      });
    }
}
