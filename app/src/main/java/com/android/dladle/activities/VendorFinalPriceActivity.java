package com.android.dladle.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.dladle.R;
import com.android.dladle.customui.ProfileImageView;
import com.bumptech.glide.Glide;

import java.util.Arrays;
import java.util.List;

public class VendorFinalPriceActivity extends BaseActivity {

    TextView mTvTotalNoOfJobs, mTvVendorTotalExp, mTvFinalPrice, mTVVendorName;
    Button mBtnAccept, mBtnDecline;
    RatingBar mRatingBar;
    ProfileImageView profileImageView;
    Button mBtnPay;
    String finalPrice = "";
    ImageButton mImageBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor_final_price);
        initView();
        performAction();
    }

    private void initView() {
        mTvFinalPrice = (TextView) findViewById(R.id.tv_price);
        mTvTotalNoOfJobs = (TextView) findViewById(R.id.tv_vendor_total_job);
        mTvVendorTotalExp = (TextView) findViewById(R.id.tv_vendor_total_exp);
        mBtnAccept = (Button) findViewById(R.id.button_accept);
        mImageBack = (ImageButton)findViewById(R.id.back_btn);
        mBtnDecline = (Button) findViewById(R.id.button_decline);
        mTVVendorName = (TextView) findViewById(R.id.tv_vendor_name);
        profileImageView = (ProfileImageView) findViewById(R.id.profile_pic);
        mRatingBar = (RatingBar) findViewById(R.id.rating_bar);
        mBtnPay = (Button) findViewById(R.id.btn_pay);
        if (getIntent().getStringExtra("image") != null) {
            Glide.with(VendorFinalPriceActivity.this)
                    .load(getIntent().getStringExtra("image"))
                    .asBitmap()
                    .error(R.drawable.ic_avatar)
                    .into(profileImageView);
        }
        mTVVendorName.setText(getIntent().getStringExtra("name"));
        if (getIntent().hasExtra("data")) {

            String data = getIntent().getStringExtra("data");
            List<String> items = Arrays.asList(data.split("\\s*,\\s*"));

            for (int i = 0; i < items.size(); i++) {
                if (items.get(i).contains("serviceId")) {
                    int index = items.get(i).indexOf(":");
                } else if (items.get(i).contains("vendorEmailId")) {
                    int index = items.get(i).indexOf(":");
                    mTvTotalNoOfJobs.setText("Nuber of DlaDle jobs: " + items.get(i).substring(index + 1));
                } else if (items.get(i).contains("finalFee")) {
                    int index = items.get(i).indexOf(":");
                    finalPrice = items.get(i).substring(index + 1);
                    mTvFinalPrice.setText("R" + items.get(i).substring(index + 1));
                }
            }
        }
    }

    private void performAction() {
        mBtnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                acceptOrDeclineJobRequest();
            }
        });
        mBtnDecline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                acceptOrDeclineJobRequest();
            }
        });
        mBtnPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pay();
            }
        });
        mImageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void acceptOrDeclineJobRequest() {

    }

    public void pay() {
        Intent i = new Intent(this, ViewCardDetailsActivity.class);
        i.putExtra("mode", "pay");
        i.putExtra("finalPrice", finalPrice);
        transitionTo(i);
    }

}
