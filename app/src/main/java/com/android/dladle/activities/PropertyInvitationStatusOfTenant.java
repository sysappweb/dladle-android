package com.android.dladle.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.dladle.R;
import com.android.dladle.customui.MyJsonObjectRequest;
import com.android.dladle.model.NotificationListModel;
import com.android.dladle.utils.CommonConstants;
import com.android.dladle.utils.Logger;
import com.android.dladle.utils.NetworkConstants;
import com.android.dladle.utils.PreferenceManager;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.gson.Gson;

import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

public class PropertyInvitationStatusOfTenant extends BaseActivity {
    TextView mDescription,mTvHeaderText;
    ImageView mBack,mHome,mStatus;
    Button mOK;
    NotificationListModel mNotificationListModel;
    CircleImageView mPropertyImage;
    String mUserType="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_property_invitation_status_of_tenant);
        initView();
        performAction();
    }
    private void initView() {
        mDescription = (TextView) findViewById(R.id.confirmation_text_summary);
        mBack = (ImageView) findViewById(R.id.back_btn);
        mPropertyImage = (CircleImageView) findViewById(R.id.property_image);
        mOK = (Button) findViewById(R.id.ok_button);
        mBack.setVisibility(View.GONE);
        mTvHeaderText=(TextView)findViewById(R.id.header_text);
        mHome=(ImageView)findViewById(R.id.home_image);
        mStatus=(ImageView)findViewById(R.id.big_tick);
        mUserType= PreferenceManager.getInstance(getApplicationContext()).getUserType();
        String notification = getIntent().getStringExtra("notification");
        Gson gson = new Gson();
        mNotificationListModel = gson.fromJson(notification, NotificationListModel.class);
        if (!mNotificationListModel.isRead()) {
            markNotificationAsRead();
        }
        int notificationTypeID=Integer.parseInt(mNotificationListModel.getNotificationTypeId());
        if(notificationTypeID== CommonConstants.LANDLORD_ACCEPT_TENANTS_PROPRTY_INVITATION)
        {
            mTvHeaderText.setText("Request Accepted");
            mDescription.setText("Your request has accepted by "+mNotificationListModel.getName());
        }
        else if(notificationTypeID==CommonConstants.LANDLORD_REJECT_TENANTS_PROPRTY_INVITATION)
        {
            mTvHeaderText.setText("Request Rejected");
            mDescription.setText("Your request has rejected by "+mNotificationListModel.getName());
            mStatus.setImageResource(R.drawable.ic_small_cross);
        }
        else if(notificationTypeID==CommonConstants.TENANT_ACCEPT_LANDLORDS_PROPERTY_INVITATION)
        {
            mTvHeaderText.setText("Request Accepted");
            mDescription.setText("Your request has accepted by "+mNotificationListModel.getName());
        }
        else
        {
            mTvHeaderText.setText("Request Rejected");
            mDescription.setText("Your request has rejected by "+mNotificationListModel.getName());
            mStatus.setImageResource(R.drawable.ic_small_cross);
        }

    }

    private void performAction() {
        mOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PropertyInvitationStatusOfTenant.this, UserProfileActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(NetworkConstants.JsonResponse.DATA, PreferenceManager.getInstance(getApplicationContext()).getUserType());

                startActivity(intent);
            }
        });
    }
    private void markNotificationAsRead() {

        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.POST, NetworkConstants.Notification.NOTIFICATION_READ + String.valueOf(mNotificationListModel.getId()) + "&read=true", null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Logger.printDebug("" + response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof TimeoutError) {
                            showMessage(getString(R.string.server_error));
                        } else {
                            showMessage(error.getMessage());
                        }
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        jsonRequest.setTag(NetworkConstants.Login.TAG);
        requestQueue.add(jsonRequest);
    }
}
