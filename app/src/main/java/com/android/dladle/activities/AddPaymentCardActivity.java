package com.android.dladle.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.dladle.R;
import com.android.dladle.customui.CustomProgressDialog;
import com.android.dladle.customui.MyJsonObjectRequest;
import com.android.dladle.utils.CardUtils;
import com.android.dladle.utils.ChangeTransformationMethod;
import com.android.dladle.utils.Logger;
import com.android.dladle.utils.NetworkConstants;
import com.android.dladle.utils.NetworkUtils;
import com.android.dladle.utils.PreferenceManager;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;


public class AddPaymentCardActivity extends BaseActivity {
    EditText mEtName, creditCard, mExpiryDate, mEtCvvNo;
    ImageView mIvInfo;
    Button mBtnSavePaymentMethod, mBtnDeletePaymentMethod;
    String mLastInput = "";
    String a = "";
    int keyDel = 0;
    CustomProgressDialog mCustomProgressDialog;
    boolean isSlash = false;
    ImageView mIvCardIcon;
    String creitCardType = "";
    ImageView mIvBack;
    boolean isEdit = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_payment_card);
        initView();
        performAction();
    }

    private void initView() {
        mEtName = (EditText) findViewById(R.id.et_name_on_card);
        creditCard = (EditText) findViewById(R.id.et_card_no);
        mExpiryDate = (EditText) findViewById(R.id.et_expiry_date);
        mBtnSavePaymentMethod = (Button) findViewById(R.id.btn_save_payment_method);
        mBtnDeletePaymentMethod = (Button) findViewById(R.id.button_delete_payment_method);
        mCustomProgressDialog = new CustomProgressDialog(AddPaymentCardActivity.this);
        mIvCardIcon = (ImageView) findViewById(R.id.card_icon);
        mIvBack = (ImageView) findViewById(R.id.back_btn);

        if (getIntent().getExtras() != null) {
            isEdit = true;
            mExpiryDate.setText(getIntent().getStringExtra("expiryDate"));
            mEtName.setText(getIntent().getStringExtra("nameonCard"));
            creditCard.setText(getIntent().getStringExtra("cardNumber"));
            showCardType(getIntent().getStringExtra("cardType"));
            creditCard.setTransformationMethod(new ChangeTransformationMethod());
            creditCard.setEnabled(false);
            creditCard.setFocusable(false);
            creditCard.setTextIsSelectable(false);
        }


    }

    private void performAction() {
        mIvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mBtnSavePaymentMethod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetworkUtils.isNetworkAvailable(AddPaymentCardActivity.this)) {
                    savePaymentMethod();
                } else {
                    showMessage(getString(R.string.no_internet));
                }
            }
        });

        creditCard.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                creditCard.setInputType(InputType.TYPE_CLASS_NUMBER);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                boolean flag = true;
                String eachBlock[] = creditCard.getText().toString().split("-");
                for (int i = 0; i < eachBlock.length; i++) {
                    if (eachBlock[i].length() > 4) {
                        flag = false;
                    }
                }
                if (flag) {

                    creditCard.setOnKeyListener(new View.OnKeyListener() {

                        @Override
                        public boolean onKey(View v, int keyCode, KeyEvent event) {

                            if (keyCode == KeyEvent.KEYCODE_DEL)
                                keyDel = 1;
                            return false;
                        }
                    });

                    if (keyDel == 0) {

                        if (((creditCard.getText().length() + 1) % 5) == 0) {

                            if (creditCard.getText().toString().split("-").length <= 3) {
                                creditCard.setText(creditCard.getText() + "-");
                                creditCard.setSelection(creditCard.getText().length());
                            }
                        }
                        a = creditCard.getText().toString();
                    } else {
                        a = creditCard.getText().toString();
                        keyDel = 0;
                    }

                } else {
                    creditCard.setText(a);
                }


                creitCardType = CardUtils.getCardID(s.toString());
                showCardType(creitCardType);

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mExpiryDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    formatCardExpiringDate(s);
                } catch (NumberFormatException e) {
                    s.clear();
                    //Toast message here.. Wrong date formate

                }
            }
        });

    }

    private void savePaymentMethod() {

        String cardHolderName = mEtName.getText().toString().trim();
        String cardNumber = creditCard.getText().toString().trim();
        String expiryDate = mExpiryDate.getText().toString().trim();
        if (cardHolderName.length() < 3) {
            mEtName.setError("Invalid name");
            mEtName.requestFocus();
        } else if (!isEdit) {
            if (cardNumber.length() < 19) {
                creditCard.setError("Invalid card");
                creditCard.requestFocus();
            }
        } else if (expiryDate.length() != 5 && !expiryDate.contains("/")) {
            mExpiryDate.setError("Invalid expiry date");
            mExpiryDate.requestFocus();
        } else {
            showProgress(true);
            JSONObject input = new JSONObject();
            try {
                input.put("cardNumber", cardNumber.replaceAll("-", ""));
                input.put("cardType", creitCardType);
                //     int positionOfSlash = expiryDate.indexOf(expiryDate);
                String expDate = expiryDate.substring(0, 2) + "20" + expiryDate.substring(3);
                input.put("expiryDate", expDate.replace("/", ""));
                input.put("nameOnCard", cardHolderName);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Logger.printDebug(input.toString());
            String url = "";
           /* if (PreferenceManager.getInstance(getApplicationContext()).isPaymentAccountSetup()) {
                url = NetworkConstants.WalletController.UPDATE_CARD;
            } else {*/
            url = NetworkConstants.WalletController.ADD_CARD;
            //  }
            MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.POST, url, input,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            showProgress(false);
                            Logger.printDebug("" + response);
                            showMessage(response.optString("Message"));
                            if (response.optString("Status").equalsIgnoreCase(NetworkConstants.SUCCESS)) {
                                PreferenceManager.getInstance(getApplicationContext()).setPaymentAccountStatus(true);
                                Intent intent = new Intent(AddPaymentCardActivity.this, WalletAddedActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                //intent.putExtra(NetworkConstants.JsonResponse.DATA, PreferenceManager.getInstance(getApplicationContext()).getUserType());
                                startActivity(intent);
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            showProgress(false);
                            if (error instanceof TimeoutError) {
                                showMessage(getString(R.string.server_error));
                            } else {
                                showMessage(error.getMessage());
                            }
                        }
                    });
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            jsonRequest.setTag(NetworkConstants.Login.TAG);
            requestQueue.add(jsonRequest);
        }


    }

    private void formatCardExpiringDate(Editable s) {
        String input = s.toString();
        if (s.length() == 2 && !mLastInput.endsWith("/") && isSlash) {
            isSlash = false;
            int month = Integer.parseInt(input);
            if (month <= 12) {
                mExpiryDate.setText(mExpiryDate.getText().toString().substring(0, 1));
                mExpiryDate.setSelection(mExpiryDate.getText().toString().length());
            } else {
                s.clear();
                mExpiryDate.setText("");
                mExpiryDate.setSelection(mExpiryDate.getText().toString().length());
                //Toast.makeText(context.getApplicationContext(), "Enter a valid month", Toast.LENGTH_LONG).show();
            }
        } else if (s.length() == 2 && !mLastInput.endsWith("/") && !isSlash) {
            isSlash = true;
            int month = Integer.parseInt(input);
            if (month <= 12) {
                mExpiryDate.setText(mExpiryDate.getText().toString() + "/");
                mExpiryDate.setSelection(mExpiryDate.getText().toString().length());
            } else if (month > 12) {
                mExpiryDate.setText("");
                mExpiryDate.setSelection(mExpiryDate.getText().toString().length());
                s.clear();

            }


        } else if (s.length() == 1) {

            int month = Integer.parseInt(input);
            if (month > 1 && month < 12) {
                isSlash = true;
                mExpiryDate.setText("0" + mExpiryDate.getText().toString() + "/");
                mExpiryDate.setSelection(mExpiryDate.getText().toString().length());
            }
        } else if (s.length() == 5) {
            if (s.toString().contains("/")) {
                Logger.printDebug(s.toString());
                compareWithCurrentDate();
            } else {
                mExpiryDate.setText("");
            }
        }
        mLastInput = mExpiryDate.getText().toString();
    }

    private void compareWithCurrentDate() {

        String input = mExpiryDate.getText().toString();
        Logger.printDebug(input);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/yy");
        simpleDateFormat.setLenient(false);
        try {
            Date expiry = simpleDateFormat.parse(input);
            boolean expired = expiry.before(new Date());
            if (expired) {
                mExpiryDate.setError("Invalid date");
                mExpiryDate.requestFocus();
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }


    private void showProgress(final boolean show) {
        super.setActionInProgress(show);
        if (show) {
            mCustomProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            mCustomProgressDialog.setCancelable(false);
            mCustomProgressDialog.show();
        } else {
            mCustomProgressDialog.dismiss();
        }
    }

    private void showCardType(String creitCardType) {
        if (creitCardType.equalsIgnoreCase("visa")) {
            mIvCardIcon.setImageResource(R.drawable.visa);
        } else if (creitCardType.equalsIgnoreCase("MasterCard")) {
            mIvCardIcon.setImageResource(R.drawable.mastercard);
        } else if (creitCardType.equalsIgnoreCase("Maestro")) {
            mIvCardIcon.setImageResource(R.drawable.maestro);
        } else if (creitCardType.equalsIgnoreCase("Dankort")) {

        } else if (creitCardType.equalsIgnoreCase("Discover")) {
            mIvCardIcon.setImageResource(R.drawable.discover);
        } else if (creitCardType.equalsIgnoreCase("China Union pay")) {
            mIvCardIcon.setImageResource(R.drawable.unionpay);
        } else if (creitCardType.equalsIgnoreCase("American Express")) {
            mIvCardIcon.setImageResource(R.drawable.americanexpress);
        }
    }


}
