package com.android.dladle.activities;

/**
 * Created by sourav on 17-11-2016.
 */

import android.animation.AnimatorInflater;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.UnderlineSpan;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.dladle.R;
import com.android.dladle.customui.MyJsonObjectRequest;
import com.android.dladle.customui.TextViewUtils;
import com.android.dladle.services.DeleteTokenService;
import com.android.dladle.utils.CommonConstants;
import com.android.dladle.utils.Logger;
import com.android.dladle.utils.NetworkConstants;
import com.android.dladle.utils.NetworkUtils;
import com.android.dladle.utils.PreferenceManager;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static android.Manifest.permission.READ_CONTACTS;

public class UserLoginActivity extends BaseActivity implements LoaderManager.LoaderCallbacks<Cursor>, BaseActivity.OnSoftKeyboardListener {

    /**
     * Id to identity READ_CONTACTS permission request.
     */
    private static final int REQUEST_READ_CONTACTS = 0;
    private boolean mLoginInProgress, isAutoLogin;
    private ObjectAnimator mProgressAnimator;

    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private TextInputLayout mPasswordContainer;
    private View mProgressView, mLoginFormView, mScrollView, mHeaderView, mLogoView;
    private Button mLoginButton;
    private TextView mForgotPassword, mProgressText, mSignUp;
    private Switch mRememberSwitch;
    private String mDeviceRegistrationToken = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initViews();
        checkAndTryAutoLogin();
    }

    private void initViews() {
        boolean isFirstLaunch = PreferenceManager.getInstance(this).isFirstTimeLaunch();
        setContentView(R.layout.activity_user_login);
        //   FirebaseApp.initializeApp(this);

        Intent i = getIntent();
        if (i.getBooleanExtra(NetworkConstants.JsonResponse.STATUS_SUCCESS, false)) {
            showMessage(getString(R.string.reset_password_success));
            isFirstLaunch = false;
        }
        super.setOnSoftKeyboardListener(this);
        mScrollView = findViewById(R.id.login_form);
        mHeaderView = findViewById(R.id.include);
        mRootView = findViewById(R.id.rootView);
        mLogoView = findViewById(R.id.headerView);
        //mDeviceRegistrationToken = FirebaseInstanceId.getInstance().getToken();
        //Logger.printDebug(mDeviceRegistrationToken);
        super.setPressBackTwiceToExitFlag(!isFirstLaunch);
        // Set up the login form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.username);
        populateAutoComplete();

        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    hindKeyBoardAndDoLogin(null, null);
                    return true;
                }
                return false;
            }
        });

        mLoginButton = (Button) findViewById(R.id.btn_login);
        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hindKeyBoardAndDoLogin(null, null);
            }
        });
        mPasswordContainer = (TextInputLayout) findViewById(R.id.password_container);

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.progress_bar);
        mProgressText = (TextView) findViewById(R.id.progress_bar_text);
        mAppBackButton = (ImageButton) findViewById(R.id.back_btn);
        mAppBackButton.setVisibility(isFirstLaunch ? View.VISIBLE : View.GONE);
        mAppBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        mForgotPassword = (TextView) findViewById(R.id.forgot_password);
        String text = getString(R.string.forgot_password);
        SpannableString content = new SpannableString(text);
        content.setSpan(new UnderlineSpan(), 0, text.length(), 0);
        mForgotPassword.setText(content);
        mForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initRestorePassword();
            }
        });
        mSignUp = (TextView) findViewById(R.id.new_reg);
        text = getString(R.string.sign_up);
        content = new SpannableString(text);
        content.setSpan(new UnderlineSpan(), 0, text.length(), 0);
        mSignUp.setText(content);
        mSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initSignUp();
            }
        });
        mSignUp.setVisibility(isFirstLaunch ? View.GONE : View.VISIBLE);
        mRememberSwitch = (Switch) findViewById(R.id.remember_me);
        mRememberSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    PreferenceManager.getInstance(getApplicationContext()).resetCredentials();
                } else if (!isAutoLogin) {
                    showMessage(getString(R.string.save_credential_checked));
                }
            }
        });
    }

    private void checkAndTryAutoLogin() {
        String email = PreferenceManager.getInstance(this).getUserName();
        String password = PreferenceManager.getInstance(this).getPassword();
        if (TextUtils.isEmpty(email) || TextUtils.isEmpty(password)) {
            return;
        }
        isAutoLogin = true;
        hindKeyBoardAndDoLogin(email, password);
    }

    private void populateAutoComplete() {
        if (!mayRequestContacts()) {
            return;
        }
        Logger.printDebug("Auto populate loader init");
        getLoaderManager().initLoader(0, null, this);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<>();
        if (cursor == null || cursor.isClosed()) {
            return;
        }
        cursor.moveToFirst();
        Logger.printDebug("onLoadFinished");
        while (!cursor.isAfterLast()) {
            Logger.printDebug("email : " + cursor.getString(ProfileQuery.ADDRESS));
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }
        addEmailsToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(UserLoginActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        mEmailView.setAdapter(adapter);
    }

    private boolean mayRequestContacts() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(READ_CONTACTS)) {
            Snackbar.make(mEmailView, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
                        }
                    });
        } else {
            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
        }
        return false;
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Volley.newRequestQueue(this).cancelAll(NetworkConstants.Login.TAG);
    }

    @Override
    protected void onDestroy() {
        super.resetOnKeyBoardListener();
        super.onDestroy();
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_READ_CONTACTS) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                populateAutoComplete();
            }
        }
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void doLogin(String aEmail, String aPassWord) {
        if (mLoginInProgress) {
            return;
        }

        // Store values at the time of the login attempt.
        final String email;
        final String password;

        if (!TextUtils.isEmpty(aEmail) && !TextUtils.isEmpty(aPassWord)) {
            email = aEmail;
            password = aPassWord;
            mEmailView.setText(email);
            mPasswordView.setText(password);
            mRememberSwitch.setChecked(true);
        } else {
            email = TextViewUtils.removeEndSpaces(mEmailView.getText().toString());
            password = mPasswordView.getText().toString();
        }

        if (!NetworkUtils.isNetworkAvailable(this)) {
            Logger.printInfo("Not connected to internet");
            showMessage(getString(R.string.no_internet));
            mLoginButton.setEnabled(true);
            mAppBackButton.setEnabled(true);
            return;
        }

        // Reset errors.
        mEmailView.setError(null);
        mPasswordContainer.setError(null);
        mPasswordView.setError(null);

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!CommonConstants.isPasswordValid(password)) {
            TextViewUtils.setPasswordViewMargin(mPasswordView);
            mPasswordView.setError(getString(R.string.error_invalid_password));
            Toast.makeText(this, getString(R.string.invalid_password_message), Toast.LENGTH_LONG).show();
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!CommonConstants.isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
            mLoginButton.setEnabled(true);
            mAppBackButton.setEnabled(true);
        } else {
            // Show a progress spinner
            showProgress(true);
            JSONObject request = new JSONObject();
            try {
                request.put(NetworkConstants.Login.EMAIL_KEY, email);
                request.put(NetworkConstants.Login.PASSWORD_KEY, password);
                Logger.printDebug("JSON : " + request);
            } catch (Exception e) {
                showProgress(false);
                e.printStackTrace();
            }
            MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.POST, NetworkConstants.Login.URL, request,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Logger.printDebug("" + response);
                            boolean needToShowForm = true;
                            try {
                                String status = response.getString(NetworkConstants.JsonResponse.STATUS);
                                String message = response.getString(NetworkConstants.JsonResponse.MESSAGE);
                                Logger.printDebug("Status : " + status + " Message : " + message);
                                if (NetworkConstants.JsonResponse.STATUS_SUCCESS.equalsIgnoreCase(status)) {
                                    JSONObject data = response.getJSONObject(NetworkConstants.JsonResponse.DATA);
                                    String userType = "";
                                    String firstName = "";
                                    String lastName = "";
                                    String mobileNumber = "";
                                    String tenantPropertyJoindate = "";
                                    float rating = 0.0f;
                                    boolean isVerified = false;
                                    if (data != null) {
                                        userType = data.getString(NetworkConstants.Registration.USER_TYPE_KEY);
                                        firstName = data.getString(NetworkConstants.Registration.FIRST_NAME_KEY);
                                        lastName = data.getString(NetworkConstants.Registration.LAST_NAME_KEY);
                                        mobileNumber = data.getString(NetworkConstants.Registration.CELL_NUMBER);
                                        isVerified = data.getBoolean("verified");
                                        tenantPropertyJoindate = data.optString("tenantPropertyJoinedDate");
                                    }
                                    Logger.printDebug("userType : " + userType + " isVerified : " + isVerified);
                                    if (isVerified) {
                                        needToShowForm = false;
                                        PreferenceManager.getInstance(getApplicationContext()).setFirstTimeLaunch();
                                        PreferenceManager.getInstance(getApplicationContext()).setUserType(userType);
                                        PreferenceManager.getInstance(getApplicationContext()).setProfileImageUrl(data.optString("profilePicture"));
                                        PreferenceManager.getInstance(getApplicationContext()).setName(firstName, lastName);
                                        PreferenceManager.getInstance(getApplicationContext()).setCellNumber(mobileNumber);
                                        PreferenceManager.getInstance(getApplicationContext()).setUserName(email);
                                        PreferenceManager.getInstance(getApplicationContext()).setRating(rating);
                                        PreferenceManager.getInstance(getApplicationContext()).setVendorType(data.optString("serviceType"));
                                        PreferenceManager.getInstance(getApplicationContext()).setVendorAccountStatus(data.optBoolean("accountSet"));
                                        PreferenceManager.getInstance(getApplicationContext()).setNotificationCount(data.optInt("notificationsCount"));
                                        PreferenceManager.getInstance(getApplicationContext()).setPaymentAccountStatus(data.optBoolean("paymentAccountSet"));
                                        PreferenceManager.getInstance(getApplicationContext()).setHomeStatus(data.optBoolean("houseStatus"));
                                        Intent refreshToken = new Intent(UserLoginActivity.this, DeleteTokenService.class);
                                        startService(refreshToken);
                                        Intent intent = new Intent();
                                        if (mRememberSwitch.isChecked()) {
                                            PreferenceManager.getInstance(getApplicationContext()).setPassword(password);
                                        } else {
                                            intent.putExtra(NetworkConstants.Registration.PASSWORD_KEY, password);
                                        }
                                        intent.putExtra(NetworkConstants.JsonResponse.DATA, userType);
                                        intent.putExtra(NetworkConstants.Login.EMAIL_KEY, email);
                                        intent.putExtra(NetworkConstants.Registration.FIRST_NAME_KEY, firstName);
                                        intent.putExtra(NetworkConstants.Registration.LAST_NAME_KEY, lastName);
                                        intent.putExtra(NetworkConstants.Registration.CELL_NUMBER, mobileNumber);
                                        intent.putExtra(NetworkConstants.Registration.RATING, rating);
                                        intent.putExtra(UserProfileActivity.FROM_LOGIN, true);
                                        intent.putExtra("propertyJoininhDate", tenantPropertyJoindate);
                                        intent.setClass(UserLoginActivity.this, UserProfileActivity.class);
                                        mProgressView.setVisibility(View.GONE);
                                        mProgressText.setVisibility(View.GONE);
                                        transitionTo(intent);
                                        finishAffinity();
                                    } else {
                                        showMessage(getString(R.string.msg_not_verified));
                                    }
                                } else if (NetworkConstants.JsonResponse.STATUS_NOT_VERIFIED.equals(status)) {
                                    showMessage(getString(R.string.msg_not_verified));
                                } else {
                                    showMessage(getString(R.string.unexpected_error));
                                    showMessage(status + " : " + message);
                                }
                            } catch (JSONException e) {
                                Logger.printError("Unable to parse JSON : " + e.getMessage());
                                showMessage(getString(R.string.unexpected_error));
                            }
                            if (needToShowForm) {
                                showProgress(false);
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error instanceof TimeoutError) {
                                showMessage(getString(R.string.server_error));
                            } else {
                                showMessage(error.getMessage());
                            }
                            showProgress(false);
                            Logger.printError("Some error : " + error.toString());
                        }
                    });
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            jsonRequest.setTag(NetworkConstants.Login.TAG);
            requestQueue.add(jsonRequest);
        }
    }

    private void hindKeyBoardAndDoLogin(final String email, final String password) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mLoginButton.setEnabled(false);
                mAppBackButton.setEnabled(false);
                doLogin(email, password);
            }
        }, isShowingKeyBoard ? CommonConstants.HIDE_KEYBOARD_DELAY : 0);
        if (isShowingKeyBoard) {
            hideKeyboard();
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    private void showProgress(final boolean show) {
        super.setActionInProgress(show);
        mLoginInProgress = show;
//        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
//        mLoginFormView.animate().setDuration(shortAnimTime/2).alpha(
//                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
//            @Override
//            public void onAnimationEnd(Animator animation) {
//                mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
//            }
//        });

        mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        mLoginButton.setVisibility(show ? View.GONE : View.VISIBLE);
        mLoginButton.setEnabled(true);
        mLogoView.setVisibility(show ? View.GONE : View.VISIBLE);
        //mAppBackButton.setVisibility(show ? View.GONE : View.VISIBLE);
        mAppBackButton.setEnabled(true);
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressText.setText(R.string.try_login);
        mProgressText.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressAnimator = (ObjectAnimator) AnimatorInflater.loadAnimator(this, R.animator.flipping);
        mProgressAnimator.setTarget(mProgressView);
        mProgressAnimator.start();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onKeyboardShown(int currentKeyboardHeight) {
        Logger.printDebug("onKeyboardShown height : " + currentKeyboardHeight);
        if (mRootView.getLayoutParams() instanceof FrameLayout.LayoutParams) {
            FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) mRootView.getLayoutParams();
            lp.bottomMargin = currentKeyboardHeight;
            mRootView.setLayoutParams(lp);
        } else if (mRootView.getLayoutParams() instanceof RelativeLayout.LayoutParams) {
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mRootView.getLayoutParams();
            lp.bottomMargin = currentKeyboardHeight;
            mRootView.setLayoutParams(lp);
        }
        if (null != mLoginButton && null != mHeaderView && null != mScrollView) {
            int formMargin = (int) getResources().getDimension(R.dimen.act_form_margin_top);
            int appHeaderMargin = (int) getResources().getDimension(R.dimen.app_header_parent_margin_top);
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mScrollView.getLayoutParams();
            lp.bottomMargin += (mLoginButton.getHeight());
            lp.topMargin += (formMargin + appHeaderMargin + mHeaderView.getHeight());
            Logger.printDebug("Top margin : " + lp.topMargin + " Buttom margin : " + lp.bottomMargin);
            mScrollView.setLayoutParams(lp);
        }
    }

    @Override
    public void onKeyboardHidden() {
        Logger.printDebug("onKeyboardHidden");
        if (mRootView.getLayoutParams() instanceof FrameLayout.LayoutParams) {
            FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) mRootView.getLayoutParams();
            lp.bottomMargin = 0;
            mRootView.setLayoutParams(lp);
        } else if (mRootView.getLayoutParams() instanceof RelativeLayout.LayoutParams) {
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mRootView.getLayoutParams();
            lp.bottomMargin = 0;
            mRootView.setLayoutParams(lp);
        }
        if (null != mLoginButton && null != mHeaderView && null != mScrollView) {
            int formMargin = (int) getResources().getDimension(R.dimen.act_form_margin_top);
            int appHeaderMargin = (int) getResources().getDimension(R.dimen.app_header_parent_margin_top);
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mScrollView.getLayoutParams();
            lp.bottomMargin -= (mLoginButton.getHeight());
            lp.topMargin -= (formMargin + appHeaderMargin + mHeaderView.getHeight());
            Logger.printDebug("Top margin : " + lp.topMargin + " Buttom margin : " + lp.bottomMargin);
            mScrollView.setLayoutParams(lp);
        }
    }


    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }

    private void initRestorePassword() {
        Intent i = new Intent(this, ForgetPasswordActivity.class);
        transitionTo(i);
    }

    private void initSignUp() {
        boolean showUserTypePicker = getResources().getBoolean(R.bool.show_distinct_user_type);
        Intent i = new Intent(this, showUserTypePicker ? UserRegisterPickerActivity.class : UserRegisterActivity.class);
        transitionTo(i);
    }
}
