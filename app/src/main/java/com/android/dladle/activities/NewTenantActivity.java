package com.android.dladle.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.dladle.R;
import com.android.dladle.adapter.RecyclerAdapterForPropertyInTenanateRequestPage;
import com.android.dladle.customui.CustomProgressDialog;
import com.android.dladle.customui.MyJsonObjectRequest;
import com.android.dladle.model.NotificationListModel;
import com.android.dladle.model.PropertyListModel;
import com.android.dladle.utils.LinearLayoutTarget;
import com.android.dladle.utils.Logger;
import com.android.dladle.utils.NetworkConstants;
import com.android.dladle.utils.NetworkUtils;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class NewTenantActivity extends BaseActivity {
    ImageView mBack, mProfileImage;
    Button mAboutTenant, mAccept, mDecline;
    RecyclerView mPropertyList;
    TextView mInviteText;
    RecyclerView mPropertyListWithTenant;
    CustomProgressDialog mCustomProgressDialog;
    ArrayList<PropertyListModel> mPropertyListModel = new ArrayList<>();
    RecyclerAdapterForPropertyInTenanateRequestPage recyclerAdapterForPropertyInTenanateRequestPage;
    String mTenantname="",mTenantId="",mTenantProfiePicture="",mHouseId="",mSelectedHomeName="";
    int mNotificationId=0;
    NotificationListModel mNotificationListModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_tenant);
        initView();
        performAction();
    }
    private void initView() {
        mBack = (ImageView) findViewById(R.id.back_btn);
        mProfileImage = (ImageView) findViewById(R.id.profile_pic);
        mAboutTenant = (Button) findViewById(R.id.button_about_tenant);
        mAccept = (Button) findViewById(R.id.button_accept);
        mCustomProgressDialog = new CustomProgressDialog(NewTenantActivity.this);
        mDecline = (Button) findViewById(R.id.button_decline);
        mPropertyList = (RecyclerView) findViewById(R.id.rv_home);
        mInviteText = (TextView) findViewById(R.id.tv_invite_text);
        mPropertyListWithTenant=(RecyclerView)findViewById(R.id.rv_home);
        mPropertyListWithTenant.setLayoutManager(new LinearLayoutManager(this));
        String notification = getIntent().getStringExtra("notification");
        Gson gson = new Gson();
        mNotificationListModel = gson.fromJson(notification, NotificationListModel.class);
        if(getIntent().getExtras()!=null)
        {
            mTenantname=mNotificationListModel.getName();
            mTenantId=mNotificationListModel.getFrom();
            mTenantProfiePicture=mNotificationListModel.getProfilePicture();
            mNotificationId=mNotificationListModel.getId();
            Glide.with(this)
                    .load(mTenantProfiePicture)
                    .asBitmap()
                    .error(R.drawable.ic_avatar)
                    .into(mProfileImage);
        }
        if (NetworkUtils.isNetworkAvailable(NewTenantActivity.this)) {
            getPlaceList();
        }
        else {
            showMessage(getResources().getString(R.string.no_internet));
        }

        if(mNotificationListModel.isRead())
        {
            mAccept.setEnabled(false);
            mDecline.setEnabled(false);
        }



    }
    private void performAction() {
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mAboutTenant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(NewTenantActivity.this, AboutTenantActivity.class);
                intent.putExtra("tenantId",mTenantId);
              startActivity(intent);
            }
        });
        mAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                acceptTenantRequest();
            }
        });
        mDecline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                declineRequest();
            }
        });

    }
    private void getPlaceList() {
        showProgress(true);
        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.GET, NetworkConstants.AddPropertyLandLord.PROPERTY_LIST, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        showProgress(false);
                        Logger.printDebug("" + response);
                        if (response.optString("Status").equalsIgnoreCase(NetworkConstants.SUCCESS)) {
                            JSONArray data = response.optJSONArray("Data");
                            if (data != null && data.length() > 0) {
                                Gson gson=new Gson();
                                for (int i = 0; i < data.length(); i++) {
                                    JSONObject proerty = data.optJSONObject(i);
                                    PropertyListModel propertyListModel = gson.fromJson(proerty.toString(),PropertyListModel.class);
                                    mPropertyListModel.add(propertyListModel);
                                }
                                mInviteText.setText(getResources().getString(R.string.invite_yext,String.valueOf(mPropertyListModel.size())));
                                  recyclerAdapterForPropertyInTenanateRequestPage=new RecyclerAdapterForPropertyInTenanateRequestPage(NewTenantActivity.this,mPropertyListModel);
                                  mPropertyListWithTenant.setAdapter(recyclerAdapterForPropertyInTenanateRequestPage);
                                  mPropertyListWithTenant.setNestedScrollingEnabled(false);



                            } else {
                            }


                        } else {

                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof TimeoutError) {
                            showMessage(getString(R.string.server_error));
                        } else {
                            showMessage(error.getMessage());
                        }
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(NewTenantActivity.this);
        jsonRequest.setTag(NetworkConstants.Login.TAG);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(NetworkConstants.TIMOUT_TIME,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonRequest);
    }
    private void showProgress(final boolean show) {
        super.setActionInProgress(show);
        if (show) {
            mCustomProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            mCustomProgressDialog.setCancelable(false);
            mCustomProgressDialog.show();
        } else {
            mCustomProgressDialog.dismiss();
        }
    }
    private void assignHome()
    {
        showProgress(true);
        JSONObject input=new JSONObject();
        try {
            input.put("emailId",mTenantId);
            input.put("houseId",mHouseId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Logger.printDebug("input"+input);
        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.POST, NetworkConstants.Property.ASSIGN_HOME, input,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        showProgress(false);
                        showMessage(response.optString("Message"));
                        Logger.printDebug("" + response);
                        if (response.optString("Status").equalsIgnoreCase(NetworkConstants.SUCCESS)) {
                            markNotificationAsRead();
                            Intent intent=new Intent(NewTenantActivity.this,LandlordRequestStatusActivity.class);
                            intent.putExtra("tenantName",mTenantname);
                            intent.putExtra("houseName",mSelectedHomeName);
                            intent.putExtra("status",1);
                            startActivity(intent);

                        } else {

                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof TimeoutError) {
                            showMessage(getString(R.string.server_error));
                        } else {
                            showMessage(error.getMessage());
                        }
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(NewTenantActivity.this);
        jsonRequest.setTag(NetworkConstants.Login.TAG);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(NetworkConstants.TIMOUT_TIME,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonRequest);
    }
    private void acceptTenantRequest()
    {
        boolean isHomeSelected=false;
        for(int j=0;j<mPropertyListModel.size();j++)
        {
            if(mPropertyListModel.get(j).isSwitchOn())
            {
                isHomeSelected=true;
                mHouseId=String.valueOf(mPropertyListModel.get(j).getHouseId());
                if(NetworkUtils.isNetworkAvailable(NewTenantActivity.this)) {
                    assignHome();
                }
                else {
                    showMessage(getResources().getString(R.string.no_internet));
                }
               return;
            }
        }
        if(!isHomeSelected)
        {
            showMessage("Please select a place for tenant");
        }
    }
    public void selectHome(int i)
    {
        for(int j=0;j<mPropertyListModel.size();j++)
        {
            if(j==i)
            {
                mPropertyListModel.get(j).setSwitchOn(true);
                if(mPropertyListModel.get(i).getEstateName().equals(""))
                {
                    mSelectedHomeName = mPropertyListModel.get(i).getComplexName();
                }
                else {
                    mSelectedHomeName = mPropertyListModel.get(i).getEstateName();
                }
            }
            else
            {
                mPropertyListModel.get(j).setSwitchOn(false);
            }
        }
        recyclerAdapterForPropertyInTenanateRequestPage.notifyDataSetChanged();
    }
    private void markNotificationAsRead()
    {

        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.POST, NetworkConstants.Notification.NOTIFICATION_READ+String.valueOf(mNotificationId)+"&read=true", null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        showProgress(false);
                        //showMessage(response.optString("Message"));
                        Logger.printDebug("" + response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof TimeoutError) {
                            showMessage(getString(R.string.server_error));
                        } else {
                            showMessage(error.getMessage());
                        }
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(NewTenantActivity.this);
        jsonRequest.setTag(NetworkConstants.Login.TAG);
        requestQueue.add(jsonRequest);
    }
    private void declineRequest()
    {
        showProgress(true);
        final JSONObject input=new JSONObject();
        try {
            input.put("emailId",mTenantId);
            input.put("houseId",mHouseId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Logger.printDebug("input"+input);
        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.POST, NetworkConstants.Property.DECLINE_REQUEST, input,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        showProgress(false);
                        showMessage(response.optString("Message"));
                        Logger.printDebug("" + response);
                        if (response.optString("Status").equalsIgnoreCase(NetworkConstants.SUCCESS)) {
                            markNotificationAsRead();
                            Intent intent=new Intent(NewTenantActivity.this,LandlordRequestStatusActivity.class);
                            intent.putExtra("tenantName",mTenantname);
                            intent.putExtra("status",0);
                            startActivity(intent);

                        } else {

                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof TimeoutError) {
                            showMessage(getString(R.string.server_error));
                        } else {
                            showMessage(error.getMessage());
                        }
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(NewTenantActivity.this);
        jsonRequest.setTag(NetworkConstants.Login.TAG);
        requestQueue.add(jsonRequest);
    }

}
