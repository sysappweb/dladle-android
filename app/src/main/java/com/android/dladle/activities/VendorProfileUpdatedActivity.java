package com.android.dladle.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.dladle.R;
import com.android.dladle.utils.NetworkConstants;

public class VendorProfileUpdatedActivity extends BaseActivity {

    Button mOk;
    TextView mTextView1, mTextView2, mTextViewHeader;
    ImageView mPhoto, mPhotoContainer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor_profile_updated);
        mOk = (Button) findViewById(R.id.ok_button);
        mOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(VendorProfileUpdatedActivity.this, UserProfileActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(NetworkConstants.JsonResponse.DATA,"vendor");
                startActivity(intent);
            }
        });
        mPhotoContainer = (ImageView) findViewById(R.id.property_pic);
        mPhoto = (ImageView) findViewById(R.id.property_pic_small);
        mPhotoContainer.setImageDrawable(getResources().getDrawable(R.drawable.circle_shape_orange));
        mPhoto.setImageDrawable(getResources().getDrawable(R.mipmap.vendor_default));
        mTextViewHeader = (TextView) findViewById(R.id.header_text);
        mTextViewHeader.setText("Great");
        mTextView1 = (TextView) findViewById(R.id.confirmation_text);
        mTextView1.setText("You are ready to work!");
        mTextView2 = (TextView) findViewById(R.id.confirmation_text_summary);
        mTextView2.setText("You can now do official jobs at DlaDle app. Make sure to be \"At work\" so people can find you.");
    }
}
