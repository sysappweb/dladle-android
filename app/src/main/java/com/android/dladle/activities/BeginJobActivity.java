package com.android.dladle.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.android.dladle.R;

public class BeginJobActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_begin_job);
    }
}
