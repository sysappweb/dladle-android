package com.android.dladle.activities;

/**
 * Created by admin on 17/06/17.
 */

public class UserDetailsResponseModel {


    /**
     * emailId : farman.bluehorse@gmail.com
     * password : null
     * verified : true
     * userType : LANDLORD
     * firstName : Farman
     * lastName : Khan
     * mobileNumber : 7318794212
     * profilePicture :
     */

    private String emailId;
    private Object password;
    private boolean verified;
    private String userType;
    private String firstName;
    private String lastName;
    private String mobileNumber;
    private String profilePicture;

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public Object getPassword() {
        return password;
    }

    public void setPassword(Object password) {
        this.password = password;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }
}
