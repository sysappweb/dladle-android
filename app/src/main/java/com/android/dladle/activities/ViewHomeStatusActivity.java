package com.android.dladle.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.dladle.R;
import com.android.dladle.customui.CustomProgressDialog;
import com.android.dladle.customui.MyJsonObjectRequest;
import com.android.dladle.utils.Logger;
import com.android.dladle.utils.NetworkConstants;
import com.android.dladle.utils.NetworkUtils;
import com.android.dladle.utils.PreferenceManager;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class ViewHomeStatusActivity extends BaseActivity {
    ImageView mImageOne, mImageTwo, mImageThree, mImageFour,mIvAudio;
    CustomProgressDialog mCustomProgressDialog;
    ImageView mRecord,mIvBack;
    Button mOK;
    ArrayList<String> stringArrayList=new ArrayList<>();
    String audioPath="";
    LinearLayout mAudioContainer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_home_status);
        initView();
        performAction();
        if(NetworkUtils.isNetworkAvailable(this)) {
            getHomeStatus();
        }
        else {
            showMessage(getResources().getString(R.string.no_internet));
        }
    }
    private void initView()
    {
        mImageOne = (ImageView) findViewById(R.id.image_one);
        mImageTwo = (ImageView) findViewById(R.id.image_two);
        mImageThree = (ImageView) findViewById(R.id.image_three);
        mImageFour = (ImageView) findViewById(R.id.image_four);
        mCustomProgressDialog = new CustomProgressDialog(this);
        mRecord = (ImageView) findViewById(R.id.record);
        mIvBack=(ImageView)findViewById(R.id.back_btn);
        mOK = (Button) findViewById(R.id.ok_button);
        mIvAudio=(ImageView)findViewById(R.id.record);
        mAudioContainer=(LinearLayout)findViewById(R.id.audio_container);
    }
    private void performAction()
    {
        mIvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ViewHomeStatusActivity.this, UserProfileActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(NetworkConstants.JsonResponse.DATA, PreferenceManager.getInstance(getApplicationContext()).getUserType());
                startActivity(intent);
            }
        });
        mImageOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewImage(0);
            }
        });
        mImageTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewImage(1);
            }
        });
        mImageThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewImage(2);
            }
        });
        mImageFour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewImage(3);
            }
        });
        mIvAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playAudio();
            }
        });
    }
    private void viewImage(int position)
    {
        Intent intent=new Intent(ViewHomeStatusActivity.this,ViewImageActivity.class);
        intent.putStringArrayListExtra("image_url",stringArrayList);
        startActivity(intent);
    }
    private void showProgress(final boolean show) {
        super.setActionInProgress(show);
        if (show) {
            mCustomProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            mCustomProgressDialog.setCancelable(false);
            mCustomProgressDialog.show();
        } else {
            mCustomProgressDialog.dismiss();
        }
    }
    private void getHomeStatus() {
        showProgress(true);


        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.GET, NetworkConstants.TenantController.VIEW_DOCS, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Logger.printDebug("" + response);

                        showProgress(false);
                        showMessage(response.optString("Message"));
                        if (response.optString("Status").equalsIgnoreCase(NetworkConstants.SUCCESS)) {


                            JSONArray data=response.optJSONArray("Data");
                            for(int i=0;i<data.length();i++)
                            {
                                JSONObject jsonObject=data.optJSONObject(i);
                                String documentType=jsonObject.optString("documentType");
                                if(documentType.equalsIgnoreCase("IMAGE"))
                                {
                                    stringArrayList.add(jsonObject.optString("documentUrl"));
                                }
                                else if(documentType.equalsIgnoreCase("AUDIO"))
                                {
                                    audioPath=stringArrayList.get(i);
                                }
                            }
                         for(int j=0;j<stringArrayList.size();j++)
                         {
                             if(j==0)
                             {
                                 Glide.with(ViewHomeStatusActivity.this)
                                         .load(stringArrayList.get(j))
                                         .asBitmap()
                                         .into(mImageOne);
                                 mImageOne.setVisibility(View.VISIBLE);
                             }
                             else if(j==1)
                             {
                                 Glide.with(ViewHomeStatusActivity.this)
                                         .load(stringArrayList.get(j))
                                         .asBitmap()
                                         .into(mImageTwo);
                                 mImageTwo.setVisibility(View.VISIBLE);
                             }
                             else if(j==3)
                             {
                                 Glide.with(ViewHomeStatusActivity.this)
                                         .load(stringArrayList.get(j))
                                         .asBitmap()
                                         .into(mImageFour);
                                 mImageFour.setVisibility(View.VISIBLE);
                             }
                             else if(j==2)
                             {
                                 Glide.with(ViewHomeStatusActivity.this)
                                         .load(stringArrayList.get(j))
                                         .asBitmap()
                                         .into(mImageThree);
                                 mImageThree.setVisibility(View.VISIBLE);
                             }
                         }
                         if(!audioPath.equals(""))
                         {
                             mAudioContainer.setVisibility(View.VISIBLE);
                         }

                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showProgress(false);
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(ViewHomeStatusActivity.this);
        jsonRequest.setTag(NetworkConstants.UpdateVendor.TAG);
        requestQueue.add(jsonRequest);
    }
    private void playAudio()
    {
        try {
            MediaPlayer mp = new MediaPlayer();
            mp.setDataSource(audioPath);
            mp.prepare();
            mp.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
