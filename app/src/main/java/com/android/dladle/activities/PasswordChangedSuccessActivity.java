package com.android.dladle.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.android.dladle.R;

public class PasswordChangedSuccessActivity extends BaseActivity {
Button mBtnCntd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_changed_success);
        initView();
        performAction();
    }
    private void initView()
    {
        mBtnCntd=(Button)findViewById(R.id.btn_continue);
    }
    private void performAction()
    {
        mBtnCntd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }
}
