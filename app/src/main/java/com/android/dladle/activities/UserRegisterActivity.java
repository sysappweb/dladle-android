package com.android.dladle.activities;

/**
 * Created by sourav on 17-11-2016.
 */

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.android.dladle.R;
import com.android.dladle.fragments.BaseFragment;
import com.android.dladle.fragments.RegistrationFragment;

public class UserRegisterActivity extends BaseActivity implements BaseFragment.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_register);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.fragment_container, new RegistrationFragment(), "Reg_Fragment");
        fragmentTransaction.commit();
    }

    public void onBackKeyPressed() {
        onBackPressed();
    }

    @Override
    public void notifyActivity() {}

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
