package com.android.dladle.activities;

/**
 * Created by sourav on 17-11-2016.
 */

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.android.dladle.R;
import com.android.dladle.utils.CommonConstants;

public class UserRegisterPickerActivity extends BaseActivity {

    private Button mLandlord, mTenant, mVendor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_activity);

        mLandlord = (Button) findViewById(R.id.btn_landlord);
        mTenant = (Button) findViewById(R.id.btn_tenant);
        mVendor = (Button) findViewById(R.id.btn_vendor);

        mAppBackButton = (ImageButton) findViewById(R.id.back_btn);
        mAppBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        mLandlord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startRegActivity(CommonConstants.FRAGMENT_KEY_LANDLORD);
            }
        });

        mVendor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startRegActivity(CommonConstants.FRAGMENT_KEY_VENDOR);
            }
        });

        mTenant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startRegActivity(CommonConstants.FRAGMENT_KEY_TENANT);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void startRegActivity(String key) {
        Intent intent = new Intent();
        intent.setClass(this, UserRegisterActivity.class);
        intent.putExtra(CommonConstants.FRAGMENT_KEY,key);
        transitionTo(intent);
    }
}
