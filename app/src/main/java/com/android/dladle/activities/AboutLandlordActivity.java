package com.android.dladle.activities;

import android.content.ContentValues;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.provider.Contacts;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.dladle.R;
import com.android.dladle.adapter.RecyclerAdapterForReview;
import com.android.dladle.customui.CustomProgressDialog;
import com.android.dladle.customui.MyJsonObjectRequest;
import com.android.dladle.model.ReviewModel;
import com.android.dladle.utils.LinearLayoutTarget;
import com.android.dladle.utils.Logger;
import com.android.dladle.utils.NetworkConstants;
import com.android.dladle.utils.NetworkUtils;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AboutLandlordActivity extends BaseActivity {
    TextView mName, mSurname, mEmail, mContactNo, mProfileType, mRating;
    RatingBar mRatingBar;
    ImageView mBack, mUserProfile;
    RecyclerView mReviewList;
    CustomProgressDialog mCustomProgressDialog;
    Button mBtnSaveContacts;
    String mLandlordName = "", mLandlordPhoneNo = "",mLandlordEmail="";
    ArrayList<ReviewModel> reviewModelArrayList=new ArrayList<>();
    RecyclerView recyclerView;
    RecyclerAdapterForReview recyclerAdapterForReview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_landlord);
        initView();
        performAction();
    }

    private void initView() {
        mName = (TextView) findViewById(R.id.tv_name);
        mSurname = (TextView) findViewById(R.id.tv_surname);
        mEmail = (TextView) findViewById(R.id.tv_email);
        mContactNo = (TextView) findViewById(R.id.tv_cellno);
        mProfileType = (TextView) findViewById(R.id.et_usertype);
        mRating = (TextView) findViewById(R.id.et_rating);
        mRatingBar = (RatingBar) findViewById(R.id.rating_bar);
        mBack = (ImageView) findViewById(R.id.back_btn);
        mUserProfile = (ImageView) findViewById(R.id.profile_pic);
        mReviewList = (RecyclerView) findViewById(R.id.rv_review);
        mCustomProgressDialog = new CustomProgressDialog(AboutLandlordActivity.this);
        recyclerView = (RecyclerView)findViewById(R.id.rv_review);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setNestedScrollingEnabled(false);
        mBtnSaveContacts = (Button) findViewById(R.id.button_save_contact);
        String landlordId = getIntent().getStringExtra("landlordIdId");
        if (NetworkUtils.isNetworkAvailable(this)) {
            getDetails(landlordId);
        } else {
            showMessage(getResources().getString(R.string.no_internet));
        }
    }

    private void performAction() {
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mBtnSaveContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveContactsToDevice();
            }
        });

    }

    private void getDetails(String landlordId) {
        showProgress(true);
        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.GET, NetworkConstants.User.GET_USER_DETAILS + landlordId, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        showProgress(false);
                        Logger.printDebug("" + response);
                        if (response.optString("Status").equalsIgnoreCase(NetworkConstants.SUCCESS)) {
                            JSONObject landlordDetails = response.optJSONObject("Data");

                            mLandlordEmail = landlordDetails.optString("emailId");
                            String firstname=landlordDetails.optString("firstName");
                            String lastName=landlordDetails.optString("lastName");
                            mLandlordPhoneNo=landlordDetails.optString("mobileNumber");
                            mLandlordName=firstname + " "+lastName;
                            String profilePicture=landlordDetails.optString("profilePicture");

                            Glide.with(AboutLandlordActivity.this)
                                    .load(profilePicture)
                                    .asBitmap()
                                    .error(R.drawable.blankimage)
                                    .into(mUserProfile);
                            float rating= (float) landlordDetails.optDouble("rating");
                            int ratingPoviderCount=landlordDetails.optInt("numberOfRatedUsers");
                            mName.setText("Name : "+firstname);
                            mSurname.setText("Surname : "+lastName);
                            mEmail.setText("Email : "+mLandlordEmail);
                            if(mLandlordPhoneNo==null)
                            {
                                mContactNo.setText("Cell Number : Not provided" );
                            }
                            else {
                                mContactNo.setText("Cell Number : " + mLandlordPhoneNo);
                            }
                            mProfileType.setText("Profile type : Landlord");
                            mRating.setText("Rating from " +String.valueOf(ratingPoviderCount) +" people");
                            mRatingBar.setRating(rating);
                            JSONArray jsonArray=landlordDetails.optJSONArray("ratingViewDetails");
                            for(int i=0;i<jsonArray.length();i++)
                            {

                                JSONObject jsonObject=jsonArray.optJSONObject(i);
                                long rate = jsonObject.optLong("rate");
                                String comment=jsonObject.optString("comment");
                                String name=jsonObject.optString("name");
                                ReviewModel reviewModel=new ReviewModel();
                                reviewModel.setRate(rate);
                                reviewModel.setComment(comment);
                                reviewModel.setName(name);
                                reviewModelArrayList.add(reviewModel);
                            }

                            recyclerAdapterForReview=new RecyclerAdapterForReview(AboutLandlordActivity.this,reviewModelArrayList);
                            recyclerView.setAdapter(recyclerAdapterForReview);

                        } else {

                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof TimeoutError) {
                            showMessage(getString(R.string.server_error));
                        } else {
                            showMessage(error.getMessage());
                        }
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(AboutLandlordActivity.this);
        jsonRequest.setTag(NetworkConstants.Login.TAG);
        requestQueue.add(jsonRequest);
    }

    private void showProgress(final boolean show) {
        super.setActionInProgress(show);
        if (show) {
            mCustomProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            mCustomProgressDialog.setCancelable(false);
            mCustomProgressDialog.show();
        } else {
            mCustomProgressDialog.dismiss();
        }
    }

    private void saveContactsToDevice() {
        ContentValues values = new ContentValues();
        values.put(Contacts.People.NUMBER,mLandlordPhoneNo);
        values.put(Contacts.People.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_CUSTOM);
        values.put(Contacts.People.LABEL,mLandlordName);
        values.put(Contacts.People.NAME,mLandlordName);
        values.put(Contacts.People.PRIMARY_EMAIL_ID,mLandlordEmail);
        Uri dataUri = getContentResolver().insert(Contacts.People.CONTENT_URI, values);
        Uri updateUri = Uri.withAppendedPath(dataUri, Contacts.People.Phones.CONTENT_DIRECTORY);
        values.clear();
        values.put(Contacts.People.Phones.TYPE, Contacts.People.TYPE_MOBILE);
        values.put(Contacts.People.NUMBER, mLandlordPhoneNo);
        updateUri = getContentResolver().insert(updateUri, values);
        showMessage(getResources().getString(R.string.contact_save));
    }
}
