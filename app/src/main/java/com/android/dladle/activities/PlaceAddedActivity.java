package com.android.dladle.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.dladle.R;
import com.android.dladle.utils.Logger;
import com.android.dladle.utils.NetworkConstants;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import de.hdodenhof.circleimageview.CircleImageView;

public class PlaceAddedActivity extends Activity {
    TextView mDescription;
    ImageView mBack,mHome;
    CircleImageView mPropertyImage;
    Button mOK;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_added);
        initView();
        performAction();
    }

    private void initView() {
        mDescription = (TextView) findViewById(R.id.confirmation_text_summary);
        mBack = (ImageView) findViewById(R.id.back_btn);
        mPropertyImage = (CircleImageView) findViewById(R.id.property_image);
        mOK = (Button) findViewById(R.id.ok_button);
        mBack.setVisibility(View.GONE);
        mHome=(ImageView)findViewById(R.id.home_image);
        String placeName = getIntent().getStringExtra("placeName");
        mDescription.setText(getResources().getString(R.string.add_tenant,placeName));
       String imagePath = getIntent().getStringExtra("placeImage");

        Logger.printDebug("imagepath"+imagePath);
        if(imagePath.equals(""))
        {
            mPropertyImage.setVisibility(View.GONE);
            mHome.setVisibility(View.VISIBLE);
        }
        else {




            Glide.with(this).load("file:///" + imagePath).asBitmap().centerCrop().into(new BitmapImageViewTarget(mPropertyImage) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    mPropertyImage.setImageDrawable(circularBitmapDrawable);
                }
            });
        }

    }

    private void performAction() {
        mOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PlaceAddedActivity.this, UserProfileActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(NetworkConstants.JsonResponse.DATA,"landlord");
                startActivity(intent);
            }
        });
    }
}
