package com.android.dladle.activities;

import android.animation.AnimatorInflater;
import android.animation.ObjectAnimator;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.provider.Contacts;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.dladle.R;
import com.android.dladle.adapter.RecyclerAdpterForContact;
import com.android.dladle.adapter.RecyclerAdpterForViewContact;
import com.android.dladle.customui.CustomProgressDialog;
import com.android.dladle.customui.MyJsonObjectRequest;
import com.android.dladle.model.ContactModel;
import com.android.dladle.model.TenantListModel;
import com.android.dladle.utils.Logger;
import com.android.dladle.utils.NetworkConstants;
import com.android.dladle.utils.NetworkUtils;
import com.android.dladle.utils.PreferenceManager;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.android.dladle.R.*;

public class ViewContactActivity extends BaseActivity {
    Button mBtnSave;
    RecyclerView mRecyclerViewForContactList;
    ArrayList<ContactModel> mContactModelArrayList = new ArrayList<>();
    ImageView mIvVBack;
    int mHouseId;
    LinearLayoutManager mLinearLayoutManager;
    CustomProgressDialog mCustomProgressDialog;
    RecyclerAdpterForViewContact recyclerAdpterForViewContact;
    String userType = "";
    LinearLayout mLlAnotherContact;
    TextView mTvAddAnotherContact;
    View mSaveBtnDivider;
   public String mLandlordImagePath="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_contact);
        initView();
        performAction();
    }

    private void initView() {
        mHouseId = getIntent().getIntExtra("houseId", 0);
        mRecyclerViewForContactList = (RecyclerView) findViewById(R.id.rv_contact);
        mIvVBack = (ImageView) findViewById(R.id.back_btn);
        mCustomProgressDialog = new CustomProgressDialog(ViewContactActivity.this);
        mBtnSave = (Button) findViewById(R.id.button_save);
        mLinearLayoutManager = new LinearLayoutManager(this);
        mRecyclerViewForContactList.setLayoutManager(mLinearLayoutManager);
        mTvAddAnotherContact = (TextView) findViewById(R.id.tv_add_another_contact);
        mLlAnotherContact = (LinearLayout) findViewById(R.id.add_contact_container);
        mSaveBtnDivider = (View) findViewById(R.id.button_divider);
        userType = PreferenceManager.getInstance(getApplicationContext()).getUserType();
        if (userType.equalsIgnoreCase("landlord")) {
            mTvAddAnotherContact.setVisibility(View.VISIBLE);
            mLlAnotherContact.setVisibility(View.VISIBLE);
            mSaveBtnDivider.setBackgroundResource(mipmap.line_green);
        }
        if(NetworkUtils.isNetworkAvailable(this)) {
            getContactListByHouseId();
        }else {
            showMessage(getResources().getString(string.no_internet));
        }
    }

    private void performAction() {
        mBtnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveContacts();
            }
        });
        mIvVBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mTvAddAnotherContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ViewContactActivity.this, AddImportantContactsActivity.class);
                intent.putExtra("houseId", mHouseId);
                intent.putExtra("migratedFrom","ViewContact");
                startActivity(intent);
            }
        });
    }

    private void getContactListByHouseId() {
        String url = "";
        if (userType.equalsIgnoreCase("landlord")) {
            url = NetworkConstants.Property.CONTACT_LIST + mHouseId;
        } else {
            url = NetworkConstants.Property.CONTACT_LIST;
            mLandlordImagePath=getIntent().getStringExtra("landlordImage");
        }
        if (!NetworkUtils.isNetworkAvailable(this)) {
            showMessage(getString(R.string.no_internet));
        } else {
            showProgress(true);
            JSONObject request = new JSONObject();
            MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.GET, url, request,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Logger.printDebug("" + response);
                            if (response.optString("Status").equalsIgnoreCase(NetworkConstants.SUCCESS)) {
                                JSONArray data = response.optJSONArray("Data");
                                mContactModelArrayList.clear();
                                for (int i = 0; i < data.length(); i++) {
                                    JSONObject jsonObject = data.optJSONObject(i);
                                    int id = jsonObject.optInt("propertyContactId");
                                    String contactName = jsonObject.optString("name");
                                    String contactType = jsonObject.optString("contactType");
                                    String contactAddress = jsonObject.optString("address");
                                    String contactNumber = jsonObject.optString("contactNumber");
                                    Logger.printDebug("No " + contactNumber);
                                    ContactModel contactModel = new ContactModel(contactType, contactAddress, contactNumber, contactName, id);
                                    mContactModelArrayList.add(contactModel);
                                }
                                recyclerAdpterForViewContact = new RecyclerAdpterForViewContact(ViewContactActivity.this, mContactModelArrayList);
                                mRecyclerViewForContactList.setAdapter(recyclerAdpterForViewContact);
                                showProgress(false);
                            } else {
                                showProgress(false);
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error instanceof TimeoutError) {
                                showMessage(getString(R.string.server_error));
                            } else {
                                showMessage(error.getMessage());
                            }
                            showProgress(false);
                            Logger.printError("Some error : " + error.toString());
                        }
                    });
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            jsonRequest.setTag(NetworkConstants.Login.TAG);
            requestQueue.add(jsonRequest);
        }

    }

    private void showProgress(final boolean show) {
        super.setActionInProgress(show);
        if (show) {
            mCustomProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            mCustomProgressDialog.setCancelable(false);
            mCustomProgressDialog.show();
        } else {
            mCustomProgressDialog.dismiss();
        }
    }

    private void saveContacts() {
        for (int i = 0; i < mContactModelArrayList.size(); i++) {
            ContactModel contactModel = mContactModelArrayList.get(i);
            String name = contactModel.getName();
            String phone = contactModel.getPhoneNo();
            String address = contactModel.getAddress();
            ContentValues values = new ContentValues();
            values.put(Contacts.People.NUMBER, phone);
            values.put(Contacts.People.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_CUSTOM);
            values.put(Contacts.People.LABEL, name);
            values.put(Contacts.People.NAME, name);
            Uri dataUri = getContentResolver().insert(Contacts.People.CONTENT_URI, values);
            Uri updateUri = Uri.withAppendedPath(dataUri, Contacts.People.Phones.CONTENT_DIRECTORY);
            values.clear();
            values.put(Contacts.People.Phones.TYPE, Contacts.People.TYPE_MOBILE);
            values.put(Contacts.People.NUMBER, phone);
            updateUri = getContentResolver().insert(updateUri, values);
        }
        showMessage("Contcts saved successfully");
    }

    public void deleteContact(int contactId, final int position) {
        showProgress(true);

        JSONObject input = new JSONObject();
        try {
            input.put("contactId", contactId);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.POST, NetworkConstants.Property.DELETE_CONTACT, input,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        showProgress(false);
                        Logger.printDebug("" + response);
                        int length = 0;

                        if (response.optString("Status").equalsIgnoreCase(NetworkConstants.SUCCESS)) {
                            mContactModelArrayList.remove(position);
                            mRecyclerViewForContactList.setAdapter(null);
                            recyclerAdpterForViewContact = new RecyclerAdpterForViewContact(ViewContactActivity.this, mContactModelArrayList);
                            mRecyclerViewForContactList.setAdapter(recyclerAdpterForViewContact);

                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showProgress(false);
                        if (error instanceof TimeoutError) {
                            showMessage(getString(R.string.server_error));
                        } else {
                            showMessage(error.getMessage());
                        }
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        jsonRequest.setTag(NetworkConstants.Login.TAG);
        requestQueue.add(jsonRequest);
    }
}
