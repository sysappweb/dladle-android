package com.android.dladle.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.dladle.R;
import com.android.dladle.customui.CustomProgressDialog;
import com.android.dladle.customui.MyJsonObjectRequest;
import com.android.dladle.utils.Logger;
import com.android.dladle.utils.NetworkConstants;
import com.android.dladle.utils.NetworkUtils;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class LandlordRequestStatusActivity extends BaseActivity {
ImageView mStatus,mUserProfile,mBack;
    TextView mStatusDesription,mBottomText;
    Button mOk;
    RatingBar mRating;
    CustomProgressDialog mCustomProgressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landlord_request_status);
        initView();
        performAction();
    }
    private void initView()
    {
        mStatus=(ImageView)findViewById(R.id.iv_status);
        mUserProfile=(ImageView)findViewById(R.id.profile_pic);
        mBack=(ImageView)findViewById(R.id.back_btn);
        mStatusDesription=(TextView)findViewById(R.id.body_text);
        mOk=(Button)findViewById(R.id.button_ok);
        mBottomText=(TextView)findViewById(R.id.tv_bottom_txt);
        mRating=(RatingBar)findViewById(R.id.rating_bar);
        mCustomProgressDialog=new CustomProgressDialog(this);
        mBack.setVisibility(View.GONE);
        int status= getIntent().getIntExtra("status",0);
        if(status==1)
        {
            if(NetworkUtils.isNetworkAvailable(LandlordRequestStatusActivity.this)) {
                getDetailsOfLease();
            }
            else {
                showMessage(getResources().getString(R.string.no_internet));
            }
            mStatus.setImageResource(R.mipmap.tick);
            String houseName=getIntent().getStringExtra("houseName");
            String tenantName=getIntent().getStringExtra("tenantName");
            mBottomText.setText("You have added "+tenantName+" to your " +houseName +" property.We will notify " +tenantName);
        }
        else
        {
            mStatus.setImageResource(R.drawable.ic_small_cross);
            String tenantName=getIntent().getStringExtra("tenantName");
            mBottomText.setText("You have added "+tenantName+" .We will notify " +tenantName);
        }

    }
    private void performAction()
    {
      mOk.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
              Intent intent = new Intent(LandlordRequestStatusActivity.this, UserProfileActivity.class);
              intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
              intent.putExtra(NetworkConstants.JsonResponse.DATA,"landlord");
              startActivity(intent);

          }
      });
    }
    private void getDetailsOfLease()
    {
        showProgress(true);
        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.GET, NetworkConstants.LeaseController.LEASE_VIEW_BY_HOUSEID, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        showProgress(false);
                        Logger.printDebug("" + response);
                        if (response.optString("Status").equalsIgnoreCase(NetworkConstants.SUCCESS)) {
                            JSONObject data=response.optJSONObject("Data");
                            String leaseStartDate=convertDate(data.optString("leaseStartDate"));
                            mStatusDesription.setText("New Tenant added!\nLease begiins "+leaseStartDate+".");


                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showProgress(false);
                        if (error instanceof TimeoutError) {
                            showMessage(getString(R.string.server_error));
                        } else {
                            showMessage(error.getMessage());
                        }
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        jsonRequest.setTag(NetworkConstants.Login.TAG);
        requestQueue.add(jsonRequest);
    }
    private void showProgress(final boolean show) {
        super.setActionInProgress(show);
        if (show) {
            mCustomProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            mCustomProgressDialog.setCancelable(false);
            mCustomProgressDialog.show();
        } else {
            mCustomProgressDialog.dismiss();
        }
    }
    private String convertDate(String leaseDate)
    {
        SimpleDateFormat form = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date date = null;
        try
        {
            date = form.parse(leaseDate);
        }
        catch (ParseException e)
        {

            e.printStackTrace();
        }
        SimpleDateFormat postFormater = new SimpleDateFormat("dd MMMMM");
        String newDateStr = postFormater.format(date);
        return newDateStr;
    }

}
