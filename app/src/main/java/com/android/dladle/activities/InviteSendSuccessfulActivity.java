package com.android.dladle.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.android.dladle.R;
import com.android.dladle.utils.NetworkConstants;

public class InviteSendSuccessfulActivity extends BaseActivity {
ImageView mBack;
    Button mOK;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_send_successful);
        initView();
        performAction();
    }
    private void initView()
    {
        mBack=(ImageView)findViewById(R.id.back_btn);
        mOK=(Button)findViewById(R.id.button_ok);
        mBack.setVisibility(View.GONE);

    }
    private void performAction()
    {
        mOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(InviteSendSuccessfulActivity.this, UserProfileActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(NetworkConstants.JsonResponse.DATA,"landlord");
                startActivity(intent);
            }
        });
    }
}
