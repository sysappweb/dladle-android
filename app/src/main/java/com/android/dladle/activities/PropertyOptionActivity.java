package com.android.dladle.activities;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.dladle.R;
import com.android.dladle.customui.CustomProgressDialog;
import com.android.dladle.customui.MyJsonObjectRequest;
import com.android.dladle.model.NotificationListModel;
import com.android.dladle.model.PropertyListModel;
import com.android.dladle.model.TenantListModel;
import com.android.dladle.utils.CommonConstants;
import com.android.dladle.utils.LinearLayoutTarget;
import com.android.dladle.utils.Logger;
import com.android.dladle.utils.NetworkConstants;
import com.android.dladle.utils.NetworkUtils;
import com.android.dladle.utils.PreferenceManager;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PropertyOptionActivity extends BaseActivity {
    private ImageView mTenantOneImage, mTenanteTwoImage, mTenateThreeImage, mBack, mInviteTenantAndHomeDivider, mProfilePicOverlay;
    LinearLayout mPropertyImage;
    private TextView mTenanteOneName, mTenanteTwoName, mTenanteThreeName, mPropertyName;
    private Button mImportContacts, mAdviceForLandlords, mInviteTenante, mMyHome, mCallVendor, mTerminateLease, mDeletePlace, mAdviceForTenant, mRemoveTenant, mLeaveProperty;
    private final int MY_PERMISSIONS_REQUEST_CALLPHONE = 1;
    int mHouseId = 0, mPropertyId = 0, mTenantCount = 0, mContactCount = 0, mNotificationCount = 0;
    boolean mIsHome = false,mLeaseStatus;
    String mPropertyImagePath = "", mPropertyTitle = "";
    CustomProgressDialog mCustomProgressDialog;
    LinearLayout mHomeContainer, mInvitecontainer, mHomeAndInviteTenantContainer, mTenantContainer, mProfileOneContainer, mProfiletwoContainer, mProfilethreeContainer;
    View mDividerForAboutLandlord;
    ImageView mMoreNotification;
    LinearLayout mNotificationContainer;
    ArrayList<NotificationListModel> notificationListModelArrayList = new ArrayList<>();
    ArrayList<TenantListModel> tenantListModelArrayList = new ArrayList<>();
    TextView mNotificationOne, mNotificationTwo;
    String mUserType = "";
    View mViewAdvForLandlords, mViewForAddContact, mViewForCallVendor, mViewForTenantAdvice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_property_option);
        initView();
        getNotificationList();
        performAction();

    }

    private void initView() {
        mPropertyImage = (LinearLayout) findViewById(R.id.property_image);
        mTenantOneImage = (ImageView) findViewById(R.id.profile_pic1);
        mTenanteTwoImage = (ImageView) findViewById(R.id.profile_pic2);
        mTenateThreeImage = (ImageView) findViewById(R.id.profile_pic3);
        mBack = (ImageView) findViewById(R.id.back_btn);
        mTenanteOneName = (TextView) findViewById(R.id.tv_user_name1);
        mTenanteTwoName = (TextView) findViewById(R.id.tv_user_name2);
        mTenanteThreeName = (TextView) findViewById(R.id.tv_user_name3);
        mPropertyName = (TextView) findViewById(R.id.titleView);
        mImportContacts = (Button) findViewById(R.id.button_import_contacts);
        mAdviceForLandlords = (Button) findViewById(R.id.button_adv_landlords);
        mInviteTenante = (Button) findViewById(R.id.button_invite_tenant);
        mMyHome = (Button) findViewById(R.id.button_my_home);
        mCallVendor = (Button) findViewById(R.id.button_call_vendor);
        mTerminateLease = (Button) findViewById(R.id.button_terminate_lease);
        mDeletePlace = (Button) findViewById(R.id.button_delete_place);
        mCustomProgressDialog = new CustomProgressDialog(PropertyOptionActivity.this);
        mHomeContainer = (LinearLayout) findViewById(R.id.my_home_container);
        mInvitecontainer = (LinearLayout) findViewById(R.id.invite_tenante_container);
        mHomeAndInviteTenantContainer = (LinearLayout) findViewById(R.id.invite_tenant_and_home_container);
        mInviteTenantAndHomeDivider = (ImageView) findViewById(R.id.home_invitetenant_divider);
        mDividerForAboutLandlord = (View) findViewById(R.id.view_adv_for_landlord);
        mMoreNotification = (ImageView) findViewById(R.id.notification_more);
        mNotificationContainer = (LinearLayout) findViewById(R.id.notification_container);
        mNotificationOne = (TextView) findViewById(R.id.notification_one);
        mNotificationTwo = (TextView) findViewById(R.id.notification_two);
        mTenantContainer = (LinearLayout) findViewById(R.id.tenant_conatiner);
        mProfiletwoContainer = (LinearLayout) findViewById(R.id.profile_two_container);
        mProfileOneContainer = (LinearLayout) findViewById(R.id.profile_one_container);
        mProfilethreeContainer = (LinearLayout) findViewById(R.id.profile_three_container);
        mViewAdvForLandlords = (View) findViewById(R.id.view_adv_for_landlord);
        mViewForCallVendor = (View) findViewById(R.id.view_call_vendor);
        mViewForAddContact = (View) findViewById(R.id.view_add_contact);
        mAdviceForTenant = (Button) findViewById(R.id.button_adv_tenant);
        mViewForTenantAdvice = (View) findViewById(R.id.view_adv_for_tenant);
        mProfilePicOverlay = (ImageView) findViewById(R.id.profie_pic_overlay);
        mLeaveProperty = (Button) findViewById(R.id.button_leave_property);
        mRemoveTenant = (Button) findViewById(R.id.button_remove_tenant);


        // intent value
        mHouseId = getIntent().getIntExtra("houseId", 0);
        Log.d("HouseId", String.valueOf(mHouseId));
        mPropertyId = getIntent().getIntExtra("propertyId", 0);
        mNotificationCount = getIntent().getIntExtra("notificationCount", 0);
        mContactCount = getIntent().getIntExtra("contactCount", 0);
        mTenantCount = getIntent().getIntExtra("tenantCount", 0);
        mIsHome = getIntent().getBooleanExtra("isHome", false);
        mPropertyImagePath = getIntent().getStringExtra("propertyImagePath");
        mLeaseStatus=getIntent().getBooleanExtra("leaseStatus",false);
        mPropertyTitle = getIntent().getStringExtra("propertyName");
        mUserType = PreferenceManager.getInstance(getApplicationContext()).getUserType();
        mPropertyName.setText(mPropertyTitle);
        Glide.with(this)
                .load(mPropertyImagePath)
                .asBitmap()
                .error(R.drawable.blankimage)
                .into(new LinearLayoutTarget(this, (LinearLayout) mPropertyImage));
        // visibility
        if (mContactCount >= 2) {
            mImportContacts.setText("Manage contact");
        }
        if (mUserType.equalsIgnoreCase("Landlord")) {
            mAdviceForTenant.setVisibility(View.GONE);
            mViewForTenantAdvice.setVisibility(View.GONE);
            if (mIsHome || mTenantCount >= 3) {
                mHomeAndInviteTenantContainer.setVisibility(View.GONE);
                mDeletePlace.setVisibility(View.GONE);
                mTerminateLease.setVisibility(View.VISIBLE);
            }
            if (mTenantCount > 0 && mTenantCount < 3) {
                mInviteTenantAndHomeDivider.setVisibility(View.GONE);
                mHomeContainer.setVisibility(View.GONE);
                mDeletePlace.setVisibility(View.GONE);
                mTerminateLease.setVisibility(View.VISIBLE);
            }
            if (mIsHome) {
                mDeletePlace.setVisibility(View.VISIBLE);
                mTerminateLease.setVisibility(View.GONE);
                mAdviceForLandlords.setVisibility(View.GONE);
                mDividerForAboutLandlord.setVisibility(View.GONE);
            } else {
                mDeletePlace.setVisibility(View.GONE);
                if(NetworkUtils.isNetworkAvailable(PropertyOptionActivity.this)) {
                    getTenantList();
                }
                else {
                    showMessage(getResources().getString(R.string.no_internet));
                }
            }
            if (mContactCount >= 4) {
                mImportContacts.setText("Manage contacts");
            }
            if ((mTenantCount == 0) || mIsHome) {
                if(!mLeaseStatus) {
                    mDeletePlace.setVisibility(View.VISIBLE);
                }
                else {
                    mDeletePlace.setVisibility(View.GONE);
                }
            }
            if (mTenantCount == 1) {
                mTerminateLease.setVisibility(View.VISIBLE);
                mRemoveTenant.setVisibility(View.GONE);
            }
            if (mTenantCount > 1) {
                mTerminateLease.setVisibility(View.VISIBLE);
                mRemoveTenant.setVisibility(View.VISIBLE);
            }


        } else if (mUserType.equalsIgnoreCase("Tenant")) {
            mTerminateLease.setVisibility(View.GONE);
            mLeaveProperty.setVisibility(View.VISIBLE);
            mImportContacts.setText("View Contacts");
            mHomeAndInviteTenantContainer.setVisibility(View.GONE);
            mDeletePlace.setVisibility(View.GONE);
            mAdviceForLandlords.setVisibility(View.GONE);
            mAdviceForTenant.setVisibility(View.VISIBLE);
            mViewForTenantAdvice.setVisibility(View.VISIBLE);
            mViewAdvForLandlords.setVisibility(View.GONE);
            mViewForCallVendor.setBackground(ContextCompat.getDrawable(PropertyOptionActivity.this, R.mipmap.line_blue));
            mViewForAddContact.setBackground(ContextCompat.getDrawable(PropertyOptionActivity.this, R.mipmap.line_blue));
           if (NetworkUtils.isNetworkAvailable(PropertyOptionActivity.this)) {
               getTenantList();
           }
           else
           {
               showMessage(getResources().getString(R.string.no_internet));
           }
            mTenantContainer.setVisibility(View.VISIBLE);
        }


    }

    private void performAction() {
        mImportContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mUserType.equalsIgnoreCase("tenant") || mContactCount >= 2) {
                    Intent intent = new Intent(PropertyOptionActivity.this, ViewContactActivity.class);
                    intent.putExtra("houseId", mHouseId);
                    intent.putExtra("landlordName", getIntent().getStringExtra("landlordName"));
                    intent.putExtra("landlordImage", getIntent().getStringExtra("landlordImagePath"));
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(PropertyOptionActivity.this, AddImportantContactsActivity.class);
                    intent.putExtra("houseId", mHouseId);
                    startActivity(intent);
                }
            }
        });
        mAdviceForLandlords.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PropertyOptionActivity.this, LandlordAdviceActivity.class);
                startActivity(intent);
            }
        });
        mAdviceForTenant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PropertyOptionActivity.this, TenantAdviceActivity.class);
                startActivity(intent);
            }
        });
        mInviteTenante.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mContactCount >= 2) {

                    Intent intent = new Intent(PropertyOptionActivity.this, AddTenantByLandlord.class);
                    intent.putExtra("houseId", mHouseId);
                    startActivity(intent);
                } else {
                    showMessage("Please add atleast two contact before sending invite");
                }
            }
        });
        mMyHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               showAlertForMakeHome();
            }
        });
        mCallVendor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PropertyOptionActivity.this, RequestVendorActivity.class);
                intent.putExtra("houseId", mHouseId);
                startActivity(intent);
            }
        });
        mTerminateLease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showWarningForLeaseTermination();
            }
        });
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mDeletePlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              showAlertForDeleteHome();
            }
        });
        mMoreNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PropertyOptionActivity.this, NotificationListActivity.class);
                intent.putExtra("notificationList", notificationListModelArrayList);
                startActivity(intent);
            }
        });
        mProfiletwoContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              /*  if (mUserType.equalsIgnoreCase("tenant")) {
                    Intent intent = new Intent(PropertyOptionActivity.this, AboutLandlordActivity.class);
                    intent.putExtra("landlordIdId", getIntent().getStringExtra("landlordEmail"));
                    startActivity(intent);
                } else {*/
                Intent intent = new Intent(PropertyOptionActivity.this, AboutTenantActivity.class);
                if (tenantListModelArrayList.size() >= 2) {
                    intent.putExtra("tenantId", tenantListModelArrayList.get(1).getEmailId());
                }
                startActivity(intent);
                //  }

            }
        });
        mProfileOneContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PropertyOptionActivity.this, AboutTenantActivity.class);
                if (tenantListModelArrayList.size() > 0) {
                    intent.putExtra("tenantId", tenantListModelArrayList.get(0).getEmailId());
                }
                startActivity(intent);
            }
        });
        mProfilethreeContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PropertyOptionActivity.this, AboutTenantActivity.class);
                if (tenantListModelArrayList.size() > 2) {
                    intent.putExtra("tenantId", tenantListModelArrayList.get(2).getEmailId());
                }
                startActivity(intent);
            }
        });
        mNotificationOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickOnNotification(0);
            }
        });
        mNotificationTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickOnNotification(1);

            }
        });
        mLeaveProperty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showWarningForLeavingProperty();
            }
        });
        mRemoveTenant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PropertyOptionActivity.this, SelectTenantActivity.class);
                intent.putExtra("tenantList", tenantListModelArrayList);
                intent.putExtra("propertyName", mPropertyTitle);
                intent.putExtra("houseId", mHouseId);
                intent.putExtra("propertyImage", mPropertyImagePath);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALLPHONE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    makeCall();
                } else {

                    Toast.makeText(getApplicationContext(), "You have denied the permission to make call", Toast.LENGTH_SHORT).show();

                }
                return;
            }


        }
    }

    private void makeCall() {
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:07318794212"));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        startActivity(intent);
    }

    private void deletePlace() {
        showProgress(true);
        JSONObject input = new JSONObject();
        try {
            input.put("houseId", mHouseId);
            input.put("propertyId", mPropertyId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Logger.printDebug("" + input);
        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.POST, NetworkConstants.Property.DELETE_PLACE, input,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        showProgress(false);
                        Toast.makeText(getApplicationContext(), response.optString("Message"), Toast.LENGTH_SHORT).show();
                        Logger.printDebug("" + response);
                        if (response.optString("Status").equalsIgnoreCase(NetworkConstants.SUCCESS)) {
                            Intent intent = new Intent(PropertyOptionActivity.this, UserProfileActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra(NetworkConstants.JsonResponse.DATA, "landlord");
                            startActivity(intent);
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showProgress(false);
                        if (error instanceof TimeoutError) {
                            showMessage(getString(R.string.server_error));
                        } else {
                            showMessage(error.getMessage());
                        }
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(PropertyOptionActivity.this);
        jsonRequest.setTag(NetworkConstants.Login.TAG);
        requestQueue.add(jsonRequest);
    }

    private void showProgress(final boolean show) {
        super.setActionInProgress(show);
        if (show) {
            mCustomProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            mCustomProgressDialog.setCancelable(false);
            mCustomProgressDialog.show();
        } else {
            mCustomProgressDialog.dismiss();
        }
    }

    private void makeHome() {
        showProgress(true);
        JSONObject input = new JSONObject();
        try {
            input.put("houseId", mHouseId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Logger.printDebug("" + input);
        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.POST, NetworkConstants.Property.HOME, input,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        showProgress(false);
                        Logger.printDebug("" + response);
                        Toast.makeText(getApplicationContext(), response.optString("Message"), Toast.LENGTH_SHORT).show();
                        if (response.optString("Status").equalsIgnoreCase(NetworkConstants.SUCCESS)) {
                            Intent intent = new Intent(PropertyOptionActivity.this, UserProfileActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra(NetworkConstants.JsonResponse.DATA, "landlord");
                            startActivity(intent);
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showProgress(false);
                        if (error instanceof TimeoutError) {
                            showMessage(getString(R.string.server_error));
                        } else {
                            showMessage(error.getMessage());
                        }
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(PropertyOptionActivity.this);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(NetworkConstants.TIMOUT_TIME,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        jsonRequest.setTag(NetworkConstants.Login.TAG);
        requestQueue.add(jsonRequest);
    }

    private void getTenantList() {
        showProgress(true);
        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.GET, NetworkConstants.Property.TENANT_LIST + mHouseId, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        showProgress(false);
                        Logger.printDebug("" + response);
                        int length = 0;
                        if (response.optString("Status").equalsIgnoreCase(NetworkConstants.SUCCESS)) {
                            JSONArray jsonArray = response.optJSONArray("Data");
                            if (jsonArray != null) {
                                length = jsonArray.length();
                                Gson gson = new Gson();
                                for (int i = 0; i < length; i++) {
                                    JSONObject jsonObject = jsonArray.optJSONObject(i);
                                    tenantListModelArrayList.add(gson.fromJson(jsonObject.toString(), TenantListModel.class));
                                }
                            }
                        }
                        if (length > 0) {
                            mTenantContainer.setVisibility(View.VISIBLE);
                            if (length == 1) {
                                mProfileOneContainer.setVisibility(View.VISIBLE);
                                mTenanteOneName.setText(tenantListModelArrayList.get(0).getFirstName());
                            } else if (length == 2) {
                                mProfileOneContainer.setVisibility(View.VISIBLE);
                                mTenanteOneName.setText(tenantListModelArrayList.get(0).getFirstName());
                                mProfiletwoContainer.setVisibility(View.VISIBLE);
                                mTenanteTwoName.setText(tenantListModelArrayList.get(1).getFirstName());
                            } else if (length == 3) {
                                mProfileOneContainer.setVisibility(View.VISIBLE);
                                mTenanteOneName.setText(tenantListModelArrayList.get(0).getFirstName());
                                mProfiletwoContainer.setVisibility(View.VISIBLE);
                                mTenanteTwoName.setText(tenantListModelArrayList.get(1).getFirstName());
                                mProfilethreeContainer.setVisibility(View.VISIBLE);
                                mTenanteThreeName.setText(tenantListModelArrayList.get(2).getFirstName());
                            }
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showProgress(false);
                        if (error instanceof TimeoutError) {
                            showMessage(getString(R.string.server_error));
                        } else {
                            showMessage(error.getMessage());
                        }
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(PropertyOptionActivity.this);
        jsonRequest.setTag(NetworkConstants.Login.TAG);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(NetworkConstants.TIMOUT_TIME,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonRequest);
    }

    private void getNotificationList() {
        showProgress(true);
        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.GET, NetworkConstants.Notification.NOTIFICATION_LIST_BY_HOUSEID + String.valueOf(mHouseId), null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        showProgress(false);
                        int length = 0;
                        Logger.printDebug("" + response);
                        if (response.optString("Status").equalsIgnoreCase(NetworkConstants.SUCCESS)) {
                            JSONArray jsonArray = response.optJSONArray("Data");
                            if (jsonArray != null) {
                                length = jsonArray.length();
                                Gson gson = new Gson();
                                for (int i = 0; i<length; i++) {
                                    JSONObject jsonObject = jsonArray.optJSONObject(i);
                                    NotificationListModel notificationListModel = gson.fromJson(jsonObject.toString(), NotificationListModel.class);
                                    if (!notificationListModel.isRead()) {
                                        String data = notificationListModel.getData();
                                        List<String> items=Arrays.asList(data.split("\\s*,\\s*"));
                                        for(int j=0;j<items.size();j++)
                                        {
                                            if(items.get(j).contains("houseId"))
                                            {
                                                int index=items.get(j).lastIndexOf(":");
                                                notificationListModel.setHouseId(Integer.parseInt(items.get(j).substring(index+1)));
                                            }
                                            if(items.get(j).contains("leaseEndDate"))
                                            {
                                                int index=items.get(j).lastIndexOf(":");
                                                notificationListModel.setLeaseEnddate(items.get(j).substring(index+1));
                                            }
                                        }
                                        notificationListModelArrayList.add(0,notificationListModel);
                                    }
                                }
                            }
                        }
                        length=notificationListModelArrayList.size();
                        if (length == 0) {
                            mNotificationContainer.setVisibility(View.GONE);
                        } else {
                            mNotificationContainer.setVisibility(View.VISIBLE);
                            if (length >= 2) {
                                mNotificationTwo.setText(convertDate(notificationListModelArrayList.get(1).getTime()) + " : " + notificationListModelArrayList.get(1).getBody());
                                mNotificationOne.setText(convertDate(notificationListModelArrayList.get(0).getTime()) + " : " + notificationListModelArrayList.get(0).getBody());
                            }
                            if (length == 1) {
                                mNotificationOne.setText(convertDate(notificationListModelArrayList.get(0).getTime()) + " : " + notificationListModelArrayList.get(0).getBody());
                                mNotificationTwo.setVisibility(View.GONE);
                            }

                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showProgress(false);
                        if (error instanceof TimeoutError) {
                            showMessage(getString(R.string.server_error));
                        } else {
                            showMessage(error.getMessage());
                        }
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(PropertyOptionActivity.this);
        jsonRequest.setTag(NetworkConstants.Login.TAG);
        requestQueue.add(jsonRequest);
    }

    //to terminate lease wih all tenant
    private void terminateLeaseWithTenat(int leaseid) {
        showProgress(true);
        JSONObject input = new JSONObject();
        try {
            input.put("houseId", mHouseId);
            input.put("leaseId", leaseid);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Logger.printDebug(input.toString());

        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.POST, NetworkConstants.LeaseController.TERMINATION_REQUEST, input,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        showProgress(false);
                        Logger.printDebug("" + response);
                        showMessage(response.optString("Message"));
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showProgress(false);
                        if (error instanceof TimeoutError) {
                            showMessage(getString(R.string.server_error));
                        } else {
                            showMessage(error.getMessage());
                        }
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        jsonRequest.setTag(NetworkConstants.Login.TAG);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(NetworkConstants.TIMOUT_TIME,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonRequest);
    }

    private void showWarningForLeaseTermination() {
        final AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(PropertyOptionActivity.this);
        builder.setTitle("Terminate lease")
                .setMessage("Do you want to terminate lease?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        getDetailsOfLease();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void showWarningForLeavingProperty() {
        final AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(PropertyOptionActivity.this);
        builder.setTitle("Leave property")
                .setMessage("Do you want to leave this property?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        leaveProperty();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    //tenant want to leave property
    private void leaveProperty() {
        showProgress(true);
        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.POST, NetworkConstants.LeaseController.TENANT_LEAVE, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        showProgress(false);
                        Logger.printDebug("" + response);
                        if (response.optString("Status").equalsIgnoreCase(NetworkConstants.SUCCESS)) {
                            Intent intent = new Intent(PropertyOptionActivity.this, TenantRemovedActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("houseName", mPropertyTitle);
                            startActivity(intent);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showProgress(false);
                        if (error instanceof TimeoutError) {
                            showMessage(getString(R.string.server_error));
                        } else {
                            showMessage(error.getMessage());
                        }
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        jsonRequest.setTag(NetworkConstants.Login.TAG);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(NetworkConstants.TIMOUT_TIME,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonRequest);
    }

    private void getDetailsOfLease() {
        showProgress(true);
        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.GET, NetworkConstants.LeaseController.LEASE_VIEW_BY_HOUSEID + mHouseId, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        showProgress(false);
                        Logger.printDebug("" + response);
                        if (response.optString("Status").equalsIgnoreCase(NetworkConstants.SUCCESS)) {
                            JSONObject data = response.optJSONObject("Data");
                            int leaseId = data.optInt("leaseId");
                            terminateLeaseWithTenat(leaseId);

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showProgress(false);
                        if (error instanceof TimeoutError) {
                            showMessage(getString(R.string.server_error));
                        } else {
                            showMessage(error.getMessage());
                        }
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        jsonRequest.setTag(NetworkConstants.Login.TAG);
        requestQueue.add(jsonRequest);
    }

    private String convertDate(String leaseDate) {
        SimpleDateFormat form = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        java.util.Date date = null;
        try {
            date = form.parse(leaseDate);
        } catch (ParseException e) {

            e.printStackTrace();
        }
        SimpleDateFormat postFormater = new SimpleDateFormat("d MMM ''yy");
        String newDateStr = postFormater.format(date);
        return newDateStr;
    }

    private void clickOnNotification(int position) {
        Gson gson = new Gson();
        NotificationListModel notificationListModel1 = notificationListModelArrayList.get(position);
        int notificationTypeId = Integer.parseInt(notificationListModel1.getNotificationTypeId());
        Context context = PropertyOptionActivity.this;
        switch (notificationTypeId) {
            case CommonConstants.LANDLORD_PROPERTY_REQ_TO_TENANT:
                Intent intent1 = new Intent(context, InviteFromLandlordActivity.class);
                intent1.putExtra("notification", gson.toJson(notificationListModel1));
                startActivity(intent1);
                break;
            case CommonConstants.TENANT_PROPERTY_REQ_TO_LANDLORD:
                Intent intent = new Intent(context, NewTenantActivity.class);
                intent.putExtra("notification", gson.toJson(notificationListModel1));
                startActivity(intent);
                break;
            case CommonConstants.TENANT_ACCEPT_LANDLORDS_PROPERTY_INVITATION:
                Intent intent5 = new Intent(context, PropertyInvitationStatusOfTenant.class);
                intent5.putExtra("notification", gson.toJson(notificationListModel1));
                startActivity(intent5);
                break;
            case CommonConstants.TENANT_REJECT_LANDLORDS_PROPERTY_INVITATION:
                Intent intent7 = new Intent(context, PropertyInvitationStatusOfTenant.class);
                intent7.putExtra("notification", gson.toJson(notificationListModel1));
                startActivity(intent7);
                break;
            case CommonConstants.LANDLORD_ACCEPT_TENANTS_PROPRTY_INVITATION:
                Intent intent8 = new Intent(context, PropertyInvitationStatusOfTenant.class);
                intent8.putExtra("notification", gson.toJson(notificationListModel1));
                startActivity(intent8);
                break;
            case CommonConstants.LANDLORD_REJECT_TENANTS_PROPRTY_INVITATION:
                Intent intent2 = new Intent(context, InviteFromLandlordActivity.class);
                intent2.putExtra("notification", gson.toJson(notificationListModel1));
                startActivity(intent2);
                break;
            case CommonConstants.LEASE_TERMINATE_REQ_FROM_LANDLORD:
                Intent intent6 = new Intent(context, LeaseTerminationReqFromLandlord.class);
                intent6.putExtra("notification", gson.toJson(notificationListModel1));
                startActivity(intent6);
                break;
            case CommonConstants.LEASE_TERMINATE_REQ_FROM_TENANT:
                Intent intent11 = new Intent(context, LeaseTerminationReqFromTenantActivity.class);
                intent11.putExtra("notification", gson.toJson(notificationListModel1));
                startActivity(intent11);
                break;
            case CommonConstants.TENANT_ACCEPTED_LESAE_TERMINATION:
                Intent intent3 = new Intent(context, TerminationRequestStatusActivity.class);
                intent3.putExtra("notification", gson.toJson(notificationListModel1));
                startActivity(intent3);
                break;
            case CommonConstants.TENANT_REJECTED_LESAE_TERMINATION:
                Intent intent4 = new Intent(context, TerminationRequestStatusActivity.class);
                intent4.putExtra("notification", gson.toJson(notificationListModel1));
                startActivity(intent4);
                break;
            case CommonConstants.LANDLORD_ACCEPTED_LESAE_TERMINATION:
                break;
            case CommonConstants.LANDLORD_REJECTED_LESAE_TERMINATION:
                break;
            case CommonConstants.TENANT_LEFT_HOME:
                Intent intent9 = new Intent(context, TenantRemovedActivity.class);
                intent9.putExtra("notification", gson.toJson(notificationListModel1));
                startActivity(intent9);
                break;
            case CommonConstants.TENANT_REMOVED_BY_LANDLORD:
                Intent intent10 = new Intent(context, TenantRemovedActivity.class);
                intent10.putExtra("notification", gson.toJson(notificationListModel1));
                startActivity(intent10);
                break;
            case CommonConstants.RATE_TENANT:
                Intent intent12 = new Intent(context, ReviewActivity.class);
                intent12.putExtra("notification", gson.toJson(notificationListModel1));
                startActivity(intent12);
                break;
            case CommonConstants.RATE_LANDLORD:
                Intent intent13 = new Intent(context, ReviewActivity.class);
                intent13.putExtra("notification", gson.toJson(notificationListModel1));
                startActivity(intent13);
                break;
            case CommonConstants.RATE_VENDOR:
                break;
            default:
                break;
        }
    }

    private void showAlertForDeleteHome()
    {
        final AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(PropertyOptionActivity.this);
        builder.setTitle("Delete property")
                .setMessage("Do you want to delete this property?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if(NetworkUtils.isNetworkAvailable(PropertyOptionActivity.this)) {
                            deletePlace();
                        }
                        else {
                            showMessage(getResources().getString(R.string.no_internet));
                        }
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
    private void showAlertForMakeHome()
    {
        final AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(PropertyOptionActivity.this);
        builder.setTitle("Make home")
                .setMessage("Do you want to make this property to your home?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if(NetworkUtils.isNetworkAvailable(PropertyOptionActivity.this)) {
                            makeHome();
                        }
                        else {
                            showMessage(getResources().getString(R.string.no_internet));
                        }
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
}
