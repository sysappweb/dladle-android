package com.android.dladle.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.dladle.R;
import com.android.dladle.adapter.ImagePagerAdapter;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class ViewImageActivity extends BaseActivity {
    protected ViewPager mViewPager;
    protected ImagePagerAdapter imagePagerAdapter;
    protected ImageView mIvBack;
    protected TextView mTvCurrentImagePosition;
    int mTotalImageLength=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_image);
        initView();
    }

    private void initView() {
        mViewPager = (ViewPager) findViewById(R.id.viewPager);
        mIvBack=(ImageView)findViewById(R.id.iv_back);
        mTvCurrentImagePosition=(TextView)findViewById(R.id.current_image_position);
        Intent i = getIntent();
        ArrayList<String> list = i.getStringArrayListExtra("image_url");
        mTotalImageLength=list.size();
        mTvCurrentImagePosition.setText(String.valueOf(mTotalImageLength)+"/"+"1");
        imagePagerAdapter=new ImagePagerAdapter(this,list);
        mViewPager.setAdapter(imagePagerAdapter);
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                mTvCurrentImagePosition.setText(String.valueOf(mTotalImageLength)+"/"+String.valueOf(position+1));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mIvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


    }
}
