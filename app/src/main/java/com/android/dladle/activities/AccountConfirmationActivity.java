package com.android.dladle.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.android.dladle.R;
import com.android.dladle.customui.BoldTextView;

public class AccountConfirmationActivity extends BaseActivity {
    BoldTextView userWelcomeMessage;
    TextView emailVerificationText;
    Button mBtnCntd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_confirmation);
        initView();
        performAction();
    }

    private void initView() {
        userWelcomeMessage = (BoldTextView)findViewById(R.id.tv_user_welcome_msg);
        emailVerificationText=(TextView)findViewById(R.id.text_email_verification_text);
        mBtnCntd=(Button)findViewById(R.id.button_ok);

    }

    private void performAction() {

    }

}
