package com.android.dladle.activities;

import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.dladle.R;
import com.android.dladle.customui.CustomProgressDialog;
import com.android.dladle.customui.MyJsonObjectRequest;
import com.android.dladle.utils.AudioUtils;
import com.android.dladle.utils.ImageUtils;
import com.android.dladle.utils.Logger;
import com.android.dladle.utils.NetworkConstants;
import com.android.dladle.utils.NetworkUtils;
import com.android.dladle.utils.PhotoManager;
import com.android.dladle.utils.PreferenceManager;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class AddStatusHomeActivity extends BaseActivity {
    ImageView mImageOne, mImageTwo, mImageThree, mImageFour;
    private AlertDialog mAlertDialog;
    private boolean isCropSupported = true;
    ArrayList<String> imagePath = new ArrayList<>();
    LinearLayout mMoreImageContaniner, mAddMoreImage;
    Button mOK;
    AlertDialog alertDialog;
    CustomProgressDialog mCustomProgressDialog;
    ImageView mRecord,mIvBack;
    int imageNo = 0;
    String AudioSavePathInDevice = "";
    MediaRecorder mediaRecorder;
    public static final int RequestPermissionCode = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_status_home);
        initView();
        performAction();
    }
    private void initView()
    {
        mAddMoreImage = (LinearLayout) findViewById(R.id.tv_add_more_image);
        mMoreImageContaniner = (LinearLayout) findViewById(R.id.more_image_conatiner);
        mCustomProgressDialog = new CustomProgressDialog(this);
        mRecord = (ImageView) findViewById(R.id.record);
        mIvBack=(ImageView)findViewById(R.id.back_btn);
        mImageOne = (ImageView) findViewById(R.id.image_one);
        mImageTwo = (ImageView) findViewById(R.id.image_two);
        mImageThree = (ImageView) findViewById(R.id.image_three);
        mImageFour = (ImageView) findViewById(R.id.image_four);
        mOK = (Button) findViewById(R.id.ok_button);
        imagePath.add("");
        imagePath.add("");
        imagePath.add("");
        imagePath.add("");
    }
    private void performAction()
    {
        mIvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mAddMoreImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMoreImageContaniner.setVisibility(View.VISIBLE);
                mAddMoreImage.setVisibility(View.GONE);
            }
        });
        mRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recordAudio();
            }
        });

        mImageOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageNo = 1;
                selectImage();
            }
        });
        mImageTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageNo = 2;
                selectImage();
            }
        });
        mImageThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageNo = 3;
                selectImage();
            }
        });
        mImageFour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageNo = 4;
                selectImage();
            }
        });
        mOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetworkUtils.isNetworkAvailable(AddStatusHomeActivity.this)) {
                    if (validationChecking()) {
                        sendRequestToVendor();
                    }
                } else {
                    showMessage(getResources().getString(R.string.no_internet));
                }


            }
        });
    }
    private void recordAudio() {
        if (checkPermissionForAudio()) {

            File folder = new File(Environment.getExternalStorageDirectory() + "/dladle/vendor/audio");
            boolean success = true;
            if (!folder.exists()) {
                Logger.printDebug("not exist");
                success = folder.mkdirs();
            }
            if (success) {
                // Do something on success
                Logger.printDebug("success");
            } else {
                // Do something else on failure
                Logger.printDebug("Failed");
            }


            AudioSavePathInDevice =
                    Environment.getExternalStorageDirectory().getAbsolutePath() +
                            "/dladle/vendor/audio/" + getCurrentDateTime() + ".mp3";

            MediaRecorderReady();

            try {
                mediaRecorder.prepare();
                popUpForRecording();
                mediaRecorder.start();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        alertDialog.dismiss();
                        mediaRecorder.stop();
                    }
                }, 60000);
            } catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            Toast.makeText(AddStatusHomeActivity.this, "Recording started",
                    Toast.LENGTH_LONG).show();
        } else {
            requestPermission();
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(AddStatusHomeActivity.this, new
                String[]{WRITE_EXTERNAL_STORAGE, RECORD_AUDIO}, RequestPermissionCode);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case RequestPermissionCode:
                if (grantResults.length > 0) {
                    boolean StoragePermission = grantResults[0] ==
                            PackageManager.PERMISSION_GRANTED;
                    boolean RecordPermission = grantResults[1] ==
                            PackageManager.PERMISSION_GRANTED;

                    if (StoragePermission && RecordPermission) {
                        Toast.makeText(AddStatusHomeActivity.this, "Permission Granted",
                                Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(AddStatusHomeActivity.this, "Permission Denied",
                                Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }


    public void MediaRecorderReady() {
        mediaRecorder = new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        mediaRecorder.setOutputFile(AudioSavePathInDevice);
    }

    private void selectImage() {

        final CharSequence[] items = {"Take Photo", "Choose from Gallery", "Remove Picture", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    if (checkPermission(100)) {
                        try {
                            Uri photoUri = PhotoManager.getProfilePhotoURI(AddStatusHomeActivity.this);
                            if (null != photoUri) {
                                Intent intent = PhotoManager.getCameraIntentForThumbnail(photoUri);
                                startActivityForResult(intent, 100);
                            }
                        } catch (ActivityNotFoundException ex) {
                            Logger.printError(ex.getMessage());
                        }
                    } else {
                        showMessage("Please enable permission for Camera");
                    }
                } else if (items[item].equals("Choose from Gallery")) {
                    if (checkPermission(200)) {
                        try {
                            startActivityForResult(PhotoManager.galleryIntent(AddStatusHomeActivity.this), 200);
                        } catch (ActivityNotFoundException ex) {
                            Logger.printError(ex.getMessage());
                        }
                    } else {
                        showMessage("Please enable permission for Gallery");
                    }
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                } else if (items[item].equals("Remove Picture")) {
                    if (imageNo == 1) {
                        mImageOne.setImageResource(R.drawable.ic_camera_icon);
                        mImageOne.setScaleType(ImageView.ScaleType.CENTER);
                        imagePath.add(0,"");
                    } else if (imageNo == 2) {
                        mImageTwo.setImageResource(R.drawable.ic_camera_icon);
                        mImageTwo.setScaleType(ImageView.ScaleType.CENTER);
                        imagePath.add(1,"");
                    } else if (imageNo == 3) {
                        mImageThree.setImageResource(R.drawable.ic_camera_icon);
                        mImageThree.setScaleType(ImageView.ScaleType.CENTER);
                        imagePath.add(2,"");
                    } else {
                        mImageFour.setImageResource(R.drawable.ic_camera_icon);
                        mImageFour.setScaleType(ImageView.ScaleType.CENTER);
                        imagePath.add(3,"");
                    }
                }
            }
        });
        mAlertDialog = builder.create();
        mAlertDialog.show();
    }

    private boolean checkPermission(final int type) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        ArrayList<String> arr = new ArrayList<String>();
        ArrayList<String> arrFinal = new ArrayList<String>();
        if (200 == type) {
            arr.add(READ_EXTERNAL_STORAGE);
        } else if (100 == type) {
            arr.add(CAMERA);
        } else if (300 == type) {
            arr.add(ACCESS_FINE_LOCATION);
        } else {
            arr.add(CAMERA);
            arr.add(READ_EXTERNAL_STORAGE);
            arr.add(ACCESS_FINE_LOCATION);
        }
        arrFinal.addAll(arr);
        for (String s : arr) {
            if (AddStatusHomeActivity.this.checkSelfPermission(s) == PackageManager.PERMISSION_GRANTED) {
                arrFinal.remove(s);
            }
        }
        if (arrFinal.size() == 0) {
            return true;
        }
        String[] a = arrFinal.toArray(new String[arrFinal.size()]);
        requestPermissions(a, type);
        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 100) { // Gallery
                try {
                    Intent i = PhotoManager.getCroppedProfileThumbMailIntent(AddStatusHomeActivity.this);
                    if (i != null && isCropSupported) {
                        // startActivityForResult(i, 300);
                    } else {
                        if (imageNo == 1) {
                            imagePath.add(0, PhotoManager.updateAndCachePropertyPic(AddStatusHomeActivity.this, mImageOne, data.getData()));
                        } else if (imageNo == 2) {
                            imagePath.add(1, PhotoManager.updateAndCachePropertyPic(AddStatusHomeActivity.this, mImageTwo, data.getData()));
                        } else if (imageNo == 3) {
                            imagePath.add(2, PhotoManager.updateAndCachePropertyPic(AddStatusHomeActivity.this, mImageThree, data.getData()));
                        } else {
                            imagePath.add(3, PhotoManager.updateAndCachePropertyPic(AddStatusHomeActivity.this, mImageFour, data.getData()));
                        }

                    }
                } catch (ActivityNotFoundException ex) {
                    Logger.printError(ex.getMessage());
                } finally {
                    if (imageNo == 1) {
                        imagePath.add(0, PhotoManager.updateAndCachePropertyPic(AddStatusHomeActivity.this, mImageOne, data.getData()));
                    } else if (imageNo == 2) {
                        imagePath.add(1, PhotoManager.updateAndCachePropertyPic(AddStatusHomeActivity.this, mImageTwo, data.getData()));
                    } else if (imageNo == 3) {
                        imagePath.add(2, PhotoManager.updateAndCachePropertyPic(AddStatusHomeActivity.this, mImageThree, data.getData()));
                    } else {
                        imagePath.add(3, PhotoManager.updateAndCachePropertyPic(AddStatusHomeActivity.this, mImageFour, data.getData()));
                    }
                }
            } else if (requestCode == 200) { //Camera
                try {
                    Intent i = PhotoManager.getCroppedProfileThumbMailIntent(AddStatusHomeActivity.this);
                    if (i != null && isCropSupported) {
                        // startActivityForResult(i, 300);
                    } else {
                        if (imageNo == 1) {
                            imagePath.add(0, PhotoManager.updateAndCachePropertyPic(AddStatusHomeActivity.this, mImageOne, data.getData()));
                        } else if (imageNo == 2) {
                            imagePath.add(1, PhotoManager.updateAndCachePropertyPic(AddStatusHomeActivity.this, mImageTwo, data.getData()));
                        } else if (imageNo == 3) {
                            imagePath.add(2, PhotoManager.updateAndCachePropertyPic(AddStatusHomeActivity.this, mImageThree, data.getData()));
                        } else {
                            imagePath.add(3, PhotoManager.updateAndCachePropertyPic(AddStatusHomeActivity.this, mImageFour, data.getData()));
                        }
                    }
                } catch (ActivityNotFoundException ex) {
                    Logger.printError(ex.getMessage());
                } finally {
                    if (imageNo == 1) {
                        imagePath.add(0, PhotoManager.updateAndCachePropertyPic(AddStatusHomeActivity.this, mImageOne, data.getData()));
                    } else if (imageNo == 2) {
                        imagePath.add(1, PhotoManager.updateAndCachePropertyPic(AddStatusHomeActivity.this, mImageTwo, data.getData()));
                    } else if (imageNo == 3) {
                        imagePath.add(2, PhotoManager.updateAndCachePropertyPic(AddStatusHomeActivity.this, mImageThree, data.getData()));
                    } else {
                        imagePath.add(3, PhotoManager.updateAndCachePropertyPic(AddStatusHomeActivity.this, mImageFour, data.getData()));
                    }
                }
            } else if (requestCode == 300) { //Crop
                if (imageNo == 1) {
                    imagePath.add(0, PhotoManager.updateAndCachePropertyPic(AddStatusHomeActivity.this, mImageOne, data.getData()));
                } else if (imageNo == 2) {
                    imagePath.add(1, PhotoManager.updateAndCachePropertyPic(AddStatusHomeActivity.this, mImageTwo, data.getData()));
                } else if (imageNo == 3) {
                    imagePath.add(2, PhotoManager.updateAndCachePropertyPic(AddStatusHomeActivity.this, mImageThree, data.getData()));
                } else {
                    imagePath.add(3, PhotoManager.updateAndCachePropertyPic(AddStatusHomeActivity.this, mImageFour, data.getData()));
                }
            }
        }
    }

    private boolean checkPermissionForAudio() {
        return true;
    }

    private void sendRequestToVendor() {
        showProgress(true);
        JSONObject input = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        try {

            if (!AudioSavePathInDevice.equals("")) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("documentType", "AUDIO");
                jsonObject.put("base64Document", new AudioUtils().convertAudioToBaseString(AudioSavePathInDevice));
                jsonArray.put(jsonObject);
            }
            for (int i = 0; i < imagePath.size(); i++) {
                if (imagePath.get(i) != null && !imagePath.get(i).equals("")) {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("documentType", "IMAGE");
                    jsonObject.put("base64Document", ImageUtils.readFileAsBase64String(imagePath.get(i)));
                    jsonArray.put(jsonObject);
                }


            }
            input.put("imageList", jsonArray);
            Logger.printDebug(input.toString());


        } catch (JSONException e) {
            e.printStackTrace();
        }


        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.POST, NetworkConstants.TenantController.ADD_DOCS, input,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Logger.printDebug("" + response);

                        showProgress(false);
                        showMessage(response.optString("Message"));
                        if (response.optString("Status").equalsIgnoreCase(NetworkConstants.SUCCESS)) {
                            PreferenceManager.getInstance(getApplicationContext()).setHomeStatus(true);
                            Intent intent = new Intent(AddStatusHomeActivity.this, UserProfileActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra(NetworkConstants.JsonResponse.DATA, PreferenceManager.getInstance(getApplicationContext()).getUserType());
                            startActivity(intent);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showProgress(false);
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(AddStatusHomeActivity.this);
        jsonRequest.setTag(NetworkConstants.UpdateVendor.TAG);
        requestQueue.add(jsonRequest);
    }

    private void showProgress(final boolean show) {
        super.setActionInProgress(show);
        if (show) {
            mCustomProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            mCustomProgressDialog.setCancelable(false);
            mCustomProgressDialog.show();
        } else {
            mCustomProgressDialog.dismiss();
        }
    }
    private void popUpForRecording() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.recording_dialog, null);
        dialogBuilder.setView(dialogView);
        final TextView textView = (TextView) dialogView.findViewById(R.id.tv_timer);
        final Button stopRecording=(Button)dialogView.findViewById(R.id.button_stop_record);
        alertDialog = dialogBuilder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
        new CountDownTimer(60000, 1000) {

            public void onTick(long millisUntilFinished) {
                textView.setText("00 : " + String.valueOf(millisUntilFinished / 1000));
            }

            public void onFinish() {
                textView.setText("Done");
            }

        }.start();
        stopRecording.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                mediaRecorder.stop();
            }
        });
    }

    private boolean validationChecking() {
        String imageOnePath = imagePath.get(0);
        String imageTwoPath = imagePath.get(1);
        String imagePathThree = imagePath.get(2);
        String imagePathFour = imagePath.get(3);
       if (imageOnePath.equals("") && imageTwoPath.equals("") && imagePathThree.equals("") && imagePathFour.equals("") &&  AudioSavePathInDevice.equals("")) {
            showMessage("Please provide image or audio or short decription");
        }

        return true;

    }
    private String getCurrentDateTime() {
        Calendar c = Calendar.getInstance();
        return String.valueOf(c.get(Calendar.MILLISECOND));
    }

}
