package com.android.dladle.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.dladle.R;
import com.android.dladle.utils.LinearLayoutTarget;
import com.bumptech.glide.Glide;

public class RenewLeaseActivity extends BaseActivity {
    ImageView mBack,mProfileImage;
    Button mRenew,mTerminte;
    TextView mDesc,mTitle;
    String propertyName,propertyImagePath;
    int propertyId;
    LinearLayout mPropertyImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_renew_lease);
        initView();
        performAction();
    }
    private void initView()
    {
        mBack=(ImageView)findViewById(R.id.back_btn);
        mProfileImage=(ImageView)findViewById(R.id.profile_pic);
        mTitle=(TextView)findViewById(R.id.titleView);
        mRenew=(Button)findViewById(R.id.button_renew_lease);
        mTerminte=(Button)findViewById(R.id.button_terminate_lease);
        mDesc=(TextView)findViewById(R.id.renew_txt);
        mPropertyImage=(LinearLayout)findViewById(R.id.property_image);
        propertyName=getIntent().getStringExtra("propertyName");
        propertyImagePath=getIntent().getStringExtra("propertyImagePath");
        propertyId=getIntent().getIntExtra("houseId",0);
        mTitle.setText(propertyName);
        Glide.with(this)
                .load(propertyImagePath)
                .asBitmap()
                .error(R.drawable.blankimage)
                .into(new LinearLayoutTarget(this, (LinearLayout)mPropertyImage));

    }
    private void performAction()
    {
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mRenew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        mTerminte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RenewLeaseActivity.this, ReviewActivity.class);
                intent.putExtra("houseId",propertyId);
                intent.putExtra("propertyName",propertyName);
                intent.putExtra("propertyImagePath",propertyImagePath);
                startActivity(intent);
            }
        });
    }
}
