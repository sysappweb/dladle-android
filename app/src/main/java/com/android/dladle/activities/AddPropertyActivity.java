package com.android.dladle.activities;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import com.android.dladle.R;
import com.android.dladle.entities.Property;
import com.android.dladle.fragments.AddContactsFragment;
import com.android.dladle.fragments.AddPropertyFragment;
import com.android.dladle.fragments.BaseFragment;
import com.android.dladle.fragments.PropertyAddedFragment;
import com.android.dladle.utils.Logger;

/**
 * Created by sourav on 15-03-2017.
 */

public class AddPropertyActivity extends BaseActivity implements BaseFragment.OnFragmentInteractionListener, BaseActivity.OnSoftKeyboardListener {

    public interface FragmentInteraction {
        public boolean doFragmentInteraction(Property p);
    }

    private Button mSubmitButton;
    private RadioGroup mPropertyType;
    private View mConfirmationView, mAddPropView;
    private static final int PHASE_ADD_PROP_DETAILS = 0;
    private static final int PHASE_ADD_CONTACT_DETAILS = 1;
    private static final int PHASE_ADD_PROP_FINAL = 2;
    private int mPhase = PHASE_ADD_PROP_DETAILS;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_property);
        mRootView = findViewById(R.id.rootView);
        super.setOnSoftKeyboardListener(this);
        mAddPropView = findViewById(R.id.add_property_container);
        mConfirmationView = findViewById(R.id.property_confirmation);
        mSubmitButton = (Button) findViewById(R.id.submit_button);
        mSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAndDoSubmit();
            }
        });
        mPropertyType = (RadioGroup) findViewById(R.id.toggle);
        mAppBackButton = (ImageButton) findViewById(R.id.back_btn);
        mAppBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        initRadioGroupAndFragment();
        addOrChangeFragment(AddPropertyFragment.KEY_FLAT);
    }
    private void checkAndDoSubmit() {
        if (PHASE_ADD_PROP_DETAILS == mPhase) {
            registerProperty();
        } /*else if (PHASE_ADD_CONTACT_DETAILS == mPhase) {
            onPropertyAddedWithoutContact();
        } else {
            onPropertyAddFinished();
        }*/
    }
    private void initRadioGroupAndFragment() {
        mPropertyType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                int key = AddPropertyFragment.KEY_FLAT;
                if (checkedId == R.id.one) {
                    key = AddPropertyFragment.KEY_FLAT;
                } else if (checkedId == R.id.two) {
                    key = AddPropertyFragment.KEY_TOWN_HOUSE;
                } else if (checkedId == R.id.three) {
                    key = AddPropertyFragment.KEY_HOUSE;
                } else {
                    Logger.printError("Unknown Radio button");
                }
                addOrChangeFragment(key);
            }
        });
    }
    private void addOrChangeFragment(int key) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        Bundle b = new Bundle();
        b.putInt(AddPropertyFragment.KEY,key);
        AddPropertyFragment fragment = new AddPropertyFragment();
        fragment.setArguments(b);
        fragmentTransaction.replace(R.id.property_type_container, fragment, "Add_Place_Fragment");
        fragmentTransaction.commit();
    }
    @Override
    public void onBackKeyPressed() {

    }
    @Override
    public void notifyActivity() {

    }
    private void registerProperty() {
        Property lNewProperty = new Property();
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("Add_Place_Fragment");
        if (fragment instanceof AddPropertyFragment) {
            ((AddPropertyFragment) fragment).doFragmentInteraction(lNewProperty);

        }
    }
    private void onPropertyAddedWithoutContact() {
        mAddPropView.setVisibility(View.GONE);
        mConfirmationView.setVisibility(View.VISIBLE);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        AddContactsFragment fragment = new AddContactsFragment();
        fragmentTransaction.replace(R.id.property_confirmation, fragment, "Confirmation_Fragment");
        fragmentTransaction.commit();
        mPhase=PHASE_ADD_PROP_FINAL;
    }
    public void onPropertyAddFinished () {
        mAddPropView.setVisibility(View.GONE);
        mConfirmationView.setVisibility(View.VISIBLE);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        PropertyAddedFragment fragment = new PropertyAddedFragment();
        fragmentTransaction.replace(R.id.property_confirmation, fragment, "Confirmation_Fragment");
        fragmentTransaction.commit();

    }
    @Override
    protected void onDestroy() {
        super.resetOnKeyBoardListener();
        super.onDestroy();
    }
    @Override
    public void onKeyboardShown(int currentKeyboardHeight) {
        Logger.printDebug("onKeyboardShown height : "+currentKeyboardHeight);
        if(mRootView.getLayoutParams() instanceof FrameLayout.LayoutParams) {
            FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) mRootView.getLayoutParams();
            lp.bottomMargin = currentKeyboardHeight;
            mRootView.setLayoutParams(lp);
        } else if (mRootView.getLayoutParams() instanceof RelativeLayout.LayoutParams) {
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mRootView.getLayoutParams();
            lp.bottomMargin = currentKeyboardHeight;
            mRootView.setLayoutParams(lp);
        }
    }
    @Override
    public void onKeyboardHidden() {
        Logger.printDebug("onKeyboardHidden");
        if(mRootView.getLayoutParams() instanceof FrameLayout.LayoutParams) {
            FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) mRootView.getLayoutParams();
            lp.bottomMargin = 0;
            mRootView.setLayoutParams(lp);
        } else if (mRootView.getLayoutParams() instanceof RelativeLayout.LayoutParams) {
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mRootView.getLayoutParams();
            lp.bottomMargin = 0;
            mRootView.setLayoutParams(lp);
        }
    }
}
