package com.android.dladle.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.dladle.R;
import com.android.dladle.customui.CustomProgressDialog;
import com.android.dladle.customui.MyJsonObjectRequest;
import com.android.dladle.utils.Logger;
import com.android.dladle.utils.NetworkConstants;
import com.android.dladle.utils.PreferenceManager;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.skyfishjy.library.RippleBackground;

import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

public class VendorSearchActivity extends BaseActivity {
    int mServiceId = 0;
    String mVendorType;
    Timer timer;
    CustomProgressDialog mCustomProgressDialog;
    int count = 0;
    ImageButton mIvBack;
    LinearLayout mLlReviewedContainer, mLlQuotingContainer;
    TextView mTvReviewed, mTvQuoting, mTvRequestSent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor_search);

        if (getIntent().getExtras() != null) {
            mServiceId = getIntent().getIntExtra("serviceId", 0);
            mVendorType = getIntent().getStringExtra("vendorType");
            startSearching();
            initView();
        }


    }

    private void getSelectedVendorDetails() {


        MyJsonObjectRequest searchrequest = new MyJsonObjectRequest(Request.Method.GET, NetworkConstants.VendorController.SERVICE_DETAILS + mServiceId, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Logger.printDebug("" + response);

                        //    showMessage(response.optString("Message"));
                        if (response.optString("Status").equalsIgnoreCase(NetworkConstants.SUCCESS)) {


                            timer.cancel();
                            JSONObject data = response.optJSONObject("Data");
                            int vendorid = data.optInt("vendorId");
                            String vendorName = data.optString("vendorName");
                            String vendorEmailId = data.optString("vendorEmailId");
                            String vendorProfileImage = data.optString("vendorProfileImage");
                            String jobType = data.optString("jobType");
                            String yearsOfExperience = data.optString("yearsOfExperience");
                            // vendorRating
                            int numberOfJobs = data.optInt("numberOfJobs");
                            int proposedFeeStartRange = data.optInt("proposedFeeStartRange");
                            int proposedFeeEndRange = data.optInt("proposedFeeEndRange");


                            Intent intent = new Intent(VendorSearchActivity.this, ConfirmJobActivity.class);
                            intent.putExtra("vendorid", vendorid);
                            intent.putExtra("vendorName", vendorName);
                            intent.putExtra("vendorEmailId", vendorEmailId);
                            intent.putExtra("vendorProfileImage", vendorProfileImage);
                            intent.putExtra("jobType", jobType);
                            intent.putExtra("yearsOfExperience", yearsOfExperience);
                            intent.putExtra("serviceId", mServiceId);
                            intent.putExtra("numberOfJobs", String.valueOf(numberOfJobs));
                            intent.putExtra("proposedFeeStartRange", String.valueOf(proposedFeeStartRange));
                            intent.putExtra("proposedFeeEndRange", String.valueOf(proposedFeeEndRange));
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);


                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof TimeoutError) {
                            //showMessage(getString(R.string.server_error));
                        } else {
                            //showMessage(error.getMessage());
                        }
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        searchrequest.setTag(NetworkConstants.Login.TAG);
        requestQueue.add(searchrequest);
    }

    private void startSearching() {
        timer = new Timer();
        TimerTask hourlyTask = new TimerTask() {
            @Override
            public void run() {
                count++;
                if (count < 15) {

                    getSelectedVendorDetails();
                } else {
                    VendorSearchActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            showMessage("No vendor found for ths job");
                            onBackPressed();
                        }
                    });


                }
            }
        };
        timer.schedule(hourlyTask, 0l, 1000 * 01 * 60);

        // to stop time called timer.cancel();

    }


    private void initView() {
        mIvBack = (ImageButton) findViewById(R.id.back_btn);
        mLlReviewedContainer = (LinearLayout) findViewById(R.id.job_reviewed_container);
        mLlQuotingContainer = (LinearLayout) findViewById(R.id.quote_conatainer);
        mTvReviewed = (TextView) findViewById(R.id.tv_reviewed);
        mTvQuoting = (TextView) findViewById(R.id.tv_quoting);
        TextView textView = (TextView) findViewById(R.id.tv_heading);
        TextView textView1 = (TextView) findViewById(R.id.request_sent);
        textView.setText(getResources().getString(R.string.job_status, mVendorType, mVendorType));
        textView1.setText(getResources().getString(R.string.job_request_send_to, mVendorType));
        mTvReviewed.setText(getResources().getString(R.string.job_revied_by, mVendorType));
        mTvQuoting.setText(getResources().getString(R.string.vendor_quoting, mVendorType));
        mIvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }


}
