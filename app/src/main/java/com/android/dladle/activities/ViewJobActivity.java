package com.android.dladle.activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.dladle.R;
import com.android.dladle.customui.CustomProgressDialog;
import com.android.dladle.customui.MyJsonObjectRequest;
import com.android.dladle.customui.ProfileImageView;
import com.android.dladle.utils.Logger;
import com.android.dladle.utils.NetworkConstants;
import com.android.dladle.utils.NetworkUtils;
import com.android.dladle.utils.PreferenceManager;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class ViewJobActivity extends BaseActivity {
    ImageView mIvOne, mIvTwo, mIvThree, mIvFour;
    RadioButton mRbCalendar, mRbClock;
    Button mBtnYes, mBtnNo;
    int mServiceId, mNotificationId;
    String mAudioPath = "";
    String serviceDescription = "";
    String serviceTime = "";
    ArrayList<String> imagePathArrayList = new ArrayList<>();
    LinearLayout mLlAudioContainer;
    TextView mTvServiceDescHeader, mTvServiceDesc;
    String date = "";
    String time = "";
    CustomProgressDialog mCustomProgressDialog;
    View divider;
    TextView mTvImageHeader;
    RatingBar mRatingBar;
    String mClientEmail = "";
    TextView mCountDownTimer;
    float mMinPrice, mMaxPrice;
    Button mBtnFinalQuote;
    LinearLayout mFinalPriceContainer, mViewJobContainer;
    boolean isFinalPrice = false;
    ProfileImageView profileImageView;
    LinearLayout timerContainer;
    TextView mBtnPropertyAddresss, mTvProposedFees;
    String rating, image_path, name,location;
    boolean isActioned=false;
    CountDownTimer countDownTimer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_job);
        initView();
        performAction();
    }

    private void initView() {
        mIvOne = (ImageView) findViewById(R.id.image_one);
        mIvTwo = (ImageView) findViewById(R.id.image_two);
        mIvThree = (ImageView) findViewById(R.id.image_three);
        mIvFour = (ImageView) findViewById(R.id.image_four);
        mRbCalendar = (RadioButton) findViewById(R.id.calendar);
        mRbClock = (RadioButton) findViewById(R.id.time);
        mBtnYes = (Button) findViewById(R.id.yes);
        profileImageView = (ProfileImageView) findViewById(R.id.profile_pic);
        mCountDownTimer = (TextView) findViewById(R.id.count_down_timer);
        divider = (View) findViewById(R.id.divider);
        mTvImageHeader = (TextView) findViewById(R.id.image_header);
        mBtnNo = (Button) findViewById(R.id.no);
        mTvServiceDescHeader = (TextView) findViewById(R.id.tv_service_desc_text);
        mTvServiceDesc = (TextView) findViewById(R.id.tv_short_descriptiob);
        mLlAudioContainer = (LinearLayout) findViewById(R.id.audio_container);
        mCustomProgressDialog = new CustomProgressDialog(this);
        mRatingBar = (RatingBar) findViewById(R.id.rating_bar);
        mFinalPriceContainer = (LinearLayout) findViewById(R.id.final_price_container);
        mViewJobContainer = (LinearLayout) findViewById(R.id.job_accept_container);
        mBtnFinalQuote = (Button) findViewById(R.id.btn_submit_final_quote);
        timerContainer = (LinearLayout) findViewById(R.id.timer_container);
        mBtnPropertyAddresss = (TextView) findViewById(R.id.property_address);
        mTvProposedFees = (TextView) findViewById(R.id.proposed_fees_range);
        mNotificationId = getIntent().getIntExtra("notificationId", 0);
        isActioned = getIntent().getBooleanExtra("actioned", false);


        if (isActioned)

        {
            mBtnYes.setEnabled(false);
            mBtnNo.setEnabled(false);
            timerContainer.setVisibility(View.GONE);
        }


        Glide.with(ViewJobActivity.this)
                .load(getIntent().getStringExtra("image"))
                .asBitmap()
                .error(R.drawable.ic_avatar)
                .into(profileImageView);
        if (getIntent().hasExtra("data")) {
            isFinalPrice = true;
            mViewJobContainer.setVisibility(View.GONE);
            mFinalPriceContainer.setVisibility(View.VISIBLE);
            isFinalPrice = true;
            String data = getIntent().getStringExtra("data");
            List<String> items = Arrays.asList(data.split("\\s*,\\s*"));

            for (int i = 0; i < items.size(); i++) {
                if (items.get(i).contains("serviceId")) {
                    int index = items.get(i).indexOf(":");
                    mServiceId = Integer.parseInt(items.get(i).substring(index + 1));
                    Log.d("DlaDle", String.valueOf(mServiceId));
                } else if (items.get(i).contains("vendorEmailId")) {
                    int index = items.get(i).indexOf(":");
                    mClientEmail = items.get(i).substring(index + 1);
                    Log.d("DlaDle", String.valueOf(mClientEmail));
                } else if (items.get(i).contains("feeStartRange")) {
                    int index = items.get(i).indexOf(":");
                    mMinPrice = Float.parseFloat(items.get(i).substring(index + 1));
                    Log.d("DlaDle", String.valueOf(mMinPrice));
                } else if (items.get(i).contains("feeEndRange")) {
                    int index = items.get(i).indexOf(":");
                    mMaxPrice = Float.parseFloat(items.get(i).substring(index + 1));
                    Log.d("DlaDle", String.valueOf(mMaxPrice));
                }
            }
        } else {
            mViewJobContainer.setVisibility(View.VISIBLE);
            mFinalPriceContainer.setVisibility(View.GONE);
            mServiceId = getIntent().getIntExtra("serviceId", 0);
            mClientEmail = getIntent().getStringExtra("clientEmail");
        }
        if (NetworkUtils.isNetworkAvailable(this)) {
            getViewJobDetails();
        } else {
            showMessage(getResources().getString(R.string.no_internet));
        }

    }

    private void performAction() {
        mBtnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // markNotificationAsRead();
                markNotificationAsActioned();
                Intent intent = new Intent(ViewJobActivity.this, ServicsCostActivity.class);
                intent.putExtra("serviceId", mServiceId);
                intent.putExtra("notificationId", mNotificationId);
                intent.putExtra("name",name);
                intent.putExtra("rating",rating);
                intent.putExtra("location",location);
                intent.putExtra("image",image_path);
                startActivity(intent);

            }
        });
        mBtnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  markNotificationAsRead();
                markNotificationAsActioned();
                Intent intent = new Intent(ViewJobActivity.this, UserProfileActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(NetworkConstants.JsonResponse.DATA, PreferenceManager.getInstance(getApplicationContext()).getUserType());
                startActivity(intent);
            }
        });
        mLlAudioContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playAudio();
            }
        });
        mIvOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToGalleryPage();
            }
        });
        mIvTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToGalleryPage();
            }
        });
        mIvThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToGalleryPage();
            }
        });
        mIvFour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToGalleryPage();
            }
        });

        mBtnFinalQuote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlertForFinalPrice();
            }
        });


    }

    private void getViewJobDetails() {
        mCustomProgressDialog.show();
        final MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.GET, NetworkConstants.VendorController.VIEW_JOB + String.valueOf(mServiceId), null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mCustomProgressDialog.hide();
                        mCustomProgressDialog.dismiss();
                        Logger.printDebug("" + response);
                        if (response.optString("Status").equalsIgnoreCase("success")) {
                            JSONObject data = response.optJSONObject("Data");
                            boolean isEmergency = data.optBoolean("emergency");
                            serviceTime = data.optString("serviceNeedTime");
                            int spaceIndex = serviceTime.indexOf(" ");
                            date = serviceTime.substring(0, spaceIndex);
                            time = serviceTime.substring(spaceIndex + 1);
                            serviceDescription = data.optString("serviceDescription");
                            location= data.optString("propertyAddress");
                            mBtnPropertyAddresss.setText(data.optString("propertyAddress"));
                            JSONArray jsonArray = data.optJSONArray("serviceDocuments");
                            JSONObject feesRange = data.optJSONObject("serviceEstimateView");
                            String minFee = feesRange.optString("feeStartRange");
                            String maxFee = feesRange.optString("feeEndRange");
                            JSONObject user = data.optJSONObject("user");
                            name = user.optString("name");
                            image_path = user.optString("profilePicture");
                            rating = user.optString("rating");


                            if (minFee != null && maxFee != null) {
                                if (!minFee.equalsIgnoreCase("null")) {
                                    mTvProposedFees.setText(minFee + " - " + maxFee);
                                    mTvProposedFees.setVisibility(View.VISIBLE);
                                }
                            }


                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.optJSONObject(i);
                                String type = jsonObject.optString("documentType");
                                if (type.equalsIgnoreCase("IMAGE")) {
                                    imagePathArrayList.add(jsonObject.optString("base64"));
                                } else {
                                    mAudioPath = jsonObject.optString("base64");
                                }
                            }
                            if (!(getIntent().hasExtra("data"))) {
                                // markNotificationAsRead();
                                markNotificationAsActioned();
                            }

                            showDetails();

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mCustomProgressDialog.hide();
                        if (error instanceof TimeoutError) {
                            showMessage(getString(R.string.server_error));
                        } else {
                            showMessage(error.getMessage());
                        }
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(ViewJobActivity.this);
        jsonRequest.setTag(NetworkConstants.Login.TAG);
        requestQueue.add(jsonRequest);
    }

    private void markNotificationAsActioned() {

        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.GET, NetworkConstants.Notification.NOTIFICATION_ACTIONED + mNotificationId, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        showProgress(false);
                        Logger.printDebug("" + response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof TimeoutError) {
                            showMessage(getString(R.string.server_error));
                        } else {
                            showMessage(error.getMessage());
                        }
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(ViewJobActivity.this);
        jsonRequest.setTag(NetworkConstants.Login.TAG);
        requestQueue.add(jsonRequest);
    }

    private void showDetails() {
        if (imagePathArrayList.size() == 1) {
            mIvTwo.setVisibility(View.GONE);
            mIvThree.setVisibility(View.GONE);
            mIvFour.setVisibility(View.GONE);
            Glide.with(ViewJobActivity.this)
                    .load(imagePathArrayList.get(0))
                    .asBitmap()
                    .error(R.drawable.ic_avatar)
                    .into(mIvOne);
        } else if (imagePathArrayList.size() == 2) {
            mIvThree.setVisibility(View.GONE);
            mIvFour.setVisibility(View.GONE);
            Glide.with(ViewJobActivity.this)
                    .load(imagePathArrayList.get(0))
                    .asBitmap()
                    .error(R.drawable.ic_avatar)
                    .into(mIvOne);
            Glide.with(ViewJobActivity.this)
                    .load(imagePathArrayList.get(1))
                    .asBitmap()
                    .error(R.drawable.ic_avatar)
                    .into(mIvTwo);
        } else if (imagePathArrayList.size() == 3) {
            mIvFour.setVisibility(View.GONE);
            Glide.with(ViewJobActivity.this)
                    .load(imagePathArrayList.get(0))
                    .asBitmap()
                    .error(R.drawable.ic_avatar)
                    .into(mIvOne);
            Glide.with(ViewJobActivity.this)
                    .load(imagePathArrayList.get(1))
                    .asBitmap()
                    .error(R.drawable.ic_avatar)
                    .into(mIvTwo);
            Glide.with(ViewJobActivity.this)
                    .load(imagePathArrayList.get(2))
                    .asBitmap()
                    .error(R.drawable.ic_avatar)
                    .into(mIvTwo);
        } else if (imagePathArrayList.size() == 4) {
            Glide.with(ViewJobActivity.this)
                    .load(imagePathArrayList.get(0))
                    .asBitmap()
                    .error(R.drawable.ic_avatar)
                    .into(mIvOne);
            Glide.with(ViewJobActivity.this)
                    .load(imagePathArrayList.get(1))
                    .asBitmap()
                    .error(R.drawable.ic_avatar)
                    .into(mIvTwo);
            Glide.with(ViewJobActivity.this)
                    .load(imagePathArrayList.get(2))
                    .asBitmap()
                    .error(R.drawable.ic_avatar)
                    .into(mIvTwo);
            Glide.with(ViewJobActivity.this)
                    .load(imagePathArrayList.get(3))
                    .asBitmap()
                    .error(R.drawable.ic_avatar)
                    .into(mIvTwo);
        } else {
            mIvOne.setVisibility(View.GONE);
            mIvTwo.setVisibility(View.GONE);
            mIvThree.setVisibility(View.GONE);
            mIvFour.setVisibility(View.GONE);
            divider.setVisibility(View.GONE);
            mTvImageHeader.setVisibility(View.GONE);
        }

        if (mAudioPath.equals("")) {
            mLlAudioContainer.setVisibility(View.GONE);
        }
        if (serviceDescription.equals("")) {
            mTvServiceDesc.setVisibility(View.GONE);
            mTvServiceDescHeader.setVisibility(View.GONE);
        } else {
            mTvServiceDesc.setText(serviceDescription);
        }

        if (!isActioned) {
          countDownTimer=  new CountDownTimer(50000, 1000) {

                public void onTick(long millisUntilFinished) {
                    mCountDownTimer.setText("00 : " + String.valueOf(millisUntilFinished / 1000));
                }

                public void onFinish() {
                    // call for check
                    onBackPressed();
                }

            }.start();
        }


        setServceDate();
        setTime();
    }

    private void playAudio() {
        try {
            MediaPlayer player = new MediaPlayer();
            player.setAudioStreamType(AudioManager.STREAM_MUSIC);
            player.setDataSource(mAudioPath);
            player.prepare();
            player.start();
        } catch (Exception e) {
            // TODO: handle exception
        }
    }


    private void setServceDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date testDate = null;
        try {
            testDate = sdf.parse(date);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        SimpleDateFormat formatter = new SimpleDateFormat("d MMM ''yy");
        String newFormat = formatter.format(testDate);
        mRbCalendar.setText(newFormat);
    }

    private void setTime() {
        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
            final Date dateObj = sdf.parse(time);
            System.out.println(dateObj);
            mRbClock.setText(new SimpleDateFormat("K:mm").format(dateObj));
        } catch (final ParseException e) {
            e.printStackTrace();
        }
    }

    private void goToGalleryPage() {
        Intent intent = new Intent(ViewJobActivity.this, ViewImageActivity.class);
        intent.putExtra("image_url", imagePathArrayList);
        startActivity(intent);
    }

    private void markNotificationAsRead() {
        mCustomProgressDialog.show();
        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.POST, NetworkConstants.Notification.NOTIFICATION_READ + String.valueOf(mNotificationId) + "&read=true", null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Logger.printDebug("" + response);
                        mCustomProgressDialog.hide();

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mCustomProgressDialog.hide();
                        if (error instanceof TimeoutError) {
                            showMessage(getString(R.string.server_error));
                        } else {
                            showMessage(error.getMessage());
                        }
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        jsonRequest.setTag(NetworkConstants.Login.TAG);
        requestQueue.add(jsonRequest);
    }

    private void getRating() {
        JSONObject input = new JSONObject();
        try {
            input.put("userEmailId", mClientEmail);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        showProgress(true);
        MyJsonObjectRequest jsonRequest = new MyJsonObjectRequest(Request.Method.POST, NetworkConstants.ReviewController.GET_REVIEW_DETAILS, input,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        showProgress(false);
                        Logger.printDebug("" + response);
                        if (response.optString("Status").equalsIgnoreCase(NetworkConstants.SUCCESS)) {
                            JSONObject data = response.optJSONObject("Data");
                            if (data != null && data.length() > 0) {


                                int count = data.optInt("count");
                                if (count == 0) {
                                    mRatingBar.setVisibility(View.GONE);
                                } else {
                                    double rating = data.optDouble("rate");
                                    mRatingBar.setRating((float) rating);
                                }


                            }


                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof TimeoutError) {
                            showMessage(getString(R.string.server_error));
                        } else {
                            showMessage(error.getMessage());
                        }
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(ViewJobActivity.this);
        jsonRequest.setTag(NetworkConstants.Login.TAG);
        requestQueue.add(jsonRequest);
    }

    private void showProgress(final boolean show) {
        super.setActionInProgress(show);
        if (show) {
            mCustomProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            mCustomProgressDialog.setCancelable(false);
            mCustomProgressDialog.show();
        } else {
            mCustomProgressDialog.dismiss();
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ViewJobActivity.this, UserProfileActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(NetworkConstants.JsonResponse.DATA, PreferenceManager.getInstance(getApplicationContext()).getUserType());
        startActivity(intent);
    }


    private void showAlertForFinalPrice() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ViewJobActivity.this);
        alertDialog.setTitle("Final Price");
        alertDialog.setMessage("Please enter final price");

        final EditText input = new EditText(ViewJobActivity.this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        input.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);
        alertDialog.setView(input);

        alertDialog.setPositiveButton("Submit",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        String price = input.getText().toString();
                        if (TextUtils.isEmpty(input.getText().toString().trim())) {
                            showMessage("Please enter final price");
                        } else {
                            Intent intent = new Intent(ViewJobActivity.this, QuoteFinalPrice.class);
                            intent.putExtra("ServiceId", String.valueOf(mServiceId));
                            intent.putExtra("NotificationId", mNotificationId);
                            intent.putExtra("MinPrice", String.valueOf(mMinPrice));
                            intent.putExtra("MaxPrice", String.valueOf(mMaxPrice));
                            intent.putExtra("FinalPrice", price);
                            intent.putExtra("employerName", getIntent().getStringExtra("employerName"));
                            intent.putExtra("rating", getIntent().getStringExtra("rating"));
                            intent.putExtra("image", getIntent().getStringExtra("image"));
                            startActivity(intent);

                        }
                    }
                });

        alertDialog.setNegativeButton("Cancle",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        alertDialog.show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (countDownTimer!=null)
        {
            countDownTimer.cancel();
        }
    }

}