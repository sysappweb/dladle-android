package com.android.dladle.activities;

/**
 * Created by sourav on 17-11-2016.
 */

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.android.dladle.R;
import com.android.dladle.adapter.RecyclerAdapterForHome;
import com.android.dladle.customui.CustomDialog;
import com.android.dladle.customui.MyJsonObjectRequest;
import com.android.dladle.fragments.BaseFragment;
import com.android.dladle.fragments.ProfileDetailsFragment;
import com.android.dladle.fragments.ProfileEditFragment;
import com.android.dladle.fragments.ProfileEditFragmentVendor;
import com.android.dladle.utils.CommonConstants;
import com.android.dladle.utils.Logger;
import com.android.dladle.utils.NetworkConstants;
import com.android.dladle.utils.PhotoManager;
import com.android.dladle.utils.PreferenceManager;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class UserProfileActivity extends BaseActivity
        implements BaseFragment.OnFragmentInteractionListener,
        NavigationView.OnNavigationItemSelectedListener,
        BaseFragment.ModificationWatcher,
        CustomDialog.DialogInteraction {

    public interface FragmentInteraction {
        public void doFragmentInteraction();
    }

    private static final int PROFILE_DETAILS_INDEX = 100;
    private static final int PROFILE_EDIT_INDEX = 200;
    private static final int WALLET_INDEX = 300;
    private static final int FAQ_INDEX = 400;
    private static final int CONTACT_US_INDEX = 500;
    private static final int TNC_INDEX = 600;
    private static final int SIGN_OUT_INDEX = 700;
    private static final int PROFILE_CHANGE_PASSWORD = 800;
    private BaseFragment mFragment;
    private Menu mNavMenu;

    private boolean isFromLogin, isProfileUpdated, mHasModified, mIsNavItemClicked = false;
    private int mCurrentIndex = PROFILE_DETAILS_INDEX;
    private int mPrevIndex = -1;
    private int mDialogIndex = -1;
    public static final String FROM_LOGIN = "from_login";
    private CustomDialog mDialog;
    private AlertDialog mAlertDialog;
    private Toolbar mToolbar;
    private Handler mHandler;
    private DrawerLayout mDrawer;
    private MenuItem mLeftButton;
    private String mFragmentKey;
    private TextView countTextView;
    private FrameLayout redCircle;
    private int mNotificationCount = 0;
    String tenantPropertyJoiningDate = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.setStatusBarColor(getResources().getColor(R.color.profile_screen_bg));
        }
        mHandler = new Handler();
        Intent intent = getIntent();
        if (intent.hasExtra(FROM_LOGIN)) {
            isFromLogin = intent.getBooleanExtra(FROM_LOGIN, false);
            if (isFromLogin) {
                showRememberMeDialog(intent.getStringExtra(NetworkConstants.Registration.EMAIL_KEY), intent.getStringExtra(NetworkConstants.Registration.PASSWORD_KEY));
            }
        }
        mFragmentKey = intent.getStringExtra(NetworkConstants.JsonResponse.DATA);
        setContentView(R.layout.activity_user_profile);
        setupActionBar();
        super.setPressBackTwiceToExitFlag(true);
        isProfileUpdated = PreferenceManager.getInstance(this).isProfileUpdated();
        Logger.printInfo("User type : " + mFragmentKey);
        if (!TextUtils.isEmpty(mFragmentKey)) {
            mCurrentIndex = PROFILE_DETAILS_INDEX;
            initFragment();
        } else {
            Logger.printError("Mis-matched User type");
            mCurrentIndex = PROFILE_DETAILS_INDEX;
            initFragment();
        }


    }

    @Override
    public void onBackKeyPressed() {
        if (!isFromLogin) {
            onBackPressed();
        }
    }

    @Override
    public void notifyActivity() {
        if (CommonConstants.FRAGMENT_KEY_VENDOR.equalsIgnoreCase(mFragmentKey)) {
            if (!isProfileUpdated) {
                mCurrentIndex = PROFILE_EDIT_INDEX;
                initFragment();
            }
        } else if (CommonConstants.FRAGMENT_KEY_LANDLORD.equalsIgnoreCase(mFragmentKey)) {
            // Do nothing for Landlord
        } else if (CommonConstants.FRAGMENT_KEY_TENANT.equalsIgnoreCase(mFragmentKey)) {
            // Do nothing for Tenant
        } else {
            Logger.printInfo("Invalid Fragment");
        }
    }

    public void handleLeftButtonClick() {
        if (PROFILE_EDIT_INDEX == mCurrentIndex) {
            checkAndShowCancelMessage();
        } else if (PROFILE_DETAILS_INDEX == mCurrentIndex) {
            if (CommonConstants.FRAGMENT_KEY_VENDOR.equalsIgnoreCase(mFragmentKey)) {
                if (isProfileUpdated) {
                    startWalletActivity();
                } else {
                    showMessage("Please Setup your Account to use this feature");
                }
            }
        }
    }

    private void checkAndShowCancelMessage() {
        if (mHasModified) {
            dismissDialogs();
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setTitle(R.string.discard_title);
            alertDialogBuilder.setMessage(R.string.discard_msg);
            alertDialogBuilder.setCancelable(false);
            alertDialogBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    mCurrentIndex = PROFILE_DETAILS_INDEX;
                    mHasModified = false;
                    initFragment();
                }
            });

            alertDialogBuilder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //Do Nothing
                }
            });

            mAlertDialog = alertDialogBuilder.create();
            mAlertDialog.show();
        } else {
            mCurrentIndex = PROFILE_DETAILS_INDEX;
            initFragment();
        }
    }

    private void setupActionBar() {
        mRootView = findViewById(R.id.rootView);
        mDrawer = (DrawerLayout) mRootView;
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mToolbar.setNavigationIcon(R.drawable.ic_nav_drawer);
        setUpNavigationView();
    }

    private void setUpNavigationView() {
        NavigationView view = (NavigationView) findViewById(R.id.nav_view);
        View navHeader = view.getHeaderView(0);
        mNavMenu = view.getMenu();

        boolean isVendor = CommonConstants.FRAGMENT_KEY_VENDOR.equalsIgnoreCase(mFragmentKey);
        MenuItem itemWallet = mNavMenu.findItem(R.id.nav_wallet);
        MenuItem itemAddStatusHome = mNavMenu.findItem(R.id.nav_add_home_status);

        itemAddStatusHome.setVisible(false);

        itemWallet.setVisible(!isVendor);
        Intent i = getIntent();
        TextView txtName = (TextView) navHeader.findViewById(R.id.name);
        String lName = PreferenceManager.getInstance(UserProfileActivity.this).getFirstName();
        String lLast = PreferenceManager.getInstance(UserProfileActivity.this).getLastName();
        txtName.setText(lName + " " + lLast);

        TextView txtEmail = (TextView) navHeader.findViewById(R.id.email);
        String email = i.getStringExtra(NetworkConstants.Registration.EMAIL_KEY);
        txtEmail.setText(email);

        view.setNavigationItemSelectedListener(this);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawer, mToolbar, R.string.openDrawer, R.string.closeDrawer) {
            @Override
            public void onDrawerClosed(View drawerView) {
                if (mIsNavItemClicked) {
                    mIsNavItemClicked = false;
                    if (TNC_INDEX == mDialogIndex) {
                        showTNC();
                        mDialogIndex = -1;
                    } else if (SIGN_OUT_INDEX == mDialogIndex) {
                        showSignOutDialog();
                        mDialogIndex = -1;
                    } else if (FAQ_INDEX == mDialogIndex) {
                        showFAQ();
                        mDialogIndex = -1;
                    } else if (CONTACT_US_INDEX == mDialogIndex) {
                        showContactUs();
                        mDialogIndex = -1;
                    } else if (PROFILE_CHANGE_PASSWORD == mCurrentIndex) {
                        mCurrentIndex = mPrevIndex != -1 ? mPrevIndex : PROFILE_DETAILS_INDEX;
                        mPrevIndex = -1;
                        initRestorePassword();
                    } else {
                        initFragment();
                    }
                }
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                TextView txtName = (TextView) drawerView.findViewById(R.id.name);
                String lName = PreferenceManager.getInstance(UserProfileActivity.this).getFirstName();
                String lLast = PreferenceManager.getInstance(UserProfileActivity.this).getLastName();
                txtName.setText(lName + " " + lLast);
                super.onDrawerOpened(drawerView);
            }
        };
        mDrawer.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }

    private Runnable mPendingRunnable = new Runnable() {
        @Override
        public void run() {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            switch (mCurrentIndex) {
                case PROFILE_DETAILS_INDEX:
                    mFragment = new ProfileDetailsFragment();
                    break;
                case PROFILE_EDIT_INDEX:
                    boolean isVendor = CommonConstants.FRAGMENT_KEY_VENDOR.equalsIgnoreCase(mFragmentKey);
                    mFragment = isVendor ? new ProfileEditFragmentVendor() : new ProfileEditFragment();
                    break;
                default:
                    mFragment = new ProfileDetailsFragment();
                    break;
            }
            fragmentTransaction.replace(R.id.fragment_container, mFragment, "Profile_Fragment");
            fragmentTransaction.commitAllowingStateLoss();
        }
    };

    private void initFragment() {
        isProfileUpdated = PreferenceManager.getInstance(this).isProfileUpdated();
        hideKeyboard();
        updateToolBar();
        mHandler.post(mPendingRunnable);
    }

    private void updateToolBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(getTitleResource());
        updateLeftMenu();
        mToolbar.setNavigationIcon(R.drawable.ic_nav_drawer);
    }

    private void updateLeftMenu() {
        boolean isVendor = CommonConstants.FRAGMENT_KEY_VENDOR.equalsIgnoreCase(mFragmentKey);
        Logger.printInfo("updateLeftMenu : " + mCurrentIndex);
        if (null != mLeftButton) {
            if (PROFILE_DETAILS_INDEX == mCurrentIndex && isVendor) {
                mLeftButton.setIcon(R.drawable.ic_wallet_icon);
                mLeftButton.setVisible(true);
            } else if (PROFILE_EDIT_INDEX == mCurrentIndex) {
                mLeftButton.setIcon(R.drawable.ic_cancel);
                mLeftButton.setVisible(false);
            } else {
                mLeftButton.setVisible(false);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.profile_menu, menu);
        mLeftButton = menu.findItem(R.id.left_button);
        final MenuItem alertMenuItem = menu.findItem(R.id.notification_count);

        FrameLayout rootView = (FrameLayout) alertMenuItem.getActionView();
        if (PROFILE_EDIT_INDEX == mCurrentIndex) {
            alertMenuItem.setVisible(false);
        } else {
            countTextView = (TextView) rootView.findViewById(R.id.view_alert_count_textview);
            redCircle = (FrameLayout) rootView.findViewById(R.id.view_alert_red_circle);
            int notificationCount = PreferenceManager.getInstance(getApplicationContext()).getNotificationCount();
            Logger.printDebug("notificationcount" + notificationCount);
            if (0 < mNotificationCount) {
                countTextView.setText(String.valueOf(mNotificationCount));
            } else {
                countTextView.setText("");
                alertMenuItem.setVisible(false);
            }
            redCircle.setVisibility((mNotificationCount > 0) ? VISIBLE : GONE);
        }
        updateLeftMenu();
        alertMenuItem.getActionView().setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UserProfileActivity.this, NotificationListActivity.class);
                intent.putExtra("migratedFrom", "UserProfile");
                startActivity(intent);
            }
        });
        mLeftButton.setVisible(false);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            if (null != mDrawer && !mDrawer.isDrawerOpen(Gravity.LEFT)) {
                mDrawer.openDrawer(Gravity.LEFT);
            }
            return true;
        } else if (id == R.id.left_button) {
            handleLeftButtonClick();
            return true;
        } else if (id == R.id.notification_count) {
            Intent intent = new Intent(UserProfileActivity.this, NotificationListActivity.class);
            intent.putExtra("migratedFrom", "UserProfile");
            startActivity(intent);

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private String getTitleResource() {
        int res;
        if (PROFILE_DETAILS_INDEX == mCurrentIndex) {
            //res = R.string.profile_settings;
            return "";
        } else if (PROFILE_EDIT_INDEX == mCurrentIndex) {
            res = R.string.edit_profile;
        } else if (CONTACT_US_INDEX == mCurrentIndex) {
            res = R.string.nav_contact_us;
        } else if (FAQ_INDEX == mCurrentIndex) {
            res = R.string.nav_help;
        } else {
            res = R.string.app_name;
        }
        return getString(res);
    }

    public void onStop() {
        super.onStop();
        dismissDialogs();
    }

    private void dismissDialogs() {
        if (null != mDialog && mDialog.isShowing()) {
            mDialog.dismiss();
        }
        if (null != mAlertDialog && mAlertDialog.isShowing()) {
            mAlertDialog.dismiss();
        }
    }

    private void showSignOutDialog() {
        dismissDialogs();
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(R.string.sign_out_msg);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                doSignOut();
            }
        });

        alertDialogBuilder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Do Nothing
            }
        });

        mAlertDialog = alertDialogBuilder.create();
        mAlertDialog.show();
    }

    private void showRememberMeDialog(final String email, final String key) {
        boolean isShow = !PreferenceManager.getInstance(getApplicationContext()).isDoNotShowAgain();
        if (isShow && !TextUtils.isEmpty(email) && !TextUtils.isEmpty(key)) {
            dismissDialogs();
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            LayoutInflater layoutInflater = LayoutInflater.from(this);
            View checkBox = layoutInflater.inflate(R.layout.checkbox, null);

            final CheckBox doNotShowAgain = (CheckBox) checkBox.findViewById(R.id.skip);
            alertDialogBuilder.setView(checkBox);
            alertDialogBuilder.setCancelable(false);
            doNotShowAgain.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    PreferenceManager.getInstance(getApplicationContext()).setDoNotShowAgain(isChecked);
                }
            });
            doNotShowAgain.setChecked(false);
            alertDialogBuilder.setMessage(R.string.save_credential_msg);
            alertDialogBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    PreferenceManager.getInstance(getApplicationContext()).setUserName(email);
                    PreferenceManager.getInstance(getApplicationContext()).setPassword(key);
                }
            });

            alertDialogBuilder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    PreferenceManager.getInstance(getApplicationContext()).setDoNotShowAgain(doNotShowAgain.isChecked());
                }
            });
            mAlertDialog = alertDialogBuilder.create();
            mAlertDialog.show();
        }
    }

    private void showTNC() {
        dismissDialogs();
        Runnable r = new Runnable() {
            @Override
            public void run() {
                Activity context = UserProfileActivity.this;
                if (null != context && !context.isFinishing()) {
                    mDialog = new CustomDialog(context, CustomDialog.TYPE_TNC);
                    mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    mDialog.getWindow().getAttributes().windowAnimations = R.style.customDialogAnimations;
                    mDialog.show();
                }
            }
        };
        mHandler.postDelayed(r, 100);
    }

    private void showFAQ() {
        dismissDialogs();
        Runnable r = new Runnable() {
            @Override
            public void run() {
                Activity context = UserProfileActivity.this;
                if (null != context && !context.isFinishing()) {
                    mDialog = new CustomDialog(context, CustomDialog.TYPE_FAQ);
                    mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    mDialog.getWindow().getAttributes().windowAnimations = R.style.customDialogAnimations;
                    mDialog.show();
                }
            }
        };
        mHandler.postDelayed(r, 100);
    }

    private void showContactUs() {
        dismissDialogs();
        Runnable r = new Runnable() {
            @Override
            public void run() {
                Activity context = UserProfileActivity.this;
                if (null != context && !context.isFinishing()) {
                    mDialog = new CustomDialog(context, CustomDialog.TYPE_CONTACT_US);
                    mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    mDialog.getWindow().getAttributes().windowAnimations = R.style.customDialogAnimations;
                    mDialog.show();
                }
            }
        };
        mHandler.postDelayed(r, 100);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        final DrawerLayout lDrawer = (DrawerLayout) mRootView;
        lDrawer.closeDrawers();
        switch (item.getItemId()) {
            case R.id.nav_profile_settings:
                mIsNavItemClicked = mCurrentIndex != PROFILE_DETAILS_INDEX;
                mCurrentIndex = PROFILE_DETAILS_INDEX;
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                Fragment fragment = new ProfileDetailsFragment();
                fragmentTransaction.replace(R.id.fragment_container, fragment, "Profile_Fragment");
                fragmentTransaction.commitAllowingStateLoss();
                break;
            case R.id.nav_edit:
                mIsNavItemClicked = mCurrentIndex != PROFILE_EDIT_INDEX;
                mCurrentIndex = PROFILE_EDIT_INDEX;
                //initFragment();
                break;
            case R.id.nav_help:
                mIsNavItemClicked = mDialogIndex != FAQ_INDEX;
                mDialogIndex = FAQ_INDEX;
                //initFragment();
                break;
            case R.id.nav_contact_us:
                mIsNavItemClicked = mDialogIndex != CONTACT_US_INDEX;
                mDialogIndex = CONTACT_US_INDEX;
                //initFragment();
                break;
            case R.id.nav_tnc:
                mIsNavItemClicked = mDialogIndex != TNC_INDEX;
                mDialogIndex = TNC_INDEX;
                //showTNC();
                break;
            case R.id.nav_sign_out:
                mIsNavItemClicked = mDialogIndex != SIGN_OUT_INDEX;
                mDialogIndex = SIGN_OUT_INDEX;
                //showSignOutDialog();
                break;
            case R.id.nav_change_password:
                mIsNavItemClicked = mCurrentIndex != PROFILE_CHANGE_PASSWORD;
                mPrevIndex = mCurrentIndex;
                mCurrentIndex = PROFILE_CHANGE_PASSWORD;
                break;
            case R.id.nav_wallet:
                startWalletActivity();
                break;
            case R.id.nav_add_home_status:
                startHomeStatusActivity();
                break;
            default:
                mIsNavItemClicked = false;
                break;
        }
        if (item.isChecked()) {
            item.setChecked(false);
        } else {
            item.setChecked(true);
        }
        item.setChecked(true);
        return true;
    }

    private void doSignOut() {
        PreferenceManager.getInstance(this).resetUserData(true);
        PhotoManager.removeProfilePic(this);
        Intent i = new Intent(this, UserLoginActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        transitionTo(i);
        finishAffinity();
    }

    private void startWalletActivity() {

        if (PreferenceManager.getInstance(getApplicationContext()).isPaymentAccountSetup()) {
            Intent i = new Intent(this, ViewCardDetailsActivity.class);
            i.putExtra("mode", "edit");
            transitionTo(i);
        } else {
            Intent i = new Intent(this, WalletActivity.class);
            transitionTo(i);
        }
    }

    private void startHomeStatusActivity() {


        if (!PreferenceManager.getInstance(getApplicationContext()).isHomeStatus()) {
            Intent i = new Intent(this, AddStatusHomeActivity.class);
            transitionTo(i);
        } else {
            Intent i = new Intent(this, ViewHomeStatusActivity.class);
            transitionTo(i);
        }
    }

    private void initRestorePassword() {
        Intent i = new Intent(this, ResetOrChangePasswordActivity.class);
        i.putExtra(ResetOrChangePasswordActivity.CHANGE_PASSWORD_TAG, true);
        transitionTo(i);
    }

    @Override
    public void onBackPressed() {
        final DrawerLayout lDrawer = (DrawerLayout) mRootView;
        if (lDrawer.isDrawerOpen(GravityCompat.START)) {
            lDrawer.closeDrawers();
            return;
        }
        if (mCurrentIndex == PROFILE_EDIT_INDEX && mHasModified) {
            handleLeftButtonClick();
            return;
        }
        if (mCurrentIndex != PROFILE_DETAILS_INDEX) {
            mCurrentIndex = PROFILE_DETAILS_INDEX;
            initFragment();
            return;
        }
        super.onBackPressed();
    }

    @Override
    public void onPositiveButtonClicked(int type) {

    }

    @Override
    public void onNegativeButtonClicked(int type) {

    }

    @Override
    public void notifyOnEdit(boolean isModified) {
        mHasModified = isModified;
    }


    public void showHideAddStatusOption(boolean status) {
        MenuItem itemAddStatusHome = mNavMenu.findItem(R.id.nav_add_home_status);

        itemAddStatusHome.setVisible(status);
        if (status) {
            if (PreferenceManager.getInstance(getApplicationContext()).isHomeStatus()) {
                itemAddStatusHome.setTitle("View Home Status");
            }
        } else {
            // if no of joining days less than 15 then if will excute
            int joinBefore = 13;
            //replace 13 with how many days tenant jooined in property

            if (13 < 15) {
                itemAddStatusHome.setVisible(true);
            } else {
                itemAddStatusHome.setVisible(false);
            }


        }


    }

    public void replaceFragment() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        Fragment fragment = new ProfileEditFragment();
        fragmentTransaction.replace(R.id.fragment_container, fragment, "Profile_Fragment");
        fragmentTransaction.commitAllowingStateLoss();
    }

    private void getNotificationCount() {
        MyJsonObjectRequest searchrequest = new MyJsonObjectRequest(Request.Method.GET, NetworkConstants.Notification.NOTIFICATION_COUNT, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Logger.printDebug("" + response);

                        //    showMessage(response.optString("Message"));
                        if (response.optString("Status").equalsIgnoreCase(NetworkConstants.SUCCESS)) {


                            JSONObject data = response.optJSONObject("Data");
                            mNotificationCount = data.optInt("notificationCount");
                            invalidateOptionsMenu();

                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof TimeoutError) {
                            //showMessage(getString(R.string.server_error));
                        } else {
                            //showMessage(error.getMessage());
                        }
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        searchrequest.setTag(NetworkConstants.Login.TAG);
        requestQueue.add(searchrequest);
    }

  /*  @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.profile_menu, menu);
        mLeftButton = menu.findItem(R.id.left_button);
        final MenuItem alertMenuItem = menu.findItem(R.id.notification_count);
        mLeftButton.setVisible(false);
        FrameLayout rootView = (FrameLayout) alertMenuItem.getActionView();
        if (PROFILE_EDIT_INDEX == mCurrentIndex) {
            alertMenuItem.setVisible(false);
        } else {
            countTextView = (TextView) rootView.findViewById(R.id.view_alert_count_textview);
            redCircle = (FrameLayout) rootView.findViewById(R.id.view_alert_red_circle);
            Logger.printDebug("notificationcount" + mNotificationCount);
            if (0 < mNotificationCount) {
                countTextView.setText(String.valueOf(mNotificationCount));
            } else {
                countTextView.setText("");
                alertMenuItem.setVisible(false);
            }
            redCircle.setVisibility((mNotificationCount > 0) ? VISIBLE : GONE);
        }
       // updateLeftMenu();
        alertMenuItem.getActionView().setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UserProfileActivity.this, NotificationListActivity.class);
                intent.putExtra("migratedFrom", "UserProfile");
                startActivity(intent);
            }
        });
        mLeftButton.setVisible(false);
        return super.onPrepareOptionsMenu(menu);
    }*/

    @Override
    protected void onResume() {
        super.onResume();
        getNotificationCount();
    }
}