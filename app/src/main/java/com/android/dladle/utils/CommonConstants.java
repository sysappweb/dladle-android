package com.android.dladle.utils;

import android.text.TextUtils;

/**
 * Created by sourav on 17-11-2016.
 */

public class CommonConstants {

    public static final long BACK_PRESS_TIME_DELAY = 2500L;
    public static final long APP_BACK_PRESS_DELAY = 350L;
    public static final long HIDE_KEYBOARD_DELAY = 500L;
    public static final long UPGRADE_DIALOG_DELAY = 100L;

    public static final String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{8,20})";

    public static final String FRAGMENT_KEY = "fragment_key";
    public static final String FRAGMENT_KEY_LANDLORD = "landlord";
    public static final String FRAGMENT_KEY_TENANT = "tenant";
    public static final String FRAGMENT_KEY_VENDOR = "vendor";
    public static final String USER_NAME = "user_name";

    public static final String PACKAGE_NAME = "com.android.dladle";
    public static final String WELCOME_SERVICE_INTENT = "com.dladle.action.WAKEUP";
    public static final String FORCE_UPGRADE_INTENT = "com.dladle.action.FORCE_UPGRADE";

    // Notification Id list
    public static final int LANDLORD_PROPERTY_REQ_TO_TENANT=1;
    public static final int TENANT_ACCEPT_LANDLORDS_PROPERTY_INVITATION=2;
    public static final int TENANT_REJECT_LANDLORDS_PROPERTY_INVITATION=3;
    public static final int TENANT_PROPERTY_REQ_TO_LANDLORD=4;
    public static final int LANDLORD_ACCEPT_TENANTS_PROPRTY_INVITATION=5;
    public static final int LANDLORD_REJECT_TENANTS_PROPRTY_INVITATION=6;
    public static final int LEASE_TERMINATE_REQ_FROM_LANDLORD=7;
    public static final int TENANT_ACCEPTED_LESAE_TERMINATION=8;
    public static final int TENANT_REJECTED_LESAE_TERMINATION=9;
    public static final int LEASE_TERMINATE_REQ_FROM_TENANT=10;
    public static final int LANDLORD_ACCEPTED_LESAE_TERMINATION=11;
    public static final int LANDLORD_REJECTED_LESAE_TERMINATION=12;
    public static final int TENANT_LEFT_HOME=13;
    public static final int TENANT_REMOVED_BY_LANDLORD=14;
    public static final int RATE_TENANT=15;
    public static final int RATE_LANDLORD=16;
    public static final int RATE_VENDOR=17;
    public static final int SERVICE_REQUEST_TO_VENDOR=18;
    public static final int VENDOR_REQ_ACCEPTED = 19;
    public static final int VENDOR_REQ_REJECTED = 20;
    public static final int VENDOR_FINAL_PRICE = 21;















    public static final String IMAGE_META_TAG="data:image"+"//"+"png;base64,";

    public static boolean isEmailValid(String email) {
        if (email.contains("@")) {
            String[] tokens = email.split("@");
            if (tokens.length == 2) {
                for (String s : tokens) {
                    Logger.printDebug("split : " + s);
                    if (TextUtils.isEmpty(s)) {
                        return false;
                    }
                }
                String[] secondTokens = tokens[1].split("\\.");
                if (null != secondTokens && secondTokens.length == 2) {
                    for (String s1 : secondTokens) {
                        Logger.printDebug("split : " + s1);
                        if (TextUtils.isEmpty(s1)) {
                            return false;
                        }
                    }
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean isPasswordValid(String password) {
        if (!TextUtils.isEmpty(password)) {
            Logger.printDebug("password is valid : "+(password.matches(CommonConstants.PASSWORD_PATTERN)));
            return password.matches(CommonConstants.PASSWORD_PATTERN);
        }
        return false;
    }

    public static boolean isValidIdNumber (String idNumber) {
        return !TextUtils.isEmpty(idNumber);
    }

    public static boolean isValidOTP (String aOtp) {
        return (!TextUtils.isEmpty(aOtp) && aOtp.length() == 4 && TextUtils.isDigitsOnly(aOtp));
    }

    public static boolean isLowerVersion (String aCurrentVersion, String aExpectedVersion) {
        boolean retValue = false;
        Logger.printDebug("Current version : "+aCurrentVersion);
        Logger.printDebug("New version : "+aExpectedVersion);
        if (!TextUtils.isEmpty(aCurrentVersion) && !TextUtils.isEmpty(aExpectedVersion)) {
            if (TextUtils.equals(aCurrentVersion, aExpectedVersion)) {
                return false;
            }
            String[] token1 = aCurrentVersion.split("\\.");
            String[] token2 = aExpectedVersion.split("\\.");
            if (null != token1 && null != token2 && 3 == token1.length && 3 == token2.length) {
                for (int i = 0; i < 3; i++) {
                    try {
                        if (Integer.parseInt(token1[i]) < Integer.parseInt(token2[i])) {
                            retValue = true;
                            break;
                        }
                    } catch (NumberFormatException ex) {
                        Logger.printError("NumberFormatException in version info : "+ex.getMessage());
                        break;
                    }
                }
            }
        }
        return retValue;
    }
}
