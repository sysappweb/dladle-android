package com.android.dladle.utils;

import java.io.BufferedReader;

/**
 * Created by admin on 02/07/17.
 */

public class CardUtils {
    public static final int INVALID = -1;
    public static final int VISA = 0;
    public static final int MASTERCARD = 1;
    public static final int AMERICAN_EXPRESS = 2;
    public static final int EN_ROUTE = 3;
    public static final int DINERS_CLUB = 4;

    private static final String[] cardNames =
            {"Visa",
                    "Mastercard",
                    "American Express",
                    "En Route",
                    "Diner's CLub/Carte Blanche",
            };


    public static String getCardID(String cardNumber) {
        String cType = "";
        if (cardNumber.startsWith("4")) {
            cType = "Visa";
        } else if (cardNumber.startsWith("5")) {

            if (cardNumber.startsWith("5018") || cardNumber.startsWith("5020") || cardNumber.startsWith("5038") || cardNumber.startsWith("5612") || cardNumber.startsWith("5893")) {
                cType = "Maestro";
            } else if (cardNumber.startsWith("50") || cardNumber.startsWith("51") || cardNumber.startsWith("52") || cardNumber.startsWith("53") || cardNumber.startsWith("54") || cardNumber.startsWith("55")) {
                cType = "MasterCard";
            } else if (cardNumber.startsWith("5019")) {
                cType = "Dankort";
            }
        } else if (cardNumber.startsWith("6")) {

            if (cardNumber.startsWith("6304") || cardNumber.startsWith("6706") || cardNumber.startsWith("6771") || cardNumber.startsWith("6709")) {
                cType = "Laser";
            }
            else if(cardNumber.startsWith("6759") || cardNumber.startsWith("6761") || cardNumber.startsWith("6762") || cardNumber.startsWith("6763") || cardNumber.startsWith("6390"))
            {
                cType = "Maestro";
            }
            else if(cardNumber.startsWith("62"))
            {
                cType = "China Union pay";
            }
            else {
                cType = "Discover";
            }
        } else if (cardNumber.startsWith("37") || cardNumber.startsWith("34")) {
            cType = "American Express";
        }
        else if(cardNumber.startsWith("0604"))
        {
            cType = "Maestro";
        }
        else if(cardNumber.startsWith("88"))
        {
            cType = "China Unio npay";
        }

        else {
            cType = "Unknown type";
        }
        return cType;
    }

    public static boolean isNumber(String n) {
        try {
            double d = Double.valueOf(n).doubleValue();
            return true;
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static String getCardName(int id) {
        return (id > -1 && id < cardNames.length ? cardNames[id] : "");
    }

    public static boolean validCCNumber(String n) {
        try {
      /*
      ** known as the LUHN Formula (mod10)
      */
            int j = n.length();

            String[] s1 = new String[j];
            for (int i = 0; i < n.length(); i++) s1[i] = "" + n.charAt(i);

            int checksum = 0;

            for (int i = s1.length - 1; i >= 0; i -= 2) {
                int k = 0;

                if (i > 0) {
                    k = Integer.valueOf(s1[i - 1]).intValue() * 2;
                    if (k > 9) {
                        String s = "" + k;
                        k = Integer.valueOf(s.substring(0, 1)).intValue() +
                                Integer.valueOf(s.substring(1)).intValue();
                    }
                    checksum += Integer.valueOf(s1[i]).intValue() + k;
                } else
                    checksum += Integer.valueOf(s1[0]).intValue();
            }
            return ((checksum % 10) == 0);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

}
