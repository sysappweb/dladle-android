package com.android.dladle.utils;

import android.util.Base64;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by admin on 01/07/17.
 */

public class AudioUtils {


public String convertAudioToBaseString(String filePath)
{
    byte[] audioBytes;
    try {

        // Just to check file size.. Its is correct i-e; Not Zero
        File audioFile = new File(filePath);
      //  long fileSize = audioFile.length();

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        FileInputStream fis = new FileInputStream(new File(filePath));
        byte[] buf = new byte[1024];
        int n;
        while (-1 != (n = fis.read(buf)))
            baos.write(buf, 0, n);
        audioBytes = baos.toByteArray();

        // Here goes the Base64 string
        return Base64.encodeToString(audioBytes, Base64.DEFAULT);

    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
}






}
