package com.android.dladle.utils;

/**
 * Created by sourav on 17-11-2016.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;

import java.text.DecimalFormat;

public class PreferenceManager {
    SharedPreferences mPref;
    SharedPreferences.Editor mEditor;
    Context mContext;
    private static PreferenceManager sInstance;

    // shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "dladle-preferences";

    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";
    private static final String USER_NAME = "current_user";
    private static final String USER_TYPE = "user_type";
    private static final String PASSWORD = "session_key";
    private static final String CELL_NUMBER = "number";
    private static final String RATING = "rating";
    private static final String FIRST_NAME = "first_name";
    private static final String VENDOR_ACCOUNT_SETUP_STATUS = "vendoraccount";
    private static final String LAST_NAME = "last_name";
    private static final String PROFILE_IMAGE_URL = "profile_img";
    private static final String EXPECTED_VERSION = "expected_version";
    private static final String CURRENT_VERSION = "current_version";
    private static final String PROFILE_UPDATED = "profile_updated";
    private static final String DO_NOT_SHOW = "do_not_show_again";
    private static final String PAYMENT_ACC_SETUP = "is_payment_account_setup";
    private static final String HOME_STATUS_SETUP="is_home_status";
    private static final String NOTIFICATION_COUNT="notification_count";

    private PreferenceManager(Context context) {
        if (null != context) {
            if (Build.VERSION.SDK_INT >= 24) {
                this.mContext = context.getApplicationContext().createDeviceProtectedStorageContext();
            } else {
                this.mContext = context.getApplicationContext();
            }
            mPref = mContext.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
            mEditor = mPref.edit();
        }
    }

    public static PreferenceManager getInstance(Context context) {
        if (null == sInstance) {
            sInstance = new PreferenceManager(context);
        }
        return sInstance;
    }

    public SharedPreferences getPreference() {
        return mPref;
    }

    public void setFirstTimeLaunch() {
        mEditor.putBoolean(IS_FIRST_TIME_LAUNCH, false);
        mEditor.commit();
    }

    public void setUserName(String userName) {
        mEditor.putString(USER_NAME,userName);
        mEditor.commit();
    }

    public void setVendorType(String type) {
        mEditor.putString("vendor_type",type);
        mEditor.commit();
    }

    public String getVendorType() {
        return mPref.getString("vendor_type","");
    }

    public void setUserType(String userType) {
        mEditor.putString(USER_TYPE,userType);
        mEditor.commit();
    }
    public void setUserImage(String userImage) {
        mEditor.putString("user_image",userImage);
        mEditor.commit();
    }
    public String getUserImage() {
        return mPref.getString("user_image","");
    }
    public String getUserType() {
        return mPref.getString(USER_TYPE,"");
    }
    public void setPassword(String password) {
        try {
            password = EncryptionUtils.encrypt(EncryptionUtils.generateKey(getUserName()),password);
        } catch (Exception e) {
            e.printStackTrace();
        }
        mEditor.putString(PASSWORD,password);
        mEditor.commit();
    }

    public void resetCredentials() {
        mEditor.remove(PASSWORD);
        mEditor.commit();
    }

    public void resetUserData(boolean clearCredential) {
        mEditor.remove(FIRST_NAME);
        mEditor.remove(LAST_NAME);
        if (clearCredential) {
            mEditor.remove(PASSWORD);
            mEditor.remove(USER_NAME);
        }
        mEditor.remove(PROFILE_IMAGE_URL);
        mEditor.remove(CELL_NUMBER);
        mEditor.remove(RATING);
        mEditor.commit();
    }

    public String getUserName() {
        return mPref.getString(USER_NAME,"");
    }

    public String getPassword() {
        String password = mPref.getString(PASSWORD,"");
        try {
            password = EncryptionUtils.decrypt(EncryptionUtils.generateKey(getUserName()),password);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return password;
    }

    public boolean isFirstTimeLaunch() {
        return mPref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }

    public void setExpectedVersion(String aNewVersion) {
        mEditor.putString(EXPECTED_VERSION, aNewVersion);
        mEditor.commit();
    }

    public String getExpectedVersion() {
        return mPref.getString(EXPECTED_VERSION, "");
    }

    public void setCurrentVersion(String aNewVersion) {
        mEditor.putString(CURRENT_VERSION, aNewVersion);
        mEditor.commit();
    }

    public String getCurrentVersion() {
        return mPref.getString(CURRENT_VERSION, "");
    }

    public boolean isProfileUpdated() {
        return mPref.getBoolean(PROFILE_UPDATED,false);
    }

    public void setProfileUpdateStatus(boolean flag) {
        mEditor.putBoolean(PROFILE_UPDATED,flag);
        mEditor.commit();
    }

    public void setDoNotShowAgain(boolean flag) {
        mEditor.putBoolean(DO_NOT_SHOW,flag);
        mEditor.commit();
    }

    public boolean isDoNotShowAgain() {
        return mPref.getBoolean(DO_NOT_SHOW,false);
    }

    public void setCellNumber(String number) {
        mEditor.putString(CELL_NUMBER,number);
        mEditor.commit();
    }

    public String getCellNumber() {
        return mPref.getString(CELL_NUMBER,"");
    }

    public void setName(String firstName,String lastName) {
        mEditor.putString(FIRST_NAME,firstName);
        mEditor.putString(LAST_NAME,lastName);
        mEditor.commit();
    }

    public String getFirstName() {
        return mPref.getString(FIRST_NAME,"");
    }

    public String getLastName() {
        return mPref.getString(LAST_NAME,"");
    }

    public void setRating(float rating) {
        mEditor.putFloat(RATING,rating);
        mEditor.commit();
    }
    public void setRatingCount(int count) {
        mEditor.putInt("rating_count",count);
        mEditor.commit();
    }
    public String getRatingCount() {
       DecimalFormat format = new DecimalFormat();
        format.setDecimalSeparatorAlwaysShown(false);
        return format.format(mPref.getInt("rating_count",0));
    }
    public float getRating() {
        float rating=mPref.getFloat(RATING,0);
        return Float.parseFloat(String.format("%.2f", rating));
    }

    public void setProfileImageUrl(String url) {
        mEditor.putString(PROFILE_IMAGE_URL,url);
        mEditor.commit();
    }

    public String getProfileImageUrl() {
        return mPref.getString(PROFILE_IMAGE_URL,"");
    }

    public void setVendorAccountStatus(boolean status) {
        mEditor.putBoolean(VENDOR_ACCOUNT_SETUP_STATUS,status);
        mEditor.commit();
    }
    public boolean getVendorAccountStatus() {
        return mPref.getBoolean(VENDOR_ACCOUNT_SETUP_STATUS,false);
    }
    public boolean isPaymentAccountSetup()
    {
        return mPref.getBoolean(PAYMENT_ACC_SETUP,false);
    }
    public void setPaymentAccountStatus(boolean status)
    {
        mEditor.putBoolean(PAYMENT_ACC_SETUP,status);
        mEditor.commit();
    }
    public boolean isHomeStatus()
    {
        return mPref.getBoolean(HOME_STATUS_SETUP,false);
    }
    public void setHomeStatus(boolean status)
    {
        Logger.printDebug(String.valueOf(status));
        mEditor.putBoolean(HOME_STATUS_SETUP,status);
        mEditor.commit();
    }
    public int getNotificationCount()
    {
        return mPref.getInt(NOTIFICATION_COUNT,0);
    }
    public void setNotificationCount(int count)
    {
        mEditor.putInt(NOTIFICATION_COUNT,count);
        mEditor.commit();
    }
}
