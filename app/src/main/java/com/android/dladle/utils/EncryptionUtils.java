package com.android.dladle.utils;

import android.util.Base64;

import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by sourav on 10-01-2017.
 */

public class EncryptionUtils {
    private static final String ALGO = "AES";

    static String encrypt(Key key, String Data) throws Exception {
        Cipher c = Cipher.getInstance(ALGO);
        c.init(Cipher.ENCRYPT_MODE, key);
        byte[] encVal = c.doFinal(Data.getBytes());
        String encryptedValue = Base64.encodeToString(encVal,Base64.DEFAULT);
        return encryptedValue;
    }

    static String decrypt(Key key, String encryptedData) throws Exception {
        Cipher c = Cipher.getInstance(ALGO);
        c.init(Cipher.DECRYPT_MODE, key);
        byte[] decodedValue = Base64.decode(encryptedData, Base64.DEFAULT);
        byte[] decValue = c.doFinal(decodedValue);
        String decryptedValue = new String(decValue);
        return decryptedValue;
    }

    static Key generateKey(String lkey) throws Exception {
        int length = lkey.length();
        if (length < 16) {
            StringBuilder sb = new StringBuilder();
            for (int i = length; i < 16; i++) {
                sb.append(i%10);
            }
            lkey = lkey + sb.toString();
        } else {
            lkey = lkey.substring(0,16);
        }
        Key key = new SecretKeySpec(lkey.getBytes(), ALGO);
        Logger.printDebug("Key Length : "+key.getEncoded().length);
        return key;
    }
}
