package com.android.dladle.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by admin on 05/09/17.
 */

public class CommonUtils {

    public int get_count_of_days(String Created_date_String) {
        Logger.printDebug("PostedDate" + Created_date_String);
        String Expire_date_String = GetToday();
        Logger.printDebug("PostedDate" + Expire_date_String);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        Date Created_convertedDate = null, Expire_CovertedDate = null, todayWithZeroTime = null;
        try {
            Created_convertedDate = dateFormat.parse(Created_date_String);
            Expire_CovertedDate = dateFormat.parse(Expire_date_String);

            Date today = new Date();

            todayWithZeroTime = dateFormat.parse(dateFormat.format(today));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long timeDiff = Expire_CovertedDate.getTime() - Created_convertedDate.getTime();
        return (int) TimeUnit.DAYS.convert(timeDiff, TimeUnit.MILLISECONDS);


    }

    public String GetToday() {
        Date presentTime_Date = Calendar.getInstance().getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        return dateFormat.format(presentTime_Date);
    }
}
