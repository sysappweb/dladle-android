package com.android.dladle.utils;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.widget.ImageView;

import com.android.dladle.R;
import com.android.dladle.customui.RoundedImageView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by sourav on 01-03-2017.
 */

public class PhotoManager {

    public static Intent getCameraIntentForThumbnail(Uri photoURI) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (null != photoURI) {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        }
        return intent;
    }
    public static Intent getCameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        }
        return intent;
    }
    public static Uri getProfilePhotoURI(Context context) {
        if (null == context) {
            return null;
        }
        File photoFile = null;
        try {
            photoFile = getOrCreateProfilePicFile(context);
        } catch (IOException ex) {
            Logger.printError(ex.getMessage());
        }
        return FileProvider.getUriForFile(context, "com.android.dladle.fileprovider", photoFile);

    }


    public static Uri getPropertyPhotoURI(Context context) {
        if (null == context) {
            return null;
        }
        File photoFile = null;
        try {
            photoFile = createImageFile(context);
        } catch (IOException ex) {
            Logger.printError(ex.getMessage());
        }
        return FileProvider.getUriForFile(context, "com.android.dladle.fileprovider", photoFile);

    }

    public static String getPropertyImagePathFromUri (Uri uri, Context context) {
        if (null != uri && null != context) {
            String uriString = uri.toString();
            if (uriString.contains("my_images")) {
                String name = uri.getLastPathSegment();
                File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
                File image = new File(storageDir,name);
                return image.getAbsolutePath();
            }
        }
        return null;
    }
    public static Intent getCroppedProfileThumbMailIntent(Context context) {
        File photoFile = null;
        try {
            photoFile = getOrCreateProfilePicFile(context);
        } catch (IOException ex) {
            Logger.printError(ex.getMessage());
        }
        if (photoFile != null && photoFile.exists()) {
            Uri photoURI = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".fileprovider", photoFile);
            //Uri photoURI = Uri.fromFile(photoFile);
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            cropIntent.setType("image/*");
            List<ResolveInfo> list = context.getPackageManager().queryIntentActivities(cropIntent, 0);
            int size = list.size();
            if (size == 0) {
                Logger.printInfo("No component found to crop image");
            } else {
                cropIntent.setData(photoURI);
                cropIntent.setFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                cropIntent.putExtra("crop", "true");
                cropIntent.putExtra("aspectX", 1);
                cropIntent.putExtra("aspectY", 1);
                cropIntent.putExtra("outputX", 95);
                cropIntent.putExtra("outputY", 95);
                cropIntent.putExtra("return-data", true);
                if (size == 1) {
                    ResolveInfo res = list.get(0);
                    cropIntent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
                }
                Logger.printInfo("component : " + cropIntent.toString());
            }
            return cropIntent;
        }
        return null;
    }

    public static Intent galleryIntent(Context context) {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        return intent;
    }

    public static void setProfilePic(Context context, RoundedImageView aImageView) {
        String filePath = getProfilePicFilePath(context);
        File image = new File(filePath);
        Logger.printInfo("image length : " + image.length());
        boolean isFileExist = image.exists() && image.length() != 0;
        if (isFileExist) {
            int targetW = (int) context.getResources().getDimension(R.dimen.profile_pic_size);
            int targetH = targetW;
            // Get the dimensions of the bitmap
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmOptions.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(filePath, bmOptions);
            int photoW = bmOptions.outWidth;
            int photoH = bmOptions.outHeight;

            // Determine how much to scale down the image
            int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

            // Decode the image file into a Bitmap sized to fill the View
            bmOptions.inJustDecodeBounds = false;
            bmOptions.inSampleSize = scaleFactor;
            bmOptions.inPurgeable = true;

            Bitmap bitmap = BitmapFactory.decodeFile(filePath, bmOptions);
            aImageView.setImageBitmap(bitmap);
        } else {
            aImageView.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_avatar));
        }
    }

    public static void setPicture(Context context, ImageView view, String path) {
        if (null != context && null != view && null != path) {
            File image = new File(path);
            Logger.printInfo("image length : " + image.length());
            boolean isFileExist = image.exists() && image.length() != 0;
            if (isFileExist) {
                int targetW = (int) context.getResources().getDimension(R.dimen.profile_pic_size);
                int targetH = targetW;
                // Get the dimensions of the bitmap
                BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                bmOptions.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(path, bmOptions);
                int photoW = bmOptions.outWidth;
                int photoH = bmOptions.outHeight;

                // Determine how much to scale down the image
                int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

                // Decode the image file into a Bitmap sized to fill the View
                bmOptions.inJustDecodeBounds = false;
                bmOptions.inSampleSize = scaleFactor;
                bmOptions.inPurgeable = true;

                Bitmap bitmap = BitmapFactory.decodeFile(path, bmOptions);
                Bitmap temp = Bitmap.createScaledBitmap(bitmap, targetW, targetH, false);
                view.setScaleType(ImageView.ScaleType.FIT_XY);
                view.setImageBitmap(temp);
            }
        }
    }

    private static File createImageFile(Context context) throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        image.getAbsolutePath();
        return image;
    }

    private static File getOrCreateProfilePicFile(Context context) throws IOException {
        String filePath = getProfilePicFilePath(context);
        File file = new File(filePath);
        if (!file.exists()) {
            new File(filePath).createNewFile();
        }
        return file;
    }

    private static String getProfilePicFilePath(Context context) {
        String fileName = "profile.jpg";
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        return storageDir.getAbsolutePath() + File.separator + fileName;
    }

    public static void removeProfilePic(Context context) {
        String filePath = getProfilePicFilePath(context);
        File file = new File(filePath);
        if (file.exists()) {
            file.delete();
        }
    }

    public static Bitmap getRoundedCroppedBitmap(Bitmap bitmap, int radius) {
        Bitmap finalBitmap;
        if (bitmap.getWidth() != radius || bitmap.getHeight() != radius)
            finalBitmap = Bitmap.createScaledBitmap(bitmap, radius, radius,
                    false);
        else
            finalBitmap = bitmap;
        Bitmap output = Bitmap.createBitmap(finalBitmap.getWidth(),
                finalBitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, finalBitmap.getWidth(),
                finalBitmap.getHeight());

        paint.setAntiAlias(true);
        paint.setFilterBitmap(true);
        paint.setDither(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(Color.parseColor("#BAB399"));
        //paint.setColor(Color.TRANSPARENT);
        canvas.drawCircle(finalBitmap.getWidth() / 2 + 0.7f,
                finalBitmap.getHeight() / 2 + 0.7f,
                finalBitmap.getWidth() / 2 + 0.1f, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(finalBitmap, rect, rect, paint);
        finalBitmap.recycle();
        return output;
    }

    public static void updateAndCacheProfilePic(Context context, ImageView imageView, Uri photoUri) {
        if (null != context && null != photoUri && null != imageView) {
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), photoUri);
                imageView.setImageBitmap(bitmap);
                saveBitmapInThread(getProfilePicFilePath(context), bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static String updateAndCachePropertyPic(Context context, ImageView imageView, Uri photoUri) {
        if (null != context && null != photoUri && null != imageView) {
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), photoUri);
                int targetW = (int) context.getResources().getDimension(R.dimen.profile_pic_size);
                int targetH = targetW;
                Bitmap temp = Bitmap.createScaledBitmap(bitmap, targetW, targetH, false);
               imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                imageView.setImageBitmap(temp);
                File file = createImageFile(context);
                saveBitmapInThread(file.getPath(), bitmap);
                return file.getAbsolutePath();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    public static String updateAndCacheprofilePic(Context context, RoundedImageView imageView, Uri photoUri) {
        if (null != context && null != photoUri && null != imageView) {
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), photoUri);
                int targetW = (int) context.getResources().getDimension(R.dimen.profile_pic_size);
                int targetH = targetW;
                Bitmap temp = Bitmap.createScaledBitmap(bitmap, targetW, targetH, false);
                imageView.setImageBitmap(temp);
                File file = createImageFile(context);
                saveBitmapInThread(file.getPath(), bitmap);
                return file.getAbsolutePath();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    private static void saveBitmapInThread(final String filePath, final Bitmap bitmap) {
        new Thread() {
            public void run() {
                OutputStream outStream = null;
                File file = new File(filePath);
                if (file.exists()) {
                    file.delete();
                    file = new File(filePath);
                }
                try {
                    outStream = new FileOutputStream(file);
                    bitmap.compress(Bitmap.CompressFormat.PNG, 60, outStream);
                    outStream.flush();
                    outStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }
}