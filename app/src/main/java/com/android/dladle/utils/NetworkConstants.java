package com.android.dladle.utils;

/**
 * Created by soura on 11-12-2016.
 */

public class NetworkConstants {

    public static final String TNC_URL = "https://sysappweb.xyz:9095/DlaDle_TC.html";
    public static final String HOME_URL = "https://sysappweb.xyz:9095";
    public static final String FAQ_URL = "https://sysappweb.xyz:9095/DlaDle_FAQ.html";
    public static final String CONTACT_US_URL = "https://sysappweb.xyz:9095/DlaDle_Contact_US.html";
    //public static final String SERVER_URL = "https://dladle-webservice.herokuapp.com";
    public static final String SERVER_URL = "https://www.sysappweb.xyz:9095";
    public static final String BASE_URL = SERVER_URL + "/api";
    public static final String SEPARATOR = "/";
    public static final String SUCCESS="Success";
    public static final int TIMOUT_TIME = 50000;

    public class JsonResponse {
        public static final String STATUS = "Status";
        public static final String MESSAGE = "Message";
        public static final String DATA = "Data";

        public static final String STATUS_SUCCESS = "Success";
        public static final String STATUS_NOT_VERIFIED = "Not Verified";
        public static final String STATUS_FAILED = "Fail";
    }

    public class Registration {
        public static final String TAG = "DlaDle-Registration";
        public static final String URL = BASE_URL + SEPARATOR + "user" + SEPARATOR + "register";
        public static final String EMAIL_KEY = "emailId";
        public static final String FIRST_NAME_KEY = "firstName";
        public static final String ID_KEY = "identityNumber";
        public static final String LAST_NAME_KEY = "lastName";
        public static final String PASSWORD_KEY = "password";
        public static final String USER_TYPE_KEY = "userType";
        public static final String CELL_NUMBER = "mobileNumber";
        public static final String RATING = "rating";
    }

    public class Login {
        public static final String TAG = "DlaDle-Login";
        public static final String URL = BASE_URL + SEPARATOR + "user" + SEPARATOR + "login";
        public static final String EMAIL_KEY = "emailId";
        public static final String PASSWORD_KEY = "password";
    }

    public class ForgotPassword {
        public static final String TAG = "DlaDle-ForgotPassword";
        public static final String URL = BASE_URL + SEPARATOR + "user" + SEPARATOR + "forgot-password";
        public static final String EMAIL_KEY = "emailId";
    }

    public class ResetPassword {
        public static final String TAG = "DlaDle-ResetPassword";
        public static final String URL = BASE_URL + SEPARATOR + "user" + SEPARATOR + "reset-password";
        public static final String EMAIL_KEY = "emailId";
        public static final String NEW_PASSWORD = "newPassword";
        public static final String OTP = "otp";
    }

    public class ChangePassword extends ResetPassword {
        public static final String TAG = "DlaDle-ResetPassword";
        public static final String URL = BASE_URL + SEPARATOR + "user" + SEPARATOR + "change-password";
        public static final String OLD_PASSWORD = "currentPassword";
        public static final String EMAIL = "emailId";
        public static final String NEW_CONF_PWD = "newConfirmPassword";
        public static final String NEW_PASSWORD = "newPassword";
    }

    public class Welcome {
        public static final String TAG = "DlaDle-Welcome";
        public static final String URL = SERVER_URL + SEPARATOR + "welcome";
        public static final String NEED_UPGRADE_TO_VERSION = "need_upgrade_to_version";
    }

    public class UpdateVendor {
        public static final String TAG = "Update-Vendor";
        public static final String URL = BASE_URL + SEPARATOR + "user" + SEPARATOR + "vendor" + SEPARATOR + "update";
        public static final String BUSINESS_ADDR = "businessAddress";
        public static final String CELL_NUM = "cellNumber";
        public static final String EXPERIENCE = "experienceType";
        public static final String SERVICE_TYPE = "serviceType";
        public static final String HAS_TOOL = "tools";
        public static final String HAS_TRANSPORT = "transport";
    }

    public class UpdateUser {
        public static final String TAG = "Update-User";
        public static final String URL_LANDLORD = BASE_URL + SEPARATOR + "user" + SEPARATOR + "landlord" + SEPARATOR + "update";
        public static final String URL_TENANT = BASE_URL + SEPARATOR + "user" + SEPARATOR + "tenant" + SEPARATOR + "update";
        public static final String CELL_NUM = "cellNumber";
        public static final String FIRST_NAME = "firstName";
        public static final String LAST_NAME = "lastName";
    }

    public class AddPropertyLandLord {
        public static final String TAG = "Add-Property";
        public static final String ADD_PLACE = BASE_URL + SEPARATOR + "property" + SEPARATOR + "add";
        public static final String PROPERTY_LIST = BASE_URL + SEPARATOR + "property" + SEPARATOR + "list";
        public static final String TENANT_LIST = BASE_URL + SEPARATOR + "property" + SEPARATOR + "tenant" + SEPARATOR + "list"+SEPARATOR;
        public static final String ADDRESS = "address";
        public static final String COMPLEX_NAME = "complexName";
        public static final String ESTATE = "estate";
        public static final String ESTATE_NAME = "estateName";
        public static final String IMAGE_URL = "imgUrl";
        public static final String PLACE_TYPE = "placeType";
        public static final String UNIT_NUMBER = "unitNumber";
        public static final String UPLOAD_IAMGE=BASE_URL + SEPARATOR + "property" + SEPARATOR + "upload" + SEPARATOR + "image";
    }
    public class Contact {
        public static final String ADD_CONTACT = BASE_URL + SEPARATOR + "contact" + SEPARATOR + "add";
        public static final String CONTACT_LIST = BASE_URL + SEPARATOR + "contact" + SEPARATOR + "add";
    }
    public class Select
    {
        public static final String GET_CONTACT_TYPE = BASE_URL + SEPARATOR + "select" + SEPARATOR + "contacttype";
        public static final String GET_VENDOR_EXP = BASE_URL + SEPARATOR + "select" + SEPARATOR + "yearsofexperiencetype";
        public static final String GET_VENDOR_TYPE = BASE_URL + SEPARATOR + "select" + SEPARATOR + "vendorservicetype";
        public static final String GET_REJECTION_REASON = BASE_URL + SEPARATOR + "select" + SEPARATOR + "rejectionreason";

    }
    public class User
    {
        public static final String ADD_DEVICE_ID = BASE_URL + SEPARATOR + "user" + SEPARATOR + "add"  + SEPARATOR +"deviceid";
        public static final String GET_USER_DETAILS=BASE_URL + SEPARATOR + "user" + SEPARATOR + "get" + SEPARATOR + "details?emailId=";
        public static final String DELETE_ACCOUNT=BASE_URL + SEPARATOR + "user" + SEPARATOR + "delete";
        public static final String UPDATE_PROFILE_IMAGE = BASE_URL + SEPARATOR + "user" + SEPARATOR + "profile" + SEPARATOR + "upload" +SEPARATOR +"image";

    }
    public class Property
    {
        public static final String DELETE_PLACE = BASE_URL + SEPARATOR + "property" + SEPARATOR + "delete";
        public static final String ADD_CONTACT = BASE_URL + SEPARATOR + "property" + SEPARATOR + "add" +SEPARATOR +"contact";
        public static final String INVITE = BASE_URL + SEPARATOR + "property" + SEPARATOR + "invite";
        public static final String HOME = BASE_URL + SEPARATOR + "property" + SEPARATOR + "home";
        public static final String TENANT_LIST = BASE_URL + SEPARATOR + "property" + SEPARATOR + "tenant"+SEPARATOR +"list"+SEPARATOR;
        public static final String ASSIGN_HOME=BASE_URL + SEPARATOR + "property" + SEPARATOR + "assign";
        public static final String DECLINE_REQUEST = BASE_URL + SEPARATOR + "property" + SEPARATOR + "decline";
        public static final String DELETE_CONTACT =  BASE_URL + SEPARATOR + "property" + SEPARATOR   + "delete" + SEPARATOR+ "contact" ;
        public static final String CONTACT_LIST = BASE_URL + SEPARATOR + "property" + SEPARATOR + "contact" +SEPARATOR + "list" + SEPARATOR;
    }
    public class Notification
    {
        public static final String NOTIFICATION_LIST = BASE_URL + SEPARATOR + "notification" + SEPARATOR + "list";
        public static final String NOTIFICATION_READ = BASE_URL + SEPARATOR + "notification" + SEPARATOR + "read?notificationId=";
        public static final String NOTIFICATION_ACTIONED = BASE_URL + SEPARATOR + "notification" + SEPARATOR + "actioned"+SEPARATOR;
        public static final String NOTIFICATION_LIST_BY_HOUSEID =  BASE_URL + SEPARATOR + "notification" + SEPARATOR + "list" + SEPARATOR;
        public static final String NOTIFICATION_COUNT = BASE_URL + SEPARATOR + "notification" + SEPARATOR + "count";

    }
    public class Tenant
    {
        public static final String SEND_REQUEST_TO_LANDLORD = BASE_URL + SEPARATOR + "property" + SEPARATOR + "request";
    }
    public class LeaseController
    {
        public static final String TERMINATE_LEASE = BASE_URL + SEPARATOR + "lease" + SEPARATOR + "terminate" + SEPARATOR + "request";
        public static final String TENANT_LEAVE =  BASE_URL + SEPARATOR + "lease" + SEPARATOR + "leave";
        public static final String REMOVE_TENNANT =  BASE_URL + SEPARATOR + "lease" + SEPARATOR + "remove" + SEPARATOR + "tenant";
        public static final String TERMINATION_ACCEPT = BASE_URL + SEPARATOR + "lease" + SEPARATOR + "terminate" + SEPARATOR + "accept";
        public static final String TERMINATION_REJECT = BASE_URL + SEPARATOR + "lease" + SEPARATOR + "terminate" + SEPARATOR + "reject";
        public static final String TERMINATION_REQUEST = BASE_URL + SEPARATOR + "lease" + SEPARATOR + "terminate" + SEPARATOR + "request";
        public static final String LEASE_VIEW = BASE_URL + SEPARATOR + "lease" + SEPARATOR + "view";
        public static final String LEASE_VIEW_BY_HOUSEID = BASE_URL + SEPARATOR + "lease" + SEPARATOR + "view" + SEPARATOR;
    }
    public class WalletController
    {
        public static final String ADD_CARD = BASE_URL + SEPARATOR + "wallet" + SEPARATOR + "add" + SEPARATOR + "card";
        public static final String DELETE_CARD = BASE_URL + SEPARATOR + "wallet" + SEPARATOR + "delete" +  SEPARATOR + "card";
        public static final String UPDATE_CARD = BASE_URL + SEPARATOR + "wallet" + SEPARATOR + "update" +  SEPARATOR + "card";
        public static final String VIEW_CARD = BASE_URL + SEPARATOR + "wallet" + SEPARATOR + "view" +  SEPARATOR + "card";

    }
    public class VendorController
    {
        public static final String REQUEST_VENDOR = BASE_URL + SEPARATOR + "vendor" + SEPARATOR + "post" + SEPARATOR + "request";
        public static final String OFF_WORK = BASE_URL + SEPARATOR + "vendor" + SEPARATOR + "off" + SEPARATOR + "work";
        public static final String AT_WORK = BASE_URL + SEPARATOR + "vendor" + SEPARATOR + "on" + SEPARATOR + "work";
        public static final String WORK_STATUS = BASE_URL + SEPARATOR + "vendor" + SEPARATOR + "work" + SEPARATOR + "status";
        public static final String PROVIDE_ESTIMATION = BASE_URL + SEPARATOR + "vendor" + SEPARATOR + "work" + SEPARATOR + "estimate";
        public static final String VIEW_JOB = BASE_URL + SEPARATOR + "vendor" + SEPARATOR + "view" + SEPARATOR + "work" +SEPARATOR;
        public static final String SERVICE_DETAILS = BASE_URL + SEPARATOR + "vendor" + SEPARATOR + "result" +  SEPARATOR;
        public static final String ACCEPT_SELECTED_VENDOR = BASE_URL + SEPARATOR + "vendor" + SEPARATOR + "accept";
        public static final String REJECT_SELECTED_VENDOR = BASE_URL + SEPARATOR + "vendor" + SEPARATOR + "reject";
        public static final String VENDOR_FINAL_PRICE = BASE_URL + SEPARATOR + "vendor" + SEPARATOR + "work" + SEPARATOR + "estimate" + SEPARATOR + "finalprice" ;
    }

    public class PaymentController
    {
        public static final String PAY = BASE_URL + SEPARATOR + SEPARATOR + "payment" + SEPARATOR + "pay";
    }





    public class TenantController
    {
        public static final String ADD_DOCS = BASE_URL + SEPARATOR + "property" + SEPARATOR + "add" + SEPARATOR + "documents";
        public static final String VIEW_DOCS = BASE_URL + SEPARATOR + "property" + SEPARATOR + "view" + SEPARATOR + "documents";
    }
    public class ReviewController
    {
        public static final String POST_REVIEW = BASE_URL + SEPARATOR + "rating" + SEPARATOR + "post";
        public static final String GET_REVIEW_DETAILS = BASE_URL + SEPARATOR + "rating" + SEPARATOR + "view";
    }

}
