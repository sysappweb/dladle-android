package com.android.dladle.utils;

/**
 * Created by sourav on 17-11-2016.
 */

import android.util.Log;

public class Logger {

    private static final String TAG = "Dladle-App";

    private static final boolean LOG_INFO_ENABLED = true;
    private static final boolean LOG_DEBUG_ENABLED = true;
    private static final boolean LOG_ERROR_ENABLED = true;

    public static void printInfo(String msg) {
        if (LOG_INFO_ENABLED) {
            Log.i(TAG, msg);
        }
    }

    public static void printDebug(String msg) {
        if (LOG_DEBUG_ENABLED) {
            Log.d(TAG, msg);
        }
    }

    public static void printError(String msg) {
        if (LOG_ERROR_ENABLED) {
            Log.e(TAG, msg);
        }
    }
}
