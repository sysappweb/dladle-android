package com.android.dladle;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.android.dladle.activities.WelcomeActivity;

/**
 * Created by sourav on 16-12-2016.
 */

public class BrandScreen extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = new Intent(this, WelcomeActivity.class);
        startActivity(intent);
        finish();
    }
}
